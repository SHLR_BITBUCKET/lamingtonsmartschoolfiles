﻿
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.Models.Json;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SmartSchool.Api
{
    public class QuizController : ApiController
    {
        // QuizManager quiz = new QuizManager();
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        //private readonly ISessionStore _UsesessionStore;
        //public get_session_data1 listData;
        //public QuizController(ISessionStore _GetsessionStore)
        //{
        //    try
        //    {
        //        _UsesessionStore = _GetsessionStore;
        //        listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
        //        if (listData == null)
        //        {
        //            throw new Exception("Session not been set in QuizController");
        //        }
        //    }bid
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.LogException(ex);
        //    }

        //}
        int? cid = Convert.ToInt32(HttpContext.Current.Session["ColgId"].ToString());
        int? bid = Convert.ToInt32(HttpContext.Current.Session["BrcId"].ToString());
        string ac_year = HttpContext.Current.Session["year"].ToString();
        public object GetQuiz()
        {
            SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
            var q = dbContext.Quizs.Where(a=> a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => new { Id = a.QuizID, name = a.QuizName }).ToList();
            return q;
        }

        // GET: api/Quiz
        public QuizModel Get(int id)
        {
            SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();

            List<Config> tempcon = new List<Config>
            {
                new Config() { ShuffleQuestions = true, Automove = true, Allowback = false, ShowPager = false }
            };
            List<QuestionType> tempqtype = new List<QuestionType>
            {
                new QuestionType() { Id = 1, IsActive = true, Name = "MCQ" }
            };

            var q1 = dbContext.Quizs.Where(a => a.QuizID == id && a.Status == true && a.college_id == cid && a.branch_id == bid && a.year == ac_year).FirstOrDefault();
            var qq = q1.Questions.Where(a => a.Status == true && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => new
            {
                Id = a.QuestionID,
                Name = a.QuestionText,
                QuestionTypeId = 1,
                Options = a.Answers.Select(b => new
                {
                    Id = b.AnswerID,
                    QuestionId = a.QuestionID,
                    Name = b.AnswerText,
                    IsAnswer = b.IsAnswer ?? false
                })
            }).Take(10).ToList();

            dbContext.Configuration.LazyLoadingEnabled = false;
            QuizModel qz = new QuizModel()
            {
                quiz = q1,
                Config = tempcon,
                questions = qq.OrderBy(x => Guid.NewGuid()).Take(10).ToList()
            };

            //var js = JsonConvert.SerializeObject(qz);


            return qz;

            // return quiz.GetQuizs();
        }


        public int GetSession()
        {
            var user = User.Identity.Name;
            int quizid = Int32.Parse(HttpContext.Current.Session["QuizId"].ToString());
            return quizid;
        }



        public List<Quiz> Gets(int subject, int unit)
        {
            SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
            dbContext.Configuration.LazyLoadingEnabled = false;

            var sub = dbContext.Quizs.Where(x => x.SubjectID == unit && x.ClassID == subject && x.Status==true && x.college_id == cid && x.branch_id == bid && x.year == ac_year).ToList();
            return sub;
        }


        public class semisterData
        {
            public int? SemID { get; set; }
            public string Name { get; set; }
            public Nullable<int> CourseID { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public Nullable<System.DateTime> ModifiedDate { get; set; }
            public Nullable<int> SemisterID { get; set; }
        }


        public List<AddClass> GetClass()
        {
            SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
            dbContext.Configuration.LazyLoadingEnabled = false;

            var usercourse = (from a in dbContext.students where a.email == User.Identity.Name && a.college_id == cid && a.branch_id == bid && a.year == ac_year select a.class_id).FirstOrDefault();
            var sub = dbContext.AddClasses.Where(a => a.class_id == usercourse && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => a).ToList();

            //return dbcontext.Subject_Master.ToList();
            return sub;
        }

        public class SubDic
        {
            public int? id { get; set; }
            public string name { get; set; }
        }

        public List<SubDic> GetSubject()
        {
            SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();

            dbContext.Configuration.LazyLoadingEnabled = false;
            var usercourse = (from a in dbContext.students where a.email == User.Identity.Name && a.college_id == cid && a.branch_id == bid && a.year == ac_year select a.class_id).FirstOrDefault();
            var clases = dbContext.class_sections.Where(a => a.class_id == usercourse && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => a.id).FirstOrDefault();
            var sub = dbContext.teacher_subjects.Where(a => a.class_section_id == clases && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => a.subject_id).FirstOrDefault();
            var subject = dbContext.subjects.Where(a => a.id == sub && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => new SubDic { id = a.id, name = a.name }).ToList();
            return subject;
        }

        public JsonResult SaveScore(ScoreModel model)
        {

            JsonResult jsonResult = new JsonResult()
            {
                Success = false
            };
            try
            {
                // 1.2 get student_id
                int totalmarks = 100;
                var username = User.Identity.Name;
                student student = dbContext.students.Where(a => a.email.ToLower() == username && a.college_id == cid && a.branch_id == bid && a.year == ac_year).SingleOrDefault();
                var StudentCourse = (from a in dbContext.students.Where(a => a.email == username && a.college_id == cid && a.branch_id == bid && a.year == ac_year) select a.class_id).SingleOrDefault();
                //var SemesterID = (from b in dbcontext.Subject_Master.Where(x => x.SubjectID != 0) select b.Semester).SingleOrDefault(); 

                StudentResult stu_Result = new StudentResult()
                {

                    SubjectId = model.SubjectID,
                    ScoredMarks = model.ScoredMarks,
                    TotalMarks = totalmarks,

                    ClassId = model.ClassID,
                    UserId = student.id,
                    No_attempts = 1,
                    CreatedDate = DateTime.Now,
                    college_id = cid,
                    branch_id = bid,
                    year = ac_year
                };

                dbContext.StudentResults.Add(stu_Result);

                if (dbContext.SaveChanges() > 0)

                    jsonResult.Success = true;
            }
            catch (Exception ex)
            {
                // SystemLogManager.Add("Api|Notify|Get", ex.Message, ex.InnerException.Message ?? "-");
            }

            return jsonResult;
        }

        public JsonResult SaveRating(RatingModel Model)
        {
            JsonResult jsonResult = new JsonResult()
            {
                Success = false
            };

            try
            {
                var username = User.Identity.Name;
                student student = dbContext.students.Where(a => a.email.ToLower() == username && a.college_id == cid && a.branch_id == bid && a.year == ac_year).SingleOrDefault();
                var StudentCourse = (from a in dbContext.students.Where(a => a.email == username && a.college_id == cid && a.branch_id == bid && a.year == ac_year) select a.class_id).SingleOrDefault();

                StudentClassRating sc = new StudentClassRating()
                {
                    StudentID = student.id,
                    ClassID = StudentCourse,
                    Rating = Model.Rating,
                    CreatedDate = DateTime.Now,
                    college_id = cid,
                    branch_id = bid,
                    year = ac_year
                };

                dbContext.StudentClassRatings.Add(sc);

                if (dbContext.SaveChanges() > 0)

                    jsonResult.Success = true;

            }
            catch (Exception ex)
            {
                // SystemLogManager.Add("Api|Notify|Get", ex.Message, ex.InnerException.Message ?? "-");
            }

            return jsonResult;
        }

        public JsonResult Submit(List<qu> QueAnswered)
        {
            JsonResult jsonResult = new JsonResult()

            {
                Success = true
            };
            int Qzid = QueAnswered[0].QuizId;
            var student = User.Identity.Name;
            var studid = dbContext.students.Where(a => a.email == student && a.college_id == cid && a.branch_id == bid && a.year == ac_year).Select(a => a.id).FirstOrDefault();
            int sid = Convert.ToInt32(studid);
            var qid = dbContext.Quizs.Where(a => a.QuizID == Qzid && a.college_id == cid && a.branch_id == bid && a.year == ac_year).FirstOrDefault();
            foreach (var loop in QueAnswered)
            {

                int? jj = loop.QuestionId;
                Test_History sl = new Test_History()
                {
                    StudentID = sid,
                    QuizID = Qzid,
                    QuestionID = jj,
                    OptID = loop.anseid,
                    CreaetedDate = DateTime.Now,
                    college_id = cid,
                    branch_id = bid,
                    year = ac_year

                };
                dbContext.Test_History.Add(sl);
                dbContext.SaveChanges();
                //var 
            }


            return jsonResult;
        }

    }
}
