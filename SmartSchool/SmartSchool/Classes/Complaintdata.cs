﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class Complaintdata
    {

        public int Cid { get; set; }
        public string Complaint_type { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Description { get; set; }
        public string Action_taken { get; set; }
        public string Assigned { get; set; }
        public string Note { get; set; }
        public string Image { get; set; }
        public Nullable<int> College_id { get; set; }
        public Nullable<int> Branch_id { get; set; }
        public string Created_by { get; set; }
        public string Modified_by { get; set; }

        public int Ctid { get; set; }
        public string Complaint_type1 { get; set; }
      
    }
}