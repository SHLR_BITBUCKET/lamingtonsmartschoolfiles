﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class Contentdata
    {

        public int Cid { get; set; }
        public string Title { get; set; }
        public string Contentname { get; set; }
        public string Available { get; set; }
        public string File { get; set; }


        public int? Type { get; set; }
       
        public string All_Super_Admin { get; set; }
        public string Student { get; set; }
        public string For_All_Classes { get; set; }
        public Nullable<int> Class_id { get; set; }
        public Nullable<int> Cls_sec_id { get; set; }
        public string Note { get; set; }
        public string Is_active { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string FilePath { get; internal set; }

        public int Id { get; set; }
        public string ContentType1 { get; set; }
       
    }
}