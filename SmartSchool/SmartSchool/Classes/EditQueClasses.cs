﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class EditQueClasses
    {
        public string question { get; set; }
        public List<string> AnswerText { get; set; }
        public List<string> options { get; set; }
        public List<bool> IsAnswer { get; set; }
        public List<int> answerID { get; set; }
        public int? quizId { get; set; }
    }
}