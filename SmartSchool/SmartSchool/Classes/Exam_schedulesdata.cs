﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class Exam_schedulesdata
    {
        public string ExamName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SubjectName { get; set; }
        public int Section_id { get; set; }

        public int Esid { get; set; }
        public Nullable<int> Session_id { get; set; }
        public Nullable<int> Exam_id { get; set; }
        public Nullable<int> Teacher_subject_id { get; set; }
        public Nullable<System.DateTime> Date_of_exam { get; set; }
        public string Start_to { get; set; }
        public string End_from { get; set; }
        public string Room_no { get; set; }
        public Nullable<int> Full_marks { get; set; }
        public Nullable<int> Passing_marks { get; set; }
        public string Note { get; set; }
        public string Is_active { get; set; }
        public Nullable<System.DateTime> Created_at { get; set; }
        public Nullable<System.DateTime> Updated_at { get; set; }
        public Nullable<int> College_id { get; set; }
        public Nullable<int> Branch_id { get; set; }
        public string Created_by { get; set; }
        public string Modified_by { get; set; }

        public Nullable<int> Class_id { get; set; }

        public int Id { get; set; }
        public string Class1 { get; set; }

        public string Section1 { get; set; }
       

        public int Eid { get; set; }
        public string Name { get; set; }

         public Nullable<int> exam_group_id { get; set; }




        public string Day_name { get; set; }
        public string Start_time { get; set; }
        public string End_time { get; set; }
        
    }
}