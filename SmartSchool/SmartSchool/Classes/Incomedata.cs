﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class Incomedata
    {

        public int Iid { get; set; }
        public string Inc_head_id { get; set; }
        public string Name { get; set; }
        public string Invoice_no { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Amount { get; set; }
        public string Note { get; set; }
        public string Is_active { get; set; }
        public string Is_deleted { get; set; }
        public Nullable<System.DateTime> Created_at { get; set; }
        public Nullable<System.DateTime> Updated_at { get; set; }
        public string Documents { get; set; }
        public Nullable<int> College_id { get; set; }
        public Nullable<int> Branch_id { get; set; }
        public string Created_by { get; set; }
        public string Modified_by { get; set; }

        public int Ihid { get; set; }
        public string Income_category { get; set; }
    }
}