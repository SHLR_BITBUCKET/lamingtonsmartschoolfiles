﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class LeaveTypes
    {
        public int id { get; set; }
        public string type { get; set; }
        public int? allotedleave { get; set; }
    }
}