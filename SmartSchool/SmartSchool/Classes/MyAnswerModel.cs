﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class MyAnswerModel
    {

        public int? QuizID { get; set; }
        public string QuizName { get; set; }

        public int? QuestionID { get; set; }
        public string QuestionName { get; set; }

        public bool isAnswer { get; set; }
        public int AnswerID { get; set; }
        public string AnswerName { get; set; }

    }
}