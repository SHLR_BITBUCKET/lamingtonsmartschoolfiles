﻿using SmartSchool.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class ResetPassword
    {

        public string Email { get; set; }
        public int Id { get; set; }

        public string Password { get; set; }


        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
    public class StudentAddmissionDetails
    {

            public int? admission_no { get; set; }
            public string roll_no { get; set; }
            public string admission_date { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
           
            public string class_id { get; set; }
            public string section_id { get; set; }
        public string Medium { get; set; }
        public string pincode { get; set; }
            public string branchname { get; set; }
           
            public string gender  { get; set; }
            public string state { get; set; }
           
            public string city { get; set; }
        public string sts_number { get; set; }

        public string dob { get; set; }
            public string category_id { get; set; }
            public string religion { get; set; }
        public string adhar_no { get; set; }
        public string cast { get; set; }
            public string user_id { get; set; }
            [Required(ErrorMessage = "You must provide a phone number")]

            [DataType(DataType.PhoneNumber)]
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]

            [Display(Name = nameof(Resource1.Emergency_Number), ResourceType = typeof(Resource1))]
            public string mobileno { get; set; }


            [EmailAddress(ErrorMessage = "Invalid Email Address")]
            public string email { get; set; }
           
            public string StudentPhoto { get; set; }
            public string blood_group { get; set; }
            public string school_house_id { get; set; }
            public string height { get; set; }
        public DateTime? measurement_date { get; set; }
            public string weight { get; set; }
            public string AsonDate { get; set; }
            public string father_name { get; set; }
            public string father_phone { get; set; }
            public string father_occupation { get; set; }
            public string FatherPhoto { get; set; }
            public string father_aadhaar_no { get; set; }
            public string mother_name { get; set; }
            public string mother_phone { get; set; }
            public string mother_occupation { get; set; }
            public string MotherPhoto { get; set; }
            public string mother_aadhaar_no { get; set; }
           
            public string guardian_name { get; set; }
            public string guardian_relation { get; set; }
            public string guardian_email { get; set; }
            public string GuardianPhoto { get; set; }
           
            public string guardian_phone { get; set; }
            public string guardian_occupation { get; set; }
            public string guardian_address { get; set; }
            public string current_address { get; set; }
            public string permanent_address { get; set; }
            public string route_id { get; set; }
            public string hostel_room_id { get; set; }
            public string RoomNumber { get; set; }
            public string bank_account_no { get; set; }
            public string bank_name { get; set; }
            public string ifsc_code { get; set; }

            [Required(ErrorMessage = "You must Select RTE")]
            public string rte { get; set; }
            public string previous_school { get; set; }
            public string note { get; set; }
            public string Title1 { get; set; }
            public string Documents1 { get; set; }
            public string Title2 { get; set; }
            public string Documents2 { get; set; }
            public string Title3 { get; set; }
            public string Documents3 { get; set; }
            public string Title4 { get; set; }
            public string Documents4 { get; set; }
            public string Sibalingname { get; set; }
            public string Sibalingid { get; set; }
            public bool? IsActive { get; set; }
            public string ParentUserName { get; set; }
            public int? id { get; set; }
    
            public string UserName { get; set; }
            public string Password { get; set; }

            public string ParentPassword { get; set; }
            public string Certificate { get; set; }
         
            
            public string caste_certificate_no { get; set; }
            public string gr_number { get; set; }
            public string micr_number { get; set; }

            public string branch_name { get; set; }
        public string subject_id { get; set; }
        public string Iagroupid { get; set; }
        public string income_certificate_no { get; set; }
  
       
    }
}