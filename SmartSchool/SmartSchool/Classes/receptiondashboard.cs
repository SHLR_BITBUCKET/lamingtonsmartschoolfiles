﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class receptiondashboard
    {
        public int Admission_Enquiry { get; set; }
        public int Visitor_Book { get; set; }
        public string Phone_Call_Log { get; set; }
        public string Complain { get; set; }
        public string staff { get; set; }
        public string Postal_Dispatch { get; set; }
        public string Postal_Receive { get; set; }
    }
}