﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Classes
{
    public class Visitors_bookdata
    {


        public int Vp_id { get; set; }

        public int Vid { get; set; }
        public string Source { get; set; }
        public string Purpose { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Id_proof { get; set; }
        public Nullable<int> No_of_pepple { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string In_time { get; set; }
        public string Out_time { get; set; }
        public string Note { get; set; }
        public string Image { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Created_by { get; set; }
        public string Modified_by { get; set; }


    }


    public class get_marksresults
    {
        public string[] Get_marks { get; set; }
        public string[] Get_esids { get; set; }
        public string Student_id { get; set; }
        public int Examid { get;  set; }
        public int Classid { get; set; }
        public int Sectionid { get; set; }
    }


    
}