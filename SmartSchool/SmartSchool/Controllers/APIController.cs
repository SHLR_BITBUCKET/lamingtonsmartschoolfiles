﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SmartSchool.Controllers
{
    public class APIController : ApiController
    {
        private SmartSchoolHDEntities db = new SmartSchoolHDEntities();

        public HttpResponseMessage GetAllStudentCount()
        {
            string stu = "Total Registered Students = " + db.students.Select(a => a.id).Count();
            if(stu != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, stu);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }

        // GET: api/AddClasses/5
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetAllStaffCount()
        {
            string c = "Total Staffs = " + db.staffs.Select(a => a.id).Count();
            if (c != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, c);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }

        // GET: api/AddClasses/5
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetTotalFeeCollect()
        {
            string c = "Total Fees Collected Rs " + db.fee_deposit_amountdetails.Select(a => a.amount).Sum();
            if (c != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, c);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }


        [ResponseType(typeof(string))]
        public HttpResponseMessage GetTotalExpense()
        {
            string c = "Total Expenses Rs " + db.expenses.Select(a => a.amount).Sum();
            if (c != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, c);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }


        public class anydata
        {
            public string SchoolName { get; set; }
            public int? StudentCount { get; set; }

        }


        public class Staffanydata
        {
            public string SchoolName { get; set; }

            public int? StaffCount { get; set; }
        }

     



        [ResponseType(typeof(string))]
        public HttpResponseMessage GetSchoolwiseStudents()
        {
            List<int> colgid = db.InstitutionDetails.Select(a => a.college_id).ToList();
            List<anydata> list = new List<anydata>();
          
            foreach (var ci in colgid)
            {             
                int? cstu = db.students.Where(a=>a.college_id == ci).Select(a => a.college_id).Count();
                string colName = db.InstitutionDetails.Where(a => a.college_id == ci).Select(a => a.college_name).FirstOrDefault();
                anydata obj = new anydata()
                {
                    SchoolName = colName,
                    StudentCount = cstu
                };
                if (cstu != 0)
                {
                    list.Add(obj);
                }
            }

            if (list != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }



        [ResponseType(typeof(string))]
        public HttpResponseMessage GetSchoolwiseStaffs()
        {
            List<int> colgid = db.InstitutionDetails.Select(a => a.college_id).ToList();
            Object ob = new Object();
            List<Staffanydata> list = new List<Staffanydata>();

            foreach (var ci in colgid)
            {
                int? cstu = db.staffs.Where(a => a.college_id == ci).Select(a => a.college_id).Count();
                string colName = db.InstitutionDetails.Where(a => a.college_id == ci).Select(a => a.college_name).FirstOrDefault();
                Staffanydata obj = new Staffanydata()
                {
                    SchoolName = colName,
                    StaffCount = cstu
                };
                if (cstu != 0)
                {
                    list.Add(obj);
                }
            }

            if (list != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }





        public class Studentsclassanydata
        {
            public string SchoolName { get; set; }

            public int? ClassWiseStudentsCount { get; set; }

            public string ClassName{ get; set; }
        }



        [ResponseType(typeof(string))]
        public HttpResponseMessage GetSchoolclasswiseStudents()
        {
            List<int> colgid = db.InstitutionDetails.Select(a => a.college_id).ToList();
            List<Studentsclassanydata> list = new List<Studentsclassanydata>();

            foreach (var ci in colgid)
            {
                List<int> clasid = db.AddClasses.Select(a => a.class_id).ToList();
                foreach(var cid in clasid)
                {
                    int? cstu = db.students.Where(a => a.college_id == ci && a.class_id == cid).Select(a => a.id).Count();
                    string colName = db.InstitutionDetails.Where(a => a.college_id == ci).Select(a => a.college_name).FirstOrDefault();
                    string className = db.AddClasses.Where(a => a.college_id == ci && a.class_id == cid).Select(a => a.class_name).FirstOrDefault();
                    Studentsclassanydata obj = new Studentsclassanydata()
                    {
                        SchoolName = colName,
                        ClassName = className,
                        ClassWiseStudentsCount = cstu
                    };
                    if (className != null)
                    {
                        list.Add(obj);
                    }
                }               
            }

            if (list != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Data Available");
            }
        }
    }
}
