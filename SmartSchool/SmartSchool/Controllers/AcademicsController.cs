﻿using System.Web.Mvc;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System;
using System.Net;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SmartSchool.Interfaces;
using static SmartSchool.SchoolMasterClasses.StudentInformation;

namespace SmartSchool.Controllers
{
    public class AcademicsController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        public get_staff_classes gs = new get_staff_classes();

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public AcademicsController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ClassTimetable()
        {
            bind_section();
            GetClassSection(null);
            return View();
        }

        public void bind_Add_clas()
        {
            var u_lists = dbContext.AddClasses.Where(cs => cs.college_id == listData.CollegeID && cs.branch_id == listData.BranchID && cs.year == listData.academic_year).OrderBy(a => a.class_name).ToList().Select(b => new SelectListItem { Value = b.class_id.ToString(), Text = b.class_name.ToString() }).ToList();
            ViewBag.addcls = u_lists;
        }

        public void bind_section()
        {
            var u_list = dbContext.sections.Where(cs => cs.college_id == listData.CollegeID && cs.branch_id == listData.BranchID && cs.year == listData.academic_year).OrderBy(a => a.id).ToList().Select(b => new SelectListItem { Value = b.id.ToString(), Text = b.section1.ToString() }).ToList();
            ViewBag.clas1 = u_list;
        }

        [HttpGet]
        public ActionResult GetClassSection(int? classid)
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.sections = gs.get_staff_wise_classes(classid, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return View();
        }


        public void bind_Subject()
        {
            List<subject> c = dbContext.subjects.Where(x => x.id != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            subject sub = new subject
            {
                name = " select subject Name",
                id = 0
            };
            c.Insert(0, sub);
            SelectList u_list = new SelectList(c, "id", "name ", 0);
            ViewBag.clas11 = u_list;
        }

        [HttpGet]
        public ActionResult student_attendance()
        {
            var getstaffdata = gs.get_staff_wise_classes(0, listData.CollegeID, listData.BranchID, listData.academic_year,  listData.roleid);
            IQueryable<AddClass> iq = dbContext.AddClasses;
            List<SelectListItem> lc = new List<SelectListItem>();
            foreach (var group in getstaffdata)
            {
                int clsid = Convert.ToInt32(group.class_id);
                lc.Add(iq.OrderBy(a => a.class_name).Where(k => k.class_id == clsid).Select(b => new SelectListItem { Value = b.class_id.ToString(), Text = b.class_name }).Distinct().FirstOrDefault());
            }
            ViewBag.cls = lc;
            return View();
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year,  listData.roleid);          
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getTeacher(int? ddlsection, int? ddlclass)
        {
            var teacherdata = gs.getteacher(ddlclass, ddlsection, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(teacherdata, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getTeacherSubjects(int? ddlsection, int? ddlclass, int? ddlstafid)
        {
            int? stafid = gs.getStaffId(listData.CollegeID, listData.BranchID, listData.academic_year);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            var result = dbContext.getSubjects(listData.CollegeID, listData.BranchID, stafid, ddlclass, ddlsection, listData.academic_year, listData.roleid).ToList();

            foreach (var re in result)
            {
                selectLists.Add(new SelectListItem
                {
                    Text = re.name,
                    Value = re.id.ToString()
                });
            }
            //SelectListItem st = new SelectListItem()
            //{
            //    Text = "Select",
            //    Value = "0"
            //};
            //selectLists.Insert(0, st);
            return Json(selectLists, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult AddClassTimetable(subject model, int? class_ids)
        {
            bind_Add_clas();
            GetClassSection(null);
            student_attendance();
            if (class_ids != null)
            {
                var getvaltt1 = (from a in dbContext.timetables
                                 join b in dbContext.teacher_subjects on a.teacher_subject_id equals b.id
                                 join c in dbContext.subjects on b.subject_id equals c.id
                                 join d in dbContext.class_sections on b.class_section_id equals d.id
                                 join e in dbContext.AddClasses on d.class_id equals e.class_id
                                 where d.class_id == class_ids && a.college_id == listData.CollegeID && a.college_id == listData.BranchID && a.year == listData.academic_year
                                 select new SchoolMasterClasses.timetables
                                 {
                                     classes = e.class_name,
                                     name = c.name,
                                     day_name = a.day_name,
                                     start_time = a.start_time,
                                     end_time = a.end_time,
                                     room_no = a.room_no
                                 }).ToList();

                ViewBag.Gettimetable = getvaltt1;
                ViewBag.getname = getvaltt1.Select(a => a.name).Distinct().ToList();
                ViewBag.cls_id = getvaltt1.Select(a => a.classes).Distinct().ToList();
                var s = ViewBag.cls_id;
                return View();
            }
            var getvaltt = (from a in dbContext.timetables
                            join b in dbContext.teacher_subjects on a.teacher_subject_id equals b.id
                            join c in dbContext.subjects on b.subject_id equals c.id
                            join d in dbContext.class_sections on b.class_section_id equals d.id
                            where a.college_id == listData.CollegeID && a.college_id == listData.BranchID && a.year == listData.academic_year
                            select new SchoolMasterClasses.timetables
                            {
                                name = c.name,
                                day_name = a.day_name,
                                start_time = a.start_time,
                                end_time = a.end_time,
                                room_no = a.room_no
                            }).ToList();

            ViewBag.Gettimetable = getvaltt;
            ViewBag.getname = getvaltt.Select(a => a.name).Distinct().ToList();
            ViewBag.cls_id = class_ids;
            return View();
        }

        [HttpPost]
        public JsonResult AddClassTimetable(string ddlsub, List<timetable> allval, int? ddlcls, int? ddlsec)
        {
            student_attendance();
            bind_Subject();
            if (ddlsub != "" & ddlsec != null & ddlcls != null)
            {
                var subid = (from a in dbContext.teacher_subjects
                             join b in dbContext.class_sections on a.class_section_id equals b.id
                             where a.subject_id.ToString() == ddlsub && b.class_id == ddlcls && b.section_id == ddlsec && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                             select a.id).FirstOrDefault();
                int succ = 0;
                if (allval != null)
                {
                    foreach (var alval in allval)
                    {
                        if (alval.start_time != "" && alval.end_time != "" && alval.room_no != "")
                        {
                            //var removetags = Regex.Replace(alval.day_name, "\n", String.Empty).Trim();
                            timetable tt = new timetable()
                            {
                                day_name = alval.day_name,
                                start_time = alval.start_time,
                                end_time = alval.end_time,
                                room_no = alval.room_no,
                                college_id = listData.CollegeID,
                                branch_id = listData.BranchID,
                                created_at = DateTime.Now,
                                created_by = listData.user,
                                is_active = true,
                                teacher_subject_id = subid,
                                year = listData.academic_year,
                                day_order = alval.day_order

                            };
                            dbContext.timetables.AddOrUpdate(k => new { k.teacher_subject_id, k.college_id, k.branch_id, k.day_name }, tt);
                            succ = dbContext.SaveChanges();
                        }
                        else
                        {
                            return Json("Please Fill up the Table..?", JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (succ > 0)
                    {
                        return Json("Data Saved Successfully", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Please Fill up the Table..?", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Choose Class, Section and Subject", JsonRequestBehavior.AllowGet);
            }
            return Json(new { response = "Uncheck", }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult timetbl()
        {
            bind_Subject();
            bind_section();
            GetClassSection(null);
            return View();
        }

        [HttpPost]
        public ActionResult timetbl(int? class_id, int? section_id, subject model)
        {
            bind_Subject();
            bind_section();
            GetClassSection(class_id);
            var getvaltt = (from a in dbContext.timetables
                            join b in dbContext.teacher_subjects on a.teacher_subject_id equals b.id
                            join c in dbContext.subjects on b.subject_id equals c.id
                            join d in dbContext.class_sections on b.class_section_id equals d.id
                            join e in dbContext.AddClasses on d.class_id equals e.class_id
                            where d.class_id == class_id & d.section_id == section_id & a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                            select new SchoolMasterClasses.timetables
                            {
                                name = c.name,
                                day_name = a.day_name,
                                start_time = a.start_time,
                                end_time = a.end_time,
                                room_no = a.room_no,
                                classes = e.class_name
                            }).ToList();

            ViewBag.Gettimetable = getvaltt;
            ViewBag.getname = getvaltt.Select(a => a.name).Distinct().ToList();
            ViewBag.cls_id = getvaltt.Select(a => a.classes).FirstOrDefault();
            return View();
        }

        [HttpGet]
        public ActionResult AssignClassTeacher()
        {
            GetClassSection(null);
            bind_section();
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            bind_Teacher();
            var assignTeacher = (from a in dbContext.class_teacher
                                 join b in dbContext.sections on a.section_id equals b.id
                                 join c in dbContext.staffs on a.staff_id equals c.id
                                 join d in dbContext.AddClasses on a.class_id equals d.class_id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                                 select new SchoolMasterClasses.AssignClassteacher
                                 {
                                     classes = d.class_name,
                                     section = b.section1,
                                     name = c.name,
                                     id = a.id,
                                 }).ToList();
            ViewBag.AssignClsTeacher = assignTeacher;
            return View();
        }


        [HttpPost]
        public ActionResult AssignClassTeacher(class_teacher cls_Teacher, class_sections class_Sections, int? class_id, int? section_id, int? staff_id)
        {
            GetClassSection(class_id);
            bind_section();
            //bind_Teacher();

            var assignTeacher = (from a in dbContext.class_teacher
                                 join b in dbContext.sections on a.section_id equals b.id
                                 join c in dbContext.staffs on a.staff_id equals c.id
                                 join d in dbContext.AddClasses on a.class_id equals d.class_id
                                 where a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new SchoolMasterClasses.AssignClassteacher
                                 {
                                     classes = d.class_name,
                                     section = b.section1,
                                     name = c.name,
                                     id = a.id,
                                 }).ToList();
            ViewBag.AssignClsTeacher = assignTeacher;

            var Updatedata = (from a in dbContext.class_teacher where a.class_id == class_id && a.section_id == section_id && a.staff_id == staff_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).ToList().FirstOrDefault();
            if (Updatedata == null)
            {
                class_teacher clsteach = new class_teacher()
                {
                    class_id = cls_Teacher.class_id,
                    staff_id = cls_Teacher.staff_id,
                    section_id = cls_Teacher.section_id,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    is_active = true,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    year = listData.academic_year

                };
                dbContext.class_teacher.Add(clsteach);
            }
            else
            {
                Updatedata.class_id = cls_Teacher.class_id;
                Updatedata.staff_id = cls_Teacher.staff_id;
                Updatedata.section_id = cls_Teacher.section_id;
                Updatedata.college_id = listData.CollegeID;
                Updatedata.branch_id = listData.BranchID;
                Updatedata.is_active = true;
                Updatedata.modified_by = "Admin";
                Updatedata.modified_at = DateTime.Now;
                Updatedata.year = listData.academic_year;
            }
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Saved Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }

            return RedirectToAction("AssignClassTeacher");
        }

        private void bind_Teacher()
        {
            List<staff> c = dbContext.staffs.Where(x => x.id != 0 && x.roll_id!= "dd184641-0108-450d-8f21-68a5d36922f4" && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            staff sta = new staff
            {
                name = " Please select staff name",
                id = 0
            };
            c.Insert(0, sta);
            SelectList u_list = new SelectList(c, "id", "name ", 0);

            ViewBag.teacher = u_list;
        }

        [HttpGet]
        public ActionResult EditAssignClassTeacher(int? Id)
        {
            bind_Teacher();
            bind_section();
            GetClassSection(null);
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = dbContext.class_teacher.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();

            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        [HttpPost]
        public ActionResult EditAssignClassTeacher(class_teacher clsteachers, int? Id, int? class_id, int? section_id, int? staff_id)
        {
            //bind_Teacher();
            bind_section();
            GetClassSection(null);
            var data = dbContext.class_teacher.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();

            if (data.id == clsteachers.id)
            {
                data.class_id = clsteachers.class_id;
                data.section_id = clsteachers.section_id;
                data.staff_id = clsteachers.staff_id;
                data.is_active = true;
                data.modified_by = listData.user;
                data.modified_at = DateTime.Now;
                data.college_id = listData.CollegeID;
                data.branch_id = listData.BranchID;
                data.year = listData.academic_year;
                
            };
            dbContext.class_teacher.AddOrUpdate(data);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Edit Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }

            return RedirectToAction("AssignClassTeacher");
        }


        [HttpGet]
        public ActionResult DeleteAssignClassTeacher(class_teacher clsteachers, int? Id)
        {
            var data = dbContext.class_teacher.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();
            data.modified_at = DateTime.Now;
            data.modified_by = listData.user;
            dbContext.SaveChanges();
            if (data.id == clsteachers.id)
            {
                data.class_id = clsteachers.class_id;
                data.section_id = clsteachers.section_id;
                data.staff_id = clsteachers.staff_id;
                data.is_active = clsteachers.is_active;
                data.modified_by = clsteachers.modified_by;
                data.created_at = DateTime.Now;
                data.college_id = clsteachers.college_id;
                data.branch_id = clsteachers.branch_id;
                data.modified_at = DateTime.Now;

            };
            dbContext.class_teacher.Remove(data);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Delete Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }

            return RedirectToAction("AssignClassTeacher");
        }

        [HttpGet]
        public ActionResult addSubject()
        {

            var getsubject = (from a in dbContext.subjects where a.is_active != null && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).ToList();
            ViewBag.subjectdata = getsubject;
            return View();
        }

        [HttpPost]
        public ActionResult addSubject(subject subjt, string Type)
        {
            subject sbj = new subject()
            {
                name = subjt.name,
                code = subjt.code,
                type = Type,
                is_active = true,
                created_at = DateTime.Now,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year,
                created_by = listData.user,
                

            };
            dbContext.subjects.Add(sbj);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Subject Saved Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("addSubject");

        }

        [HttpGet]
        public ActionResult EditSubject(int? Id)
        {
            bind_Subject();
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = dbContext.subjects.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        [HttpPost]
        public ActionResult EditSubject(subject sb, int? Id, int? Subject_id)
        {
            bind_Subject();
            var data = dbContext.subjects.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();
            var sub = (from a in dbContext.subjects where a.id == Subject_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a.name).FirstOrDefault();
            if (data.id == sb.id)
            {
                data.name = sub;
                data.code = sb.code;
                data.type = sb.type;
                data.is_active = true;
                data.updated_at = DateTime.UtcNow;
                data.modified_by = listData.user;
                data.created_at = DateTime.Now;
                data.college_id = listData.CollegeID;
                data.branch_id = listData.BranchID;
                data.year = listData.academic_year;
            };
            dbContext.subjects.AddOrUpdate(data);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Edit Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }

            return RedirectToAction("addSubject");
        }

        [HttpGet]
        public ActionResult DeleteSubject(int? Id)
        {
            if (Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbContext.subjects.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();
            if (data == null)
            {
                return HttpNotFound();
            }

            dbContext.subjects.Remove(data ?? throw new InvalidOperationException());
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while Deleting data, please contact admin";
            }
            return RedirectToAction("addSubject");
        }

        [HttpPost]
        public JsonResult callgetsecsub(string ddlsection, string ddlclass)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlsection))
            {
                int sectionid = Convert.ToInt32(ddlsection);
                int classid = Convert.ToInt32(ddlclass);
                result = cmc.get_class_secsub(classid, sectionid, listData.CollegeID, listData.BranchID, listData.academic_year);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult AssignSubject()
        {
            GetClassSection(null);
            //bind_section();
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            bind_Teacher();
            bind_Subject();

            var gg = (from dd in dbContext.teacher_subjects
                      join b in dbContext.subjects on dd.subject_id equals b.id
                      join c in dbContext.staffs on dd.teacher_id equals c.id
                      join d in dbContext.class_sections on dd.class_section_id equals d.id
                      join e in dbContext.AddClasses on d.class_id equals e.class_id
                      join f in dbContext.sections on d.section_id equals f.id
                      where dd.college_id == listData.CollegeID && dd.branch_id == listData.BranchID && dd.year == listData.academic_year
                      select new teacher_subj
                      {
                          id = dd.id,
                          name = b.name,
                          Teacher_name = c.name,
                          Classes = e.class_name,
                          Section = f.section1
                      }).ToList();
            ViewBag.GetTeacherSubject = gg;
            return View();
        }

        [HttpPost]
        public ActionResult AssignSubject(class_sections model, int? class_id, int? section_id, int? subject_id, int? Staff_id)
        {
            GetClassSection(class_id);
            bind_section();
            //bind_Teacher();
            bind_Subject();
            var ts_id = (from a in dbContext.class_sections where a.class_id == class_id && a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a.id.ToString()).ToList();
            string id = ts_id.First();
            int td_id = Convert.ToInt32(id);

            var Updatedata = (from a in dbContext.teacher_subjects where a.class_section_id == td_id && a.subject_id == subject_id && a.teacher_id == Staff_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).ToList().FirstOrDefault();

            var repeat = (from k in dbContext.teacher_subjects where k.class_section_id == td_id && k.subject_id == subject_id && k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year select k).FirstOrDefault();
            var sessionyear = (from a in dbContext.sessions.Where(x => x.is_active == true && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a.session1).FirstOrDefault();
            if (repeat == null)
            {
                if (Updatedata == null)
                {
                    teacher_subjects teach_sub = new teacher_subjects()
                    {
                        session_id = listData.academicyear_id,
                        class_section_id = td_id,
                        subject_id = subject_id,
                        teacher_id = Staff_id,
                        is_active = true,
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        year = listData.academic_year
                    };
                    dbContext.teacher_subjects.Add(teach_sub);
                    dbContext.SaveChanges();
                }
                else
                {
                    Updatedata.session_id = Convert.ToInt32(sessionyear);
                    Updatedata.class_section_id = td_id;
                    Updatedata.subject_id = subject_id;
                    Updatedata.teacher_id = Staff_id;
                    Updatedata.is_active = true;
                    Updatedata.updated_at = DateTime.Now;
                    Updatedata.college_id = listData.CollegeID;
                    Updatedata.branch_id = listData.BranchID;
                    Updatedata.year = listData.academic_year;
                    Updatedata.modified_by = listData.user;
                }
                try
                {
                    int c = dbContext.SaveChanges();
                    TempData["message"] = "Subject Assigned Successfully";
                }

                catch (Exception e)
                {
                    Console.Write(e);
                    TempData["message"] = "There was an error while saving data, please contact admin";
                }
            }
            else
            {
                TempData["message"] = "Already This Subject is Assigned..!";
            }
            return RedirectToAction("AssignSubject");
        }


        [HttpGet]
        public ActionResult DeleteAssignSubject(teacher_subjects sb, int? Id)
        {
            var sessionyear = (from a in dbContext.sessions.Where(x => x.is_active == true && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a.session1).FirstOrDefault();            
            var data = dbContext.teacher_subjects.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(pp => pp).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();
            if (data.id == sb.id)
            {
                data.session_id = listData.academicyear_id;
                data.class_section_id = sb.class_section_id;
                data.subject_id = sb.subject_id;
                data.teacher_id = sb.teacher_id;
                data.is_active = true;
                data.updated_at = DateTime.Now;
                data.modified_by = listData.user;
                data.college_id = listData.CollegeID;
                data.branch_id = listData.BranchID;
                data.year = listData.academic_year;
            };
            dbContext.teacher_subjects.Remove(data);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Delete Successfully";
            }
            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("AssignSubject");
        }

        private void bind_Year()
        {
            var u_list = dbContext.sessions.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID).OrderBy(a => a.sid).ToList().Select(b => new SelectListItem { Value = b.sid.ToString(), Text = b.session1.ToString() }).ToList();
            ViewBag.year = u_list;
        }


        //---------Adding of Main Classes Starts------------------------

        [HttpGet]
        public ActionResult Add_class()
        {
            var ad_cls = (from n in dbContext.AddClasses where  n.college_id == listData.CollegeID && n.branch_id == listData.BranchID && n.year == listData.academic_year orderby n.class_name select n).ToList();
            ViewBag.getclassname = ad_cls;
            return View();
        }

        [HttpPost]
        public ActionResult Add_class(AddClass addcls, string class_name)
        {
            var ad = (from a in dbContext.AddClasses where a.class_name == class_name && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
            if (ad == null)
            {
                AddClass addClass = new AddClass()
                {
                    class_name = addcls.class_name,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year = listData.academic_year,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                };
                dbContext.AddClasses.Add(addClass);
            }
            else
            {
                ad.class_name = addcls.class_name;
                ad.college_id = listData.CollegeID;
                ad.branch_id = listData.BranchID;
                ad.year = listData.academic_year;
                ad.updated_by = listData.user;
                ad.updated_at = DateTime.Now;
            }
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Saved Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Add_class");
        }


        //---------Adding of Main Classes Ends------------------------

        [HttpGet]
        public ActionResult Promote(int? id, int? class_id1, int? section_id1)
        {
            GetClassSection(class_id1);
            bind_Year();
            bind_section();
            return View();
        }

        [HttpPost]
        public ActionResult Promote(int? class_id1, int? section_id1, List<listpromotestudents> allStudents, int? clsid, int? secid, string promoteyear)
        {
            GetClassSection(class_id1);
            bind_Year();
            bind_section();
            int save = 0;

            if (class_id1 != 0 && class_id1 != null)
            {
                var promotes = (from a in dbContext.students
                                join b in dbContext.AddClasses on a.class_id equals b.class_id
                                join c in dbContext.sections on a.section_id equals c.id
                                where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.class_id == class_id1 && a.section_id == section_id1 && a.year == listData.academic_year)
                                select new SchoolMasterClasses.Promote
                                {
                                    id = a.id,
                                    AdmissionNumber = a.admission_no,
                                    FirstName = a.firstname,
                                    FatherName = a.firstname,
                                    DateOfBirth = a.dob

                                }).ToList();
                ViewBag.studentlist = promotes;
            }
            if (allStudents != null)
            {
                foreach (var alval in allStudents)
                {
                    var data = (from a in dbContext.students where a.admission_no == alval.admission && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
                    if (data != null)
                    {
                        if (alval.passORfail == "Pass" && alval.continueORleave == "continue")
                        {
                            data.class_id = clsid;
                            data.section_id = secid;
                            data.year = promoteyear;
                            data.college_id = listData.CollegeID;
                            data.branch_id = listData.BranchID;
                            data.modified_by = listData.user;
                            data.updated_date = DateTime.Now;
                            save += dbContext.SaveChanges();
                        }
                    }
                }
                if (save > 0)
                {
                    return Json(save, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(save = 0, JsonRequestBehavior.AllowGet);
                }
            }
            return View();
        }


        //-----------------------Mapping of class Sections Starts------------------------

        [HttpGet]
        public ActionResult map_class_section()
        {
            bind_Add_clas();
            var list = (from a in dbContext.class_sections
                        join b in dbContext.AddClasses on a.class_id equals b.class_id
                        join c in dbContext.sections on a.section_id equals c.id
                        where a.is_active != null && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                        select new SchoolMasterClasses.StuClass
                        {
                            id = a.id,
                            class1 = b.class_name,
                            section1 = c.section1,
                        }
                        ).ToList();
            ViewBag.classlist = list;
            bind_section();
            return View();
        }
        [HttpPost]
        public ActionResult map_class_section(StuClass model, class_sections model1, int? class_id, int? section_id, FormCollection connchk)
        {
            bind_Add_clas();
            bind_section();

            var cls = (from a in dbContext.class_sections where a.class_id == class_id & a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
            if (cls == null)
            {
                class_sections clsob = new class_sections()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year = listData.academic_year,
                    class_id = model.Class_id,
                    created_by = listData.user,
                    created_at = DateTime.Now,
                    is_active = true,
                    section_id = model.section_id
                    
                };
                dbContext.class_sections.Add(clsob);
                int c = dbContext.SaveChanges();
                if (c > 0)
                {
                    TempData["message"] = "Data Saved Successfully";
                }
                else
                {
                    TempData["message"] = "There was an error while saving the data, please contact admin..!";
                }
            }
            else
            {
                TempData["message"] = "Class " + class_id + " and Section " + section_id + " Already Assigned..Try Another..!";
            }
            return RedirectToAction("map_class_section");
        }


        [HttpGet]
        public ActionResult update_class_section(int? Id)
        {
            bind_Add_clas();
            bind_section();
            var edit = dbContext.class_sections.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            return View(edit);

        }

        [HttpPost]
        public ActionResult update_class_section(class_sections model, int? Id, int? class_id)
        {
            bind_Add_clas();
            bind_section();
            var update = dbContext.class_sections.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            update.class_id = model.class_id;
            update.section_id = model.section_id;
            update.college_id = model.college_id;
            update.branch_id = model.branch_id;
            update.modified_by = listData.user;
            update.updated_at = DateTime.Now;
            dbContext.class_sections.AddOrUpdate(update);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Updated Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin";

            }
            return RedirectToAction("map_class_section");
        }


        [HttpGet]
        public ActionResult delete_class_section(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbContext.class_sections.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();
            if (data == null)
            {
                return HttpNotFound();
            }

            dbContext.class_sections.Remove(data ?? throw new InvalidOperationException());
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Deleted Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("map_class_section");
        }

        //-----------------------Mapping of class Sections Ends------------------------


        //---------Adding, Deleting and Updating of Main Section Starts------------------------

        [HttpGet]
        public ActionResult AddSection()
        {
            var getsectiondata = (from a in dbContext.sections where a.is_active != null && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).ToList();
            ViewBag.sectiondata = getsectiondata;
            return View();
        }

        [HttpPost]
        public ActionResult AddSection(section model)
        {
            section sec = new section()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year,
                section1 = model.section1,
                created_at = DateTime.Now,
                created_by = listData.user,
                is_active = true
            };
            dbContext.sections.Add(sec);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Saved Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("AddSection");
        }


        [HttpGet]
        public ActionResult UpdateSection(int? Id)
        {
            var edit = dbContext.sections.Where(a => a.id == Id).FirstOrDefault();
            var getsectiondata = (from a in dbContext.sections where a.is_active != null && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).ToList();
            ViewBag.sectiondata = getsectiondata;
            return View(edit);
        }

        [HttpPost]
        public ActionResult UpdateSection(section model, int? Id)
        {
            var edit = dbContext.sections.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            if (edit.id == Id)
            {
                edit.section1 = model.section1;
                edit.modified_by = listData.user;
                edit.updated_at = DateTime.Now;
                edit.college_id = listData.CollegeID;
                edit.branch_id = listData.BranchID;
            }
            dbContext.sections.AddOrUpdate(edit);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Updated Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin";
            }

            return RedirectToAction("AddSection");
        }


        [HttpGet]
        public ActionResult DeleteSection(int? Id)
        {

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbContext.sections.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();
            if (data == null)
            {
                return HttpNotFound();
            }
            dbContext.sections.Remove(data ?? throw new InvalidOperationException());
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Deleted Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("AddSection");
        }

        //---------Adding, Deleting and Updating of Main Section Ends--------------------------

    }
}