﻿using System;
using System.Web.Mvc;
using SmartSchool.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Data.Entity;
using System.IO;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using SmartSchool.Classes;
using SmartSchool.SchoolMasterClasses;
using Newtonsoft.Json;
using System.Dynamic;
using System.Data.SqlClient;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using GoogleTranslateFreeApi;
using SmartSchool.Interfaces;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;
using System.Collections;

namespace SmartSchool.Controllers
{
    public class AdminController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public get_staff_classes gs = new get_staff_classes();
        public string ActiveStatus = "True";
        public static string selectedDate = "";
        public SmsTextLocalClass sms = new SmsTextLocalClass();

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public AdminController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
        public DateTime created_at = DateTime.Now;
        public DateTime modified_at = DateTime.Now;

        public class SelectDate
        {
            public DateTime? X { get; set; }
            public double? Y { get; set; }
        }

        public void GoogleTrans()
        {
            GoogleTranslator xt = new GoogleTranslator();
            TranslationResult jj = xt.TranslateAsync("Help", Language.English, Language.Kannada).Result;
        }

        public class YearData
        {
            public string X { get; set; }
            public int? Z { get; set; }
            public double? Y { get; set; }

        }

        public class IAMarks
        {
            public int? X { get; set; } //IA name
            public string Y { get; set; } //Student ID

            public int? A { get; set; } //AVg marks

            public int? B { get; set; } //class id

            public string cn { get; set; } //class name
        }

        public class SubjectWiseIAMarks
        {

            public int? X { get; set; } //IA name
            public string Y { get; set; } //Student ID

            public int? A { get; set; } //AVg marks
            public int? B { get; set; } //class id
            public string cn { get; set; }//class name


            public string C { get; set; } //Subject Name
            public int? classCount { get; set; }
        }

        public class StudentAttendence
        {
            public string X { get; set; } //StudentID

            public string A { get; set; } //Present Absent

            public int? B { get; set; } //Present Absent

            public string cn { get; set; } //class name

        }
        public class StudentGenderCategory
        {
            public string A { get; set; }
            public string B { get; set; }
            public int? C { get; set; }
            public string cn { get; set; }
            public int? ClassWiseCount { get; set; }
            public int? genderCountMale { get; set; }
            public int? genderCountFeMale { get; set; }
            public int? D { get; set; }
        }


        protected void Indexlang(string lang_val)//This method is used to change the Language
        {
            if (lang_val != null)
            {

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang_val);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang_val);

                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang_val);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang_val);
            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang_val;
            Response.Cookies.Add(cookie);
        }

        public ActionResult Index()
        {
            ViewBag.smsCount =  getNoOfSMSCount();
            Thread.CurrentThread.CurrentCulture.ClearCachedData();

            var getlang = (from lan in dbContext.languages where lan.is_active == true && lan.college_id == listData.CollegeID && lan.branch_id == listData.BranchID && lan.year == listData.academic_year select lan.code).FirstOrDefault();
            if (getlang != null)
            {
                Indexlang(getlang);
            }
            else
            {
                Indexlang("en");
            }
            var getdate = DateTime.Now;
            DateTime currdate = DateTime.Now;

            var data = (from s in dbContext.fee_deposit_amountdetails where (s.year == listData.academic_year && s.date.Value.Month == currdate.Month && s.college_id == listData.CollegeID && s.branch_id == listData.BranchID && s.year == listData.academic_year) select s.amount).Sum();
            double amt = Convert.ToDouble(data);
            ViewBag.fee_total = amt;

            var exp = (from s in dbContext.expenses where (s.year == listData.academic_year && s.date.Value.Month == currdate.Month && s.college_id == listData.CollegeID && s.branch_id == listData.BranchID && s.year == listData.academic_year) select s.amount).Sum();
            double expamt = Convert.ToDouble(exp);
            ViewBag.expenseamount = expamt;

            int stu = dbContext.students.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.id).Count();
            ViewBag.TotalStudents = stu;

            int No_of_Admin = (from r in dbContext.staffs where (r.roll_id == "021e7592-3119-477c-bf20-aa9fa1cd1e74" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalAdmin = No_of_Admin;

            int No_of_Teacher = (from r in dbContext.staffs where (r.roll_id == "8b1b8949-2b30-4e94-8b3d-30ab61b95e53" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalTeacher = No_of_Teacher;

            int No_of_SuperAdmin = (from r in dbContext.staffs where (r.roll_id == "dd184641-0108-450d-8f21-68a5d36922f4" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalSuperAdmin = No_of_SuperAdmin;

            int No_of_Accountant = (from r in dbContext.staffs where (r.roll_id == "b19bf44a-9ff7-4064-9ed3-1064a46d7a71" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalAccountant = No_of_Accountant;

            int No_of_Librarian = (from r in dbContext.staffs where (r.roll_id == "6f00430d-b473-4fef-9fcb-b75f95711563" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalLibrarian = No_of_Librarian;

            int No_of_Receptionist = (from r in dbContext.staffs where (r.roll_id == "2285e8a5-dd7c-4b42-af08-1acc881a1fcb" && r.college_id == listData.CollegeID && r.branch_id == listData.BranchID && r.year == listData.academic_year) select r.roll_id).Count();
            ViewBag.TotalReceptionist = No_of_Receptionist;

            int settingYear = 0;
            if (getdate.Month < 6)
            {
                settingYear = getdate.Year - 1;
            }
            ViewBag.CurrentMonth = getdate.Month;
            ViewBag.CurrentYear = settingYear;
            DateTime newdateTime = DateTime.Today;
            string CurrentDate = newdateTime.ToString("yyyy-MM-dd");
            ViewBag.CurrentDate = CurrentDate;

            ViewBag.message = dbContext.Pro_Latest_News(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).OrderByDescending(k => k.id).ToList();
            var sesval = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).Select(k => new SelectListItem
            {
                Text = k.session1,
                Value = k.session1
            }).ToList();
            //   sesval.Insert(0, new SelectListItem { Text = "Select", Value = "0" });         
            ViewBag.SessionYear = sesval;
            ViewBag.personlogo = listData.person_logo;

            ViewBag.MaleCount = dbContext.students.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.gender == "Male").Select(k => k.id).Count();
            ViewBag.FemaleCount = dbContext.students.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.gender == "Female").Select(k => k.id).Count();

            return View();
        }


        protected int? getNoOfSMSCount()
        {
            string dataString, strsms1;
            int smsCount = 0;
            WebRequest webRequest;
            HttpWebResponse webResponse;
            Stream stream = null;
            StreamReader readStream;

            try
            {
                var getSMSProvider = sms.get_provider(listData.CollegeID, listData.BranchID, listData.academic_year);
                string strUrl1 = "http://103.233.76.48/websms/getbalance.aspx?userid=" + getSMSProvider.username + "&password=" + getSMSProvider.password;

                webRequest = HttpWebRequest.Create(strUrl1);
                webResponse = (HttpWebResponse)webRequest.GetResponse();
                stream = (Stream)webResponse.GetResponseStream();
                readStream = new StreamReader(stream);
                dataString = readStream.ReadToEnd();
                strsms1 = dataString.Substring(dataString.IndexOf(':') + 1).Trim();

                webResponse.Close();
                stream.Close();
                readStream.Close();

                int.TryParse(strsms1, out smsCount);
            }
            catch (Exception ex)
            {
               // Response.Write("<script>alert('Something went wrong...');</script>");
            }
            return smsCount;
        }

        //This function changes have been Updated
        [HttpPost]
        public JsonResult YearlyIncome(string SelectedYear)
        {

            List<DataPoint> dataPointYear = new List<DataPoint>();
            var getDataFromProcedure = dbContext.Pro_YIM(listData.CollegeID, listData.BranchID, SelectedYear == "0" ? null : SelectedYear).Select(k => new YearData
            {
                X = k.date.ToString(),
                Y = k.TotalAmount,
                Z = k.CurrentMonth
            }).ToList();


            if (getDataFromProcedure.Count != 0)
            {

                foreach (var item in getDataFromProcedure)
                {
                    var dateTime = Convert.ToDateTime(item.X).ToString("yyyy-MM-dd");
                    dataPointYear.Add(new DataPoint(dateTime, Convert.ToDouble(item.Y)));
                }

            }
            else
            {
                dataPointYear.Add(new DataPoint(DateTime.Now.ToString("yyyy-MM-dd"), 0));
            }

            var Jsdata = JsonConvert.SerializeObject(dataPointYear);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult YearlyExpense(string SelectedYear)
        {
            List<DataPoint> dataPointYear = new List<DataPoint>();
            var getDataFromProcedure = dbContext.Pro_YEM(listData.CollegeID, listData.BranchID, SelectedYear == "0" ? null : SelectedYear).Select(k => new YearData
            {
                X = k.date.ToString(),
                Y = k.TotalAmount,
                Z = k.CurrentMonth
            }).ToList();


            if (getDataFromProcedure.Count != 0)
            {

                foreach (var item in getDataFromProcedure)
                {
                    var dateTime = Convert.ToDateTime(item.X).ToString("yyyy-MM-dd");
                    dataPointYear.Add(new DataPoint(dateTime, Convert.ToDouble(item.Y)));
                }

            }
            else
            {
                dataPointYear.Add(new DataPoint(DateTime.Now.ToString("yyyy-MM-dd"), 0));
            }

            var Jsdata = JsonConvert.SerializeObject(dataPointYear);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult MonthlyExpense(int SelectedMonth)
        {
            var getdate = DateTime.Now;
            if (SelectedMonth == 0)
            {
                SelectedMonth = getdate.Month;
            }
            int SetYear = getdate.Year;
            if (getdate.Month < 6)
            {
                SetYear = getdate.Year - 1;
            }
            DateTime newdateTime = DateTime.Today; // As DateTime
            selectedDate = newdateTime.ToString("yyyy-MM-dd");
            var MonthlyExpense = dbContext.expenses.Where(v => v.date.Value.Month == SelectedMonth && v.date.Value.Year == SetYear && v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).GroupBy(u => u.date)
                                 .Select(group => new SelectDate
                                 {
                                     X = group.Key,
                                     Y = group.Sum(x => x.amount)
                                 }).Distinct().OrderBy(u => u.X).ToList();
            List<DataPoint> dataPointMonthlyExpense = new List<DataPoint>();
            string[] xday;
            if ((getdate.Year % 4 == 0) && (SelectedMonth == 2))
            {
                xday = new String[29];
                for (int i = 0; i < 29; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else if (SelectedMonth == 2)
            {
                xday = new String[28];
                for (int i = 0; i < 28; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else if (SelectedMonth == 4 || SelectedMonth == 6 || SelectedMonth == 9 || SelectedMonth == 11)
            {
                xday = new String[30];
                for (int i = 0; i < 30; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else
            {
                xday = new String[31];
                for (int i = 0; i < 31; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            int loopDay = 0;
            if (MonthlyExpense.Count == 0)
            {
                for (int i = loopDay + 1; i <= xday.Length; i++)
                {
                    dataPointMonthlyExpense.Add(new DataPoint(xday[i - 1], 0));
                }
            }
            else
            {
                foreach (var item in MonthlyExpense)
                {
                    for (int i = loopDay + 1; i <= xday.Length; i++)
                    {
                        int j = i - 1;
                        DateTime DBdate = Convert.ToDateTime(item.X);
                        if (DBdate.Day == i)
                        {
                            dataPointMonthlyExpense.Add(new DataPoint(xday[j], item.Y));
                            loopDay = i;
                            break;
                        }
                        else
                        {
                            dataPointMonthlyExpense.Add(new DataPoint(xday[j], 0));
                        }
                    }
                }
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointMonthlyExpense);
            return Json(Jsdata);
        }
        [HttpPost]
        public JsonResult MonthlyIncome(int SelectedMonth)
        {
            var getdate = DateTime.Now;
            if (SelectedMonth == 0)
            {
                SelectedMonth = getdate.Month;
            }
            int SetYear = getdate.Year;
            if (getdate.Month < 6)
            {
                SetYear = getdate.Year - 1;
            }
            DateTime newdateTime = DateTime.Today; // As DateTime
            selectedDate = newdateTime.ToString("yyyy-MM-dd");
            var MonthlyIncome = dbContext.incomes.Where(v => v.date.Value.Month == SelectedMonth && v.date.Value.Year == SetYear && v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).GroupBy(u => u.date)
                                 .Select(group => new SelectDate
                                 {
                                     X = group.Key,
                                     Y = group.Sum(x => x.amount)
                                 }).Distinct().OrderBy(u => u.X).ToList();
            List<DataPoint> dataPointMonthlyIncome = new List<DataPoint>();
            string[] xday;
            if ((getdate.Year % 4 == 0) && (SelectedMonth == 2))
            {
                xday = new String[29];
                for (int i = 0; i < 29; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else if (SelectedMonth == 2)
            {
                xday = new String[28];
                for (int i = 0; i < 28; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else if (SelectedMonth == 4 || SelectedMonth == 6 || SelectedMonth == 9 || SelectedMonth == 11)
            {
                xday = new String[30];
                for (int i = 0; i < 30; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            else
            {
                xday = new String[31];
                for (int i = 0; i < 31; i++)
                {
                    newdateTime = new DateTime(getdate.Year, SelectedMonth, i + 1);
                    selectedDate = newdateTime.ToString("yyyy-MM-dd");
                    xday[i] = selectedDate;
                }
            }
            int loopDay = 0;
            if (MonthlyIncome.Count == 0)
            {
                for (int i = loopDay + 1; i <= xday.Length; i++)
                {
                    dataPointMonthlyIncome.Add(new DataPoint(xday[i - 1], 0));
                }
            }
            else
            {
                foreach (var item in MonthlyIncome)
                {
                    for (int i = loopDay + 1; i <= xday.Length; i++)
                    {
                        int j = i - 1;
                        DateTime DBdate = Convert.ToDateTime(item.X);
                        if (DBdate.Day == i)
                        {
                            dataPointMonthlyIncome.Add(new DataPoint(xday[j], item.Y));
                            loopDay = i;
                            break;
                        }
                        else
                        {
                            dataPointMonthlyIncome.Add(new DataPoint(xday[j], 0));
                        }
                    }
                }
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointMonthlyIncome);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult IAMarksClasswise()
        {
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();
            string StringStandard = " standard";

            var Classsswise = (from a in dbContext.exams
                               join b in dbContext.exam_schedules on a.eid equals b.gropu_id
                               join c in dbContext.exam_results on b.esid equals c.exam_schedule_id
                               where (b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year && c.is_active == true)
                               orderby b.class_id
                               select new SubjectWiseIAMarks()
                               {
                                   B = b.class_id,
                               }).Distinct().ToList();
            int NoOfStds = 0;
            foreach (var items10 in Classsswise)
            {
                NoOfStds++;
            }
            int[] standardMarks1 = new int[NoOfStds];
            int[] standardCount1 = new int[NoOfStds];
            int[] standardMarks2 = new int[NoOfStds];
            int[] standardCount2 = new int[NoOfStds];
            int[] standardMarks3 = new int[NoOfStds];
            int[] standardCount3 = new int[NoOfStds];

            var IAMarks = (from a in dbContext.exams
                           join b in dbContext.exam_schedules on a.eid equals b.gropu_id
                           join c in dbContext.exam_results on b.esid equals c.exam_schedule_id
                           join cs in dbContext.AddClasses on b.class_id equals cs.class_id
                           where (b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year && c.is_active == true)
                           select new IAMarks()
                           {
                               X = b.gropu_id,
                               Y = c.student_id,
                               A = c.get_marks,
                               B = b.class_id,
                               cn = cs.class_name

                           }).Distinct().ToList();

            foreach (var item in IAMarks)
            {
                int j = Convert.ToInt32(item.B) - 1;
                int fetchMarks = Convert.ToInt32(item.A);
                int examid = Convert.ToInt32(item.X);
                switch (examid)
                {
                    case 17:
                        standardMarks1[j] = standardMarks1[j] + fetchMarks;
                        standardCount1[j] = standardCount1[j] + 1;
                        break;

                    case 18:
                        standardMarks2[j] = standardMarks2[j] + fetchMarks;
                        standardCount2[j] = standardCount2[j] + 1;
                        break;

                    case 19:
                        standardMarks3[j] = standardMarks3[j] + fetchMarks;
                        standardCount3[j] = standardCount3[j] + 1;
                        break;

                    default: break;
                }
            }
            for (int k = 0; k < NoOfStds; k++)
            {
                double avg1 = 0.00, avg2 = 0.00, avg3 = 0.00;
                if (standardCount1[k] != 0) { avg1 = standardMarks1[k] / standardCount1[k]; }
                if (standardCount2[k] != 0) { avg2 = standardMarks2[k] / standardCount2[k]; }
                if (standardCount3[k] != 0) { avg3 = standardMarks3[k] / standardCount3[k]; }

                dataPointIAMarks.Add(new DataPoint((k + 1).ToString() + StringStandard, avg1, avg2, avg3));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult IAMarksSubjectwise(int SelectedClass)
        {
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();
            //int[] standardMarks1 = { 0, 0, 0, 0, 0, 0 };
            //int[] standardCount1 = { 0, 0, 0, 0, 0, 0 };
            //int[] standardMarks2 = { 0, 0, 0, 0, 0, 0 };
            //int[] standardCount2 = { 0, 0, 0, 0, 0, 0 };
            //int[] standardMarks3 = { 0, 0, 0, 0, 0, 0 };
            //int[] standardCount3 = { 0, 0, 0, 0, 0, 0 };
            //string[] Subjects = new string[6];

            //var IAMarks = (from a in dbContext.exams
            //               join b in dbContext.exam_schedules on a.eid equals b.exam_id
            //               join c in dbContext.exam_results on b.esid equals c.exam_schedule_id
            //               join d in dbContext.teacher_subjects on b.teacher_subject_id equals d.id
            //               join e in dbContext.subjects on d.subject_id equals e.id
            //               where (b.class_id == SelectedClass && a.college_id == listData.CollegeID && b.college_id == listData.CollegeID && c.college_id == listData.CollegeID && d.college_id == listData.CollegeID && e.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.branch_id == listData.BranchID && c.branch_id == listData.BranchID && d.branch_id == listData.BranchID && e.branch_id == listData.BranchID && a.is_active == true && b.is_active == true && c.is_active == true && d.is_active == true && e.is_active == true)



            var Subjectwise = (from a in dbContext.exams
                               join b in dbContext.exam_schedules on a.eid equals b.gropu_id
                               join c in dbContext.exam_results on b.esid equals c.exam_schedule_id
                               join e in dbContext.subjects on b.teacher_subject_id equals e.id
                               where (b.class_id == SelectedClass && a.college_id == listData.CollegeID && b.college_id == listData.CollegeID && c.college_id == listData.CollegeID && e.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.branch_id == listData.BranchID && c.branch_id == listData.BranchID && e.branch_id == listData.BranchID && a.year == listData.academic_year && b.year == listData.academic_year && c.year == listData.academic_year && c.year == listData.academic_year && e.year == listData.academic_year && a.is_active == true && b.is_active == true && c.is_active == true && e.is_active == true)
                               orderby e.id
                               select new SubjectWiseIAMarks()
                               {
                                   B = e.id,
                                   C = e.name
                               }).Distinct().ToList();
            int NoOfSubs = 0;
            foreach (var items1 in Subjectwise)
            {
                //Subjects[opq] = items1.C;
                NoOfSubs++;
            }

            int[] standardMarks1 = new int[NoOfSubs];
            int[] standardCount1 = new int[NoOfSubs];
            int[] standardMarks2 = new int[NoOfSubs];
            int[] standardCount2 = new int[NoOfSubs];
            int[] standardMarks3 = new int[NoOfSubs];
            int[] standardCount3 = new int[NoOfSubs];
            string[] Subjects = new string[NoOfSubs];

            int subbs = 0;
            foreach (var items1 in Subjectwise)
            {
                Subjects[subbs] = items1.C;
                subbs++;
            }

            var SubjectWiseIAMarks = (from a in dbContext.exams
                                      join b in dbContext.exam_schedules on a.eid equals b.gropu_id
                                      join c in dbContext.exam_results on b.esid equals c.exam_schedule_id
                                      join e in dbContext.subjects on b.teacher_subject_id equals e.id
                                      where (b.class_id == SelectedClass && a.college_id == listData.CollegeID && b.college_id == listData.CollegeID && c.college_id == listData.CollegeID && e.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.branch_id == listData.BranchID && c.branch_id == listData.BranchID && e.branch_id == listData.BranchID && a.year == listData.academic_year && b.year == listData.academic_year && c.year == listData.academic_year && c.year == listData.academic_year && e.year == listData.academic_year && a.is_active == true && b.is_active == true && c.is_active == true && e.is_active == true)
                                      orderby e.id
                                      select new SubjectWiseIAMarks()
                                      {
                                          X = b.gropu_id,
                                          Y = c.student_id,
                                          A = c.get_marks,
                                          B = e.id,
                                          C = e.name
                                      }).Distinct().ToList();

            foreach (var item in SubjectWiseIAMarks)
            {
                int j = 0;
                for (int q = 0; q < NoOfSubs; q++)
                {
                    if (Subjects[q] == item.C)
                    {
                        j = q;
                    }
                }

                int fetchMarks = Convert.ToInt32(item.A);
                int examid = Convert.ToInt32(item.X);
                switch (examid)
                {
                    case 17:
                        standardMarks1[j] = standardMarks1[j] + fetchMarks;
                        standardCount1[j] = standardCount1[j] + 1;
                        break;

                    case 18:
                        standardMarks2[j] = standardMarks2[j] + fetchMarks;
                        standardCount2[j] = standardCount2[j] + 1;
                        break;

                    case 19:
                        standardMarks3[j] = standardMarks3[j] + fetchMarks;
                        standardCount3[j] = standardCount3[j] + 1;
                        break;

                    default: break;
                }
            }
            for (int k = 0; k < NoOfSubs; k++)
            {
                double avg1 = 0.00, avg2 = 0.00, avg3 = 0.00;
                if (standardCount1[k] != 0) { avg1 = standardMarks1[k] / standardCount1[k]; }
                if (standardCount2[k] != 0) { avg2 = standardMarks2[k] / standardCount2[k]; }
                if (standardCount3[k] != 0) { avg3 = standardMarks3[k] / standardCount3[k]; }
                dataPointIAMarks.Add(new DataPoint(((k + 1).ToString() + " " + Subjects[k]), avg1, avg2, avg3));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult GenderWiseStatistics()
        {
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();            

            var Classsswise = (from p in dbContext.students
                               join cs in dbContext.AddClasses on p.class_id equals cs.class_id
                               where (p.year.ToString() == listData.academic_year && p.college_id == listData.CollegeID && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.branch_id == listData.BranchID  && p.is_active == true && cs.college_id == listData.CollegeID && cs.branch_id == listData.BranchID && p.gender != null)
                               group new { p, cs } by new { cs.class_name, p.gender } into gr

                               select new StudentGenderCategory()
                               {
                                   cn = gr.Select(k => k.cs.class_name).FirstOrDefault(),
                                   ClassWiseCount = gr.Select(k => k.p.admission_no).Count(),
                                   genderCountMale = gr.Where(k => k.p.gender == "Male").Select(k => k.p).Count(),
                                   genderCountFeMale = gr.Where(k => k.p.gender == "Female").Select(k => k.p).Count()

                               }).ToList().OrderBy(k => gs.PadNumbers(k.cn));



            foreach (var data in Classsswise)
            {
                dataPointIAMarks.Add(new DataPoint((data.cn), (int)data.genderCountMale, (int)data.genderCountFeMale));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

       

        [HttpPost]
        public JsonResult CategoryWiseStatistics(int SelectedClass)
        {
            var getdate = DateTime.Now;
            int settingYear = getdate.Year;
            int nextYear = settingYear + 1;
            if (getdate.Month < 6)
            {
                settingYear = getdate.Year - 1;
                nextYear = settingYear + 1;
            }
            string yyear = settingYear + "-" + nextYear;
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();
            //int[] MaleCount = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int GM = 0, SC = 0, ST = 0, OBC = 0, OTHER = 0;
            string[] categg = { "GM", "SC", "ST", "OBC", "OTHER" };
            var StuDeails = (from p in dbContext.students
                             join cs in dbContext.AddClasses on p.class_id equals cs.class_id
                             join q in dbContext.categories on p.category_id equals q.id
                             where (p.year.ToString() == yyear && cs.class_name == ("Class " + SelectedClass) && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.is_active == true && q.is_active == true && cs.college_id == listData.CollegeID)
                             orderby q.id
                             select new StudentGenderCategory()
                             {
                                 A = p.user_id,
                                 B = q.category1,
                                 C = p.class_id,
                                 cn = cs.class_name,
                                 D = p.section_id
                             }).Distinct().ToList();

            foreach (var item in StuDeails)
            {
                string StuCategory = item.B;
                switch (StuCategory)
                {
                    case "GM":
                        GM++;
                        break;

                    case "SC":
                        SC++;
                        break;

                    case "ST":
                        ST++;
                        break;

                    case "OBC":
                        OBC++;
                        break;

                    case "OTHER":
                        OTHER++;
                        break;
                }
            }
            dataPointIAMarks.Add(new DataPoint(categg[0], GM, 1.0));
            dataPointIAMarks.Add(new DataPoint(categg[1], SC, 1.0));
            dataPointIAMarks.Add(new DataPoint(categg[2], ST, 1.0));
            dataPointIAMarks.Add(new DataPoint(categg[3], OBC, 1.0));
            dataPointIAMarks.Add(new DataPoint(categg[4], OTHER, 1.0));
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult ChangeStudentsAttendence(string ChangeDate)
        {
            DateTime newdateTime = DateTime.Today;
            string CurrentDate = newdateTime.ToString("yyyy-MM-dd");
            var Classsswise = (from d in dbContext.attendence_type
                               join e in dbContext.student_attendences on d.id equals e.attendence_type_id
                               join f in dbContext.students on e.student_session_id equals f.user_id
                               where (e.college_id == listData.CollegeID && e.branch_id == listData.BranchID && e.year == listData.academic_year)
                               orderby f.class_id
                               select new SubjectWiseIAMarks()
                               {
                                   B = f.class_id,
                               }).Distinct().ToList();
            int NoOfStds = 0;
            foreach (var items10 in Classsswise)
            {
                NoOfStds++;
            }

            var ClassAttendence = (from d in dbContext.attendence_type
                                   join e in dbContext.student_attendences on d.id equals e.attendence_type_id
                                   join f in dbContext.students on e.student_session_id equals f.user_id
                                   join cs in dbContext.AddClasses on f.class_id equals cs.class_id
                                   where (e.college_id == listData.CollegeID && e.branch_id == listData.BranchID && e.year == listData.academic_year && e.date.ToString() == CurrentDate)
                                   select new StudentAttendence()
                                   {
                                       X = e.student_session_id,
                                       A = d.type,
                                       B = f.class_id,
                                       cn = cs.class_name
                                   }).ToList();
            List<DataPoint> dataPointMonthlyIncome = new List<DataPoint>();

            for (int standard = 1; standard <= NoOfStds; standard++)
            {
                int Present = 0, Late = 0, Absent = 0, Half = 0, Holiday = 0;
                foreach (var item in ClassAttendence)
                {
                    //if (item.B == standard)
                    //  {
                    if (item.A == "P")
                    {
                        Present++;
                    }
                    else if (item.A == "L")
                    {
                        Late++;
                    }
                    else if (item.A == "A")
                    {
                        Absent++;
                    }
                    else if (item.A == "H")
                    {
                        Half++;
                    }
                    else if (item.A == "HL")
                    {
                        Holiday++;
                    }
                    // }
                }
                dataPointMonthlyIncome.Add(new DataPoint((ClassAttendence.Select(k => k.cn).FirstOrDefault()), Present, Late, Absent, Half, Holiday));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointMonthlyIncome);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult SetTotalStudents()
        {
            var getdate = DateTime.Now;
            int settingYear = getdate.Year;
            int nextYear = settingYear + 1;
            if (getdate.Month < 6)
            {
                settingYear = getdate.Year - 1;
                nextYear = settingYear + 1;
            }
            string yyear = settingYear + "-" + nextYear;
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();

            var Classsswise = (from p in dbContext.students
                               join q in dbContext.categories on p.category_id equals q.id
                               join cs in dbContext.AddClasses on p.class_id equals cs.class_id
                               where (p.year.ToString() == yyear && p.college_id == listData.CollegeID && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.branch_id == listData.BranchID && p.is_active == true && q.is_active == true)
                               group new { p, q, cs } by new { cs.class_id } into gr

                               select new SubjectWiseIAMarks()
                               {
                                   cn = gr.Select(k => k.cs.class_name).FirstOrDefault(),
                                   classCount = gr.Select(k => k.p.id).Count()

                               }).ToList();

            foreach (var data in Classsswise)
            {
                dataPointIAMarks.Add(new DataPoint(data.classCount, data.cn));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult NonRTEStatistics()
        {
            var getdate = DateTime.Now;
            int settingYear = getdate.Year;
            int nextYear = settingYear + 1;
            if (getdate.Month < 6)
            {
                settingYear = getdate.Year - 1;
                nextYear = settingYear + 1;
            }
            string yyear = settingYear + "-" + nextYear;
            List<DataPoint> dataPointIAMarks = new List<DataPoint>();


            var Classsswise = (from p in dbContext.students
                               join q in dbContext.categories on p.category_id equals q.id
                               join cs in dbContext.AddClasses on p.class_id equals cs.class_id
                               where (p.year.ToString() == yyear && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.is_active == true && q.is_active == true)
                               orderby p.class_id
                               select new SubjectWiseIAMarks()
                               {
                                   B = p.class_id,
                                   cn = cs.class_name
                               }).Distinct().ToList();
            int NoOfStds = 0;
            foreach (var items10 in Classsswise)
            {
                NoOfStds++;
            }
            int[] MaleCount = new int[NoOfStds];
            int[] FemaleCount = new int[NoOfStds];

            var StuDeails = (from p in dbContext.students
                             join q in dbContext.categories on p.category_id equals q.id
                             join cs in dbContext.AddClasses on p.class_id equals cs.class_id
                             where (p.year.ToString() == yyear && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.is_active == true && q.is_active == true && p.rte != null)
                             orderby q.id
                             select new StudentGenderCategory()
                             {
                                 A = p.user_id,
                                 B = p.rte,
                                 C = p.class_id,
                                 cn = cs.class_name,
                                 D = p.section_id
                             }).Distinct().ToList();

            foreach (var item in StuDeails)
            {
                string StuGender = item.B;
                int Class = Convert.ToInt32(item.C);
                if (StuGender == "Yes")
                {
                    //rte Students
                    MaleCount[Class - 1] = MaleCount[Class - 1] + 1;
                }
                else
                {
                    //non-RTE Students
                    FemaleCount[Class - 1] = FemaleCount[Class - 1] + 1;
                }
            }
            string StringStandard = " standard";
            for (int k = 0; k < NoOfStds; k++)
            {
                dataPointIAMarks.Add(new DataPoint((k + 1).ToString() + StringStandard, MaleCount[k], FemaleCount[k]));
            }
            var Jsdata = JsonConvert.SerializeObject(dataPointIAMarks);
            return Json(Jsdata);
        }

        [HttpPost]
        public JsonResult callgetsec(string ddlclass)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlclass))
            {
                int classid = Convert.ToInt32(ddlclass);
                //result = cmc.get_class_sec(classid);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Visitor purpose dropdown binding
        private void Ddlpurpose()
        {
            List<visitorspurpose> c = dbContext.visitorspurposes.Where(x => x.vp_id != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID && x.year==listData.academic_year).ToList();
            visitorspurpose visitor = new visitorspurpose
            {
                purpose = " select visitor purpose",
                vp_id = 0
            };
            c.Insert(0, visitor);
            SelectList selectvisitor = new SelectList(c, "vp_id", "purpose ", 0);
            ViewBag.selectedvisitor = selectvisitor;
        }

        private void Bindstaffname()
        {
            List<staff> c = dbContext.staffs.Where(x => x.id != 0).ToList();
            staff st = new staff
            {
                name = " select Staff Name",
                id = 0
            };
            c.Insert(0, st);
            SelectList selectstaff = new SelectList(c, "id", "name ", 0);
            ViewBag.selectedstaff = selectstaff;
        }

        private void DdlStatus()
        {
            List<status_master> c = dbContext.status_master.Where(x => x.id != 0).ToList();
            status_master sm = new status_master
            {
                Name = " select status",
                id = 0
            };
            c.Insert(0, sm);
            SelectList selectstatus = new SelectList(c, "id", "Name ", 0);
            ViewBag.selectedstatus = selectstatus;

        }

        //complainttype dropdown binding
        private void Ddlcomplainttype()
        {
            List<complaint_type> l = dbContext.complaint_type.Where(x => x.Ctid != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID ).ToList();
            complaint_type com = new complaint_type
            {
                complaint_type1 = "select Complaint type",
                Ctid = 0
            };
            l.Insert(0, com);
            SelectList selectcomplainttype = new SelectList(l, "Ctid", "complaint_type1", 0);
            ViewBag.selectedcomplainttype = selectcomplainttype;

        }

        //Source name dropdown binding
        private void DdlSource()
        {
            List<source> s = dbContext.sources.Where(x => x.Sid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID ).ToList();
            source sou = new source
            {
                source1 = "select source name",
                Sid = 0
            };
            s.Insert(0, sou);
            SelectList selectsource = new SelectList(s, "Sid", "source1", 0);
            ViewBag.selectedsource = selectsource;
        }

        private void DdlActiontaken()
        {
            List<Actiontaken> c = dbContext.Actiontakens.Where(x => x.Atid != 0).ToList();
            Actiontaken Action = new Actiontaken
            {
                Name = " select visitor purpose",
                Atid = 0
            };
            c.Insert(0, Action);
            SelectList selectaction = new SelectList(c, "Atid", "Name ", 0);
            ViewBag.selectedaction = selectaction;
        }

        //Refrence name dropdown binding
        private void Ddlreference()
        {
            List<reference_front> r = dbContext.reference_front.Where(x => x.Sid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true).ToList();
            reference_front reference = new reference_front
            {
                reference = "select refrence name",
                Sid = 0
            };
            r.Insert(0, reference);
            SelectList selectreference = new SelectList(r, "Sid", "reference", 0);
            ViewBag.selectedrefrence = selectreference;
        }

        //Class name Dropdown binding
        public void Ddlclass()
        {
            var u_lists = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.selectedclass = u_lists;
        }

        //Class name Dropdown binding
        public void Ddlsections()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0).ToList();
            section sec = new section

            {
                section1 = "select section name",
                id = 0
            };
            c.Insert(0, sec);
            SelectList selectsection = new SelectList(c, "id", "section1", 0);
            ViewBag.selectedsection = selectsection;
        }

        [HttpPost]
        public ActionResult Admissionenquiry(enquiry model)
        {

            Ddlreference();
            DdlSource();
            Ddlclass();

            //var IntDate = DateTime.Parse(interviewdate);
            //var FollowupDate = DateTime.Parse(followupdate);
            enquiry enq = new enquiry()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                name = model.name,
                contact = model.contact,
                address = model.address,
                reference = model.reference,
                date = model.date,
                description = model.description,
                follow_up_date = model.follow_up_date,
                note = model.note,
                @class = model.@class,
                source = model.source,
                email = model.email,
                assigned = model.assigned,
                no_of_child = model.no_of_child,
                is_active = true,
                created_by = listData.user,
                CreatedDate = DateTime.Now,
                year = listData.academic_year
            };
            dbContext.enquiries.Add(enq);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record inserted successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("Admissionenquiry");
        }

        [HttpGet]
        public ActionResult Admissionenquiry(int? id)
        {
            Ddlreference();
            DdlSource();
            Ddlclass();
            //GoogleTrans();
            var DisplayAdmissionenquirys = (from a in dbContext.enquiries
                                            join b in dbContext.sources on a.source equals b.Sid.ToString()
                                            join c in dbContext.AddClasses on a.@class equals c.class_id
                                            where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                            select new Enquirydata
                                            {
                                                Aeid = a.Aeid,
                                                Name = a.name,
                                                Contact = a.contact,
                                                Source = b.source1,
                                                Date = a.date,
                                                Follow_up_date = a.follow_up_date,
                                                Classname = c.class_name
                                            }).ToList();
            ViewBag.DisplayAdmissionenquiry = DisplayAdmissionenquirys;
            if (id != null)
            {
                ViewBag.list = (from a in dbContext.enquiries
                                join b in dbContext.sources on a.source equals b.Sid.ToString()
                                join c in dbContext.AddClasses on a.@class equals c.class_id
                                where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                                select new Enquirydata
                                {
                                    Aeid = a.Aeid,
                                    Name = a.name,
                                    Contact = a.contact,
                                    Source = b.source1,
                                    Date = a.date,
                                    Classname = c.class_name,
                                    Follow_up_date = a.follow_up_date,
                                    Description = a.description,
                                    Note = a.note,
                                    No_of_child = a.no_of_child,
                                    Assigned = a.assigned,
                                    Status = a.is_active
                                }).FirstOrDefault();

                return View();
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditAdmnenquiry(enquiry model)
        {
            DdlStatus();
            Ddlreference();
            DdlSource();
            Ddlclass();
            enquiry Editadmn = dbContext.enquiries.Find(model.Aeid);
            if (Editadmn.Aeid == model.Aeid)
            {
                Editadmn.Aeid = model.Aeid;
                Editadmn.name = model.name;
                Editadmn.contact = model.contact;
                Editadmn.address = model.address;
                Editadmn.reference = model.reference;
                Editadmn.description = model.description;
                Editadmn.date = model.date;
                Editadmn.follow_up_date = model.follow_up_date;
                Editadmn.note = model.note;
                Editadmn.source = model.source;
                Editadmn.email = model.email;
                Editadmn.assigned = model.assigned;
                Editadmn.no_of_child = model.no_of_child;
                Editadmn.is_active = model.is_active;
                Editadmn.college_id = listData.CollegeID;
                Editadmn.branch_id = listData.BranchID;
                Editadmn.modified_by = listData.user;
                Editadmn.ModifiedDate = DateTime.Now;
                Editadmn.year = listData.academic_year;
            }
            dbContext.enquiries.AddOrUpdate(Editadmn);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Admissionenquiry");
        }

        [HttpGet]
        public async Task<ActionResult> EditAdmnenquiry(int? id)
        {
            Ddlreference();
            DdlSource();
            Ddlclass();
            DdlStatus();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            enquiry Editadmn = await dbContext.enquiries.FindAsync(id);
            if (Editadmn == null)
            {
                return HttpNotFound();
            }
            return View(Editadmn);

        }

        [HttpGet]
        public ActionResult DeletAdmnenquiry(int? id)
        {

            if (id != null)
            {
                enquiry Aedelete = dbContext.enquiries.Where(a => a.Aeid == id).FirstOrDefault();
                Aedelete.modified_by = listData.user;
                Aedelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();
                dbContext.enquiries.Remove(Aedelete);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Admissionenquiry", "Admin");
        }


        [HttpPost]
        public ActionResult Visitors(visitors_book model, string visitordate, string intime, string outtime, HttpPostedFileBase FileUpload)
        {
            ViewBag.getdate = DateTime.Now;
            var IntDate = DateTime.Parse(visitordate);
            Ddlpurpose();
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/Visitors/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Visitors/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }

            visitors_book visit = new visitors_book()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                purpose = model.purpose,
                name = model.name,
                email = model.email,
                contact = model.contact,
                id_proof = model.id_proof,
                no_of_pepple = model.no_of_pepple,
                date = IntDate,
                in_time = intime,
                out_time = outtime,
                note = model.note,
                image = filesnames,
                is_active = true,
                created_by = listData.user,
                CreatedDate = DateTime.Now,
                year = listData.academic_year
            };
            dbContext.visitors_book.Add(visit);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record inserted Successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }


            return RedirectToAction("Visitors");
        }

        [HttpGet]
        public ActionResult Visitors(int? id)
        {
            ViewBag.getdate = DateTime.Now;
            Ddlpurpose();
            var Visitorslist = (from a in dbContext.visitors_book
                                join b in dbContext.visitorspurposes on a.purpose equals b.vp_id.ToString()

                                where b.college_id==listData.CollegeID && a.college_id==listData.CollegeID && a.is_active==true
                                select new Visitors_bookdata
                                {
                                    Vid = a.Vid,
                                    Purpose = b.purpose,
                                    Name = a.name,
                                    Contact = a.contact,
                                    Date = a.date,
                                    In_time = a.in_time,
                                    Out_time = a.out_time,


                                }).ToList();
            ViewBag.visitorsdetails = Visitorslist;
            if (id != null)
            {
                ViewBag.list = dbContext.visitors_book.Where(x => x.Vid != 0).FirstOrDefault();
                return View();
            }
            return View();
        }

        [HttpPost]
        public ActionResult Editvisitors(visitors_book model)
        {
            Ddlpurpose();
            visitors_book Editvisitors = dbContext.visitors_book.Find(model.Vid);
            if (Editvisitors.Vid == model.Vid)
            {
                Editvisitors.Vid = model.Vid;
                Editvisitors.name = model.name;
                Editvisitors.contact = model.contact;
                Editvisitors.id_proof = model.id_proof;
                Editvisitors.no_of_pepple = model.no_of_pepple;
                Editvisitors.date = model.date;
                Editvisitors.in_time = model.in_time;
                Editvisitors.out_time = model.out_time;
                Editvisitors.is_active = model.is_active;
                Editvisitors.college_id = listData.CollegeID;
                Editvisitors.branch_id = listData.BranchID;
                Editvisitors.ModifiedDate = DateTime.Now;
                Editvisitors.modified_by = listData.user;
                Editvisitors.year = listData.academic_year;
            }
            dbContext.visitors_book.AddOrUpdate(Editvisitors);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Visitors");
        }



        [HttpGet]
        public async Task<ActionResult> Editvisitors(int? id)
        {
            Ddlpurpose();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            visitors_book Editvisitors = await dbContext.visitors_book.FindAsync(id);
            if (Editvisitors == null)
            {
                return HttpNotFound();
            }
            return View(Editvisitors);

        }

        [HttpGet]
        public ActionResult Deletevisitor(int? id)
        {

            if (id != null)
            {
                visitors_book visitorsdelete = dbContext.visitors_book.Where(s => s.Vid == id).FirstOrDefault();
                visitorsdelete.modified_by = listData.user;
                visitorsdelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.visitors_book.Remove(visitorsdelete);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Visitors", "Admin");
        }


        [HttpPost]
        public ActionResult Generalcall(general_calls model, string Calldate)
        {

            var CallDate = DateTime.Parse(Calldate);
            //var FollowupDate = DateTime.Parse(Callfollowupdate);

            general_calls calls = new general_calls()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                name = model.name,
                contact = model.contact,
                date = CallDate,
                description = model.description,
                follow_up_date = model.follow_up_date,
                call_dureation = model.call_dureation,
                note = model.note,
                call_type = model.call_type,
                is_active = true,
                created_by = listData.user,
                CreatedDate = DateTime.Now,
                year = listData.academic_year
            };
            dbContext.general_calls.Add(calls);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                ModelState.Clear();
                TempData["result"] = "Record Inserted Successfully";

            }


            else
            {
                TempData["result"] = "Something went wrong!";
            }
            return RedirectToAction("Generalcall");
        }

        [HttpGet]
        public ActionResult Generalcall(int? id)
        {
            var Calldetailslist = (from a in dbContext.general_calls.Where(x => x.Gcid != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID && x.is_active==true && x.year==listData.academic_year) select a).ToList();
            ViewBag.Calllist = Calldetailslist;
            if (id != null)
            {
                ViewBag.list = dbContext.general_calls.Where(x => x.Gcid != 0).FirstOrDefault();
                return View();
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditGeneralcall(general_calls model)
        {
            general_calls Editcall = dbContext.general_calls.Find(model.Gcid);
            if (Editcall.Gcid == model.Gcid)
            {
                Editcall.Gcid = model.Gcid;
                Editcall.name = model.name;
                Editcall.contact = model.contact;
                Editcall.date = model.date;
                Editcall.description = model.description;
                Editcall.follow_up_date = model.follow_up_date;
                Editcall.call_dureation = model.call_dureation;
                Editcall.note = model.note;
                Editcall.call_type = model.call_type;
                Editcall.is_active = model.is_active;
                Editcall.college_id = listData.CollegeID;
                Editcall.branch_id = listData.BranchID;
                Editcall.modified_by = listData.user;
                Editcall.ModifiedDate = DateTime.Now;
                Editcall.year = listData.academic_year;

            }
            dbContext.general_calls.AddOrUpdate(Editcall);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Generalcall");
        }



        [HttpGet]
        public async Task<ActionResult> EditGeneralcall(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            general_calls Editcall = await dbContext.general_calls.FindAsync(id);
            if (Editcall == null)
            {
                return HttpNotFound();
            }
            return View(Editcall);

        }

        [HttpGet]
        public ActionResult Deletegeneralcall(int? id)
        {

            if (id != null)
            {
                general_calls gcdelete = dbContext.general_calls.Where(a => a.Gcid == id).FirstOrDefault();
                gcdelete.modified_by = listData.user;
                gcdelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.general_calls.Remove(gcdelete);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Generalcall", "Admin");
        }


        [HttpPost]
        public ActionResult PostalDispatch(dispatch_receive model, string pddate, HttpPostedFileBase FileUpload)
        {
            string typename = "PostalSent";

            var CallDate = DateTime.Parse(pddate);
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/PostalDispatch/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }

                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/PostalDispatch/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }
            try
            {
                if (ModelState.IsValid)
                {
                    dispatch_receive Dis = new dispatch_receive()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        to_title = model.to_title,
                        reference_no = model.reference_no,
                        address = model.address,
                        from_title = model.from_title,
                        date = CallDate,
                        note = model.note,
                        type = typename,
                        image = filesnames,
                        is_active = true,
                        created_by = listData.user,
                        CreatedDate = DateTime.Now,
                        year = listData.academic_year
                    };
                    dbContext.dispatch_receive.Add(Dis);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        TempData["result"] = "Record inserted successfully";
                        ModelState.Clear();
                    }
                }
            }

            catch
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("PostalDispatch");
        }

        [HttpGet]
        public ActionResult PostalDispatch(int? id)
        {
            string typename = "PostalSent";
            var Post_dispatchdetails = (from a in dbContext.dispatch_receive.Where(x => x.Drid != 0 && x.type == typename &&x.college_id==listData.CollegeID && x.branch_id==listData.BranchID && x.is_active==true && x.year==listData.academic_year) select a).ToList();
            ViewBag.PostalDispatch = Post_dispatchdetails;
            if (id != null)
            {
                ViewBag.list = dbContext.dispatch_receive.Where(x => x.Drid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true && x.year == listData.academic_year).FirstOrDefault();
                return View();

            }
            return View();
        }


        [HttpPost]
        public ActionResult EditPostalDispatch(dispatch_receive model, HttpPostedFileBase FileUpload)
        {
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/PostalDispatch/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }


                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/PostalDispatch/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }
            dispatch_receive Editpd = dbContext.dispatch_receive.Find(model.Drid);
            if (Editpd.Drid == model.Drid)
            {
                Editpd.Drid = model.Drid;
                Editpd.to_title = model.to_title;
                Editpd.reference_no = model.reference_no;
                Editpd.address = model.address;
                Editpd.note = model.note;
                Editpd.from_title = model.from_title;
                Editpd.date = model.date;
                Editpd.image = filesnames;
                Editpd.is_active = model.is_active;
                Editpd.college_id = listData.CollegeID;
                Editpd.branch_id = listData.BranchID;
                Editpd.modified_by = listData.user;
                Editpd.ModifiedDate = DateTime.Now;
                Editpd.year = listData.academic_year;
            }
            dbContext.dispatch_receive.AddOrUpdate(Editpd);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("PostalDispatch");
        }



        [HttpGet]
        public async Task<ActionResult> EditPostalDispatch(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dispatch_receive Editpd = await dbContext.dispatch_receive.FindAsync(id);
            if (Editpd == null)
            {
                return HttpNotFound();
            }
            return View(Editpd);

        }


        [HttpGet]
        public ActionResult DeletePostalDispatch(int? id)
        {

            if (id != null)
            {
                dispatch_receive pddelete = dbContext.dispatch_receive.Where(a => a.Drid == id).FirstOrDefault();
                pddelete.modified_by = listData.user;
                pddelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.dispatch_receive.Remove(pddelete);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("PostalDispatch", "Admin");
        }

        [HttpPost]
        public ActionResult PostalReceive(dispatch_receive model, string prdate, HttpPostedFileBase FileUpload)
        {
            string typename = "PostalReceive";
            var CallDate = DateTime.Parse(prdate);
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/PostalReceive/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }


                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/PostalReceive/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }

            try
            {
                if (ModelState.IsValid)
                {
                    dispatch_receive rec = new dispatch_receive()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        to_title = model.to_title,
                        reference_no = model.reference_no,
                        address = model.address,
                        from_title = model.from_title,
                        date = CallDate,
                        type = typename,
                        note = model.note,
                        image = filesnames,
                        is_active = true,
                        CreatedDate = DateTime.Now,
                        created_by = listData.user,
                        year = listData.academic_year
                    };
                    dbContext.dispatch_receive.Add(rec);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        ModelState.Clear();
                        TempData["result"] = "Record inserted successfully";
                    }
                }
            }

            catch
            {
                TempData["result"] = "Something went wrong";
            }
            return RedirectToAction("PostalReceive");
        }

        [HttpGet]
        public ActionResult PostalReceive(int? id)
        {
            string typename = "PostalReceive";
            var Post_reecivedetails = (from a in dbContext.dispatch_receive.Where(x => x.Drid != 0 && x.type == typename && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true && x.year == listData.academic_year) select a).ToList();
            ViewBag.PostalReceive = Post_reecivedetails;
            if (id != null)
            {
                ViewBag.list = dbContext.dispatch_receive.Where(x => x.Drid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true && x.year == listData.academic_year).FirstOrDefault();
                return View();

            }

            return View();

        }


        [HttpPost]
        public ActionResult EditPostalReceive(dispatch_receive model)
        {
            dispatch_receive Editpd = dbContext.dispatch_receive.Find(model.Drid);
            if (Editpd.Drid == model.Drid)
            {
                Editpd.Drid = model.Drid;
                Editpd.from_title = model.from_title;
                Editpd.reference_no = model.reference_no;
                Editpd.address = model.address;
                Editpd.note = model.note;
                Editpd.to_title = model.to_title;
                Editpd.date = model.date;
                Editpd.is_active = model.is_active;
                Editpd.college_id = listData.CollegeID;
                Editpd.branch_id = listData.BranchID;
                Editpd.modified_by = listData.user;
                Editpd.ModifiedDate = DateTime.Now;
                Editpd.year = listData.academic_year;
            }
            dbContext.dispatch_receive.AddOrUpdate(Editpd);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("PostalReceive");
        }



        [HttpGet]
        public async Task<ActionResult> EditPostalReceive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dispatch_receive Editpd = await dbContext.dispatch_receive.FindAsync(id);
            if (Editpd == null)
            {
                return HttpNotFound();
            }
            return View(Editpd);

        }


        [HttpGet]
        public ActionResult DeletePostalReceive(int? id)
        {

            if (id != null)
            {
                dispatch_receive pddelete = dbContext.dispatch_receive.Where(a => a.Drid == id).FirstOrDefault();
                pddelete.modified_by = listData.user;
                pddelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.dispatch_receive.Remove(pddelete);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("PostalReceive", "Admin");
        }


        [HttpPost]
        public ActionResult Complaint(complaint model, string cdate, string FooBarDropDown, HttpPostedFileBase FileUpload)
        {
            Bindstaffname();
            DdlSource();
            DdlActiontaken();
            Ddlcomplainttype();
            var complaintdate = DateTime.Parse(cdate);
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/Complaint/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }

                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Complaint/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }
            complaint com = new complaint()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                complaint_type = model.complaint_type,
                source = model.source,
                name = model.name,
                contact = model.contact,
                email = model.email,
                date = complaintdate,
                description = model.description,
                action_taken = FooBarDropDown,
                assigned = model.assigned,
                note = model.note,
                is_active = true,
                created_dt = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year

            };
            dbContext.complaints.Add(com);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                ModelState.Clear();
                TempData["result"] = "Record inserted successfully";
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("Complaint");
        }

        [HttpGet]
        public ActionResult Complaint(int? id)
        {
            Bindstaffname();
            DdlActiontaken();
            DdlSource();
            Ddlcomplainttype();
            var Complaint = (from a in dbContext.complaints
                             join b in dbContext.complaint_type on a.complaint_type equals b.Ctid.ToString()
                             where a.college_id==listData.CollegeID && b.college_id==listData.CollegeID && a.branch_id==listData.BranchID && b.branch_id==listData.BranchID  && a.is_active==true
                            
                             select new Complaintdata
                             {
                                 Cid = a.Cid,
                                 Complaint_type = b.complaint_type1,
                                 Name = a.name,
                                 Contact = a.contact,
                                 Date = a.date,


                             }).ToList();
            ViewBag.complaintlist = Complaint;


            if (id != null)
            {
                ViewBag.list = dbContext.complaints.Where(a => a.Cid == id).FirstOrDefault();
                ViewBag.pop11 = "1";
                return View();
            }

            return View();

        }


        [HttpPost]
        public ActionResult EditComplaint(complaint model)
        {
            Bindstaffname();
            DdlSource();
            Ddlcomplainttype();
            complaint Editcom = dbContext.complaints.Find(model.Cid);
            if (Editcom.Cid == model.Cid)
            {
                Editcom.Cid = model.Cid;
                Editcom.complaint_type = model.complaint_type;
                Editcom.source = model.source;
                Editcom.name = model.name;
                Editcom.contact = model.contact;
                Editcom.date = model.date;
                Editcom.description = model.description;
                Editcom.action_taken = model.action_taken;
                Editcom.assigned = model.assigned;
                Editcom.note = model.assigned;
                Editcom.is_active = model.is_active;
                Editcom.college_id = listData.CollegeID;
                Editcom.branch_id = listData.BranchID;
                Editcom.modified_by = listData.user;
                Editcom.modified_dt = DateTime.Now;
                Editcom.year = listData.academic_year;
            }
            dbContext.complaints.AddOrUpdate(Editcom);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Complaint");
        }



        [HttpGet]
        public async Task<ActionResult> EditComplaint(int? id)
        {
            Bindstaffname();
            DdlSource();
            Ddlcomplainttype();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            complaint Editcom = await dbContext.complaints.FindAsync(id);
            if (Editcom == null)
            {
                return HttpNotFound();
            }
            return View(Editcom);

        }

        [HttpGet]
        public ActionResult Deletecomplaint(int? id)
        {

            if (id != null)
            {
                complaint Deletecom = dbContext.complaints.Where(a => a.Cid == id).FirstOrDefault();
                Deletecom.modified_by = listData.user;
                Deletecom.modified_dt = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.complaints.Remove(Deletecom);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Complaint", "Admin");
        }


        [HttpPost]
        public ActionResult Complainttype(complaint_type model)
        {
            complaint_type com = new complaint_type()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                complaint_type1 = model.complaint_type1,
                description = model.description,
                is_active = true,
                created_by = listData.user,
                CreatedDate = DateTime.Now
            };
            dbContext.complaint_type.Add(com);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record inserted Successfully";
                ModelState.Clear();

            }
            else
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("Complainttype");

        }

        [HttpGet]
        public ActionResult Complainttype()
        {
            var Complaint = (from a in dbContext.complaint_type.Where(x => x.Ctid != 0&&x.college_id==listData.CollegeID && x.branch_id==listData.BranchID  && x.is_active==true) select a).ToList();
            ViewBag.Complaintlist = Complaint;
            return View();
        }

        [HttpPost]
        public ActionResult EditComplainttype(complaint_type model)
        {

            complaint_type Editcom = dbContext.complaint_type.Find(model.Ctid);
            if (Editcom.Ctid == model.Ctid)
            {
                Editcom.Ctid = model.Ctid;
                Editcom.complaint_type1 = model.complaint_type1;
                Editcom.description = model.description;
                Editcom.is_active = model.is_active;
                Editcom.college_id = listData.CollegeID;
                Editcom.branch_id = listData.BranchID;
                Editcom.modified_by = listData.user;
                Editcom.ModifiedDate = DateTime.Now;
                Editcom.year = listData.academic_year;

            }
            dbContext.complaint_type.AddOrUpdate(Editcom);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Complainttype");
        }



        [HttpGet]
        public async Task<ActionResult> EditComplainttype(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            complaint_type Editcom = await dbContext.complaint_type.FindAsync(id);
            if (Editcom == null)
            {
                return HttpNotFound();
            }
            return View(Editcom);

        }

        [HttpGet]
        public ActionResult DeleteComplainttype(int? id)
        {
            if (id != 0)
            {
                complaint_type ctdelete = dbContext.complaint_type.Where(x => x.Ctid == id).FirstOrDefault();
                ctdelete.modified_by = listData.user;
                ctdelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.complaint_type.Remove(ctdelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Complainttype", "Admin");


        }


        [HttpPost]
        public ActionResult Source(source model)
        {

            source com = new source()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                Sid = model.Sid,
                source1 = model.source1,
                description = model.description,
                is_active = true,
                Created_by = listData.user,
                CreatedDate = DateTime.Now
            };
            dbContext.sources.Add(com);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["message"] = "Record inserted Successfully";
                ModelState.Clear();
                return RedirectToAction("Source");
            }
            else
            {
                TempData["message"] = "Something went wrong";
            }
            return RedirectToAction("Source");

        }

        [HttpGet]
        public ActionResult Source()
        {
            var source = (from a in dbContext.sources.Where(x => x.Sid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID) select a).ToList();
            ViewBag.Sourcelist = source;
            return View();
        }
        
        [HttpPost]
        public ActionResult EditSource(source model)
        {
            source Editsource = dbContext.sources.Find(model.Sid);
            if (Editsource.Sid == model.Sid)
            {
                Editsource.Sid = model.Sid;
                Editsource.source1 = model.source1;
                Editsource.description = model.description;
                Editsource.is_active = model.is_active;
                Editsource.college_id = listData.CollegeID;
                Editsource.branch_id = listData.BranchID;
                Editsource.Modified_by = listData.user;
                Editsource.ModifiedDate = DateTime.Now;
            }
            dbContext.sources.AddOrUpdate(Editsource);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("source");
        }



        [HttpGet]
        public async Task<ActionResult> EditSource(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            source Editsource = await dbContext.sources.FindAsync(id);
            if (Editsource == null)
            {
                return HttpNotFound();
            }
            return View(Editsource);

        }

        [HttpGet]
        public ActionResult Deletesource(int? id)
        {
            if (id != 0)
            {
                source sourcedelete = dbContext.sources.Where(x => x.Sid == id).FirstOrDefault();
                sourcedelete.Modified_by = listData.user;
                sourcedelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.sources.Remove(sourcedelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Source", "Admin");


        }

        [HttpPost]
        public ActionResult Reference(reference_front model)
        {
            reference_front refe = new reference_front()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                reference = model.reference,
                description = model.description,
                is_active = true,
                Created_by = listData.user,
                CraetedDate = DateTime.Now
            };
            dbContext.reference_front.Add(refe);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                ModelState.Clear();
                TempData["result"] = "Record inserted Successfully";
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }
            var Referen = (from a in dbContext.reference_front.Where(x => x.Sid != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID && x.is_active==true) select a).ToList();
            ViewBag.referencelist = Referen;
            return View();
        }

        [HttpGet]
        public ActionResult Reference()
        {
            var Referen = (from a in dbContext.reference_front.Where(x => x.Sid != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true) select a).ToList();
            ViewBag.referencelist = Referen;
            return View();
        }

        [HttpPost]
        public ActionResult EditReference(reference_front model)
        {
            reference_front Editref = dbContext.reference_front.Find(model.Sid);
            if (Editref.Sid == model.Sid)
            {
                Editref.Sid = model.Sid;
                Editref.reference = model.reference;
                Editref.description = model.description;
                Editref.is_active = model.is_active;
                Editref.college_id = listData.CollegeID;
                Editref.branch_id = listData.BranchID;
                Editref.Modified_by = listData.user;
                Editref.ModifiedDate = DateTime.Now;
                Editref.year = listData.academic_year;
            }
            dbContext.reference_front.AddOrUpdate(Editref);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Reference");
        }



        [HttpGet]
        public async Task<ActionResult> EditReference(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reference_front Editref = await dbContext.reference_front.FindAsync(id);
            if (Editref == null)
            {
                return HttpNotFound();
            }
            return View(Editref);

        }

        [HttpGet]
        public ActionResult DeleteReference(int? id)
        {
            if (id != 0)
            {
                reference_front refdelete = dbContext.reference_front.Where(x => x.Sid == id).FirstOrDefault();
                refdelete.Modified_by = listData.user;
                refdelete.ModifiedDate = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.reference_front.Remove(refdelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Reference", "Admin");
        }

        [HttpPost]
        public ActionResult Visitorspurpose(visitorspurpose model)
        {
            visitorspurpose visit = new visitorspurpose()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                purpose = model.purpose,
                description = model.description,
                is_active = model.is_active,
                created_by = listData.user,
                created_dt = DateTime.Now,
                year = listData.academic_year

            };
            dbContext.visitorspurposes.Add(visit);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Inserted Successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }
            return RedirectToAction("Visitorspurpose");
        }

        [HttpGet]
        public ActionResult Visitorspurpose()
        {
            var Visitor = (from a in dbContext.visitorspurposes.Where(x => x.vp_id != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID) select a).ToList();
            ViewBag.visitorlist = Visitor;
            return View();
        }

        [HttpPost]
        public ActionResult EditVisitorspurpose(visitorspurpose model)
        {
            visitorspurpose Editvisitors = dbContext.visitorspurposes.Find(model.vp_id);
            if (Editvisitors.vp_id == model.vp_id)
            {
                Editvisitors.vp_id = model.vp_id;
                Editvisitors.purpose = model.purpose;
                Editvisitors.description = model.description;
                Editvisitors.is_active = model.is_active;
                Editvisitors.college_id = listData.CollegeID;
                Editvisitors.branch_id = listData.BranchID;
                Editvisitors.modified_by = listData.user;
                Editvisitors.modified_dt = DateTime.Now;
                Editvisitors.year = listData.academic_year;

            }
            dbContext.visitorspurposes.AddOrUpdate(Editvisitors);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Visitorspurpose");
        }

        [HttpGet]
        public async Task<ActionResult> EditVisitorspurpose(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            visitorspurpose Editvisitor = await dbContext.visitorspurposes.FindAsync(id);
            if (Editvisitor == null)
            {
                return HttpNotFound();
            }
            return View(Editvisitor);

        }

        [HttpGet]
        public ActionResult DeleteVisitorspurpose(int? id)
        {
            if (id != 0)
            {
                visitorspurpose visitordelete = dbContext.visitorspurposes.Where(x => x.vp_id == id).FirstOrDefault();
                visitordelete.modified_by = listData.user;
                visitordelete.modified_dt = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.visitorspurposes.Remove(visitordelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Visitorspurpose", "Admin");
        }

        [HttpPost]
        public ActionResult Classsubjects(class_subjects model, FormCollection sub)
        {


            string[] res = new string[200];

            res = sub["checkbox"].Split(',');

            foreach (var sb in res)
            {
                string subjectcode = sb;
                var classname = (from a in dbContext.class_subjects.Where(x => x.class_id == model.class_id && x.subject_code == subjectcode && x.year == listData.academic_year) select a).FirstOrDefault();

                if (classname != null)
                {
                    class_subjects cs = new class_subjects()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        class_id = model.class_id,
                        sectionid = model.sectionid,
                        subject_code = sb,
                        subject_name = model.subject_name,
                        is_active = true,
                        created_by = listData.user,
                        created_at = DateTime.Now
                    };


                    dbContext.SaveChanges();

                }
                else
                {
                    class_subjects cs = new class_subjects()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        class_id = model.class_id,
                        sectionid = model.sectionid,
                        subject_code = sb,
                        subject_name = model.subject_name,
                        is_active = true,
                        created_by = listData.user,
                        created_at = DateTime.Now
                    };

                    dbContext.class_subjects.Add(cs);
                    dbContext.SaveChanges();
                }
            }
            Ddlclass();
            Ddlsections();

            var subjectname = (from a in dbContext.subjects.Where(x => x.id != 0) select a).ToList();
            ViewBag.subjects = subjectname;
            return View();
        }

        [HttpGet]
        public ActionResult Classsubjects()
        {
            Ddlclass();
            Ddlsections();
            var subjectname = (from a in dbContext.subjects.Where(x => x.id != 0) select a).ToList();
            ViewBag.subjects = subjectname;
            var subjectlist = (from a in dbContext.class_subjects
                               join b in dbContext.subjects on a.subject_code equals b.code
                               select new Subjectdetails
                               {
                                   SubjectName = b.name,
                                   SubjectCode = a.subject_code

                               }).ToList();
            ViewBag.subjectslist = subjectlist;

            return View();
        }

        [HttpPost]
        public ActionResult Classsub(class_subjects model)
        {
            Ddlclass();
            Ddlsections();
            var subjectlist = (from a in dbContext.class_subjects
                               join b in dbContext.subjects on a.subject_code equals b.code
                               select new Subjectdetails
                               {
                                   SubjectName = b.name,
                                   SubjectCode = a.subject_code

                               }).ToList();
            ViewBag.subjectslist = subjectlist;
            return View();
        }

        [HttpGet]
        public ActionResult Classsub(int? classid, int? sectionid)
        {
            Ddlclass();
            Ddlsections();
            var subjectlist = (from a in dbContext.class_subjects
                               join b in dbContext.subjects on a.subject_code equals b.code
                               select new Subjectdetails
                               {
                                   SubjectName = b.name,
                                   SubjectCode = a.subject_code

                               }).ToList();

            ViewBag.subjectslist = subjectlist;
            return View();
        }

        public ActionResult Staffdashboard()
        {
            var getdt = DateTime.Now;
            DateTime currdate = DateTime.Now;

            int? empid = Convert.ToInt32(dbContext.staffs.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.email == listData.user).Select(k => k.id).FirstOrDefault());

            var Students = (from a in dbContext.staffs
                            join b in dbContext.class_teacher on a.id equals b.staff_id
                            join c in dbContext.students on b.class_id equals c.class_id
                            where (b.staff_id == empid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.section_id == c.section_id && a.year == listData.academic_year)
                            select c.id).Count();
            ViewBag.TotalStudents = Students;

            var sub = (from a in dbContext.staffs
                       join b in dbContext.teacher_subjects on a.id equals b.teacher_id
                       join c in dbContext.subjects on b.subject_id equals c.id
                       join d in dbContext.class_sections on b.class_section_id equals d.id
                       where (a.id == empid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                       select c.id).Count();

            ViewBag.subject = sub;

            int? Altleave = dbContext.staff_leave_details.Where(v => v.staff_id == empid && v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.alloted_leave).FirstOrDefault();
            ViewBag.leaves = Altleave;

            int? takenleave = dbContext.staff_leave_request.Where(v => v.staff_id == empid && v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year && v.status == "approve").Select(a => a.leave_days).DefaultIfEmpty().Sum();
            ViewBag.leaves = takenleave;

            ViewBag.totalleave = Altleave - takenleave;

            var staff = dbContext.staffs.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.email == listData.user && v.year == listData.academic_year).Select(a => a.name).FirstOrDefault();
            ViewBag.stafname = staff;

            int settingYear = 0;
            if (getdt.Month < 6)
            {
                settingYear = getdt.Year - 1;
            }
            ViewBag.CurrentMonth = getdt.Month;
            ViewBag.CurrentYear = settingYear;
            DateTime newdateTime = DateTime.Today;
            string CurrentDate = newdateTime.ToString("yyyy-MM-dd");
            ViewBag.CurrentDate = CurrentDate;

            ViewBag.message = dbContext.Pro_Latest_News(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).OrderByDescending(k => k.id).ToList();
            TempData["personlogo"] = listData.person_logo;
            TempData.Keep();
            return View();
        }


        public JsonResult GetEvents()
        {

            var Booklist = (from a in dbContext.staffs
                            join b in dbContext.staff_leave_request on a.id equals b.staff_id
                            join c in dbContext.leave_types on b.leave_type_id equals c.id
                            where a.email == listData.user && b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year && a.is_active == true
                            select new staffLeavesCalendar
                            {
                                fromdate = b.leave_from.ToString(),
                                todate = b.leave_to.ToString(),
                                leavesdays = b.leave_days,
                                leaveType = c.type

                            }).ToList();



            return new JsonResult { Data = Booklist, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        public JsonResult ShowStaffTimetable()
        {

            var ttStaff = dbContext.Pro_GSTT(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).ToList();
            return new JsonResult { Data = ttStaff, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        public class staffLeavesCalendar
        {
            public string fromdate { get; set; }
            public string todate { get; set; }
            public int? leavesdays { get; set; }
            public string leaveType { get; set; }
        }

        [HttpGet]
        public ActionResult AccountantDashboard()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ReceptionDashboard()
        {
            var getdt = DateTime.Now;
            DateTime currdate = DateTime.Now;

            int Admission_Enquiry = dbContext.enquiries.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.email == listData.user && v.year == listData.academic_year).Select(v => v.Aeid.ToString()).Count();
            ViewBag.admissionenq = Admission_Enquiry;

            int Visitor_Book = dbContext.visitors_book.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(v => v.Vid.ToString()).Count();
            ViewBag.visitors = Visitor_Book;

            var Phone_Call_Log = dbContext.general_calls.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.Gcid.ToString()).Count();
            ViewBag.phncall = Phone_Call_Log;

            var Complain = dbContext.complaints.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.Cid.ToString()).Count();
            ViewBag.complaints = Complain;

            var staff = dbContext.staffs.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.email == listData.user && v.year == listData.academic_year).Select(a => a.name).FirstOrDefault();
            ViewBag.stafname = staff;

            var Postal_Dispatch = dbContext.dispatch_receive.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.Drid.ToString()).Count();
            ViewBag.PostalDisp = Postal_Dispatch;

            var Postal_Receive = dbContext.dispatch_receive.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).Select(a => a.Drid.ToString()).Count();
            ViewBag.PostalDisp = Postal_Receive;

            int settingYear = 0;
            if (getdt.Month < 6)
            {
                settingYear = getdt.Year - 1;
            }
            ViewBag.CurrentMonth = getdt.Month;
            ViewBag.CurrentYear = settingYear;
            DateTime newdateTime = DateTime.Today;
            string CurrentDate = newdateTime.ToString("yyyy-MM-dd");
            ViewBag.CurrentDate = CurrentDate;

            ViewBag.message = dbContext.Pro_Latest_News(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).OrderByDescending(k => k.id).ToList();

            return View();
        }

    }
}
