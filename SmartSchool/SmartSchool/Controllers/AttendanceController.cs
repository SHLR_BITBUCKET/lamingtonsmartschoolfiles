﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using SmartSchool.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity.Migrations;
using System.Web.UI.WebControls;
using SmartSchool.SchoolMasterClasses;
using SmartSchool.Interfaces;

namespace SmartSchool.Controllers
{
    public class AttendanceController : Controller
    {
        // GET: Attendance
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public AttendanceController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AttendanceController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }

        public get_staff_classes gs = new get_staff_classes();
        public ActionResult Index()
        {
            return View();
        }

        private void bind_class_rooms()
        {
            var u_lists = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.clas = u_lists;

        }
        private void bind_month()
        {
            var yr = dbContext.student_attendences.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(y => y.date.Month).ToList();
            ViewBag.year = yr;
        }

        private void bind_year()
        {
            var dt = DateTime.Now.Year;
            List<int> yl = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                yl.Add(dt - i);
            }
            ViewBag.year = yl;
        }

        [HttpGet]
        public ActionResult student_attendance()
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
            return View();
        }



        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            dynamic result = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult student_attendance(student_attendences model, int? class_id, int? section_id)
        {
            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);

            var attendancetype = dbContext.attendence_type.Where(a => a.is_active == "true").OrderBy(a => a.id).ToList();
            ViewBag.attd_list = attendancetype;
            if (class_id != null && section_id != null)
            {
                var result = (from x in dbContext.students.Where(a => a.class_id == class_id && a.section_id == section_id && a.year == listData.academic_year)
                              from y in dbContext.student_attendences
                              .Where(y => y.student_session_id == x.user_id && y.date == model.date && y.year == listData.academic_year).DefaultIfEmpty()
                              select new SchoolMasterClasses.Stud_attendance
                              {
                                  user_id = x.user_id,
                                  AdmissionNumber = x.admission_no,
                                  RollNumber = x.roll_no,
                                  FirstName = x.firstname,
                                  LastName = x.lastname,
                                  atype_id = y.attendence_type_id,
                                  remark = y.remark,
                                  aid = y.id,
                                  class_id = x.class_id,
                                  section_id = x.section_id

                              }).ToList();
                ViewBag.stud_a = result;
                TempData["result"] = result.Count;
            }
            return View();
        }


        [HttpPost]
        public JsonResult SaveAttendance(List<student_attendences> allStudnts, string holiday)
        {
            int result = 0;
            int attendance_type;
            if (allStudnts != null)
            {
                foreach (var student in allStudnts)
                {
                    if (!string.IsNullOrEmpty(holiday))
                    {
                        attendance_type = Convert.ToInt32(holiday);
                    }
                    else
                    {
                        attendance_type = student.attendence_type_id;
                    }
                    if (student.id.Equals(0))
                    {
                        student_attendences obj = new student_attendences
                        {
                            student_session_id = student.student_session_id,
                            date = student.date,
                            attendence_type_id = attendance_type,
                            remark = student.remark,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year,
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID

                        };
                        dbContext.student_attendences.AddOrUpdate(k=> new {k.student_session_id, k.year, k.date, k.college_id, k.branch_id }, obj);
                        result += dbContext.SaveChanges();
                    }
                    else
                    {
                        student_attendences sa = dbContext.student_attendences.Find(student.id);
                        if (sa.id == student.id)
                        {
                            sa.attendence_type_id = attendance_type;
                            sa.remark = student.remark;
                            sa.updated_at = DateTime.Now;
                            sa.modified_by = listData.user;
                            sa.college_id = listData.CollegeID;
                            sa.branch_id = listData.BranchID;
                        }
                        dbContext.student_attendences.AddOrUpdate(sa);
                        result += dbContext.SaveChanges();
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult attendance_by_date(int? class_id, int? Section_id, DateTime? date)
        {
            student_attendance();
            if (class_id != null && Section_id != null)
            {
                var stud_att = (from a in dbContext.students
                                join b in dbContext.student_attendences on a.user_id equals b.student_session_id
                                join k in dbContext.attendence_type on b.attendence_type_id equals k.id
                                where b.date == date && a.class_id == class_id && a.section_id == Section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                select new SchoolMasterClasses.Stud_attendance
                                {
                                    id = a.id,
                                    AdmissionNumber = a.admission_no,
                                    RollNumber = a.roll_no,
                                    FirstName = a.firstname,
                                    LastName = a.lastname,
                                    attendance = k.key_value

                                }).ToList();
                ViewBag.stud_a = stud_att;
              //  ViewBag.sectionid = Section_id;
                ViewBag.AttDate = date;
            }

            return View();
        }



        [HttpGet]
        public ActionResult attendance_report()
        {
            student_attendance();
            bind_class_rooms();
            bind_month();
            bind_year();
            return View();
        }

        [HttpPost]
        public ActionResult attendance_report(int? class_id, int? section_id, int? Month, int? year)
        {
            student_attendance();
            var Mn = Month.Value;
            bind_month();
            bind_year();
            TempData["Message"] = Mn;
            var stu_rep = (from a in dbContext.student_attendences
                           join b in dbContext.students on new { Student_session_id = a.student_session_id } equals new { Student_session_id = b.user_id }
                           where
                             b.class_id == class_id &&
                             b.section_id == section_id &&
                             a.date.Month == Mn && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                           group new { a, b } by new
                           {
                               a.student_session_id,
                               b.firstname,
                               b.lastname
                           } into g
                           select new SchoolMasterClasses.Stud_attendance_cls_sec
                           {
                               user_id = g.Key.student_session_id,
                               FirstName = g.Key.firstname,
                               LastName = g.Key.lastname,
                               present = g.Count(p => (
                               p.a.attendence_type_id == 1 ? (System.Int64?)1 : null) != null),
                               late = g.Count(p => (
                                p.a.attendence_type_id == 2 ? (System.Int64?)1 : null) != null),
                               absent = g.Count(p => (
                               p.a.attendence_type_id == 3 ? (System.Int64?)1 : null) != null),
                               halfday = g.Count(p => (
                               p.a.attendence_type_id == 4 ? (System.Int64?)1 : null) != null),
                               holiday = g.Count(p => (
                               p.a.attendence_type_id == 5 ? (System.Int64?)1 : null) != null)
                           }).ToList();

            ViewBag.count = stu_rep;
            ViewBag.month = Mn;
            ViewBag.selyear = year;
            ViewBag.sectionid = section_id;
            return View();
        }

        public JsonResult get_student_attendance_report(string stud_id)
        {
            var rep = (from a in dbContext.student_attendences
                       join b in dbContext.students on a.student_session_id equals b.user_id
                       join c in dbContext.attendence_type on a.attendence_type_id equals c.id
                       where a.student_session_id.ToString() == stud_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                       select new SchoolMasterClasses.Stud_attendance_cls_sec
                       {
                           id = a.id,
                           user_id = b.user_id,
                           FirstName = b.firstname,
                           LastName = b.lastname,
                           date = a.date,
                           RollNumber = b.roll_no,
                           Year = b.year,
                           remark = a.remark,
                           attendance_type_id = a.attendence_type_id,
                       }).Distinct().ToList();
            ViewBag.stud_report = rep;

            return Json(new { rep }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Attlist(string id, int? month, int? class_id, int? section_id, int? year)
        {
            var Mn = month.Value;
            bind_class_rooms();
            bind_month();
            bind_year();
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.pop11 = "1";
            var pr = (from a in dbContext.student_attendences
                      join b in dbContext.students on a.student_session_id equals b.user_id
                      join c in dbContext.attendence_type on a.attendence_type_id equals c.id
                      where a.student_session_id == id && a.date.Month == Mn && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                      select new SchoolMasterClasses.Stud_attendance_cls_sec
                      {
                          id = a.id,
                          FirstName = b.firstname,
                          LastName = b.lastname,
                          type = c.type,
                          RollNumber = b.roll_no,
                          AdmissionNumber = b.admission_no,
                          Year = b.year,
                          date = a.date

                      }).Distinct().ToList();

            ViewBag.list11 = pr;
            return Json(pr, JsonRequestBehavior.AllowGet);
        }


    }

}
