﻿using SmartSchool.Models;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Data;
using System.Web;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using iTextSharp.text.pdf.draw;
using System.Text.RegularExpressions;
using SmartSchool.SchoolMasterClasses;
using SmartSchool.Interfaces;
using GoogleTranslateFreeApi;
using System.Web.Script.Serialization;
using System.Net;

namespace SmartSchool.Controllers
{
    public class CertificateController : Controller
    {
        // GET: Certificate
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        public get_staff_classes gs = new get_staff_classes();
        //    public int CollegeID = 2015, BranchID = 1;

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public CertificateController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }


        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }



        private void BindCategory()
        {
            List<category> c = dbContext.categories.Where(x => x.id != 0).ToList();
            category sem = new category
            {
                category1 = "----Please select category----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "category1 ", 0);
            ViewBag.viewbagcatlist = selectcat;
        }

        private void BindClass()
        {
            var u_lists = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.viewbagclasslist = u_lists;
        }

        private void BindSection()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0).ToList();
            section sem = new section
            {
                section1 = "----Please Select Section----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            ViewBag.viewbagsectionlist = selectsect;
        }

        private void BindCertificate()
        {
            List<certificate> c = dbContext.certificates.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            certificate sem = new certificate
            {
                certificate_name = "Please Select certificates",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "certificate_name ", 0);
            ViewBag.viewbagcertificatelist = selectsect;
        }

        private void BindIdcard()
        {
            List<id_card> c = dbContext.id_card.Where(x => x.id != 0).ToList();
            id_card sem = new id_card
            {
                title = "Please Select Id Card",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "title ", 0);
            ViewBag.viewbagidcardlist = selectsect;
        }

        public ActionResult certificate()
        {
            BindClass();
            BindSection();

            var studetails = (from a in dbContext.certificates.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;

            // return View(studetails);
            return View();

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult certificate(certificate cm, HttpPostedFileBase ImageFile, FormCollection collection)
        {
            string stufilesnames = "";
            if (ImageFile != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(ImageFile.FileName);
                string dirPath = Server.MapPath("~/Content/Certificate/Background/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }


                ImageFile.SaveAs(Path.Combine(Server.MapPath("~/Content/Certificate/Background/"), savedFileName)); // Save the file
                stufilesnames = savedFileName;
            }
            var studetails = (from a in dbContext.certificates.Where(x => x.certificate_name != null) select a).ToList();
            short adm;
            short.TryParse(collection["stupho"], out adm);


            if (studetails != null)
            {
                certificate schhouse = new certificate
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year=listData.academic_year,
                    certificate_name = cm.certificate_name,
                    left_header = cm.left_header,
                    center_header = cm.center_header,
                    right_header = cm.right_header,
                    left_footer = cm.left_footer,
                    center_footer = cm.center_footer,
                    right_footer = cm.right_footer,
                    header_height = cm.header_height,
                    footer_height = cm.footer_height,
                    content_height = cm.content_height,
                    content_width = cm.content_width,
                    enable_student_image = adm,
                    enable_image_height = cm.enable_image_height,
                    background_image = stufilesnames,
                    certificate_text = cm.certificate_text,
                    status = 1,
                    created_at = DateTime.Now,
                    created_by = listData.user

                };
                dbContext.certificates.Add(schhouse);
            }
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            certificate();
            return View();

        }

        [HttpPost]
        public JsonResult EditCertificate(int values)
        {
            var updte = (from device in dbContext.certificates where device.id == values select device).FirstOrDefault();
            return Json(new
            {
                responstext = updte.id,
                updte.certificate_name,
                updte.left_header,
                updte.center_header,
                updte.right_header,
                updte.left_footer,
                updte.right_footer,
                updte.center_footer,
                updte.background_image,
                updte.header_height,
                updte.content_height,
                updte.footer_height,
                updte.content_width,
                updte.enable_student_image,
                updte.enable_image_height
            });

        }


        public JsonResult EditCertificateupdate(int values, string name, string lefthead, string centerhead, string righthead, string body, string leftfoot, string centerfoot, string rightfoot, int headerheight, int footerheight, int bodyheight, int bodywidth)
        {

            var player = (from a in dbContext.certificates.Where(x => x.college_id == 2015 && x.id == values && x.year == listData.academic_year) select a).FirstOrDefault();

            if (player != null)
            {

                certificate ft = new certificate

                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    certificate_name = player.certificate_name,
                    certificate_text = player.certificate_text,
                    left_header = player.left_header,
                    center_header = player.center_header,
                    right_header = player.right_header,
                    left_footer = player.left_footer,
                    right_footer = player.right_footer,
                    center_footer = player.center_footer,
                    background_image = player.background_image,
                    header_height = player.header_height,
                    content_height = player.content_height,
                    footer_height = player.footer_height,
                    content_width = player.content_width,
                    enable_student_image = player.enable_student_image,
                    enable_image_height = player.enable_image_height,
                    updated_at = DateTime.Now,
                    status = 1,
                    modified_by = listData.user
                };
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult DeleteCertificate(int? id)
        //{
        //    var employee = dbContext.certificates.SingleOrDefault(x => x.id == id);
        //    dbContext.certificates.Remove(employee ?? throw new InvalidOperationException());
        //    dbContext.SaveChanges();
        //    return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        //}
        [HttpPost]
        public JsonResult DeleteCertificate(int? id)
        {

            if (id != null)
            {
                var employee = dbContext.certificates.Where(x => x.id == id).FirstOrDefault();
                //enquiry Aedelete = dbContext.enquiries.Where(a => a.Aeid == id).FirstOrDefault();
                employee.modified_by = listData.user;
                employee.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.certificates.Remove(employee);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult EditStdentdccard(int values)
        {
            var updte = (from device in dbContext.id_card where device.id == values select device).FirstOrDefault();
            return Json(new
            {
                responstext = updte.id,
                updte.background,
                updte.logo,
                updte.sign_image,
                updte.school_name,
                updte.school_address,
                updte.title,
                updte.header_color,
                updte.enable_admission_no,
                updte.enable_student_name,
                updte.enable_class,
                updte.enable_fathers_name,
                updte.enable_mothers_name,
                updte.enable_address,
                updte.enable_dob,
                updte.enable_phone,
                updte.enable_blood_group
            });

        }

        [HttpPost]
        public JsonResult EditStdentidupdate(int values, string name, string addrs, string headclr, string title, short chkadms, short chkstname, short chkcls, short chkfat, short chkmat, short chkstadr, short chkphone, short chkdob, short chkbdgp, HttpPostedFileBase files)
        {

            var player = (from a in dbContext.id_card.Where(x => x.college_id == listData.CollegeID && x.id == values && x.year == listData.academic_year) select a).FirstOrDefault();

            if (player != null)
            {
                id_card ft = new id_card

                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    background = player.background,
                    logo = player.logo,
                    sign_image = player.sign_image,
                    school_name = name,
                    school_address = addrs,
                    title = title,
                    header_color = headclr,
                    enable_admission_no = chkadms,
                    enable_student_name = chkstname,
                    enable_class = chkcls,
                    enable_fathers_name = chkfat,
                    enable_mothers_name = chkmat,
                    enable_address = chkstadr,
                    enable_dob = chkdob,
                    enable_phone = chkphone,
                    enable_blood_group = chkbdgp,
                    status = 1,
                    modified_by = listData.user,
                    
                    

                };


                dbContext.SaveChanges();
            }




            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteStudentIdcard(int? id)
        {
            //var employee = dbContext.id_card.SingleOrDefault(x => x.id == id);
            //dbContext.id_card.Remove(employee ?? throw new InvalidOperationException());
            //dbContext.SaveChanges();


            if (id != null)
            {
                var employee = dbContext.id_card.Where(x => x.id == id).FirstOrDefault();
                //enquiry Aedelete = dbContext.enquiries.Where(a => a.Aeid == id).FirstOrDefault();
                employee.modified_by = listData.user;
                employee.Modified_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.id_card.Remove(employee);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult generatecertificate()
        {
            BindSection();
            BindClass();
            BindCertificate();

            return View();

        }
        [HttpPost]
        public ActionResult generatecertificate(int Class, int Section, int Certificate)
        {
            BindSection();
            BindClass();
            BindCertificate();
            var cer = (from a in dbContext.certificates.Where(x => x.college_id == listData.CollegeID && x.id == Certificate && x.year == listData.academic_year) select a.id).FirstOrDefault();

            var studetails = (from a in dbContext.students
                              join b in dbContext.AddClasses on a.class_id equals b.class_id
                              join c in dbContext.sections on a.section_id equals c.id
                              join d in dbContext.categories on a.category_id equals d.id
                              // join e in dbContext.certificates on a.college_id equals e.college_id
                              where (a.class_id == Class && a.section_id == Section && a.year == listData.academic_year)
                              select new StudentInformation
                              {
                                  Class = b.class_name,
                                  Section = c.section1,
                                  AdmissionNumber = a.admission_no,
                                  FirstName = a.firstname,
                                  FatherName = a.father_name,
                                  DateOfBirth = a.dob,
                                  Gender = a.gender,
                                  Category = d.category1,
                                  MobileNumber = a.mobileno,
                                  id = cer

                              }).ToList();
            ViewBag.studentlist = studetails;
            return View();

        }


        //public FileResult GenerateCertificate(int values)
        //{
        //    //    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
        //    //    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    //    pdfDoc.Open();

        //    //    //Top Heading
        //    //    Chunk chunk = new Chunk("Your Credit Card Statement Report has been Generated", FontFactory.GetFont("Arial", 20, Font.BOLDITALIC, BaseColor.MAGENTA));
        //    //    pdfDoc.Add(chunk);

        //    //    //Horizontal Line
        //    //    Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        //    //    pdfDoc.Add(line);

        //    //    //Table
        //    //    PdfPTable table = new PdfPTable(2);
        //    //    table.WidthPercentage = 100;
        //    //    //0=Left, 1=Centre, 2=Right
        //    //    table.HorizontalAlignment = 0;
        //    //    table.SpacingBefore = 20f;
        //    //    table.SpacingAfter = 30f;

        //    //    //Cell no 1
        //    //    PdfPCell cell = new PdfPCell();
        //    //    cell.Border = 0;
        //    //    //Image image = Image.GetInstance(Server.MapPath("~/Content/Upload/salma.jpg"));
        //    //    //image.ScaleAbsolute(200, 150);
        //    //    //cell.AddElement(image);
        //    //    //table.AddCell(cell);

        //    //    //Cell no 2
        //    //    chunk = new Chunk("Name: Mrs. Salma Mukherji,\nAddress: Latham Village, Latham, New York, US, \nOccupation: Nurse, \nAge: 35 years", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.PINK));
        //    //    cell = new PdfPCell();
        //    //    cell.Border = 0;
        //    //    cell.AddElement(chunk);
        //    //    table.AddCell(cell);

        //    //    //Add table to document
        //    //    pdfDoc.Add(table);

        //    //    //Horizontal Line
        //    //    line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        //    //    pdfDoc.Add(line);

        //    //    //Table
        //    //    table = new PdfPTable(5);
        //    //    table.WidthPercentage = 100;
        //    //    table.HorizontalAlignment = 0;
        //    //    table.SpacingBefore = 20f;
        //    //    table.SpacingAfter = 30f;

        //    //    //Cell
        //    //    cell = new PdfPCell();
        //    //    chunk = new Chunk("This Month's Transactions of your Credit Card");
        //    //    cell.AddElement(chunk);
        //    //    cell.Colspan = 5;
        //    //    cell.BackgroundColor = BaseColor.PINK;
        //    //    table.AddCell(cell);

        //    //    table.AddCell("S.No");
        //    //    table.AddCell("NYC Junction");
        //    //    table.AddCell("Item");
        //    //    table.AddCell("Cost");
        //    //    table.AddCell("Date");

        //    //    table.AddCell("1");
        //    //    table.AddCell("David Food Store");
        //    //    table.AddCell("Fruits & Vegetables");
        //    //    table.AddCell("$100.00");
        //    //    table.AddCell("June 1");

        //    //    table.AddCell("2");
        //    //    table.AddCell("Child Store");
        //    //    table.AddCell("Diaper Pack");
        //    //    table.AddCell("$6.00");
        //    //    table.AddCell("June 9");

        //    //    table.AddCell("3");
        //    //    table.AddCell("Punjabi Restaurant");
        //    //    table.AddCell("Dinner");
        //    //    table.AddCell("$29.00");
        //    //    table.AddCell("June 15");

        //    //    table.AddCell("4");
        //    //    table.AddCell("Wallmart Albany");
        //    //    table.AddCell("Grocery");
        //    //    table.AddCell("$299.50");
        //    //    table.AddCell("June 25");

        //    //    table.AddCell("5");
        //    //    table.AddCell("Singh Drugs");
        //    //    table.AddCell("Back Pain Tablets");
        //    //    table.AddCell("$14.99");
        //    //    table.AddCell("June 28");

        //    //    pdfDoc.Add(table);

        //    //    Paragraph para = new Paragraph();
        //    //    para.Add("Hello Salma,\n\nThank you for being our valuable customer. We hope our letter finds you in the best of health and wealth.\n\nYours Sincerely, \nBank of America");
        //    //    pdfDoc.Add(para);

        //    //    //Horizontal Line
        //    //    line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        //    //    pdfDoc.Add(line);

        //    //    para = new Paragraph();
        //    //    para.Add("This PDF is generated using iTextSharp. You can read the turorial:");
        //    //    para.SpacingBefore = 20f;
        //    //    para.SpacingAfter = 20f;
        //    //    pdfDoc.Add(para);

        //    //    //Creating link
        //    //    chunk = new Chunk("How to Create a Pdf File");
        //    //    chunk.Font = FontFactory.GetFont("Arial", 25, Font.BOLD, BaseColor.RED);
        //    //    chunk.SetAnchor("https://www.yogihosting.com/create-pdf-asp-net-mvc/");
        //    //    pdfDoc.Add(chunk);

        //    //    pdfWriter.CloseStream = false;
        //    //    pdfDoc.Close();
        //    //    Response.Buffer = true;
        //    //    Response.ContentType = "application/pdf";
        //    //    Response.AddHeader("content-disposition", "attachment;filename=Credit-Card-Report.pdf");
        //    //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //    Response.Write(pdfDoc);
        //    //    Response.End();
        //    //    Response.Flush();
        //    //    //Response.Redirect("/Admin/Index");

        //    //    return RedirectToAction("/Admin/Index");


        //    //MemoryStream workStream = new MemoryStream();
        //    //Document document = new Document();
        //    //PdfWriter.GetInstance(document, workStream).CloseStream = false;

        //    //document.Open();
        //    //document.Add(new Paragraph("Hello World"));
        //    //document.Add(new Paragraph(DateTime.Now.ToString()));
        //    //document.Close();

        //    //byte[] byteInfo = workStream.ToArray();
        //    //workStream.Write(byteInfo, 0, byteInfo.Length);
        //    //workStream.Position = 0;
        //    //// Force the pdf document to be displayed in the browser
        //    //string fileName = "Certifcate";
        //    //Response.AppendHeader("Content-Disposition", "inline; filename=" + fileName + ";");

        //    //string path = AppDomain.CurrentDomain.BaseDirectory + "Content/Pdf/";
        //    //return File(path + fileName, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        //    int fid = values;
        //    //  var files = d.GetFiles();
        //    //  string filename = (from a in dbContext.id_card.Wheref.id == fid                               select f.FilePath).First();

        //    CreatePdf();
        //}

        //public async System.Threading.Tasks.Task<string> transkannadaAsync( string value )
        //{
        //    var translator = new GoogleTranslator();

        //    Language from = Language.Auto;
        //    Language to = GoogleTranslator.GetLanguageByName("Kannada");

        //    TranslationResult result = await translator.TranslateLiteAsync("Hello. How are you?", from, to);

        //    string s=result.ToString();
        //    return s;
        //}


        public string TranslateGoogle(string text, string fromCulture, string toCulture)
        {
            fromCulture = fromCulture.ToLower();
            toCulture = toCulture.ToLower();

            // normalize the culture in case something like en-us was passed 
            // retrieve only en since Google doesn't support sub-locales
            string[] tokens = fromCulture.Split('-');
            if (tokens.Length > 1)
                fromCulture = tokens[0];

            // normalize ToCulture
            tokens = toCulture.Split('-');
            if (tokens.Length > 1)
                toCulture = tokens[0];

            string url = string.Format(@"http://translate.google.com/translate_a/t?client=j&text={0}&hl=en&sl={1}&tl={2}", HttpUtility.UrlEncode(text), fromCulture, toCulture);

            // Retrieve Translation with HTTP GET call
            string html = null;
            try
            {
                WebClient web = new WebClient();
                // MUST add a known browser user agent or else response encoding doen't return UTF-8 (WTF Google?)
                web.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0");
                web.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");

                // Make sure we have response encoding to UTF-8
                web.Encoding = Encoding.UTF8;
                html = web.DownloadString(url);
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
                return null;
            }
            string result = html.Replace("\"", string.Empty).Trim();
            if (string.IsNullOrEmpty(result))
            {
                Exception ex = new Exception();
                ExceptionLogging.LogException(ex);
                return null;
            }
            string unicodeString = "ಚಂದನ್";

            // Create two different encodings.
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte array.
            byte[] unicodeBytes = unicode.GetBytes(unicodeString);

            // Perform the conversion from one encoding to the other.
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string asciiString = new string(asciiChars);

            return result;
        }


        private string WriteInput(string input)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            byte[] asciiArray = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, byteArray);
            string finalString = Encoding.ASCII.GetString(asciiArray);

            string sBuffer;
            sBuffer = Encoding.Unicode.GetString(asciiArray);

            //  return sBuffer
           UTF8Encoding utf8 = new UTF8Encoding();

            // A Unicode string with two characters outside an 8-bit code range.
            String unicodeString = input;
            Console.WriteLine("Original string:");
            Console.WriteLine(unicodeString);

            // Encode the string.
            Byte[] encodedBytes = utf8.GetBytes(unicodeString);
            Console.WriteLine();
            Console.WriteLine("Encoded bytes:");
            for (int ctr = 0; ctr < encodedBytes.Length; ctr++)
            {
                Console.Write("{0:X2} ", encodedBytes[ctr]);
                if ((ctr + 1) % 25 == 0)
                    Console.WriteLine();
            }
            Console.WriteLine();

            // Decode bytes back to string.
            String decodedString = Encoding.UTF8.GetString(encodedBytes);


            return decodedString;
        }
        public ActionResult GenerateCertificatenew(int values, int ADM)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            PdfPTable tableLayout = new PdfPTable(5);
            doc.SetMargins(0f, 0f, 0f, 0f);

            var studetails = (from a in dbContext.certificates.Where(x => x.id == values && x.year == listData.academic_year) select a).ToList();
            ViewBag.studentlist = studetails;
            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();


            var bannerdetails = (from a in dbContext.sch_settings.Where(x => x.college_id == listData.CollegeID && x.branch__id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a.image).FirstOrDefault();
            foreach (var item in ViewBag.studentlist)
            {
                string strAttachment = Server.MapPath("~/Content/Logo/" + bannerdetails);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(strAttachment);
                jpg.ScaleToFit(280, 750);
                jpg.SetAbsolutePosition((PageSize.A4.Width - jpg.ScaledWidth) / 2, (PageSize.A4.Height - jpg.ScaledHeight) / (float)1.01);
                jpg.Alignment = iTextSharp.text.Image.UNDERLYING;
                doc.Add(jpg);
                // BaseFont bfArialUniCode = BaseFont.CreateFont(@"D:\ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont bf1 = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\Fonts\Nudi 01 E.TTF", BaseFont.IDENTITY_H, true);

                //FontFactory.Register(Path.Combine(System.Environment.GetEnvironmentVariable("windir"), "Fonts") + "\\tunga.TTF", "Arial Unicode MS");

                FontFactory.GetFont("tunga", 50, iTextSharp.text.BaseColor.BLUE);
                BaseColor black = new BaseColor(00, 00, 00);
                BaseColor NavyBlue = new BaseColor(00, 0, 128);
                // BaseColor TitleColor = new BaseColor(102, 204, 255);
                BaseColor TitleColor = new BaseColor(79, 129, 189);
                BaseColor liteblue = new BaseColor(48, 84, 129);
                BaseColor liteRed = new BaseColor(255, 0, 0);
                iTextSharp.text.Font font = FontFactory.GetFont("vardana", 16, iTextSharp.text.Font.BOLD, NavyBlue);
                iTextSharp.text.Font font1 = FontFactory.GetFont("vardana", 10, iTextSharp.text.Font.BOLD, black);
                iTextSharp.text.Font font2 = FontFactory.GetFont("vardana", 12, iTextSharp.text.Font.BOLD, black);
                //iTextSharp.text.Font font2 = new iTextSharp.text.Font(bf1, 10f, iTextSharp.text.Font.BOLD, black);
                iTextSharp.text.Font font3 = new iTextSharp.text.Font(bf1, 10f, iTextSharp.text.Font.BOLD, black);

                Paragraph paragraph = new Paragraph("");
                paragraph.ExtraParagraphSpace = 100;
                Paragraph chunk = new Paragraph("\n\n");
                doc.Add(chunk);
                doc.Add(chunk);
               // doc.Add(chunk);
                //doc.Add(chunk);
                //doc.Add(chunk); doc.Add(chunk);
                doc.Add(paragraph);
                Paragraph chunk1 = new Paragraph("\n\n\n");
                PdfPTable table = new PdfPTable(3);

                table.DefaultCell.Border = Rectangle.NO_BORDER;
                Paragraph chunk2 = new Paragraph("\n");

                PdfPCell cell1 = new PdfPCell();
                cell1.Border = Rectangle.NO_BORDER;

                cell1.Colspan = 3;
                cell1.AddElement(chunk2);


                Paragraph lh = new Paragraph();
                lh = new Paragraph(item.left_header, font1);
                lh.SpacingBefore = 1000;
             //   lh.Alignment = Element.ALIGN_LEFT;
                //  table.SpacingBefore = 200;
                table.AddCell(lh);
                Paragraph ch = new Paragraph();
                ch = new Paragraph(item.center_header, font);
                ch.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ch);

                Paragraph rh = new Paragraph();
                rh = new Paragraph(item.right_header, font1);
                rh.Alignment = Element.ALIGN_RIGHT;
                table.AddCell(rh);
                Paragraph ct = new Paragraph();
                PdfPCell cell = new PdfPCell();
                cell.Border = Rectangle.NO_BORDER;

                cell.Colspan = 3;
                cell.AddElement(chunk1);
                table.AddCell(cell);

                var pattern = @"\[(.*?)\]";
                var matches = Regex.Matches(item.certificate_text, pattern);

                var ss = (from a in dbContext.students
                          join b in dbContext.AddClasses on a.class_id equals b.class_id
                          join c in dbContext.sections on a.section_id equals c.id
                          join d in dbContext.categories on a.category_id equals d.id
                          join e in dbContext.castemasters on a.cast equals e.id
                          where (a.admission_no == ADM && a.year == listData.academic_year)
                          select new StudentInformation
                          {
                              Class = b.class_name,
                              Section = c.section1,
                              AdmissionNumber = a.admission_no,
                              FirstName = a.firstname,
                              FatherName = a.father_name,

                              MotherName = a.mother_name,
                              PermanentAddress = a.permanent_address,
                              DateOfBirth = a.dob,
                              Gender = a.gender,
                              Category = d.category1,
                              MobileNumber = a.mobileno,
                              CurrentAddress = a.current_address,
                              GuardianName = a.guardian_name,
                              RollNumber = a.roll_no,
                              Date = a.created_date.ToString(),
                              AdmissionDate = a.admission_date,
                              Email = a.email,
                              Caste = e.id.ToString(),
                              PreviousSchoolDetails = a.previous_school
                          }).ToList();
                ViewBag.stucertificate = ss;

                string mystr = item.certificate_text;
                foreach (var m in ViewBag.stucertificate)
                {
                    List<string> myvariables = new List<string>();

                    string S = "HI";
                    PdfPCell txtcell = new PdfPCell();
                    table.DefaultCell.Border = Rectangle.NO_BORDER;

                    txtcell.Border = Rectangle.NO_BORDER;

                    if (mystr.Contains("[name]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.FirstName);

                        txtcell = new PdfPCell(new Phrase("1.Name of Pupil", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.FirstName, font2));
                        txtcell.Border = 0;
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);

                    }

                    if (mystr.Contains("[father_name]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.FatherName);
                        txtcell = new PdfPCell(new Phrase("2.Father Name", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell); 
                        txtcell = new PdfPCell(new Phrase(": " + @m.FatherName, font2));
                        txtcell.Border = 0;
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }

                    if (mystr.Contains("[mother_name]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.MotherName);

                        txtcell = new PdfPCell(new Phrase("3.Mother Name", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.MotherName, font2));
                        txtcell.Colspan = 2;
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }

                    txtcell = new PdfPCell(new Phrase("4.Nationality", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + "Indian", font2));
                    txtcell.Colspan = 2;
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    if (mystr.Contains("[cast]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.Caste);
                        txtcell = new PdfPCell(new Phrase("5.Caste and Sub caste", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.Caste + " - " + @m.Category, font2));
                        txtcell.Border = 0;
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    if (mystr.Contains("[cast]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.Caste);
                        txtcell = new PdfPCell(new Phrase("6.Place On Birth", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.PermanentAddress, font2));
                        txtcell.Border = 0;
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    if (mystr.Contains("[dob]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.DateOfBirth);
                        txtcell = new PdfPCell(new Phrase("7.Date Of Birth", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.DateOfBirth, font2));
                        txtcell.Colspan = 2;
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }

                    if (mystr.Contains("[previous_school]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.PreviousSchoolDetails);
                        txtcell = new PdfPCell(new Phrase("8.Last School Attended", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.PreviousSchoolDetails, font2));
                        txtcell.Colspan = 2;
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);

                    }
                    if (mystr.Contains("[admission_date]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.AdmissionDate);
                        txtcell = new PdfPCell(new Phrase("9.Date of Admission", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.AdmissionDate, font2));
                        txtcell.Colspan = 2;
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    txtcell = new PdfPCell(new Phrase("10.Progress", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);
                    txtcell = new PdfPCell(new Phrase("11.Conduct", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    txtcell = new PdfPCell(new Phrase("12.Date Of leaving school", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    if (mystr.Contains("[class]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.Class);
                        txtcell = new PdfPCell(new Phrase("13.Class and Section", font2));
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(": " + @m.Class + " - " + @m.Section, font2));
                        txtcell.Colspan = 2;
                        txtcell.Border = 0;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }

                    txtcell = new PdfPCell(new Phrase("14.Reason of leaving school", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    txtcell = new PdfPCell(new Phrase("15.Remarks", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    txtcell = new PdfPCell(new Phrase("16.Attendance", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    txtcell = new PdfPCell(new Phrase("17.Medium of Instruction", font2));
                    txtcell.Border = 0;
                    table.AddCell(txtcell);
                    txtcell = new PdfPCell(new Phrase(": " + " ", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 2;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);


                    txtcell = new PdfPCell(new Phrase("Certified the above information is in accordance with the school register", font2));
                    txtcell.Border = 0;
                    txtcell.Colspan = 3;
                    table.AddCell(txtcell);
                    table.AddCell(cell1);

                    if (mystr.Contains("[present_address]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.CurrentAddress);
                        txtcell = new PdfPCell(new Phrase("CurrentAddress:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.CurrentAddress, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    if (mystr.Contains("[guardian]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.GuardianName);
                        txtcell = new PdfPCell(new Phrase("Guardian Name:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.GuardianName, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    if (mystr.Contains("[created_at]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.DATE);
                        txtcell = new PdfPCell(new Phrase("DATE:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.DATE, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }


                    if (mystr.Contains("[roll_no]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.RollNumber);
                        txtcell = new PdfPCell(new Phrase("RollNumber:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.RollNumber, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }

                    if (mystr.Contains("[gender]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.Gender);
                        txtcell = new PdfPCell(new Phrase("Gender:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.Gender, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    
                    if (mystr.Contains("[email]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.Email);
                        txtcell = new PdfPCell(new Phrase("Mother Name:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.MotherName, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    if (mystr.Contains("[phone]"))
                    {
                        myvariables.Add(mystr.Split('[', ']')[1]);
                        mystr = mystr.Replace("[" + mystr.Split('[', ']')[1] + "]", @m.MobileNumber);
                        txtcell = new PdfPCell(new Phrase("Mother Name:", font2));
                        table.AddCell(txtcell);
                        txtcell = new PdfPCell(new Phrase(@m.MobileNumber, font2));
                        txtcell.Colspan = 2;
                        table.AddCell(txtcell);
                        table.AddCell(cell1);
                    }
                    Paragraph ck = new Paragraph("/n");
                    string ck1 = ck.ToString();

                    table.AddCell(cell1);

                }

                Paragraph lf = new Paragraph();
                lf = new Paragraph(item.left_footer);
                lf.Alignment = Element.ALIGN_LEFT;
                table.AddCell(lf);

                Paragraph cf = new Paragraph();
                cf = new Paragraph(item.center_footer);
                cf.Alignment = Element.ALIGN_CENTER;
                table.AddCell(cf);

                Paragraph rf = new Paragraph();
                rf = new Paragraph(item.right_footer);
                rf.Alignment = Element.ALIGN_RIGHT;
                table.AddCell(rf);

                doc.Add(table);


            }


            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            var fName = string.Format("Certificate.pdf", DateTime.Now.ToString("s"));
            Session[fName] = workStream;
            return Json(new { success = true, fName }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult DownloadFile(string fName)
        {
            var ms = Session[fName] as MemoryStream;
            if (ms == null)
                return new EmptyResult();
            Session[fName] = null;
            return File(ms, "application/pdf", fName);
        }




        public ActionResult GenerateIdcardnew(int values, int admissno)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            //  Document doc = new Document();

            Document doc = new Document(iTextSharp.text.PageSize.A7, 5, 5, 5, 5);
            doc.SetMargins(0f, 0f, 0f, 0f);

            PdfPTable tableLayout = new PdfPTable(5);
            doc.SetMargins(0f, 0f, 0f, 0f);

            string strAttachment = Server.MapPath("~/Content/Pdf/" + strPDFFileName);

            var studetails = (
     from a in dbContext.students

     join b in dbContext.id_card on a.college_id equals b.college_id
     join c in dbContext.AddClasses on a.class_id equals c.class_id
     join d in dbContext.sections on a.section_id equals d.id

     where (a.college_id == listData.CollegeID && b.id == values && a.admission_no == admissno && a.year == listData.academic_year)
     select new { a, b, c, d }
            ).ToList();
            ViewBag.studentlist = studetails;
            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();
            var bannerdetails = (from a in dbContext.sch_settings.Where(x => x.college_id == listData.CollegeID && x.branch__id == listData.BranchID && x.year == listData.academic_year) select a.app_logo).FirstOrDefault();

            foreach (var item in ViewBag.studentlist)
            {

                //string imageFilePath = System.Web.Hosting.HostingEnvironment.MapPath(item.b.logo);

                string imageFilePath = Server.MapPath("~/Content/Logo/" + bannerdetails);

                BaseColor NavyBlue = new BaseColor(00, 0, 128);
                BaseColor black = new BaseColor(000, 000, 000);

                BaseFont bf = BaseFont.CreateFont("C:/Windows/Fonts/Calibri.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                iTextSharp.text.Font NORMAL6 = new iTextSharp.text.Font(bf, 12f, iTextSharp.text.Font.BOLD, NavyBlue);

                iTextSharp.text.Font NORMAL = new iTextSharp.text.Font(bf, 12f, iTextSharp.text.Font.BOLD, black);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);

                jpg.Alignment = iTextSharp.text.Image.UNDERLYING;
                jpg.ScaleAbsolute(200f, 40f);

                PdfPTable table = new PdfPTable(2);
                table.WidthPercentage = 100;
                table.TotalWidth = 20;
                table.PaddingTop = 10;
                table.HorizontalAlignment = Element.ALIGN_LEFT;
                table.DefaultCell.Border = Rectangle.NO_BORDER;
                Paragraph lh = new Paragraph();
                PdfPCell cell = new PdfPCell();
                cell.Border = Rectangle.NO_BORDER;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;

                doc.Add(jpg);

                Paragraph paragraph = new Paragraph("");
                paragraph.ExtraParagraphSpace = 100;
                Paragraph chunk = new Paragraph("\n\n");
                doc.Add(chunk);
               
                doc.Add(paragraph);
                Paragraph chunk1 = new Paragraph("\n\n\n");

                LineSeparator line = new LineSeparator(NORMAL6);
                
                cell.Colspan = 2;
                cell.AddElement(line);

                cell.AddElement(Chunk.NEWLINE);          
                table.AddCell(cell);

                Paragraph ct = new Paragraph();
                ct = new Paragraph("Name", NORMAL);

                ct.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ct);


                Paragraph lf = new Paragraph();
                lf = new Paragraph(": " + item.a.firstname, NORMAL);
                lf.Alignment = Element.ALIGN_LEFT;

                table.AddCell(lf);

                Paragraph cf = new Paragraph();
                cf = new Paragraph("Admission No", NORMAL);
                cf.Alignment = Element.ALIGN_CENTER;
                table.AddCell(cf);

                Paragraph rf = new Paragraph();
                rf = new Paragraph(": " + item.a.admission_no);
                rf.Alignment = Element.ALIGN_CENTER;
                table.AddCell(rf);


                Paragraph st = new Paragraph();
                st = new Paragraph("STS NO", NORMAL);
                st.Alignment = Element.ALIGN_CENTER;
                table.AddCell(st);

                Paragraph stf = new Paragraph();
                stf = new Paragraph(": " + item.a.sts_number);
                stf.Alignment = Element.ALIGN_CENTER;
                table.AddCell(stf);


                Paragraph cl = new Paragraph();
                cl = new Paragraph("Class", NORMAL);
                cl.Alignment = Element.ALIGN_CENTER;
                table.AddCell(cl);
                Paragraph cl1 = new Paragraph();
                cl1 = new Paragraph(": " + item.c.class_name, NORMAL);
                cl.Alignment = Element.ALIGN_CENTER;
                table.AddCell(cl1);

                Paragraph fn = new Paragraph();
                fn = new Paragraph("Father Name", NORMAL);
                fn.Alignment = Element.ALIGN_CENTER;
                table.AddCell(fn);
                Paragraph fn1 = new Paragraph();
                if (item.a.father_name != null)
                {
                    fn1 = new Paragraph(": " + item.a.father_name, NORMAL);
                }
                else
                {
                    fn1 = new Paragraph("");
                }
                table.AddCell(fn1);


                Paragraph ad = new Paragraph();
                ad = new Paragraph("Address", NORMAL);
                ad.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ad);
                Paragraph ad1 = new Paragraph();
                if (item.a.father_name != null)
                {
                    ad1 = new Paragraph(": " + item.a.current_address, NORMAL);
                }
                else
                {
                    ad1 = new Paragraph("");
                }
                ad1.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ad1);




                Paragraph ph = new Paragraph();
                ph = new Paragraph("Phone", NORMAL);
                ph.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ph);
                Paragraph ph1 = new Paragraph();
                ph1 = new Paragraph(": " + item.a.mobileno);
                ph1.Alignment = Element.ALIGN_CENTER;
                table.AddCell(ph1);

                Paragraph db = new Paragraph();
                db = new Paragraph("D.O.B", NORMAL);
                db.Alignment = Element.ALIGN_CENTER;
                table.AddCell(db);
                Paragraph db1 = new Paragraph();
                db1 = new Paragraph(": " + item.a.dob, NORMAL);
                db1.Alignment = Element.ALIGN_CENTER;
                table.AddCell(db1);

                doc.Add(table);
            }


            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            var fName = string.Format("File-{0}.pdf", DateTime.Now.ToString("s"));
            Session[fName] = workStream;
            return Json(new { success = true, fName }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult DownloadFileIdcard(string fName)
        {
            var ms = Session[fName] as MemoryStream;
            if (ms == null)
                return new EmptyResult();
            Session[fName] = null;
            return File(ms, "application/pdf", fName);
        }





        [HttpGet]
        public ActionResult studentidcard()
        {
            //var studetails = (from a in dbContext.id_card.Where(x => x.college_id != 0) select a).ToList();
            //ViewBag.studentlist = studetails;
            return View();
        }
        public JsonResult module(string favorite)
        {
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult studentidcard(id_card cm, FormCollection collection, IEnumerable<HttpPostedFileBase> ImageFile)
        {
            string bag = "";
            string logo = ""; string sign = "";
            if (ImageFile != null)
            {
                string results = collection["checkboxs"];

                int i = 0;
                foreach (var file in ImageFile)
                {
                    if (file.ContentLength > 0)
                    {
                        if (i == 0)
                        {
                            string filename = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(file.FileName);
                            var path = Path.Combine(Server.MapPath("~/Content/Certificate/Background"), filename);

                            bag = "~/ Content / Certificate / Background/" + filename;

                        }
                        if (i == 1)
                        {
                            string filename = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(file.FileName);
                            var path = Path.Combine(Server.MapPath("~/Content/Certificate/logo"), filename);

                            logo = "~/Content/Certificate/logo/" + filename;

                        }
                        if (i == 2)
                        {
                            string filename = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(file.FileName);
                            var path = Path.Combine(Server.MapPath("~/Content/Certificate/Studentsign"), filename);

                            sign = "~/Content/Certificate/Studentsign/" + filename;

                        }

                    }
                    i++;
                }

                var studetails = (from a in dbContext.id_card.Where(x => x.school_name  != null) select a).ToList();
                string ss = collection["chkstname"];

                short adm;
                short.TryParse(collection["chkadmn"], out adm);



                short stunam;
                short.TryParse(collection["chkstname"], out stunam);

                short cls;
                short.TryParse(collection["chkclass"], out cls);

                short fat;
                short.TryParse(collection["chkfatnam"], out fat);

                short mat;
                short.TryParse(collection["chkmtnam"], out mat);

                short addr;
                short.TryParse(collection["chkstuaddr"], out addr);

                short pho;
                short.TryParse(collection["chkphn"], out pho);

                short dob;
                short.TryParse(collection["chkdob"], out dob);

                short bgp;
                short.TryParse(collection["chkbg"], out bgp);
                if (studetails != null)
                {
                    id_card schhouse = new id_card
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        background = bag,
                        logo = logo,
                        sign_image = sign,
                        school_name = cm.school_name,
                        school_address = cm.school_address,
                        title = cm.title,
                        header_color = cm.header_color,
                        enable_admission_no = adm,
                        enable_student_name = stunam,
                        enable_class = cls,
                        enable_fathers_name = fat,
                        enable_mothers_name = mat,
                        enable_address = addr,
                        enable_phone = pho,
                        enable_dob = dob,
                        status = 1,
                        enable_blood_group = bgp,
                        created_by = "Smartschool",

                    };
                    dbContext.id_card.Add(schhouse);
                }
            }
            int c = dbContext.SaveChanges();

            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            studentidcard();
            return View();

        }
        public ActionResult Generateidcard()
        {
            BindClass();
            BindIdcard();

            var studetails = (from a in dbContext.students
                              join b in dbContext.id_card on a.college_id equals b.college_id
                              join c in dbContext.AddClasses on a.class_id equals c.class_id
                              join d in dbContext.categories on a.category_id equals d.id

                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year)

                              select new StudentInformation
                              {
                                  AdmissionNumber = a.admission_no,
                                  FirstName = a.firstname,
                                  Class = c.class_name,
                                  FatherName = a.father_name,
                                  DateOfBirth = a.dob,
                                  Gender = a.gender,
                                  Category = d.category1,
                                  MobileNumber = a.mobileno,
                                  Idcard = b.id.ToString(),



                              }).ToList();
            ViewBag.studentlist = studetails;
            return View();

        }

        [HttpPost]
        public ActionResult Generateidcard(int Class, int Section, int Idcard)
        {

            BindClass();
            BindIdcard();

            var studetails = (from a in dbContext.students

                              join b in dbContext.id_card on a.college_id equals b.college_id
                              join c in dbContext.AddClasses on a.class_id equals c.class_id
                              join d in dbContext.categories on a.category_id equals d.id
                              where (a.college_id == listData.CollegeID && b.id == Idcard && a.class_id == Class && a.section_id == Section && a.year == listData.academic_year)

                              select new StudentInformation
                              {
                                  AdmissionNumber = a.admission_no,
                                  FirstName = a.firstname,
                                  Class = c.class_name,
                                  FatherName = a.father_name,
                                  DateOfBirth = a.dob,
                                  Gender = a.gender,
                                  Category = d.category1,
                                  MobileNumber = a.mobileno,
                                  Idcard = b.id.ToString(),



                              }).ToList();
            ViewBag.studentlist = studetails;
            return View();

        }

    }
}