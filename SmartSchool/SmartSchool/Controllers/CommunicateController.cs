﻿using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;

namespace SmartSchool.Controllers

{
    public class CommunicateController : Controller
    {
        private readonly ISessionStore _UsesessionStore;
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        public SmsTextLocalClass stl = new SmsTextLocalClass();
        public get_session_data1 listData;
        public CommunicateController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in CommunicateController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
     
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult notice_board()
        {
            IQueryable<send_notification> isn = dbContext.send_notification;
            ViewBag.isn = (from isnval in isn where isnval.college_id == listData.CollegeID && isnval.branch_id == listData.BranchID && isnval.year == listData.academic_year && isnval.is_active != false select isnval).ToList();
            return View();
        }

        [HttpGet]
        public ActionResult notice_board_delete(int? id, string edit)
        {
            notice_board();
            if (edit == null)
            {
                int c = 0;
                using (var ctx = new SmartSchoolHDEntities())
                {
                    var x = (from y in ctx.send_notification
                             where y.id == id && y.year == listData.academic_year
                             select y).FirstOrDefault();
                    if (x != null)
                    {
                        x.modified_by = listData.user;
                        x.updated_at = DateTime.Now;
                        ctx.SaveChanges();
                        ctx.send_notification.Remove(x);
                        c = ctx.SaveChanges();
                    }
                    if (c > 0)
                    {
                        TempData["message"] = "Record Deleted Successfully..!";
                        TempData.Keep();
                    }
                }
                return RedirectToAction("notice_board");
            }
            else
            {
                send_notification sn = dbContext.send_notification.Single(k => k.id == id);
                return View("send_message", sn);
            }
        }

        public ActionResult post_new_message()
        {
            return View();
        }

        [HttpGet]     
        public ActionResult send_message()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult save_send_message(string[] get_checkedval, string content, string email_subject, send_notification model, string publish_date, string notice_date, int? notification_id)
        {
            int c = 0;
            if (notification_id == 0 || notification_id == null)
            {
                send_notification el = new send_notification()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    created_at = DateTime.Now,
                    title = email_subject,
                    date = Convert.ToDateTime(notice_date),
                    publish_date = Convert.ToDateTime(publish_date),
                    message = content,
                    created_by = listData.user,
                    is_active = true,
                    created_id = 2015,
                    visible_parent = false,
                    visible_staff = false,
                    visible_student = false,
                    year = listData.academic_year
                };
                foreach (var citems in get_checkedval)
                {
                    if (citems == "Student")
                    {
                        el.visible_student = true;
                    }
                    else if (citems == "Parent")
                    {
                        el.visible_parent = true;
                    }
                    else if (citems == "Staff")
                    {
                        el.visible_staff = true;
                    }
                }
                dbContext.send_notification.Add(el);
                c = dbContext.SaveChanges();
            }

            else
            {
                IQueryable<send_notification> isn = dbContext.send_notification;
                var result = isn.SingleOrDefault(b => b.id == notification_id);
                if (result != null)
                {
                    result.title = email_subject;
                    result.publish_date = Convert.ToDateTime(publish_date);
                    result.date = Convert.ToDateTime(notice_date);
                    result.message = content;
                    foreach (var citems in get_checkedval)
                    {
                        if (citems == "Student")
                        {
                            result.visible_student = true;
                        }
                        else if (citems == "Parent")
                        {
                            result.visible_parent = true;
                        }
                        else if (citems == "Staff")
                        {
                            result.visible_staff = true;
                        }
                    }
                    c = dbContext.SaveChanges();
                }
            }
            if (c > 0)
            {
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }


        public class getcharge
        {
            public int? id { get; set; }
            public string message { get; set; }
            public string Code { get; set; }
        }

        public JsonResult GetCharge(int id)
        {
            var mes = dbContext.send_notification.Where(x => x.id == id ).Select(a => a.message).FirstOrDefault();
            return Json(mes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult send_email_sms()
        {
           // ViewBag.classes = dbContext.AddClasses.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
            ViewBag.list = new SelectList(dbContext.send_notification, "id", "title", "0");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult send_email_sms(send_notification model, string[] get_checkedval, string content, string[] get_id, string[] dropdwn, string[] multi_select, string email_subject, string[] get_id_sms, string activetab)
        {
            IQueryable<string> staffs = dbContext.AspNetRoles.Where(k => k.Name != "Student" && k.Name != "Parent").Select(k => k.Name);
            string user = null;
            dynamic return_val = null;
            if (get_checkedval != null)
            {
                foreach (var citems in get_checkedval)
                {
                    if (staffs.Contains(citems))
                    {
                        user = "Staff";
                    }
                    switch (user == "Staff" ? "Staff" : citems)
                    {
                        case "Student":
                            var allstudents = cmc.get_all_student(listData.CollegeID, listData.BranchID, listData.academic_year, null, null);
                            foreach (var als in allstudents)
                            {
                                if (als.Count != 0)
                                {
                                    foreach (var alsl in als)
                                    {
                                        return_val = cmc.send_mail(alsl.Email, email_subject, content);
                                        TempData["Students"] += alsl.AdmissionNumber + ",";
                                        return_val = return_val == false ? false : true;
                                        if (return_val == false)
                                        {
                                            break;
                                        }
                                    }
                                    if (return_val == true)
                                    {
                                        return_val = "Email Sent Successfully..!";
                                        save_data_tomessages_table(email_subject, content, TempData["Students"], activetab);
                                        TempData.Remove("Students");
                                    }
                                }
                                else
                                {
                                    return_val = "No Students Found";
                                }
                            }
                            break;

                        case "Parent":
                            var allparent = cmc.get_all_parent(listData.CollegeID, listData.BranchID, listData.academic_year, null, null);
                            foreach (var alsp in allparent)
                            {
                                if (alsp.Count != 0)
                                {
                                    foreach (var alpl in alsp)
                                    {
                                        return_val = cmc.send_mail(alpl.GuardianEmail, email_subject, content);
                                        TempData["Parent"] += alpl.AdmissionNumber + ",";
                                        return_val = return_val == false ? false : true;
                                        if (return_val == false)
                                        {
                                            break;
                                        }
                                    }
                                    if (return_val == true)
                                    {
                                        return_val = "Email Sent Successfully..!";
                                        save_data_tomessages_table(email_subject, content, TempData["Parent"], activetab);
                                        TempData.Remove("Parent");
                                    }

                                }
                                else
                                {
                                    return_val = "No Parent Found";
                                }
                            }
                            break;

                        case "Staff":
                            var allstaffs = cmc.get_all_staffs(listData.CollegeID, listData.BranchID, listData.academic_year, citems, null);
                            foreach (var alsf in allstaffs)
                            {
                                if (alsf.Count != 0)
                                {
                                    foreach (var alsfl in alsf)
                                    {
                                        return_val = cmc.send_mail(alsfl.email, email_subject, content);
                                        TempData["Staff"] += alsfl.employee_id + ",";
                                        return_val = return_val == false ? false : true;
                                        if (return_val == false)
                                        {
                                            break;
                                        }
                                    }
                                    if (return_val == true)
                                    {
                                        return_val = "Email Sent Successfully..!";
                                        save_data_tomessages_table(email_subject, content, TempData["Staff"], activetab);
                                        TempData.Remove("Staff");
                                    }

                                }
                                else
                                {
                                    return_val = "No Staff Found";
                                }
                            }
                            break;
                    }
                }
                return Json(new { return_val }, JsonRequestBehavior.AllowGet);
            }
            else if (get_id != null)
            {
                foreach (var citems in get_id)
                {
                    if (staffs.Contains(citems))
                    {
                        user = "Staff";
                    }
                    switch (user == "Staff" ? "Staff" : citems)
                    {
                        case "Student":
                            var allstudents = cmc.get_all_student(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn, null);
                            ViewBag.stulist = allstudents[0];
                            if (dropdwn != null)
                            {
                                if (allstudents.Count != 0)
                                {
                                    foreach (var als in allstudents)
                                    {
                                        foreach (var alsl in als)
                                        {
                                            return_val = cmc.send_mail(alsl.Email, email_subject, content);
                                            TempData["Students"] += alsl.AdmissionNumber + ",";
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                        }
                                        if (return_val == true)
                                        {
                                            return_val = "Email Sent Successfully..!";
                                            save_data_tomessages_table(email_subject, content, TempData["Students"], activetab);
                                            TempData.Remove("Students");
                                        }

                                    }

                                }
                                else
                                {
                                    return_val = "No Students Found";
                                }
                            }
                            break;

                        case "Parent":
                            var allparent = cmc.get_all_parent(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn, null);
                            ViewBag.stulist = allparent[0];
                            if (dropdwn != null)
                            {
                                if (allparent.Count != 0)
                                {
                                    foreach (var alsp in allparent)
                                    {
                                        foreach (var alspl in alsp)
                                        {
                                            return_val = cmc.send_mail(alspl.GuardianEmail, email_subject, content);
                                            TempData["Parent"] += alspl.AdmissionNumber + ",";
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                        }
                                        if (return_val == true)
                                        {
                                            return_val = "Email Sent Successfully..!";
                                            save_data_tomessages_table(email_subject, content, TempData["Parent"], activetab);
                                            TempData.Remove("Parent");
                                        }
                                    }

                                }
                                else
                                {
                                    return_val = "No Parent Found";
                                }
                            }
                            break;

                        case "Staff":
                            var allstaffs = cmc.get_all_staffs(listData.CollegeID, listData.BranchID, listData.academic_year, citems, dropdwn);
                            ViewBag.stulist = allstaffs[0];
                            if (dropdwn != null)
                            {
                                if (allstaffs.Count != 0)
                                {
                                    foreach (var alsf in allstaffs)
                                    {
                                        foreach (var alsfl in alsf)
                                        {
                                            return_val = cmc.send_mail(alsfl.email, email_subject, content);
                                            TempData["Staff"] += alsfl.employee_id + ",";
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                        }
                                        if (return_val == true)
                                        {
                                            return_val = "Email Sent Successfully..!";
                                            save_data_tomessages_table(email_subject, content, TempData["Staff"], activetab);
                                            TempData.Remove("Staff");
                                        }
                                    }

                                }
                                else
                                {
                                    return_val = "No Staff Found";
                                }

                            }
                            break;
                    }
                    return Json(new { return_val, ViewBag.stulist }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (multi_select != null)
            {
                foreach (var ms in multi_select)
                {
                    dynamic stuclasswise = cmc.get_students_classwise(listData.CollegeID, listData.BranchID, listData.academic_year, (Convert.ToInt32(ms)));
                    if (stuclasswise != null)
                    {
                        foreach (var stuclass in stuclasswise)
                        {
                            return_val = cmc.send_mail(stuclass.email, email_subject, content);
                            TempData["Studentsid"] += stuclass.admission_no + ",";
                            return_val = return_val == false ? false : true;
                            if (return_val == false)
                            {
                                break;
                            }
                        }
                        if (return_val == true)
                        {
                            return_val = "Email Sent Successfully..!";
                            save_data_tomessages_table(email_subject, content, TempData["Studentsid"], activetab);
                            TempData.Remove("Studentsid");
                        }
                    }
                    else
                    {
                        return_val = "No Studentsid Found";
                    }
                }
                return Json(new { return_val = "Email Sent Successfully..!" }, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        protected void save_data_tomessages_table(string email_title, string content, object userlist, string activetab)
        {
            if (userlist != null)
            {
                message sendnote = new message()
                {
                    title = email_title,
                    message1 = content,
                    send_mail = true,
                    send_sms = false,
                    is_individual = activetab == "Individual" ? true : false,
                    is_class = activetab == "Class" ? true : false,
                    is_group = activetab == "Group" ? true : false,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    user_list = userlist.ToString(),
                    year = listData.academic_year

                };
                dbContext.messages.Add(sendnote);
                dbContext.SaveChanges();
            }
        }

        public JsonResult sms_method(string[] get_id_sms, string[] get_checkedval_sms, string sms_subject, string[] dropdwn_sms, string[] multi_select_sms, string content_sms, string activetab)
        {
            IQueryable<string> staffs = dbContext.AspNetRoles.Where(k => k.Name != "Student" && k.Name != "Parent").Select(k => k.Name);
            string user = null;
            dynamic return_val = null;
            string[] mobnumbers = null;
            dynamic count_num = null;
            var role = "";
            var date = "";
            var reason = "";
            var city = "Hubli";
            var collegename = dbContext.sch_settings.Where(a => a.college_id == listData.CollegeID && a.branch__id == listData.BranchID && a.year == listData.academic_year && a.is_active == true).Select(a => a.name).FirstOrDefault();
            int i = 0;
           
                
                
                if (get_checkedval_sms != null)
                {
                    role = get_checkedval_sms[0];
                    string[] words = content_sms.Split(',');
                    date = words[0];
                    reason = words[1];
                    date = date.Substring(5);
                    reason = reason.Substring(7);

                    string message = "Dear " + role + ", Date : " + date + ", is Holiday, Due to Reason : " + reason + ", By Regards " + collegename + ".";
                    foreach (var citems in get_checkedval_sms)
                    {
                        if (staffs.Contains(citems.Trim()))
                        {
                            user = "Staff";
                        }
                        switch (user == "Staff" ? "Staff" : citems)
                        {
                            case "Student":
                                var allstudents = cmc.get_all_student(listData.CollegeID, listData.BranchID, listData.academic_year, null, null).ToList();
                                if (allstudents != null)
                                {
                                    foreach (var als in allstudents)
                                    {
                                        if (als.Count != 0)
                                        {
                                            mobnumbers = new string[als.Count];
                                            foreach (var alsl in als)
                                            {
                                                mobnumbers[i] = alsl.MobileNumber;
                                                TempData["Student"] += alsl.id + ",";
                                                i++;
                                            }
                                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, message, TempData["Student"], activetab);
                                                TempData.Remove("Student");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Students Found";
                                        }
                                    }
                                }
                                break;

                            case "Parent":

                                var allparent = cmc.get_all_parent(listData.CollegeID, listData.BranchID, listData.academic_year, null, null);
                                if (allparent != null)
                                {
                                    foreach (var alsp in allparent)
                                    {
                                        if (alsp.Count != 0)
                                        {
                                            mobnumbers = new string[alsp.Count];
                                            foreach (var alspl in alsp)
                                            {
                                                mobnumbers[i] = alspl.GuardianPhone;
                                                TempData["Parent"] += alspl.AdmissionNumber + ",";
                                                i++;
                                            }
                                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;

                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, message, TempData["Parent"], activetab);
                                                TempData.Remove("Parent");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Parent Found";
                                        }
                                    }
                                }
                                break;

                            case "Staff":
                                var allstaffs = cmc.get_all_staffs(listData.CollegeID, listData.BranchID, listData.academic_year, citems.Trim(), null);
                                if (allstaffs != null)
                                {
                                    foreach (var alsf in allstaffs)
                                    {
                                        if (alsf.Count != 0)
                                        {
                                            mobnumbers = new string[alsf.Count];
                                            foreach (var alsfl in alsf)
                                            {
                                                mobnumbers[i] = alsfl.contact_no;
                                                TempData["Staff"] += alsfl.employee_id + ",";
                                                i++;
                                            }
                                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;

                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, message, TempData["Staff"], activetab);
                                                TempData.Remove("Staff");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Staff Found";
                                        }
                                    }
                                }
                                break;
                        }
                        i = 0;
                        count_num = null;
                        mobnumbers = null;
                    }
                    return Json(new { return_val }, JsonRequestBehavior.AllowGet);
                }
                else if (get_id_sms != null)
                {
                if (content_sms != null)
                {
                    role = get_id_sms[0];
                    string[] words = content_sms.Split(',');
                    date = words[0];
                    reason = words[1];
                    date = date.Substring(5);
                    reason = reason.Substring(7);

                    string message = "Dear " + role + ", Date : " + date + ", is Holiday, Due to Reason : " + reason + ", By Regards " + collegename + ".";
                    foreach (var citems in get_id_sms)
                    {
                        if (staffs.Contains(citems))
                        {
                            user = "Staff";
                        }

                        switch (user == "Staff" ? "Staff" : citems)
                        {
                            case "Student":
                                var allstudents = cmc.get_all_student(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn_sms, null);
                                if (allstudents != null)
                                {
                                    ViewBag.anylist = allstudents[0];
                                    if (dropdwn_sms != null)
                                    {
                                        mobnumbers = new string[dropdwn_sms.Count()];
                                        if (allstudents.Count != 0)
                                        {
                                            foreach (var als in allstudents)
                                            {
                                                foreach (var alsl in als)
                                                {
                                                    mobnumbers[i] = alsl.MobileNumber;
                                                    TempData["Student"] += alsl.id + ",";
                                                    i++;
                                                }
                                            }
                                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, message, TempData["Student"], activetab);
                                                TempData.Remove("Student");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Student Found";
                                        }
                                    }
                                }
                                break;

                            case "Parent":
                                var allparent = cmc.get_all_parent(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn_sms, null);
                                if (allparent != null)
                                {
                                    ViewBag.anylist = allparent[0];

                                    if (dropdwn_sms != null)
                                    {
                                        mobnumbers = new string[dropdwn_sms.Count()];
                                        if (allparent.Count != 0)
                                        {
                                            foreach (var alsp in allparent)
                                            {
                                                foreach (var alspl in alsp)
                                                {
                                                    mobnumbers[i] = alspl.GuardianPhone;
                                                    TempData["Parent"] += alspl.user_id + ",";
                                                    i++;
                                                }
                                            }
                                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, message, TempData["Parent"], activetab);
                                                TempData.Remove("Parent");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Parent Found";
                                        }

                                    }
                                }
                                break;

                            case "Staff":
                                var allstaffs = cmc.get_all_staffs(listData.CollegeID, listData.BranchID, listData.academic_year, citems, dropdwn_sms);
                                ViewBag.anylist = allstaffs[0];
                                if (dropdwn_sms != null)
                                {
                                    mobnumbers = new string[allstaffs.Count()];
                                    if (allstaffs.Count != 0)
                                    {
                                        foreach (var alsf in allstaffs)
                                        {
                                            foreach (var alsfl in alsf)
                                            {
                                                mobnumbers[i] = alsfl.contact_no;
                                                TempData["Staff"] += alsfl.id + ",";
                                                i++;
                                            }
                                        }
                                        return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                                        return_val = return_val == false ? false : true;
                                        if (return_val == false)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            return_val = "Sms Sent Successfully..!";
                                            save_sms_data_tomessages_table(sms_subject, message, TempData["Staff"], activetab);
                                            TempData.Remove("Staff");
                                        }
                                    }
                                    else
                                    {
                                        return_val = "No Staff Found";
                                    }
                                }
                                break;
                        }
                        return Json(new { return_val = "Sms Sent Successfully..!", ViewBag.anylist }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    foreach (var citems in get_id_sms)
                    {
                        if (staffs.Contains(citems))
                        {
                            user = "Staff";
                        }

                        switch (user == "Staff" ? "Staff" : citems)
                        {
                            case "Student":
                                var allstudents = cmc.get_all_student(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn_sms, null);
                                if (allstudents != null)
                                {
                                    ViewBag.anylist = allstudents[0];
                                    if (dropdwn_sms != null)
                                    {
                                        mobnumbers = new string[dropdwn_sms.Count()];
                                        if (allstudents.Count != 0)
                                        {
                                            foreach (var als in allstudents)
                                            {
                                                foreach (var alsl in als)
                                                {
                                                    mobnumbers[i] = alsl.MobileNumber;
                                                    TempData["Student"] += alsl.id + ",";
                                                    i++;
                                                }
                                            }
                                            return_val = stl.send_message(mobnumbers, content_sms, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, content_sms, TempData["Student"], activetab);
                                                TempData.Remove("Student");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Student Found";
                                        }
                                    }
                                }
                                break;

                            case "Parent":
                                var allparent = cmc.get_all_parent(listData.CollegeID, listData.BranchID, listData.academic_year, dropdwn_sms, null);
                                if (allparent != null)
                                {
                                    ViewBag.anylist = allparent[0];

                                    if (dropdwn_sms != null)
                                    {
                                        mobnumbers = new string[dropdwn_sms.Count()];
                                        if (allparent.Count != 0)
                                        {
                                            foreach (var alsp in allparent)
                                            {
                                                foreach (var alspl in alsp)
                                                {
                                                    mobnumbers[i] = alspl.GuardianPhone;
                                                    TempData["Parent"] += alspl.user_id + ",";
                                                    i++;
                                                }
                                            }
                                            return_val = stl.send_message(mobnumbers, content_sms, listData.CollegeID, listData.BranchID, listData.academic_year);
                                            return_val = return_val == false ? false : true;
                                            if (return_val == false)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                return_val = "Sms Sent Successfully..!";
                                                save_sms_data_tomessages_table(sms_subject, content_sms, TempData["Parent"], activetab);
                                                TempData.Remove("Parent");
                                            }
                                        }
                                        else
                                        {
                                            return_val = "No Parent Found";
                                        }

                                    }
                                }
                                break;

                            case "Staff":
                                var allstaffs = cmc.get_all_staffs(listData.CollegeID, listData.BranchID, listData.academic_year, citems, dropdwn_sms);
                                ViewBag.anylist = allstaffs[0];
                                if (dropdwn_sms != null)
                                {
                                    mobnumbers = new string[allstaffs.Count()];
                                    if (allstaffs.Count != 0)
                                    {
                                        foreach (var alsf in allstaffs)
                                        {
                                            foreach (var alsfl in alsf)
                                            {
                                                mobnumbers[i] = alsfl.contact_no;
                                                TempData["Staff"] += alsfl.id + ",";
                                                i++;
                                            }
                                        }
                                        return_val = stl.send_message(mobnumbers, content_sms, listData.CollegeID, listData.BranchID, listData.academic_year);
                                        return_val = return_val == false ? false : true;
                                        if (return_val == false)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            return_val = "Sms Sent Successfully..!";
                                            save_sms_data_tomessages_table(sms_subject, content_sms, TempData["Staff"], activetab);
                                            TempData.Remove("Staff");
                                        }
                                    }
                                    else
                                    {
                                        return_val = "No Staff Found";
                                    }
                                }
                                break;
                        }
                        return Json(new { return_val = "Sms Sent Successfully..!", ViewBag.anylist }, JsonRequestBehavior.AllowGet);
                    }
                }
                }
                else if (multi_select_sms != null)
                {
                    role = multi_select_sms[0];
                    string[] words = content_sms.Split(',');
                    date = words[0];
                    reason = words[1];
                    date = date.Substring(5);
                    reason = reason.Substring(7);

                    string message = "Dear " + role + ", Date : " + date + ", is Holiday, Due to Reason : " + reason + ", By Regards " + collegename + ".";
                    foreach (var ms in multi_select_sms)
                    {
                        IQueryable<StudentInformation> stuclasswise = cmc.get_students_classwise1(listData.CollegeID, listData.BranchID, listData.academic_year, (Convert.ToInt32(ms)));
                        if (stuclasswise != null)
                        {
                            count_num = stuclasswise.Count();
                            mobnumbers = new string[count_num];
                            foreach (var stuclass in stuclasswise)
                            {
                                mobnumbers[i] = stuclass.MobileNumber;
                                TempData["Studentsid"] += stuclass.AdmissionNumber + ",";
                                i++;
                            }
                            return_val = stl.send_message(mobnumbers, message, listData.CollegeID, listData.BranchID, listData.academic_year);
                            return_val = return_val == false ? false : true;
                            if (return_val == false)
                            {
                                break;

                            }
                            save_sms_data_tomessages_table(sms_subject, message, TempData["Studentsid"], activetab);
                            TempData.Remove("Studentsid");
                        }

                        i = 0;
                        count_num = null;
                        mobnumbers = null;
                    }
                    TempData.Remove("Studentsid");

                    return Json(new { return_val }, JsonRequestBehavior.AllowGet);
                }
            
            
            i = 0;
            count_num = null;
            mobnumbers = null;
            return Json(null, JsonRequestBehavior.AllowGet);
        }


        protected void save_sms_data_tomessages_table(string sms_subject, string content_sms, object userlist, string activetab)
        {
            if (userlist != null)
            {
                message sendnote = new message()
                {
                    title = sms_subject,
                    message1 = content_sms,
                    send_mail = false,
                    send_sms = true,
                    is_individual = activetab == "Individual" ? true : false,
                    is_class = activetab == "Class" ? true : false,
                    is_group = activetab == "Group" ? true : false,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    user_list = userlist.ToString(),
                    year = listData.academic_year
                };
                dbContext.messages.Add(sendnote);
                dbContext.SaveChanges();
            }
        }

        public JsonResult json_return(string get_checkedval)
        {
            return Json(new { get_checkedval = get_checkedval }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult email_sms_log()
        {
            var ESS = (from a in dbContext.messages
                       select new SchoolMasterClasses.email_sms_for_all
                       {
                           Title = a.title,
                           Date = a.created_at,
                           email = a.send_mail.ToString(),
                           SMS = a.send_sms.ToString(),
                           Group = a.is_group.ToString(),
                           Individual = a.is_individual.ToString(),
                           Class = a.is_class.ToString()
                       }).ToList();
            ViewBag.ES = ESS;
            return View();
        }

        [HttpPost]
        public ActionResult email_sms_log(string fromDate, string toDate)
        {
            if (fromDate != null && toDate != null)
            {
                DateTime frmDt = DateTime.Parse(fromDate);
                DateTime toDt = DateTime.Parse(toDate);
                ViewBag.datedisplay = "-Records From " + frmDt.ToShortDateString() + " To " + toDt.ToShortDateString() + "";
                var ESS = (from a in dbContext.messages
                           where a.created_at >= frmDt && a.created_at <= toDt
                           select new SchoolMasterClasses.email_sms_for_all
                           {
                               Title = a.title,
                               Date = a.created_at,
                               email = a.send_mail.ToString(),
                               SMS = a.send_sms.ToString(),
                               Group = a.is_group.ToString(),
                               Individual = a.is_individual.ToString(),
                               Class = a.is_class.ToString()
                           }).ToList();
                ViewBag.ES = ESS;
            }
            return View();
        }















    }
}