﻿
using SmartSchool.Classes;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SmartSchool.Controllers
{
    public class DownloadCenterController : Controller
    {

        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();

        public get_staff_classes gs = new get_staff_classes();
        public object ZipOption { get; private set; }
        // public int CollegeID = 2015, BranchID = 1;
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public DownloadCenterController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }
        static string sesdatetime = getsessionyear.load_session_data(2015, 1);


        public string Status = "True";

        // GET: DownloadCenter
        public ActionResult Index()
        {
            return View();
        }

        public void Ddlclass()
        {
            ViewBag.selectedclass = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var result = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void Ddlcontenttype()
        {
            List<ContentType> s = dbContext.ContentTypes.Where(x => x.id != 0).ToList();
            ContentType content = new ContentType { contentname = "Please select content type", id = 0 };
            s.Insert(0, content);
            SelectList selectcontent = new SelectList(s, "id", "contentname", 0);
            ViewBag.selectedct = selectcontent;
        }

        [HttpPost]
        public ActionResult Content(content model, string udate, bool Admin, bool Student, bool Allclasses, HttpPostedFileBase FileUpload)
        {
            Ddlclass();

            Ddlcontenttype();

            var updateddate = DateTime.Parse(udate);
            string filesnames = "";
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/Content/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }

                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Content/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }

            content cont = new content()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                title = model.title,
                type = model.type,
                all_super_admin = Admin.ToString(),
                student = Student.ToString(),
                for_all_classes = Allclasses.ToString(),
                class_id = model.class_id,
                cls_sec_id = model.cls_sec_id,
                videopathlink=model.videopathlink,
                date = updateddate,
                note = model.note,
                file = filesnames,
                is_active = true,
                created_by = listData.user,
                created_at = DateTime.Now
                
            };
            dbContext.contents.Add(cont);
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Inserted Successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["result"] = "Something went wrong!";
            }

            return RedirectToAction("Content");
        }


        [HttpGet]
        public ActionResult Content()
        {
            Ddlclass();

            Ddlcontenttype();

            var syllabus = (from a in dbContext.contents
                            join b in dbContext.ContentTypes on a.type equals b.id
                            join c in dbContext.AddClasses on a.class_id equals c.class_id into thisvar
                            from hh in thisvar.DefaultIfEmpty()
                            where a.college_id == listData.CollegeID && a.is_active==true
                            select new Contentdata
                            {
                                Cid = a.cid,
                                Title = a.title,
                                Contentname = b.contentname,
                                Date = a.date,
                                Allsuperadmin = a.all_super_admin,
                                Student = a.student,
                                Forallclasses = a.for_all_classes,
                                Class = hh.class_name == null ? "For All Classes" : hh.class_name,
                                FilePath = a.file

                            }).ToList();
            ViewBag.Syllabuslist = syllabus;
            return View();
        }

        [HttpPost]
        public ActionResult EditContent(content model)
        {
            Ddlclass();

            Ddlcontenttype();
            content Editcon = dbContext.contents.Find(model.cid);
            if (Editcon.cid == model.cid)
            {
                Editcon.title = model.title;
                Editcon.type = model.type;
                Editcon.class_id = model.class_id;
                Editcon.cls_sec_id = model.cls_sec_id;
                Editcon.note = model.note;
                Editcon.is_active = model.is_active;
                Editcon.college_id = listData.CollegeID;
                Editcon.branch_id = listData.BranchID;
                Editcon.updated_at = DateTime.Now;
                Editcon.modified_by = listData.user;
            }
            dbContext.contents.AddOrUpdate(Editcon);
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }


            return View();
        }




        [HttpPost]
        public ActionResult Assignment(content model)
        {
            return View();
        }


        [HttpGet]
        public ActionResult Assignment()
        {

            var syllabus = (from a in dbContext.contents
                            join b in dbContext.ContentTypes on a.type equals b.id
                            join c in dbContext.AddClasses on a.class_id equals c.class_id
                            where a.college_id==listData.CollegeID  && a.type == 1

                         
                            select new Contentdata
                            {
                                Title = a.title,
                                Contentname = b.contentname,
                                Date = a.date,
                                Available = c.class_name,

                            }).ToList();
            ViewBag.Syllabuslist = syllabus;

            return View();
        }

        [HttpGet]
        public ActionResult DeleteAssignment(int? id)
        {
            var filename = (from a in dbContext.contents.Where(x => x.cid == id) select a.file).FirstOrDefault();
            
            string fullPath = Request.MapPath("~/Content/Content/" + filename);
            
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}
            

            if (id != null)
            {
                content Deletecon = dbContext.contents.Where(a => a.cid == id).FirstOrDefault();
                Deletecon.modified_by = listData.user;
                Deletecon.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.contents.Remove(Deletecon);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Assignment", "DownloadCenter");
        }



        [HttpPost]
        public ActionResult Studymaterial(content model)
        {

            return View();
        }


        [HttpGet]
        public ActionResult Studymaterial()
        {

            var syllabus = (from a in dbContext.contents
                            join b in dbContext.ContentTypes on a.type equals b.id
                            join c in dbContext.AddClasses on a.class_id equals c.class_id
                            where a.college_id == listData.CollegeID  && a.type == 2
                            select new Contentdata
                            {
                                Title = a.title,
                                Contentname = b.contentname,
                                Date = a.date,
                                Available = c.class_name,


                            }).ToList();
            ViewBag.Syllabuslist = syllabus;
            return View();
        }

        [HttpGet]
        public ActionResult DeleteStudymaterial(int? id)
        {
            var filename = (from a in dbContext.contents.Where(x => x.cid == id) select a.file).FirstOrDefault();
            string fullPath = Request.MapPath("~/Content/Content/" + filename);
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}

            if (id != null)
            {
                content Deletecon = dbContext.contents.Where(a => a.cid == id).FirstOrDefault();
                Deletecon.modified_by = listData.user;
                Deletecon.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.contents.Remove(Deletecon);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Assignment", "DownloadCenter");
        }

        [HttpGet]
        public ActionResult Deletecontent(int? id)
        {
            var filename = (from a in dbContext.contents.Where(x => x.cid == id) select a.file).FirstOrDefault();
            string fullPath = Request.MapPath("~/Content/Content/" + filename);
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}

            if (id != null)
            {
                content Deletecon = dbContext.contents.Where(a => a.cid == id).FirstOrDefault();
                Deletecon.modified_by = listData.user;
                Deletecon.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.contents.Remove(Deletecon);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Assignment", "DownloadCenter");
        }



        [HttpPost]
        public ActionResult Syllabus(content model)
        {

            return View();
        }


        [HttpGet]
        public ActionResult Syllabus()
        {
            var syllabus = (from a in dbContext.contents
                            join b in dbContext.ContentTypes on a.type equals b.id
                            join c in dbContext.AddClasses on a.class_id equals c.class_id
                            where a.college_id == listData.CollegeID  && a.type == 3
                            select new Contentdata
                            {
                                Title = a.title,
                                Contentname = b.contentname,
                                Date = a.date,
                                Available = c.class_name

                            }).ToList();
            ViewBag.Syllabuslist = syllabus;
            return View();
        }


        [HttpGet]
        public ActionResult DeleteSyllabus(int? id)
        {
            var filename = (from a in dbContext.contents.Where(x => x.cid == id) select a.file).FirstOrDefault();
            string fullPath = Request.MapPath("~/Content/Content/" + filename);
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}

            if (id != null)
            {
                content Deletecon = dbContext.contents.Where(a => a.cid == id).FirstOrDefault();
                Deletecon.modified_by = listData.user;
                Deletecon.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.contents.Remove(Deletecon);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Syllabus", "DownloadCenter");
        }



        [HttpPost]
        public ActionResult Other(content model)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Other()
        {

            var syllabus = (from a in dbContext.contents
                            join b in dbContext.ContentTypes on a.type equals b.id
                            join c in dbContext.AddClasses on a.class_id equals c.class_id
                            where a.college_id == listData.CollegeID && a.year == listData.academic_year && a.type == 4
                            select new Contentdata
                            {
                                Title = a.title,
                                Contentname = b.contentname,
                                Date = a.date,
                                Available = c.class_name,
                                FilePath = a.file
                            }).ToList();

            ViewBag.Syllabuslist = syllabus;
            return View();
        }

        [HttpGet]
        public ActionResult DeleteOther(int? id)
        {
            var filename = (from a in dbContext.contents.Where(x => x.cid == id) select a.file).FirstOrDefault();
            string fullPath = Request.MapPath("~/Content/Content/" + filename);
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}

            if (id != null)
            {
                content Deletecon = dbContext.contents.Where(a => a.cid == id).FirstOrDefault();
                Deletecon.modified_by = listData.user;
                Deletecon.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.contents.Remove(Deletecon);
            }
            else
            {
                //enter some error messgae like user not foud
            }

            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Other", "DownloadCenter");
        }

    }
}