﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using SmartSchool.Classes;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
//using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf.draw;

namespace SmartSchool.Controllers
{
    public class ExaminationController : Controller
    {
        public get_staff_classes gs = new get_staff_classes();

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public ExaminationController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in ExaminationController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();


        public ActionResult Index()
        {

            return View();
        }

        private void BindClass()
        {
            ViewBag.viewbagclasslist = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
        }

        private void BindSection(int? secid)
        {
            ViewBag.sections = dbContext.Pro_getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year).Where(k=>k.id == secid).Select(k => new SelectListItem()
            {
                Text = k.section,
                Value = k.id.ToString()
            }).ToList();
                
        }

        private void BindSubject(int? subid)
        {
            ViewBag.subjects = dbContext.Pro_getAllSubjects(listData.CollegeID, listData.BranchID, listData.academic_year).Where(k => k.id == subid).Select(k => new SelectListItem()
            {
                Text = k.name,
                Value = k.id.ToString()
            }).ToList();

        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var result = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Ddlsubject(int? ddlsection, int? ddlclass)
        {
            int? stafid = gs.getStaffId(listData.CollegeID, listData.BranchID, listData.academic_year);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //getting Subjects from store procedure
            var result = dbContext.getSubjects(listData.CollegeID, listData.BranchID, stafid, ddlclass, ddlsection, listData.academic_year, listData.roleid).ToList();

            foreach (var re in result)
            {
                selectLists.Add(new SelectListItem
                {
                    Text = re.name,
                    Value = re.id.ToString()
                });
            }

            return Json(selectLists, JsonRequestBehavior.AllowGet);
        }

        public void DdlIAGROUP()
        {
            List<IAGroupMaster> c = dbContext.IAGroupMasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            IAGroupMaster cla = new IAGroupMaster
            {
                Ianame = "Select",
                Iagid = 0
            };
            c.Insert(0, cla);
            SelectList selectIAGROUP = new SelectList(c, "Iagid", "Ianame", 0);
            ViewBag.selectedIAGROUP = selectIAGROUP;
        }

        public void DdlExam()
        {
            List<exam> s = dbContext.exams.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            exam exa = new exam

            {
                name = "Select",
                eid = 0
            };
            s.Insert(0, exa);
            SelectList selectexam = new SelectList(s, "eid", "name", 0);
            ViewBag.selectedexam = selectexam;
        }

        public void Iagroup()
        {
            List<IAGroupMaster> c = dbContext.IAGroupMasters.Where(device => device.Iagid != 0 && device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year && device.is_active == true).ToList();           
            SelectList selectclass = new SelectList(c, "Iagid", "Ianame", 0);
            ViewBag.viewbagdiscntlist = selectclass;
        }



        //--------------------------------Updated Data starts here-------------------------------------------------

        //IAGroupMaster starts
        [HttpGet]
        public ActionResult IAGroupMaster()
        {
            var Examlist = (from a in dbContext.IAGroupMasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
            ViewBag.Examlist = Examlist;
            return View();
        }

        [HttpPost]
        public ActionResult IAGroupMaster(IAGroupMaster model)
        {
            IAGroupMaster ex = new IAGroupMaster()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                Ianame = model.Ianame,
                maxmarks = model.maxmarks,
                passingmaxmarks = model.passingmaxmarks,
                is_active = true,
                created_at = DateTime.Now,
                year = listData.academic_year,
                created_by = listData.user

            };
            dbContext.IAGroupMasters.Add(ex);
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["message"] = "Record inserted Successfully..!";
            }
            else
            {
                TempData["message"] = "Something went wrong.. Please Contact Admin..!";
            }

            return RedirectToAction("IAGroupMaster");
        }

        [HttpPost]
        public JsonResult Editiagroupdisplay(int values)
        {
            var updte = (from device in dbContext.IAGroupMasters where device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year && device.Iagid == values && device.is_active == true select device).FirstOrDefault();
            return Json(new { responstext = updte.Iagid, updte.Ianame, updte.maxmarks, updte.passingmaxmarks });
        }

        [HttpPost]
        public JsonResult Editiagroup(int? id, string Ianame, string maxmarks, string passingmaxmarks)
        {
            var player = (from a in dbContext.IAGroupMasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.Iagid == id && x.is_active == true) select a).FirstOrDefault();
            if (player != null)
            {
                player.Ianame = Ianame;
                player.maxmarks = maxmarks;
                player.passingmaxmarks = passingmaxmarks;
                player.updated_at = DateTime.Now;
                player.is_active = true;
                player.modified_by = listData.user;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Deleteiagroup(int? id)
        {
            var employee = dbContext.IAGroupMasters.SingleOrDefault(x => x.Iagid == id && x.college_id == listData.CollegeID && x.is_active == true);
            employee.updated_at = DateTime.Now;
            employee.modified_by = listData.user;
            dbContext.SaveChanges();
            dbContext.IAGroupMasters.Remove(employee);
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);
        }
        //IAGroupMaster Ends

        //IACategory Master Starts
        [HttpGet]
        public ActionResult Exam()
        {
            var Examlist = (from a in dbContext.exams.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
            ViewBag.Examlist = Examlist;
            return View();
        }

        [HttpPost]
        public ActionResult Exam(exam model)
        {
            exam ex = new exam()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                name = model.name,
                note = model.note,
                is_active = model.is_active,
                created_at = DateTime.Now,
                year = listData.academic_year,
                created_by = listData.user

            };
            dbContext.exams.Add(ex);
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                ViewBag.result = "Record inserted Successfully";
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return RedirectToAction("Exam");
        }

        [HttpGet]
        public ActionResult DeleteCategory(int? id)
        {
            var Cdata = dbContext.exams.Where(k => k.eid == id).FirstOrDefault();
            if(Cdata != null)
            {
                dbContext.exams.Remove(Cdata);
                int c = dbContext.SaveChanges();
                if(c > 0)
                {
                    TempData["message"] = "Record Deleted Successfully..!";
                }
                else
                {
                    TempData["message"] = "Something Wrong.. Please Contact Admin..!";
                }
            }
            return RedirectToAction("Exam");
        }

        [HttpGet]
        public async Task<ActionResult> EditExam(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            exam Editexam = await dbContext.exams.FindAsync(id);
            if (Editexam == null)
            {
                return HttpNotFound();
            }
            return View(Editexam);
        }

        [HttpPost]
        public ActionResult EditExam(exam model)
        {
            exam Editexam = dbContext.exams.Find(model.eid);
            if (Editexam.eid == model.eid)
            {
                Editexam.name = model.name;
                Editexam.note = model.note;
                Editexam.is_active = model.is_active;
                Editexam.updated_at = DateTime.Now;
                Editexam.modified_by = listData.user;
            }
            dbContext.exams.AddOrUpdate(Editexam);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["message"] = "Record Edited Successfully";
            }
            else
            {
                TempData["message"] = "Something Wrong.. Please Contact Admin..!";
            }
            return RedirectToAction("Exam", "Examination");
        }
        //IACategory Master Ends





        //Exam Schedule Starts 
        [HttpGet]
        public ActionResult Examschedule()
        {
          //  student_attendance();
            BindClass();
            DdlIAGROUP();
            DdlExam();
            return View();
        }

        public class ExamScheduleVariables
        {
            public int? clasid { get; set; }
            public int? secid { get; set; }
            public int? subjectid { get; set; }
            public int? egroupid { get; set; }
            public int[] ecatid { get; set; }
            public int[] fmarks { get; set; }
            public int[] pmarks { get; set; }
            public int? rnumber { get; set; }
            public string edate { get; set; }
            public string stime { get; set; }
            public string etime { get; set; }
        }

        [HttpPost]
        public JsonResult SaveExamSchedule(List<ExamScheduleVariables> CollectionArray)
        {
            string retVal = null;
            int? c = CollectionArray[0].ecatid.Count();

            for (int i = 0; i < c; i++)
            {
                DateTime dateTime = Convert.ToDateTime(CollectionArray[0].edate);
                exam_schedules exam_Schedules = new exam_schedules
                {
                   
                class_id = CollectionArray[0].clasid,
                    section_id = CollectionArray[0].secid,
                    teacher_subject_id = CollectionArray[0].subjectid,
                    gropu_id = CollectionArray[0].egroupid,
                    category_id = CollectionArray[0].ecatid[i],
                    full_marks = CollectionArray[0].fmarks[i],
                    passing_marks = CollectionArray[0].pmarks[i],
                    room_no = CollectionArray[0].rnumber.ToString(),
                    date_of_exam = Convert.ToDateTime(CollectionArray[0].edate),
                    start_to = CollectionArray[0].stime,
                    end_from = CollectionArray[0].etime,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    year = listData.academic_year
                };
                dbContext.exam_schedules.AddOrUpdate(k => new { k.class_id, k.section_id, k.teacher_subject_id, k.gropu_id, k.category_id, k.date_of_exam, k.college_id, k.branch_id, k.room_no }, exam_Schedules);
                int s = dbContext.SaveChanges();
                if (s > 0)
                {
                    retVal = "Data Saved Successfully..!";
                }
                else
                {
                    retVal = "Something wrong please contact Admin..!";
                }
            }
         
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ExamscheduleDetails()
        {

            BindClass();
            BindSection(null);
            var examlsit = (from a in dbContext.exam_schedules
                            join b in dbContext.AddClasses on a.class_id equals b.class_id
                            join c in dbContext.sections on a.section_id equals c.id
                            join d in dbContext.exams on a.category_id equals d.eid
                            join e in dbContext.subjects on a.teacher_subject_id equals e.id
                            where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                            select new Exam_schedulesdata
                            {
                                ExamName = d.name,
                                ClassName = b.class_name,
                                SectionName = c.section1,
                                SubjectName = e.name,
                                Date_of_exam = a.date_of_exam,
                                Start_to = a.start_to,
                                End_from = a.end_from,
                                Room_no = a.room_no,
                                Full_marks = a.full_marks,
                                Passing_marks = a.passing_marks
                            }).ToList();

            ViewBag.examschedulelist = examlsit;
            return View();
        }

        [HttpPost]
        public ActionResult ExamscheduleDetails(int? class_id, int? section_id)
        {
            BindClass();
            DdlExam();
            BindSection(section_id);
            var Csmatch = dbContext.exam_schedules.Where(x => x.class_id == class_id && x.section_id == section_id).ToList();
            if (Csmatch != null)
            {
                var examlsit = (from a in dbContext.exam_schedules
                                join b in dbContext.AddClasses on a.class_id equals b.class_id
                                join c in dbContext.sections on a.section_id equals c.id
                                join d in dbContext.exams on a.category_id equals d.eid
                                join e in dbContext.subjects on a.teacher_subject_id equals e.id
                                where a.class_id == class_id && a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                select new Exam_schedulesdata
                                {
                                    ExamName = d.name,
                                    ClassName = b.class_name,
                                    SectionName = c.section1,
                                    SubjectName = e.name,
                                    Date_of_exam = a.date_of_exam,
                                    Start_to = a.start_to,
                                    End_from = a.end_from,
                                    Room_no = a.room_no,
                                    Full_marks = a.full_marks,
                                    Passing_marks = a.passing_marks
                                }).ToList();
                ViewBag.examschedulelist = examlsit;
            }

            return View();
        }

        //Exam Schedule Ends

        // Marks entry Subject Starts
        [HttpGet]
        public ActionResult Marksrentrysubject()
        {
            Iagroup();
            BindClass();
            BindSection(null);
            BindSubject(null);
            return View();
        }

        [HttpPost]
        public ActionResult Marksrentrysubject(StudentAddmissionDetails model, DateTime dateTime)
        {


            Iagroup();
            BindClass();
            
            BindSection(Convert.ToInt32(model.section_id));
            BindSubject(Convert.ToInt32(model.subject_id));
            int classid=Convert.ToInt32(model.class_id);
            int sectionid = Convert.ToInt32(model.section_id);
            int iagrupid = Convert.ToInt32(model.Iagroupid);
            int subjectid = Convert.ToInt32(model.subject_id);
            var Examlist = (from device in dbContext.exam_schedules
                            join B in dbContext.IAGroupMasters on device.gropu_id equals B.Iagid
                            join c in dbContext.exams on device.category_id equals c.eid
                            where  c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && c.year == listData.academic_year 
                            && device.class_id == classid && device.section_id == sectionid && device.teacher_subject_id == subjectid
                            && device.gropu_id == iagrupid && device.date_of_exam == dateTime
                            select new StudentInformation
                            {
                                Iagroupid = (int)device.gropu_id,
                                Iagroupname = B.Ianame,
                                Iaid = device.esid,
                                Ianame = c.name,
                                Activity1 = c.name,
                                Maxmarks = device.full_marks.ToString(),
                                Passingmarks = device.passing_marks.ToString(),
                            }).ToList();

            ViewBag.Examlist = Examlist;

            var studentlist = (from d in dbContext.students
                               join A in dbContext.AddClasses on d.class_id equals A.class_id
                               join B in dbContext.sections on d.section_id equals B.id
                               where A.college_id == listData.CollegeID && A.branch_id == listData.BranchID && A.year == listData.academic_year && d.class_id == classid && d.section_id ==sectionid
                               select new StudentInformation
                               {
                                   user_id = d.user_id,
                                   AdmissionNumber = d.admission_no,
                                   RollNumber = d.roll_no,
                                   FirstName = d.firstname,
                               }).ToList();


            ViewBag.studentlist = studentlist;
            var Marksslist = (from d in dbContext.exam_schedules
                              join A in dbContext.exam_results on d.class_id equals A.id
                              where d.college_id == listData.CollegeID && d.branch_id == listData.BranchID && d.year == listData.academic_year 
                              && d.class_id == classid && d.section_id == sectionid && d.date_of_exam == dateTime
                              select new StudentInformation
                              {
                                  Iaid = d.esid,
                                  Scoredmarks = A.get_marks.ToString(),
                                  user_id = A.student_id

                              }).ToList();
            ViewBag.Marksslist = Marksslist;
            TempData["sectionid"] = model.section_id;
            ViewBag.dateofeaxm = dateTime;
            return View();
        }


        [HttpPost]
        public JsonResult Marksregister_new_Subjectwise(List<get_marksresults> allval)
        {
            BindClass();
            int count = 0;
            foreach (var item in allval)
            {
                var Stu_id = item.Student_id;

                int inputdata = item.Get_marks.Length;
                for (int i = 0; i < inputdata; i++)
                {
                    var mark = item.Get_marks[i];
                    var esid = item.Get_esids[i];
                    var attd = "";
                    if (mark != "0")
                    {
                        attd = "Present";
                    }
                    else
                    {
                        attd = "Absent";
                    }
                    int eid = Convert.ToInt32(esid);
                    int mak = Convert.ToInt32(mark);

                    var Esid = (from a in dbContext.exam_results.Where(x => x.exam_schedule_id != 0 && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
                    if (eid != 0)
                    {
                        exam_results exa = new exam_results()
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            attendence = attd,
                            exam_schedule_id = eid,
                            student_id = Stu_id,
                            get_marks = mak,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year
                        };

                        dbContext.exam_results.Add(exa);
                        count = dbContext.SaveChanges();
                    }
                }
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }


        //Marks entry Subject Ends


        // Report Section starts
        [HttpGet]
        public ActionResult SubjectwiseIndidualreport()
        {
            BindClass();
            BindSection(null);
            BindSubject(null);
            return View();
        }

        [HttpPost]
        public ActionResult SubjectwiseIndidualreport(StudentAddmissionDetails model)
        {
            BindClass();
            BindSection(Convert.ToInt32(model.section_id));
            BindSubject(Convert.ToInt32(model.subject_id));
            int classid = Convert.ToInt32(model.class_id);
            int sectionid = Convert.ToInt32(model.section_id);
           
            int subjectid = Convert.ToInt32(model.subject_id);
            var player = (from a in dbContext.Iagexammasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).FirstOrDefault();

            var studentlist = (from d in dbContext.students
                               join A in dbContext.AddClasses on d.class_id equals A.class_id
                               join B in dbContext.sections on d.section_id equals B.id
                               where A.college_id == listData.CollegeID && A.branch_id == listData.BranchID && A.year == listData.academic_year && d.class_id == classid && d.section_id == sectionid
                               select new StudentInformation
                               {
                                   user_id = d.user_id,
                                   AdmissionNumber = d.admission_no,
                                   RollNumber = d.roll_no,
                                   FirstName = d.firstname,
                               }).ToList();
            ViewBag.studentlist = studentlist;

            var Examlist = (from device in dbContext.IAGroupMasters
                            join A in dbContext.Iagexammasters on device.Iagid equals A.Iagid
                            join B in dbContext.exams on A.examid equals B.eid
                            join c in dbContext.exam_schedules on B.eid equals c.gropu_id
                            join d in dbContext.exam_results on c.esid equals d.exam_schedule_id
                            where A.college_id == listData.CollegeID && A.branch_id == listData.BranchID && A.year == listData.academic_year
                            select new StudentInformation
                            {
                                Iagroupid = device.Iagid,
                                Iagroupname = device.Ianame,
                                Iaid = B.eid,
                                Ianame = B.name
                            }).ToList();
            ViewBag.Examlist = Examlist;

            var Examlistmarks = (from device in dbContext.students
                                 join A in dbContext.exam_results on device.user_id equals A.student_id
                                 join B in dbContext.exam_schedules on A.exam_schedule_id equals B.esid
                                 join c in dbContext.exams on B.gropu_id equals c.eid
                                 join d in dbContext.Iagexammasters on c.eid equals d.examid
                                 join e in dbContext.IAGroupMasters on d.Iagid equals e.Iagid
                                 where A.college_id == listData.CollegeID && A.branch_id == listData.BranchID && A.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     FirstName = device.firstname,
                                     user_id = device.user_id,
                                     Iagroupname = c.name,
                                     Passingmarks = A.get_marks.ToString(),
                                     Iaid = (int)A.exam_schedule_id,
                                     Ianame = e.Ianame
                                 }).ToList();
            ViewBag.Examlistmarks = Examlistmarks;
            ViewBag.Examlist = Examlist;

            var Marksslist = (from d in dbContext.exam_schedules
                              join A in dbContext.exam_results on d.class_id equals A.id
                              where d.college_id == listData.CollegeID && d.branch_id == listData.BranchID && d.year == listData.academic_year && d.class_id == classid && d.section_id == sectionid
                              select new StudentInformation
                              {
                                  Iaid = d.esid,
                                  Scoredmarks = A.get_marks.ToString(),
                                  user_id = A.student_id

                              }).ToList();
            ViewBag.Marksslist = Marksslist;
            var bannerdetails = (from a in dbContext.sch_settings.Where(x => x.college_id == listData.CollegeID && x.branch__id == listData.BranchID && x.year == listData.academic_year) select a.app_logo).FirstOrDefault();

            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");

            Document doc = new Document(PageSize.A4.Rotate());
            doc.SetMargins(0f, 2f, 5f, 0f); PdfPTable tableLayout = new PdfPTable(34);

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();
            BaseColor black = new BaseColor(00, 00, 00);
            BaseColor NavyBlue = new BaseColor(00, 0, 128);
            // BaseColor TitleColor = new BaseColor(102, 204, 255);
            BaseColor TitleColor = new BaseColor(79, 129, 189);
            BaseColor liteblue = new BaseColor(48, 84, 129);
            BaseColor liteRed = new BaseColor(255, 0, 0);
            iTextSharp.text.Font font = FontFactory.GetFont("vardana", 8, iTextSharp.text.Font.BOLD, black);
            iTextSharp.text.Font font1 = FontFactory.GetFont("vardana", 12, iTextSharp.text.Font.BOLD, liteblue);
            iTextSharp.text.Font font2 = FontFactory.GetFont("vardana", 12, iTextSharp.text.Font.BOLD, NavyBlue);

            string strAttachment = Server.MapPath("~/Content/Logo/" + bannerdetails);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(strAttachment);
            jpg.ScaleToFit(200, 300);
            jpg.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            doc.Add(jpg);

            Paragraph paragraph = new Paragraph("");
            paragraph.ExtraParagraphSpace = 100;
            Paragraph chunk = new Paragraph("\n\n");
            doc.Add(paragraph);
            Paragraph chunk1 = new Paragraph("\n\n\n");

            PdfPTable table = new PdfPTable(34);

            float[] columnWidths = { 2f, 8f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f }; // Second column will be
                                                                                                                                                                               // twice as first and third
            table.SetTotalWidth(columnWidths);
            //table.SpacingBefore = 10f;
            PdfPCell cell = new PdfPCell(new Phrase());
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            cell = new PdfPCell(new Phrase("SL NO 1", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Student Name", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("FA -1", font2));
            cell.Colspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("FA -2", font2));
            cell.Colspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("FA -1 + FA -2", font2));
            cell.Rowspan = 3;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("Terminal Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Terminal Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Grade", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);



            cell = new PdfPCell(new Phrase("FA -3", font2));
            cell.Colspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("FA -4", font2));
            cell.Colspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("FA -3 + FA -4", font2));
            cell.Rowspan = 3;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Preparatory Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Preparatory Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Grade", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Preparatory Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Preparatory Exam", font2));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Grade", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Total FA 1+2+3+4", font2));
            cell.Rowspan = 3;

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25/20%", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Remarks", font2));
            cell.Rowspan = 4;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Activity", font));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Achivement Test", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Total", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Activity", font));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Achivement Test", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Total", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Activity", font));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Achivement Test", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Total", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Activity", font));
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Achivement Test", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Total", font));
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("2", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("2", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("2", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("2", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("15", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("50", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("50", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("100", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25/20%", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("100/80", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("125/100", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);


            cell = new PdfPCell(new Phrase("50", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("15", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("50", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("100", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("25/20%", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase("100/80", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("125/100", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("100/80", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("125/100", font));

            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("200", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(cell);

            int i = 0;

            List<StudentIAmarksInformation> modalevaluation = null;
            modalevaluation = dbContext.Database.SqlQuery<StudentIAmarksInformation>(@"With DATAS AS (select a.firstname, a.user_id,b.get_marks, d.name from students a inner join exam_results b on a.user_id = b.student_id and a.college_id = b.college_id inner join exam_schedules c on b.exam_schedule_id = c.esid inner join exams d on c.category_id = d.eid inner join Iagexammaster e on d.eid=e.examid inner join IAGroupMaster f on e.Iagid=f.Iagid where teacher_subject_id = " + subjectid + " and a.college_id=  " + listData.CollegeID + " and a.branch_id= " + listData.BranchID + " and a.class_id= " + classid + " and a.section_id= " + sectionid + " ) Select * from DATAS pivot (sum(get_marks) for name IN([FA1_A1], [FA1_A2], [FA1_W], [FA2_A1], [FA2_A2], [FA2_W], [FA3_A1], [FA3_A2], [FA3_W], [FA4_A1], [FA4_A2], [FA4_W], [Terminal], [Preparatory_1], [Preparatory_2])) as p ;").ToList();

            ViewBag.report = modalevaluation;
            foreach (var item in ViewBag.report)
            {
                i++;
                string nn = i.ToString();
                cell = new PdfPCell(new Phrase(nn, font));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.FirstName, font));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);
                int FA1A1, FA1A2, FA1W, FA2A1, FA2A2, FA2W, FA3A1, FA3A2, FA3W, FA4A1, FA4A2, FA4W;

                if (item.FA1_A1 != null)
                {
                    FA1A1 = item.FA1_A1;
                    cell = new PdfPCell(new Phrase(FA1A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA1A1 = 0;
                    cell = new PdfPCell(new Phrase(FA1A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }

                if (item.FA1_A2 != null)
                {
                    FA1A2 = item.FA1_A2;
                    cell = new PdfPCell(new Phrase(FA1A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA1A2 = 0;
                    cell = new PdfPCell(new Phrase(FA1A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                if (item.FA1_W != null)
                {
                    FA1W = item.FA1_W;
                    cell = new PdfPCell(new Phrase(FA1W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA1W = 0;
                    cell = new PdfPCell(new Phrase(FA1W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                int T1 = FA1A1 + FA1A2 + FA1W;

                cell = new PdfPCell(new Phrase(T1.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);


                if (item.FA2_A1 != null)
                {
                    FA2A1 = item.FA2_A1;
                    cell = new PdfPCell(new Phrase(FA2A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA2A1 = 0;
                    cell = new PdfPCell(new Phrase(FA2A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }

                if (item.FA2_A2 != null)
                {
                    FA2A2 = item.FA2_A2;
                    cell = new PdfPCell(new Phrase(FA2A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA2A2 = 0;
                    cell = new PdfPCell(new Phrase(FA2A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                if (item.FA2_W != null)
                {
                    FA2W = item.FA2_W;
                    cell = new PdfPCell(new Phrase(FA2W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA2W = 0;
                    cell = new PdfPCell(new Phrase(FA2W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                int T2 = FA2A1 + FA2A2 + FA2W;

                cell = new PdfPCell(new Phrase(T2.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                int T12 = T1 + T2;
                cell = new PdfPCell(new Phrase(T12.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("25/20% ", font));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Terminal1", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Terminal2", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Grade", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);


                if (item.FA3_A1 != null)
                {
                    FA3A1 = item.FA3_A1;
                    cell = new PdfPCell(new Phrase(FA3A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA3A1 = 0;
                    cell = new PdfPCell(new Phrase(FA3A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }

                if (item.FA3_A2 != null)
                {
                    FA3A2 = item.FA3_A2;
                    cell = new PdfPCell(new Phrase(FA3A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA3A2 = 0;
                    cell = new PdfPCell(new Phrase(FA3A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                if (item.FA3_W != null)
                {
                    FA3W = item.FA3_W;
                    cell = new PdfPCell(new Phrase(FA3W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA3W = 0;
                    cell = new PdfPCell(new Phrase(FA3W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                int T3 = FA3A1 + FA3A2 + FA3W;

                cell = new PdfPCell(new Phrase(T3.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                if (item.FA4_A1 != null)
                {
                    FA4A1 = item.FA4_A1;
                    cell = new PdfPCell(new Phrase(FA4A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA4A1 = 0;
                    cell = new PdfPCell(new Phrase(FA4A1.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }

                if (item.FA4_A2 != null)
                {
                    FA4A2 = item.FA4_A2;
                    cell = new PdfPCell(new Phrase(FA4A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA4A2 = 0;
                    cell = new PdfPCell(new Phrase(FA4A2.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                if (item.FA4_W != null)
                {
                    FA4W = item.FA4_W;
                    cell = new PdfPCell(new Phrase(FA4W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                else
                {
                    FA4W = 0;
                    cell = new PdfPCell(new Phrase(FA4W.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                int T4 = FA4A1 + FA4A2 + FA4W;

                cell = new PdfPCell(new Phrase(T4.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                int T34 = T3 + T4;

                cell = new PdfPCell(new Phrase(T34.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("25/20% ", font));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("pre1", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("pre12", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell); cell = new PdfPCell(new Phrase(T12.ToString(), font)); ;

                cell = new PdfPCell(new Phrase("Grade", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("pre21", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("pre22", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Grade", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);


                int Gt = T1 + T2 + T3 + T4;

                cell = new PdfPCell(new Phrase(Gt.ToString(), font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("25/20%", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Remarks", font)); ;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);
            }
            doc.Add(table);
            doc.Close();

            byte[] byteInfo = workStream.ToArray();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=labtest.pdf");

            Response.Buffer = true;
            Response.Clear();
            Response.OutputStream.Write(workStream.GetBuffer(), 0, workStream.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.End();

            return new FileStreamResult(Response.OutputStream, "application/pdf");
        }

        public ActionResult Studentreportcard()
        {
            BindClass();
            BindSection(null);
            return View();
        }

        [HttpPost]
        public ActionResult Studentreportcard(int Class, int Section)
        {
            BindClass();
            BindSection(Section);
            var studetails = (from a in dbContext.students
                              join b in dbContext.AddClasses on a.class_id equals b.class_id into ab 
                              from b in ab.DefaultIfEmpty()
                              join c in dbContext.sections on a.section_id equals c.id into ac 
                              from c in ac.DefaultIfEmpty()
                              join d in dbContext.categories on a.category_id equals d.id into ad
                              from d in ad.DefaultIfEmpty()
                              join e in dbContext.exam_results on a.user_id equals e.student_id into ae
                              from e in ae.DefaultIfEmpty()
                              where (a.class_id == Class && a.section_id == Section && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                              select new StudentInformation
                              {
                                  Class = b.class_name,
                                  Section = c.section1,
                                  AdmissionNumber = a.admission_no,
                                  FirstName = a.firstname,
                                  FatherName = a.father_name,
                                  DateOfBirth = a.dob,
                                  Gender = a.gender,
                                  Category = d.category1,
                                  MobileNumber = a.mobileno,
                                  user_id = a.user_id,
                              }).Distinct().ToList();
            ViewBag.studentlist = studetails;
            return View();

        }

        public ActionResult GetStudentReport(int ADM)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            var studetails = (from a in dbContext.students.Where(x => x.admission_no == ADM) select a).ToList();
            ViewBag.studentlist = studetails;
            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();
            var bannerdetails = (from a in dbContext.sch_settings.Where(x => x.college_id == listData.CollegeID && x.branch__id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a.app_logo).FirstOrDefault();

            foreach (var item in ViewBag.studentlist)
            {
                string strAttachment = Server.MapPath("~/Content/Logo/" + bannerdetails);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(strAttachment);
                jpg.ScaleToFit(200, 300);
                jpg.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc.Add(jpg);
                BaseColor black = new BaseColor(00, 00, 00);
                BaseColor NavyBlue = new BaseColor(00, 0, 128);
                // BaseColor TitleColor = new BaseColor(102, 204, 255);
                BaseColor TitleColor = new BaseColor(79, 129, 189);
                BaseColor liteblue = new BaseColor(48, 84, 129);
                BaseColor liteRed = new BaseColor(255, 0, 0);
                iTextSharp.text.Font font = FontFactory.GetFont("vardana", 11, iTextSharp.text.Font.BOLD, NavyBlue);
                iTextSharp.text.Font font1 = FontFactory.GetFont("vardana", 12, black);
                iTextSharp.text.Font font2 = FontFactory.GetFont("vardana", 10, black);

                Paragraph paragraph = new Paragraph("");
                paragraph.ExtraParagraphSpace = 100;
                Paragraph chunk = new Paragraph("\n\n");          
                doc.Add(paragraph);
                Paragraph chunk1 = new Paragraph("\n\n\n");

                PdfPTable table = new PdfPTable(14);
                float[] widths2 = new float[] { 7, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3, 3, 3, 3 };
                table.SetWidths(widths2);
                table.WidthPercentage = 80;

                table.AddCell(new Phrase("Subject", font));
                table.AddCell(new Phrase("FA-1", font));
                table.AddCell(new Phrase("FA-2", font));
                table.AddCell(new Phrase("FA 1+2", font));
                table.AddCell(new Phrase("Terminal", font));
                table.AddCell(new Phrase("Total", font));
                table.AddCell(new Phrase("Grade", font));
                table.AddCell(new Phrase("FA-3", font));
                table.AddCell(new Phrase("FA-4", font));
                table.AddCell(new Phrase("FA 1+2+3+4", font));
                table.AddCell(new Phrase("PR-1", font));
                table.AddCell(new Phrase("Total", font));
                table.AddCell(new Phrase("Grade", font));
                table.AddCell(new Phrase("PR-2", font));

                table.AddCell(new Phrase("Marks", font));
                table.AddCell(new Phrase("50 M", font));
                table.AddCell(new Phrase("50 M", font));
                table.AddCell(new Phrase("20/25%", font));
                table.AddCell(new Phrase("80/100", font));
                table.AddCell(new Phrase("100/125", font));
                table.AddCell(new Phrase("", font));
                table.AddCell(new Phrase("50 M", font));
                table.AddCell(new Phrase("50 M", font));
                table.AddCell(new Phrase("20/25%", font));
                table.AddCell(new Phrase("80/100", font));
                table.AddCell(new Phrase("125/100%", font));
                table.AddCell(new Phrase("", font));
                table.AddCell(new Phrase("80/100", font));


                string namestu = "", Mediumstu = "", yearstu = "", class_idstu = "", section_idstu = "", user_idstu = "";
                var studdetails = (from a in dbContext.students
                                   join b in dbContext.AddClasses on a.class_id equals b.class_id
                                   join c in dbContext.sections on a.section_id equals c.id
                                   where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.admission_no == ADM)
                                   select new StudentInformation
                                   {
                                       Class = b.class_name,
                                       Section = c.section1,
                                       AdmissionNumber = a.admission_no,
                                       FirstName = a.firstname,
                                       LastName = a.lastname,
                                       Medium = a.Medium,
                                       year = a.year,
                                       user_id = a.user_id
                                   }).ToList();
                ViewBag.studdetails = studdetails;


                foreach (var item4 in ViewBag.studdetails)
                {
                    namestu = item4.FirstName + item4.LastName;
                    Mediumstu = item4.Medium;
                    yearstu = item4.year;
                    class_idstu = item4.Class;
                    section_idstu = item4.Section.ToString();
                    user_idstu = item4.user_id;
                }

                var ss = (from a in dbContext.exam_results
                          join b in dbContext.exam_schedules on a.exam_schedule_id equals b.esid
                          join c in dbContext.exams on b.gropu_id equals c.eid
                          join d in dbContext.subjects on b.teacher_subject_id equals d.id
                          join e in dbContext.students on a.student_id equals e.user_id
                          where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && e.admission_no == ADM)
                          orderby (d.name)
                          select new StudentInformation
                          {
                              esid = b.esid,
                              student_id = a.student_id,
                              get_marks = a.get_marks,
                              exam_name = c.name,
                              SubjectName = d.name
                          }).ToList();
                ViewBag.stuMarks = ss;
                string subjectname = "";
                int NumSubjects = 0;
                int FA1 = 0, FA2 = 0, FA3 = 0, FA4 = 0, Terminal = 0, PR1 = 0, PR2 = 0;
                int[] TotalStu = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                int store1 = 0, store2 = 0, store3 = 0, store4 = 0, store5 = 0, store6 = 0, store7 = 0, store8 = 0, store9 = 0, store10 = 0, store11 = 0;
                foreach (var stuItem in ViewBag.stuMarks)
                {
                    if (subjectname != stuItem.SubjectName && NumSubjects > 0)
                    {
                        store1 = FA1;
                        store2 = FA2;
                        store3 = ((FA1 + FA2) / 5);
                        store4 = Terminal;
                        store5 = ((FA1 + FA2) / 5) + Terminal;
                        store6 = FA3;
                        store7 = FA4;
                        store8 = ((FA1 + FA2 + FA3 + FA4) / 10);
                        store9 = PR1;
                        store10 = (((FA1 + FA2 + FA3 + FA4) / 10) + PR1);
                        store11 = PR2;
                        table.AddCell(new Phrase(subjectname, font2));
                        table.AddCell(new Phrase(store1.ToString(), font2));
                        table.AddCell(new Phrase(store2.ToString(), font2));
                        table.AddCell(new Phrase(store3.ToString(), font2));
                        table.AddCell(new Phrase(store4.ToString(), font2));
                        table.AddCell(new Phrase(store5.ToString(), font2));
                        table.AddCell(new Phrase(GetGrades(store5), font2));
                        table.AddCell(new Phrase(store6.ToString(), font2));
                        table.AddCell(new Phrase(store7.ToString(), font2));
                        table.AddCell(new Phrase(store8.ToString(), font2));
                        table.AddCell(new Phrase(store9.ToString(), font2));
                        table.AddCell(new Phrase(store10.ToString(), font2));
                        table.AddCell(new Phrase(GetGrades(store10), font2));
                        table.AddCell(new Phrase(store11.ToString(), font2));
                        TotalStu[0] += store1;
                        TotalStu[1] += store2;
                        TotalStu[2] += store3;
                        TotalStu[3] += store4;
                        TotalStu[4] += store5;
                        TotalStu[5] += store6;
                        TotalStu[6] += store7;
                        TotalStu[7] += store8;
                        TotalStu[8] += store9;
                        TotalStu[9] += store10;
                        TotalStu[10] += store11;
                        FA1 = 0; FA2 = 0; FA3 = 0; FA4 = 0; Terminal = 0; PR1 = 0; PR2 = 0;
                        NumSubjects++;
                    }
                    subjectname = stuItem.SubjectName;
                    string exam_name = stuItem.exam_name;
                    if (exam_name.Contains("FA1"))
                    {
                        FA1 += stuItem.get_marks;
                    }
                    if (exam_name.Contains("FA2"))
                    {
                        FA2 += stuItem.get_marks;
                    }
                    if (exam_name.Contains("FA3"))
                    {
                        FA3 += stuItem.get_marks;
                    }
                    if (exam_name.Contains("FA4"))
                    {
                        FA4 += stuItem.get_marks;
                    }
                    if (exam_name.Contains("Terminal"))
                    {
                        Terminal += stuItem.get_marks;
                    }
                    if (exam_name.Contains("Preparatory_1"))
                    {
                        PR1 += stuItem.get_marks;
                    }
                    if (exam_name.Contains("Preparatory_2"))
                    {
                        PR2 += stuItem.get_marks;
                    }
                }
                store1 = FA1;
                store2 = FA2;
                store3 = ((FA1 + FA2) / 5);
                store4 = Terminal;
                store5 = ((FA1 + FA2) / 5) + Terminal;
                store6 = FA3;
                store7 = FA4;
                store8 = ((FA1 + FA2 + FA3 + FA4) / 10);
                store9 = PR1;
                store10 = (((FA1 + FA2 + FA3 + FA4) / 10) + PR1);
                store11 = PR2;
                table.AddCell(new Phrase(subjectname, font2));
                table.AddCell(new Phrase(store1.ToString(), font2));
                table.AddCell(new Phrase(store2.ToString(), font2));
                table.AddCell(new Phrase(store3.ToString(), font2));
                table.AddCell(new Phrase(store4.ToString(), font2));
                table.AddCell(new Phrase(store5.ToString(), font2));
                table.AddCell(new Phrase(GetGrades(store5), font2));
                table.AddCell(new Phrase(store6.ToString(), font2));
                table.AddCell(new Phrase(store7.ToString(), font2));
                table.AddCell(new Phrase(store8.ToString(), font2));
                table.AddCell(new Phrase(store9.ToString(), font2));
                table.AddCell(new Phrase(store10.ToString(), font2));
                table.AddCell(new Phrase(GetGrades(store10), font2));
                table.AddCell(new Phrase(store11.ToString(), font2));
                TotalStu[0] += store1;
                TotalStu[1] += store2;
                TotalStu[2] += store3;
                TotalStu[3] += store4;
                TotalStu[4] += store5;
                TotalStu[5] += store6;
                TotalStu[6] += store7;
                TotalStu[7] += store8;
                TotalStu[8] += store9;
                TotalStu[9] += store10;
                TotalStu[10] += store11;
                NumSubjects++;

                for (int i = 0; i < 11; i++)
                {
                    TotalStu[i] = TotalStu[i] / NumSubjects;
                }

                table.AddCell(new Phrase("Total", font2));
                table.AddCell(new Phrase(TotalStu[0].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[1].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[2].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[3].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[4].ToString(), font2));
                table.AddCell(new Phrase(GetGrades(TotalStu[4]), font2));
                table.AddCell(new Phrase(TotalStu[5].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[6].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[7].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[8].ToString(), font2));
                table.AddCell(new Phrase(TotalStu[9].ToString(), font2));
                table.AddCell(new Phrase(GetGrades(TotalStu[9]), font2));
                table.AddCell(new Phrase(TotalStu[10].ToString(), font2));

                table.AddCell(new Phrase("Class Teacher Signature", font2));
                for (int i = 0; i <= 12; i++)
                {
                    table.AddCell(new Phrase("", font2));
                }
                table.AddCell(new Phrase("Principal Signature", font2));
                for (int i = 0; i <= 12; i++)
                {
                    table.AddCell(new Phrase("", font2));
                }
                table.AddCell(new Phrase("Parents Signature", font2));
                for (int i = 0; i <= 12; i++)
                {
                    table.AddCell(new Phrase("", font2));
                }

                PdfPTable table1 = new PdfPTable(6);
                float[] widths = new float[] { 8, 10, 8, 7, 9, 7 };
                table1.SetWidths(widths);
                table1.WidthPercentage = 80;

                Paragraph p6 = new Paragraph();
                p6.Add(new Paragraph("Student Name", font));
                table1.AddCell(p6);
                Paragraph p7 = new Paragraph();
                p7.Add(new Paragraph(namestu, font));
                table1.AddCell(p7);
                Paragraph p8 = new Paragraph();
                p8.Add(new Paragraph("Student ID", font));
                table1.AddCell(p8);
                Paragraph p9 = new Paragraph();
                p9.Add(new Paragraph(user_idstu, font));
                table1.AddCell(p9);
                Paragraph p10 = new Paragraph();
                p10.Add(new Paragraph("Academic Year", font));
                table1.AddCell(p10);
                Paragraph p11 = new Paragraph();
                p11.Add(new Paragraph(yearstu, font));
                table1.AddCell(p11);

                Paragraph p61 = new Paragraph();
                p61.Add(new Paragraph("Class Name", font));
                table1.AddCell(p61);
                Paragraph p71 = new Paragraph();
                p71.Add(new Paragraph(class_idstu, font));
                table1.AddCell(p71);
                Paragraph p81 = new Paragraph();
                p81.Add(new Paragraph("Section Name", font));
                table1.AddCell(p81);
                Paragraph p91 = new Paragraph();
                p91.Add(new Paragraph(section_idstu, font));
                table1.AddCell(p91);
                Paragraph p101 = new Paragraph();
                p101.Add(new Paragraph("Medium", font));
                table1.AddCell(p101);
                Paragraph p111 = new Paragraph();
                p111.Add(new Paragraph(Mediumstu, font));
                table1.AddCell(p111);

                Paragraph p1 = new Paragraph();
                p1.Add(Chunk.NEWLINE);
                p1.Add(Chunk.NEWLINE);
                p1.Add(Chunk.NEWLINE);
                p1.Add(Chunk.NEWLINE);
                p1.Add(new Paragraph("                  Examination Head                                                                                        Principal", font1));
                p1.Alignment = Element.ALIGN_CENTER;
                p1.Add(Chunk.NEWLINE);

                doc.Add(table1);
                doc.Add(Chunk.NEWLINE);
                doc.Add(table);
                doc.Add(p1);
            }
            doc.Close();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            var fName = string.Format("Certificate.pdf", DateTime.Now.ToString("s"));
            Session[fName] = workStream;
            return Json(new { success = true, fName }, JsonRequestBehavior.AllowGet);
        }

        private string GetGrades(int marks)
        {
            string grade = "";
            var bannerdetails = (from a in dbContext.grades.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).ToList();
            ViewBag.Gradesbag = bannerdetails;
            foreach (var itemGrade in ViewBag.Gradesbag)
            {
                if (marks >= itemGrade.mark_from && marks < itemGrade.mark_upto)
                {
                    grade = itemGrade.name;
                }
            }
            return grade;
        }

        public ActionResult DownloadFile(string fName)
        {
            var ms = Session[fName] as MemoryStream;
            if (ms == null)
                return new EmptyResult();
            Session[fName] = null;
            return File(ms, "application/pdf", fName);
        }

        // Report Section ends

    }
}