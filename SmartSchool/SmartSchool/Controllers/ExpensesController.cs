﻿using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;


using System.Web.Mvc;

namespace SmartSchool.Controllers
{
    public class ExpensesController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        //public static int CollegeID = 2015, BranchID = 1;
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public ExpensesController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        public ActionResult Index()
        {
            return View();
        }

        private void bind_head()
        {
            var u_list = dbContext.expense_head.OrderBy(a => a.id != 0).ToList().Select(b => new SelectListItem { Value = b.id.ToString(), Text = b.exp_category.ToString() }).ToList();
            ViewBag.expensehead = u_list;
        }

        [HttpGet]
        public ActionResult add_expenses()
        {
            bind_head();
            var list = (from a in dbContext.expenses
                        join b in dbContext.expense_head on a.exp_head_id equals b.id
                        where a.is_active != null
                        select new SchoolMasterClasses.search_expenses
                        {
                            id = a.eid,
                            Name = a.name,
                            InvoiceNo = a.invoice_no,
                            Date = a.date,
                            expense_category = b.exp_category,
                            Amount = a.amount
                        }
                        ).ToList();
            ViewBag.expensedata = list;
            return View();

        }

        [HttpPost]
        public ActionResult add_expenses(expens model, HttpPostedFileBase documents)
        {
            string filesnames = "";
            if (documents != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(documents.FileName);
                string dirPath = Server.MapPath("~/Content/Addincome/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }


                documents.SaveAs(Path.Combine(Server.MapPath("~/Content/Addincome/"), savedFileName)); // Save the file
                filesnames = savedFileName;
            }

            bind_head();
            expens obj = new expens
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                exp_head_id = model.exp_head_id,
                name = model.name,
                invoice_no = model.invoice_no,
                date = model.date,
                amount = model.amount,
                documents = filesnames,
                note = model.note,
                is_active = true,
                created_at = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year
                
                
            };
            dbContext.expenses.Add(obj);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Saved Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }
            return RedirectToAction("add_expenses");
        }
        
        [HttpGet]
        public ActionResult edit_expense(int? Id)
        {
            bind_head();
            var edit = dbContext.expenses.Where(a => a.eid == Id).FirstOrDefault();
            return View(edit);
        }

        [HttpPost]
        public ActionResult edit_expense(expens model, int? Id)
        {
            bind_head();
            var edit = dbContext.expenses.Where(a => a.eid == Id).FirstOrDefault();
            if (edit.eid == Id)
            {
                edit.exp_head_id = model.exp_head_id;
                edit.name = model.name;
                edit.amount = model.amount;
                edit.date = model.date;
                edit.modified_by = listData.user;
                edit.documents = model.documents;
                edit.updated_at = DateTime.Now;
                edit.college_id = listData.CollegeID;
                edit.branch_id = listData.BranchID;
                edit.note = model.note;
            };
            dbContext.expenses.AddOrUpdate(edit);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Updated Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }

            return RedirectToAction("add_expenses");
        }

        [HttpGet]
        public ActionResult DeleteExpense(int? Id)
        {

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbContext.expenses.Where(a => a.eid == Id).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            if (data == null)
            {
                return HttpNotFound();

            }
            dbContext.expenses.Remove(data ?? throw new InvalidOperationException());
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }
            return RedirectToAction("add_expenses");
        }
       
        [HttpGet]
        public ActionResult expense_head()
        {
            var getexpensehead = (from b in dbContext.expense_head where b.is_active != null select b).ToList();
            ViewBag.expensehead = getexpensehead;
            return View();
        }

        [HttpPost]
        public ActionResult expense_head(expense_head model)
        {
            expense_head obj = new expense_head
            {
                exp_category = model.exp_category,
                description = model.description,
                is_active = true,
                is_deleted = "false",
                created_at = DateTime.Now,
                created_by = listData.user,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID
            };
            dbContext.expense_head.Add(obj);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Saved Successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }
            return RedirectToAction("expense_head");
        }



        [HttpGet]
        public ActionResult edit_head(int Id)
        {
            var edit = dbContext.expense_head.Where(a => a.id == Id).FirstOrDefault();
            return View(edit);
        }


        [HttpPost]
        public ActionResult edit_head(expense_head model, int? Id)
        {
            bind_head();
            var edit = dbContext.expense_head.Where(a => a.id == Id).FirstOrDefault();
            if (edit.id == Id)
            {
                edit.exp_category = model.exp_category;
                edit.description = model.description;
                edit.modified_by =listData.user;
                edit.college_id = model.college_id;
                edit.branch_id = model.branch_id;
                edit.updated_at = DateTime.Now;

            };
            dbContext.expense_head.AddOrUpdate(edit);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                TempData["message"] = "Data Updated Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }
           
            return RedirectToAction("expense_head");
        }

       [HttpGet]
        public ActionResult Delete_head(expense_head model, int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbContext.expense_head.Where(a => a.id == Id).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            if (data == null)
            {
                return HttpNotFound();
            }

            dbContext.expense_head.Remove(data ?? throw new InvalidOperationException());
            int c = dbContext.SaveChanges();
            if(c>0)
            { 
            TempData["message"] = "Data Deleted Successfully";
            }
            else
            {
                TempData["message"] = "There was an error while saving data, please contact admin.?";
            }
            return RedirectToAction("add_expenses");
        }

        /////////////////////////////////////////////search Expense///////////////////////////////////////////////


        [HttpGet]
        public ActionResult search_expense(/*string fromDate, string toDate*/)
        {
            var exp = (from a in dbContext.expenses
                       join b in dbContext.expense_head on a.exp_head_id equals b.id
                        select new SchoolMasterClasses.search_expenses
                       {
                           id = a.eid,
                           Date = a.date,
                           Expense_Head = b.exp_category,
                           Name = a.name,
                           InvoiceNo = a.invoice_no,
                           Amount = a.amount

                       }).ToList();
            ViewBag.expen = exp;
            ViewBag.tot1 = exp.Sum(x => x.Amount);

            return View();


        }


        [HttpPost]
        public ActionResult search_expense(string fromDate, string toDate)
        {

            if (fromDate != null && toDate != null)
            {
                DateTime frmDt = DateTime.Parse(fromDate);
                DateTime toDt = DateTime.Parse(toDate);

                var exp = (from a in dbContext.expenses
                           join b in dbContext.expense_head on a.exp_head_id equals b.id
                           where a.date >= frmDt && a.date <= toDt && a.year == listData.academic_year

                           select new SchoolMasterClasses.search_expenses
                           {
                               id = a.eid,
                               Date = a.date,
                               Expense_Head = b.exp_category,
                               Name = a.name,
                               InvoiceNo = a.invoice_no,
                               Amount = a.amount

                           }).ToList();
                ViewBag.expen = exp;
                ViewBag.tot1 = exp.Sum(x => x.Amount);

            }
            return View();
        }




        ////////////////////////////////////search expense completed////////////////////////////////////////////////////////

    }


}

