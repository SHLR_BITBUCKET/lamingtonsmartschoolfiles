﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Migrations;

using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SmartSchool.Controllers
{
    public class FeesCollectionController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public date_format df = new date_format();
        public custom_master_class cmc = new custom_master_class();
        public get_staff_classes gs = new get_staff_classes();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public FeesCollectionController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }

        static string dateformat = null;

        // GET: FeesCollection
        public ActionResult Index()
        {

            return View();
        }
        private void BindCategory()
        {
            List<category> c = dbContext.categories.Where(x => x.id != 0 && x.college_id==listData.CollegeID && x.branch_id==listData.BranchID && x.year==listData.academic_year && x.is_active==true).ToList();
            category sem = new category
            {
                category1 = "Select Category",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "category1 ", 0);
            ViewBag.viewbagcatlist = selectcat;
        }

        private void BindSection()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0).ToList();
            section sem = new section
            {
                section1 = "Select Section",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            ViewBag.viewbagsectionlist = selectsect;
        }

        private void BindClass()
        {
            var u_lists = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.viewbagclasslist = u_lists;
        }


        private void Bindfeegroup()
        {
            List<fee_groups> c = dbContext.fee_groups.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year && x.is_active == true).ToList();
            fee_groups sem = new fee_groups
            {
                name = "Select Group",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "name ", 0);
            ViewBag.viewbagfeegrouplist = selectcat;
        }

        private void Bindfeegroupfeetype()
        {
            var StudyMaterial = (from a in dbContext.fee_groups_feetype
                                 join b in dbContext.fee_groups on a.fee_groups_id equals b.id
                                 join c in dbContext.feetypes on a.feetype_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.is_active == true)
                                 select new
                                 {
                                     Feename = b.name + "-" + c.code,
                                     Feid = a.id

                                 }).ToList();
            SelectList selectcat = new SelectList(StudyMaterial, "Feid", "Feename ", 0);
            ViewBag.viewbagfeegrouplist = selectcat;
        }

        private void Bindfeetype()
        {
            List<feetype> c = dbContext.feetypes.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year && x.is_active == true).ToList();
            feetype sem = new feetype
            {
                type = "Select feetype",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "type ", 0);
            ViewBag.viewbagfeetypelist = selectcat;
        }

        private void BindDiscount()
        {
            List<fees_discounts> c = dbContext.fees_discounts.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            SelectList selectcat = new SelectList(c, "id", "name ");
            ViewBag.viewbagdiscntlist = selectcat;
        }

        private void BindStudent()
        {
            List<student> c = dbContext.students.Where(x => x.id != 0).ToList();
            SelectList selectcat = new SelectList(c, "user_id", "firstname");
            ViewBag.viewbagStudent = selectcat;
        }
        [HttpPost]
        public JsonResult callgetsec(string ddlclass)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlclass))
            {
                int classid = Convert.ToInt32(ddlclass);
                result = cmc.get_class_sec(classid, listData.CollegeID, listData.BranchID, listData.academic_year);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult callgetstudent(string ddlclass, string ddlsec)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlsec))
            {
                int classid = Convert.ToInt32(ddlclass);
                int sectionid = Convert.ToInt32(ddlsec);
                result = (from s in dbContext.students

                          where s.class_id == classid && s.section_id == sectionid && s.college_id == listData.CollegeID && s.year == listData.academic_year
                          select new
                          {
                              user_id = s.user_id,
                              firstname = s.firstname

                          }).ToList();

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult FeeSlip(string id)
        {
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id into ab 
                                 from b in ab.DefaultIfEmpty()
                                 join c in dbContext.sections on a.section_id equals c.id into ac 
                                 from c in ac.DefaultIfEmpty()
                                 join d in dbContext.categories on a.category_id equals d.id into ad 
                                 from d in ad.DefaultIfEmpty()
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.user_id == id && a.year == listData.academic_year)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     AdmissionDate = a.admission_date,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                 }).FirstOrDefault();
            ViewBag.studentdetails = StudyMaterial;
            var Material = (from a in dbContext.fee_groups_feetype
                            join fee in dbContext.fee_groups on a.fee_groups_id equals fee.id into afee
                            from fee in afee.DefaultIfEmpty()

                            join t in dbContext.feetypes on a.feetype_id equals t.id into at 
                            from t in at.DefaultIfEmpty()
                            join d in dbContext.student_fees_master on a.fee_session_group_id equals d.fee_groups_id into ad 
                            from d in ad.DefaultIfEmpty()
                            where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && d.student_session_id == id && d.year == listData.academic_year && t.is_active == true)

                            select new StudentInformation
                            {
                                Type = t.type,
                                //Duedate = a.due_date,
                                Balance = d.amount.ToString(),
                                GRNumber = fee.name,
                                Class = t.code,




                            }).FirstOrDefault();
            ViewBag.student = Material;


            return Json(new { StudyMaterial, Material }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult studentfee()
        {
            GetClassSection(null);
            BindDiscount();
            BindStudent();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id into ab 
                                 from b in ab.DefaultIfEmpty()
                                 join c in dbContext.sections on a.section_id equals c.id into ac 
                                 from c in ac.DefaultIfEmpty()
                                 join d in dbContext.categories on a.category_id equals d.id into ad 
                                 from d in ad.DefaultIfEmpty()
                                 join e in dbContext.student_fees_master on a.user_id equals e.student_session_id 
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.year == listData.academic_year && a.is_active == true)
                                 
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     UserName = a.user_id
                                 }).DistinctBy(k => k.AdmissionNumber).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }


        [HttpPost]
        public ActionResult studentfee(int class_id, int section_id)
        {
            GetClassSection(class_id);
            BindDiscount();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id into ab
                                 from b in ab.DefaultIfEmpty()
                                 join c in dbContext.sections on a.section_id equals c.id into ac
                                 from c in ac.DefaultIfEmpty()
                                 join d in dbContext.categories on a.category_id equals d.id into ad
                                 from d in ad.DefaultIfEmpty()
                                 join e in dbContext.student_fees_master on a.user_id equals e.student_session_id 
                                 where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.year == listData.academic_year && a.is_active == true)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     UserName = a.user_id


                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }

        [HttpGet]
        public ActionResult addfee(int? id, string stuid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id into ab 
                                 from b in ab.DefaultIfEmpty()
                                 join c in dbContext.sections on a.section_id equals c.id into ac 
                                 from c in ac.DefaultIfEmpty()
                                 join d in dbContext.categories on a.category_id equals d.id into ad 
                                 from d in ad.DefaultIfEmpty()
                                 join e in dbContext.student_fees_master on a.user_id equals e.student_session_id into ae 
                                 from e in ae.DefaultIfEmpty()
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.user_id == stuid && a.admission_no == id && a.year == listData.academic_year)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                 }).Distinct().ToList();
            ViewBag.studentdetails = StudyMaterial;

            var StuFeeDetails = dbContext.Pro_StudentFeePaidDetails(listData.CollegeID, listData.BranchID, listData.academic_year, stuid).ToList();
            ViewBag.StuFeePaidDetails = StuFeeDetails;

            return View();
        }


        [HttpPost]
        public JsonResult Collectfeesdisccountvalue(int values)
        {
            //BindDiscount();
            //List<fees_discounts> c = dbContext.fees_discounts.Where(x => x.id != 0).ToList();
            //SelectList selectcat = new SelectList(c, "id", "name ");
            //ViewBag.viewbagdiscntlist = JsonConvert.SerializeObject(selectcat); 

            var updte = (from device in dbContext.fees_discounts where device.id == values select device).FirstOrDefault();
            //  JavaScriptSerializer js = new JavaScriptSerializer();
            //var ddd = selectcat;

            return Json(new { responstext = updte.description, updte.id, updte.amount }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult Getpaidamntdetals(string values, string Stuid)
        {
            //int max = (from l in dbContext.fee_deposit_amountdetails.Where(x => x.student_id == Stuid && x.feetype_id == values) select l.amount).ToList();

            //int sum = max.Sum(c => (c.amount));

            var max = (from l in dbContext.fee_deposit_amountdetails where l.college_id == listData.CollegeID && l.feetype_id == values select l).ToList();
            int sum = max.Sum(c => Convert.ToInt32(c.amount));



            var today = DateTime.Today;

            return Json(new { responstext = today, sum }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public JsonResult Collectfees(int? groupId, string Stuid, int? feeTypeId)
        
        {
            string amount;

            var listofdiscounts = (from device in dbContext.fees_discounts where device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year select new { device.id, device.name }).ToList();
            SelectList selectcat = new SelectList(listofdiscounts, "id", "name ");
            ViewBag.viewbagdiscntlist = JsonConvert.SerializeObject(selectcat);

            ObjectParameter parameterStatus = new ObjectParameter("outputval", typeof(string));
            dbContext.Pro_Collectfees(listData.CollegeID, listData.BranchID, listData.academic_year, groupId, Stuid, feeTypeId, parameterStatus).ToString();

            amount = parameterStatus.Value.ToString();
            var ddd = selectcat;
            var today = DateTime.Today;

            return Json(new { responstext = Stuid, amount, ddd, today }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Collectfeesupdate(int? groupId, int? feeAmount, string Mode, string stuid, int? feetypeid, int? feesDiscnt, int? feesdiscontId, int? Fine)
        {
            int? sumofSFDt = 0;
            ObjectParameter parameterStat = new ObjectParameter("outputval", typeof(string));
            dbContext.Pro_Update_SFM(listData.CollegeID, listData.BranchID, listData.academic_year, groupId, stuid, Convert.ToDouble(feeAmount), Convert.ToDouble(feesDiscnt), parameterStat);
            string RetStatus = parameterStat.Value.ToString();


            var getfeeGroupFeeTypeId = dbContext.fee_groups_feetype.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.fee_groups_id == groupId && k.feetype_id == feetypeid).Select(k => k.id).FirstOrDefault();
            var StufeesMasterID = dbContext.student_fees_master.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.fee_groups_id == groupId && k.student_session_id == stuid).Select(k => k.id).FirstOrDefault();

            var sumofSFD = dbContext.student_fees_deposite.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.student_fees_master_id == StufeesMasterID && k.fee_groups_feetype_id == getfeeGroupFeeTypeId).Select(k => k.amount_detail).FirstOrDefault();
            if (sumofSFD != null)
            {
                sumofSFDt = feeAmount + Convert.ToInt32(sumofSFD);
            }
            else
            {
                sumofSFDt = feeAmount;
            }
            student_fees_deposite sfd = new student_fees_deposite();
            sfd.college_id = listData.CollegeID;
            sfd.branch_id = listData.BranchID;
            sfd.student_fees_master_id = StufeesMasterID;
            sfd.fee_groups_feetype_id = getfeeGroupFeeTypeId;
            sfd.amount_detail = sumofSFDt.ToString();
            sfd.created_at = DateTime.Now;
            sfd.is_active = true;
            sfd.created_by = listData.user;
            sfd.created_at = DateTime.Now;
            sfd.year = listData.academic_year;
            dbContext.student_fees_deposite.AddOrUpdate(o => new { o.college_id, o.branch_id, o.fee_groups_feetype_id, o.student_fees_master_id }, sfd);
            dbContext.SaveChanges();

            if (feeAmount != 0)
            {
                fee_deposit_amountdetails sfad = new fee_deposit_amountdetails
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    student_id = stuid,
                    dpid = sfd.id,
                    amount = feeAmount,
                    date = DateTime.Now,
                    amount_discount = feesDiscnt.ToString(),
                    amount_fine = Fine.ToString(),
                    created_at = DateTime.Now,
                    is_active = true,
                    CollectedBy = listData.user,
                    payment_mode = Mode,
                    inv_no = 1,
                    feetype_id = feetypeid.ToString(),
                    created_by = listData.user,
                    year = listData.academic_year
                };
                dbContext.fee_deposit_amountdetails.Add(sfad);
                dbContext.SaveChanges();

                if (feesDiscnt != 0)
                {
                    student_fees_discounts sd = new student_fees_discounts
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        year = listData.academic_year,
                        student_session_id = stuid,
                        fees_discount_id = feesdiscontId,
                        payment_id = sfad.id.ToString(),
                        is_active = true,
                        created_at = DateTime.Now,
                        created_by = listData.user
                    };
                    dbContext.student_fees_discounts.AddOrUpdate(sd);
                    dbContext.SaveChanges();
                    ObjectParameter parameterStatus = new ObjectParameter("RetVal", typeof(string));
                    dbContext.Pro_GetFeesDiscount(listData.CollegeID, listData.BranchID, listData.academic_year, sfd.id, parameterStatus);
                    string newval = parameterStatus.Value.ToString();

                }
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeeAmount(string stuids, int? dpid, int inv)
        {
            var employee = dbContext.fee_deposit_amountdetails.SingleOrDefault(x => x.dpid == dpid && x.student_id == stuids && x.inv_no == inv && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID);
            employee.modified_by = listData.user;
            employee.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.fee_deposit_amountdetails.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            var fees_deposite_AMT = dbContext.student_fees_deposite.SingleOrDefault(x => x.id == dpid && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID);
            if (employee.dpid == 0 && employee.feetype_id == null && employee.student_id == null)
            {
                dbContext.student_fees_deposite.Remove(fees_deposite_AMT ?? throw new InvalidOperationException());
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult feediscount()
        {
            BindClass();

            var studetails = (from a in dbContext.fees_discounts.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
            ViewBag.studentlist = studetails;

            return View();
        }

        [HttpPost]
        public ActionResult feediscount(fees_discounts feetp)
        {
            if (ModelState.IsValid)
            {
                fees_discounts screenshot = new fees_discounts();
                screenshot.college_id = listData.CollegeID;
                screenshot.branch_id = listData.BranchID;
                screenshot.year = listData.academic_year;
                screenshot.session_id = listData.academicyear_id;
                screenshot.name = feetp.name;
                screenshot.code = feetp.code;
                screenshot.description = feetp.description;
                screenshot.created_at = DateTime.Now;
                screenshot.is_active = true;
                screenshot.created_by = listData.user;
                screenshot.amount = feetp.amount;

                dbContext.fees_discounts.AddOrUpdate(sr => new { sr.college_id, sr.branch_id, sr.name }, screenshot);
                dbContext.SaveChanges();
            }

            return RedirectToAction("feediscount");
        }

        public ActionResult feesforward()
        {
            BindSection();
            BindClass();
            return View();

        }
        [HttpPost]
        public ActionResult feesforward(int? Class, int? Section)
        {
            BindSection();
            BindClass();


            var player = (from a in dbContext.students
                              // join c in dbContext.student_fees on a.user_id equals c.student_session_id.ToString()
                          where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.class_id == Class && a.section_id == Section && a.year == listData.academic_year)
                          select new Stdentfeeclass
                          {
                              Admissionno = a.admission_no.ToString(),
                              Admissiondate = a.admission_date,
                              Rollno = a.roll_no,
                              Name = a.firstname,
                              DOB = a.dob,
                              FatherName = a.father_name,
                              UserId=a.user_id


                          }).ToList();
            ViewBag.studentlist = player;
            //   return View(studentlist);
            return View();
        }

        [HttpGet]
        public ActionResult searchpayment()
        {
            BindClass();
            return View();
        }

        [HttpPost]
        public ActionResult searchpayment(int? Class, int? Section)
        {
            BindClass();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.student_fees_master on a.user_id equals c.student_session_id.ToString()
                                 join d in dbContext.fee_groups_feetype on c.fee_groups_id equals d.id
                                 join e in dbContext.fee_groups on d.fee_groups_id equals e.id
                                 join f in dbContext.feetypes on d.feetype_id equals f.id
                                 // join h in dbContext.fee_deposit_amountdetails on a.user_id equals h.student_id.ToString()
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.class_id == Class && a.section_id == Section && a.year == listData.academic_year && a.is_active == true)

                                 select new Stdentfeeclass
                                 {
                                     Admissionno = a.admission_no.ToString(),
                                     Date = d.due_date.ToString(),
                                     Name = a.firstname,
                                     Class = b.class_name,
                                     Feegroup = e.name,
                                     Feetype = f.type,
                                     mode = "Cash",
                                     Feetypeid = f.id.ToString(),
                                     UserId = a.user_id,
                                     amount=c.amount.ToString(),
                                         }).ToList();

            ViewBag.studentlist = StudyMaterial;
           
            return View();
        }

        [HttpGet]
        public ActionResult searchduefee()
        {
            BindClass();
            // BindSection();
            Bindfeegroupfeetype();
            return View();
        }

        [HttpPost]
        public ActionResult searchduefee(string fegroupid, int Class, int Section)
        {
            BindClass();
            //BindSection();
            Bindfeegroupfeetype();
            int fgft = Convert.ToInt32(fegroupid);
            var StudyMaterial = (from a in dbContext.students
                                 //join c in dbContext.student_fees on a.user_id equals c.student_session_id.ToString()
                                 join d in dbContext.student_fees_master on a.user_id equals d.student_session_id
                                 join e in dbContext.fee_groups_feetype on d.fee_groups_id equals e.id
                                 where (a.college_id == listData.CollegeID && e.id == fgft && a.class_id == Class && a.section_id == Section && e.year == listData.academic_year)

                                 select new Stdentfeeclass
                                 {
                                     Admissionno = a.admission_no.ToString(),
                                     Rollno = a.roll_no,
                                     Name = a.firstname,
                                     DOB = a.dob,
                                     duedate = e.due_date.ToString(),
                                     amount = e.amount.ToString(),
                                     Feetype = e.feetype_id.ToString(),
                                     UserId = a.user_id,
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }
        [HttpGet]
        public ActionResult feesstatement()
        {
            BindClass();
            // BindSection();
            BindStudent();
            return View();
        }

        [HttpPost]
        public ActionResult feesstatement(int? Class, int? Section, string Name)
        {

            string stuid = Name;
            BindClass();
            //BindSection();
            BindStudent();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.user_id == stuid && a.year == listData.academic_year)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;

            var checkstudent = (from p in dbContext.student_fees

                                where p.college_id == listData.CollegeID && p.student_session_id == stuid && p.year == listData.academic_year && p.is_active == true
                                select new { p }).FirstOrDefault();

            if (checkstudent != null)
            {

                var max = (from l in dbContext.fee_deposit_amountdetails where l.student_id == stuid select l).ToList();
                int sum = max.Sum(c => Convert.ToInt32(c.amount));
                dateformat = df.get_date(listData.CollegeID, listData.BranchID, listData.academic_year);

                int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));
                int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));
                List<Stdentfeeclass> sf = new List<Stdentfeeclass>();
                var player = (from a in dbContext.student_fees_master
                              join b in dbContext.fee_groups_feetype on a.fee_groups_id equals b.fee_session_group_id
                              join c in dbContext.fee_groups on b.fee_groups_id equals c.id
                              join p in dbContext.feetypes on b.feetype_id equals p.id
                              //join k in dbContext.student_fees_deposite on b.id equals k.fee_groups_feetype_id
                              //join m in dbContext.fee_deposit_amountdetails on b.feetype_id equals m.feetype_id
                              where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && a.student_session_id == stuid && c.year == listData.academic_year)

                              select new Stdentfeeclass
                              {
                                  //id = id.ToString(),
                                  Feemasterid = b.fee_session_group_id.ToString(),
                                  Feetype = b.feetype_id.ToString(),
                                  fegroupid = c.name,
                                  feecde = p.code,
                                  duedate = b.due_date.ToString(),
                                  stats = a.is_active.ToString(),
                                  amount = b.amount.ToString(),
                                  paymentid = "",
                                  mode = "Cash",
                                  Date = b.due_date.ToString(),
                                  Discount = sumdis.ToString(),
                                  fine = sumfin.ToString(),
                                  UserId = stuid,
                                  //paid = k.amount_detail,
                                  //balance = b.amount.ToString()
                              }).ToList();

                ViewBag.studentlist = player;
                if (player == null)
                {
                    return HttpNotFound();
                }
                var paid = (from l in dbContext.fee_deposit_amountdetails where l.student_id == stuid select l).ToList();
            }
            else
            {
                var player = (
         from c in dbContext.fee_groups_feetype

         join b in dbContext.fee_groups on c.fee_groups_id equals b.id
         join d in dbContext.feetypes on c.feetype_id equals d.id
         join e in dbContext.student_fees_master on c.fee_session_group_id equals e.fee_groups_id
         where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && c.year == listData.academic_year && c.is_active == true && e.student_session_id == stuid)

         select new Stdentfeeclass
         {

             Feemasterid = c.fee_session_group_id.ToString(),
             Feetype = c.feetype_id.ToString(),
             fegroupid = b.name,
             feecde = d.code,
             duedate = c.due_date.ToString(),
             stats = "Unpaid",
             amount = c.amount.ToString(),
             paymentid = "",
             mode = "",
             Date = "",
             Discount = "",
             fine = "",
             paid = "0",
             balance = c.amount.ToString(),
             UserId = stuid

         }).ToList();

                if (player == null)
                {
                    return HttpNotFound();
                }
                ViewBag.studentlist = player;
                //   return View(studentlist);
            }
            return View();

        }

        public ActionResult balancefeereport()
        {
            BindClass();
            BindSection();
            var stufeemaster = (from a in dbContext.student_fees_master.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year) select a).FirstOrDefault();

            var max = (from l in dbContext.fee_deposit_amountdetails where l.college_id == listData.CollegeID select l).ToList();
            int sum = max.Sum(c => Convert.ToInt32(c.amount));
            int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));

            int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));
            return View();
        }
        [HttpPost]
        public ActionResult balancefeereport(int Class, int Section)
        {
            BindClass();
            BindSection();
            var stufeemaster = (from a in dbContext.student_fees_master.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year) select a).FirstOrDefault();

            var max = (from l in dbContext.fee_deposit_amountdetails where l.college_id == listData.CollegeID select l).ToList();
            int sum = max.Sum(c => Convert.ToInt32(c.amount));
            int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));

            int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));




            var StudyMaterial = (from a in dbContext.students
                                     //  join c in dbContext.student_fees on a.user_id equals c.student_session_id.ToString()
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.class_id == Class && a.section_id == Section && a.year == listData.academic_year)

                                 select new Stdentfeeclass
                                 {
                                     Admissionno = a.admission_no.ToString(),
                                     Rollno = a.roll_no,
                                     Name = a.firstname,
                                     FatherName = a.father_name,
                                     DOB = a.dob,
                                     UserId = a.user_id,
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }

        //-------------------------------------Fee type Start and Latest changes Done-----------------------------------------------

        public ActionResult feetype()
        {
            var player = (from p in dbContext.feetypes where p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.year == listData.academic_year && p.is_active == true select p).ToList();
            ViewBag.studentlist = player;
            return View();
        }

        [HttpPost]
        public ActionResult feetype(feetype feetp)
        {
            if (ModelState.IsValid)
            {
                feetype screenshot = new feetype();
                screenshot.college_id = listData.CollegeID;
                screenshot.branch_id = listData.BranchID;
                screenshot.type = feetp.type;
                screenshot.code = feetp.code;
                screenshot.description = feetp.description;
                screenshot.feecategory_id = 1;
                screenshot.created_at = DateTime.Now;
                screenshot.is_active = true;
                screenshot.created_by = listData.user;
                screenshot.year = listData.academic_year;

                dbContext.feetypes.Add(screenshot);
                dbContext.SaveChanges();
            }
            return RedirectToAction("feetype");
        }

        [HttpPost]
        public JsonResult EditCustomer(int values)
        {
            var updte = (from device in dbContext.feetypes where device.id == values && device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.type, updte.description, updte.code });
        }
        [HttpPost]
        public JsonResult Editfeetypeupdate(int? id, string type, string Des, string code)
        {
            try
            {
                var updte = (from device in dbContext.feetypes where device.id == id && device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year select device).FirstOrDefault();
                if (updte != null)
                {

                    updte.college_id = listData.CollegeID;
                    updte.branch_id = listData.BranchID;
                    updte.type = type;
                    updte.description = Des;
                    updte.code = code;
                    updte.modified_by = listData.user;
                    updte.is_active = true;
                    dbContext.feetypes.AddOrUpdate(updte);
                    int s = dbContext.SaveChanges();
                    if (s > 0)
                    {
                        TempData["message"] = "Record Updated Successfully..!";
                    }




                }
            }

            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong...Please Contact Admin..!";

            }

            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeetype(int? id)
        {
            try
            {


                var employee = dbContext.feetypes.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true);
                employee.modified_by = listData.user;
                employee.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                employee.is_active = false;
                int? s = dbContext.SaveChanges();
                if (s > 0)
                {
                    TempData["message"] = "Record Deleted Successfully....!";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong.... Please Contact Admin....!";
            }

            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        //-------------------------------------Fee type end and Latest changes Done-----------------------------------------------

        //-------------------------------------Fee Group Start and Latest changes Done-----------------------------------------------


        [HttpGet]
        public ActionResult feegroup()
        {
            var player = (from p in dbContext.fee_groups where p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.year == listData.academic_year && p.is_active == true select p).ToList();
            ViewBag.studentlist = player;
            return View();
        }
        [HttpPost]
        public ActionResult feegroup(fee_groups feetp)
        {
            var playersess = (from p in dbContext.fee_session_groups where p.college_id == listData.CollegeID && p.session_id == listData.academicyear_id && p.year == listData.academic_year && p.is_active == true select p).FirstOrDefault();
            fee_groups screenshot = new fee_groups();
            screenshot.college_id = listData.CollegeID;
            screenshot.branch_id = listData.BranchID;
            screenshot.name = feetp.name;

            screenshot.description = feetp.description;
            screenshot.created_at = DateTime.Now;
            screenshot.is_active = true;
            screenshot.created_by = listData.user;
            screenshot.year = listData.academic_year;

            dbContext.fee_groups.AddOrUpdate(screenshot);
            dbContext.SaveChanges();

            int fgid = screenshot.id;
            fee_session_groups screenshot1 = new fee_session_groups();
            screenshot1.college_id = listData.CollegeID;
            screenshot1.branch_id = listData.BranchID;
            screenshot1.fee_groups_id = fgid;
            screenshot1.session_id = listData.academicyear_id;
            screenshot1.created_at = DateTime.Now;
            screenshot1.is_active = true;
            screenshot1.modified_by = listData.user;
            screenshot1.year = listData.academic_year;

            dbContext.fee_session_groups.AddOrUpdate(k => new { k.college_id, k.branch_id, k.fee_groups_id, k.session_id }, screenshot1);
            dbContext.SaveChanges();
            return RedirectToAction("feegroup");
        }


        [HttpPost]
        public JsonResult Editfeegroup(int id)
        {

            //TempData["Id"] = values;
            var updte = (from device in dbContext.fee_groups where device.id == id && device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.name, updte.description });
            //return View(updte);
        }
        [HttpPost]
        public JsonResult Editfeegroupupdate(int id, string name, string Des)
        {
            try
            {
                var updte = (from device in dbContext.fee_groups
                             where device.id == id &&
                                      device.college_id == listData.CollegeID && device.branch_id == listData.BranchID &&
                                              device.year == listData.academic_year
                             select device).FirstOrDefault();
                //var updte = (from a in dbContext.fee_groups.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.id == id) select a).FirstOrDefault();
                if (updte != null)
                {

                    updte.college_id = listData.CollegeID;
                    updte.branch_id = listData.BranchID;
                    updte.name = name;
                    //updte.id = id;
                    updte.description = Des;
                    updte.modified_by = listData.user;
                    dbContext.fee_groups.AddOrUpdate(updte);
                    int? s = dbContext.SaveChanges();
                    if (s > 0)
                    {
                        TempData["message"] = "Record Updated Successfully..!";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong...Please Contact Admin..!";

            }


            //return RedirectToAction("Index");
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeegroup(int? id)
        {
            try
            {
                var employee = dbContext.fee_groups.SingleOrDefault(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.id == id);
                employee.modified_by = listData.user;



                var employee1 = dbContext.fee_session_groups.SingleOrDefault(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.fee_groups_id == employee.id);
                employee1.modified_by = listData.user;
                dbContext.SaveChanges();
                employee.is_active = false;
                employee1.is_active = false;
                int? s = dbContext.SaveChanges();
                if (s > 0)
                {
                    TempData["message"] = "Record Deleted Successfully....!";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong.... Please Contact Admin....!";
            }
            return Json(new { responstext = "Record Deleted " }, JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------Fee Group End and Latest changes Done-----------------------------------------------


        //-------------------------------------Fee Master start and Latest changes Done-----------------------------------------------


        [HttpGet]
        public ActionResult feesmaster()
        {
            Bindfeetype();
            Bindfeegroup();

            var player = (from a in dbContext.fee_groups_feetype
                          join b in dbContext.fee_groups on a.fee_groups_id equals b.id
                          join c in dbContext.feetypes on a.feetype_id equals c.id
                          where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.year == listData.academic_year && a.is_active == true)
                          orderby b.id
                          select new Stdentfeeclass
                          {
                              id = a.id.ToString(),
                              Feegroup = b.name,
                              amount = a.amount.ToString(),
                              feecde = c.code,
                          }).ToList();
            ViewBag.studentlist = player;
            return View();
        }

        [HttpPost]
        public ActionResult feesmaster(feemaster feetp, int fegroup_id, int feetype_id, DateTime due_date, decimal amount)
        {
            Bindfeetype();
            Bindfeegroup();
            // var feesessgrop = (from p in dbContext.fee_session_groups where p.college_id == listData.CollegeID && p.session_id == listData.academicyear_id && p.year == listData.academic_year && p.is_active == true && p.fee_groups_id == fegroup_id select p).FirstOrDefault();
            fee_groups_feetype screenshot1 = new fee_groups_feetype
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                fee_session_group_id = null,
                fee_groups_id = fegroup_id,
                feetype_id = feetype_id,
                session_id = listData.academicyear_id,
                due_date = due_date,
                amount = amount,
                created_at = DateTime.Now,
                is_active = true,
                created_by = listData.user,
                year = listData.academic_year

            };
            dbContext.fee_groups_feetype.AddOrUpdate(k => new { k.college_id, k.branch_id, k.fee_groups_id, k.feetype_id }, screenshot1);
            dbContext.SaveChanges();
            return RedirectToAction("feesmaster");
        }


        [HttpPost]
        public JsonResult Editfeemaster(int values)
        {
            var updte = (from device in dbContext.fee_groups_feetype where device.id == values && device.is_active == true && device.college_id == listData.CollegeID && device.year == listData.academic_year && device.branch_id == listData.BranchID select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.fee_groups_id, updte.feetype_id, updte.due_date, updte.amount });
        }

        public JsonResult Editfeemasterpupdate(int? eid, int feegrpid, int feetypeid, string duedate, decimal feeamount)
        {
            var player = (from device in dbContext.fee_groups_feetype where device.id == eid && device.fee_groups_id == feegrpid && device.feetype_id == feetypeid && device.is_active == true && device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year select device).FirstOrDefault();
            if (player != null)
            {
                player.college_id = listData.CollegeID;
                player.branch_id = listData.BranchID;
                player.amount = feeamount;
                player.due_date = Convert.ToDateTime(duedate);
                player.is_active = true;
                player.modified_by = listData.user;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeemaster(int? id)
        {
            var employee1 = dbContext.fee_groups_feetype.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true);
            if (employee1 != null)
            {
                employee1.is_active = false;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------Fee Master end and Latest changes Done-----------------------------------------------

        [HttpPost]
        public JsonResult Editfeediscount(int values)
        {
            //Session["Deviceid"] = values;
            var updte = (from device in dbContext.fees_discounts where device.id == values select device).FirstOrDefault();

            //return Json(new { responstext = updte }, JsonRequestBehavior.AllowGet);

            return Json(new { responstext = updte.id, updte.name, updte.description, updte.code, updte.amount });

        }

        public JsonResult Editfeediscountupdate(int values, string Name, string code, string amount, string description)
        {

            var player = (from a in dbContext.fees_discounts.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year && x.id == values) select a).FirstOrDefault();

            if (player != null)
            {
                fees_discounts ft = new fees_discounts
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,

                    name = Name,
                    description = description,
                    code = code,
                    amount = Convert.ToInt32(amount),

                    updated_at = DateTime.Now,
                    is_active = true,
                    modified_by = listData.user
                };
                dbContext.fees_discounts.AddOrUpdate(k => new { k.college_id, k.branch_id, k.id }, ft);
                dbContext.SaveChanges();





            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFeediscunt(int? id)
        {
            var employee = dbContext.fees_discounts.SingleOrDefault(x => x.id == id);
            employee.modified_by = listData.user;
            employee.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.fees_discounts.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }


        public JsonResult Editfeemaster(int? id)
        {

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();

            return Json(StudyMaterial, JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------Fee Assign start and Latest changes Done-----------------------------------------------
        [HttpGet]
        public ActionResult feeassign(int? id)
        {
            BindCategory();
            GetClassSection(null);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = (from a in dbContext.fee_groups_feetype.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.id == id && x.is_active == true && x.year == listData.academic_year) select a.fee_groups_id).FirstOrDefault();

            var player = (from a in dbContext.fee_groups_feetype
                          join b in dbContext.fee_groups on a.fee_groups_id equals b.id
                          join c in dbContext.feetypes on a.feetype_id equals c.id
                          where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.is_active == true && a.id == id)
                          orderby b.id
                          select new Stdentfeeclass
                          {
                              id = a.id.ToString(),
                              Feegroup = b.name,
                              amount = a.amount.ToString(),
                              feecde = c.code,
                          }).FirstOrDefault();
          
            ViewBag.feestudetailsview = id;
            return View();
        }

        [HttpGet]
        public ActionResult GetClassSection(int? classid)
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.sections = gs.get_staff_wise_classes(classid, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return View();
        }

        [HttpPost]
        public ActionResult feeassign(int? Class, int? Section, string Gender, string RTE, int Category, int feemaster_grp_type_id, FormCollection formCollection)
        {
            BindCategory();
            GetClassSection(Class);
 
            if (Class != null && Section != null)
            {
                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id into ab
                                     from b in ab.DefaultIfEmpty()
                                     join c in dbContext.sections on a.section_id equals c.id into ac
                                     from c in ac.DefaultIfEmpty()
                                     join d in dbContext.categories on a.category_id equals d.id into ad
                                     from d in ad.DefaultIfEmpty()
                                     where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.class_id == Class && a.section_id == Section && a.year == listData.academic_year && a.gender==Gender)
                                     select new StudentInformation
                                     {
                                         Class = b.class_name,
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.father_name,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         UserName = a.user_id,
                                         RTE = a.rte,
                                         category_id = d.id
                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;

                //if (Category != 0)
                //{
                //    StudyMaterial = StudyMaterial.Where(k => k.category_id == Category);
                //}
                //if (!String.IsNullOrEmpty(Gender))
                //{
                //    StudyMaterial = StudyMaterial.Where(k => k.Gender == Gender);
                //}
                //if (!String.IsNullOrEmpty(RTE))
                //{
                //    StudyMaterial = StudyMaterial.Where(k => k.RTE == RTE);
                //}
                //ViewBag.studentlist = StudyMaterial.ToList();

                var player = (from a in dbContext.fee_groups_feetype
                              join b in dbContext.fee_groups on a.fee_groups_id equals b.id
                              join c in dbContext.feetypes on a.feetype_id equals c.id
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby b.id
                              select new Stdentfeeclass
                              {
                                  id = a.fee_groups_id.ToString(),
                                  Feegroup = b.name,
                                  amount = a.amount.ToString(),
                                  feecde = c.code,
                              }).ToList();
                ViewBag.feestudetails = player;
                ViewBag.feestudetailsview= feemaster_grp_type_id;
            }
            return View();
        }

        [HttpPost]
        public JsonResult Feeassignsave(List<student_fees_master> allStaff)
        {
            int result = 0;
            if (allStaff != null)
            {
                foreach (var staff in allStaff)
                {
                    var player = (from a in dbContext.student_fees_master.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year && x.student_session_id == staff.student_session_id && x.fee_groups_id == staff.fee_groups_id && x.is_active == true) select a).FirstOrDefault();

                    if (player == null)
                    {
                        student_fees_master obj = new student_fees_master
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            student_session_id = staff.student_session_id,
                            amount = Convert.ToDouble(staff.amount),
                            fee_groups_id = staff.fee_groups_id,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year
                        };
                        dbContext.student_fees_master.AddOrUpdate(k => new { k.college_id, k.branch_id, k.student_session_id, k.fee_groups_id }, obj);
                        result += dbContext.SaveChanges();
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //-------------------------------------Fee Assign end and Latest changes Done-----------------------------------------------



        public ActionResult fee_assign_discnt(int? id)
        {
            BindCategory();
            BindClass();


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = (from a in dbContext.fees_discounts.Where(x => x.college_id == listData.CollegeID && x.id == id && x.is_active == true) select a.id).FirstOrDefault();
            var player = (from a in dbContext.fee_groups_feetype
                          join b in dbContext.fee_groups on a.fee_groups_id equals b.id
                          join c in dbContext.feetypes on a.feetype_id equals c.id
                          join d in dbContext.fee_session_groups on b.id equals d.fee_groups_id
                          where (a.college_id == listData.CollegeID && a.year == listData.academic_year && d.session_id == listData.academicyear_id && a.is_active == true && a.fee_session_group_id == id)
                          orderby b.id
                          select new Stdentfeeclass
                          {
                              id = a.fee_session_group_id.ToString(),

                              Feegroup = b.name,
                              amount = a.amount.ToString(),
                              feecde = c.code,


                          }).FirstOrDefault();

            ViewBag.feestudetailsview = employee;
            return View();
        }


        [HttpPost]
        public ActionResult fee_assign_discnt(int? Class, int? Section, int? Gender, int? RTE, int Category, int feemaster_grp_type_id, FormCollection formCollection)
        {
            BindCategory();
            BindClass();

            var classid = (from a in dbContext.students.Where(x => x.college_id == listData.CollegeID && x.class_id == Class) select a).ToList();

            var GroupName = (from a in dbContext.fees_discounts

                             where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                             select new { a }).FirstOrDefault();

            ViewBag.feestudetailsviewgrpname = GroupName;

            if (classid != null)
            {

                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id

                                     where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.class_id == Class && a.section_id == Section)
                                     select new StudentInformation
                                     {
                                         Class = b.class_name,
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.father_name,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         UserName = a.user_id,



                                     }).ToList();



                ViewBag.studentlist = StudyMaterial;
                var player = (from a in dbContext.fees_discounts
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby a.id
                              select new Stdentfeeclass
                              {
                                  id = a.id.ToString(),

                                  Feegroup = a.name,
                                  amount = a.amount.ToString(),
                                  feecde = a.code,

                                  description = a.description,
                              }).ToList();



                //var player = (from p in dbContext.feemasters where p.college_id == college_id select p).ToList();

                ViewBag.feestudetails = player;
            }

            else if (Category != 0)
            {

                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id
                                     where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.category_id == Category)

                                     select new StudentInformation
                                     {
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.firstname,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         MobileNumber = a.mobileno,
                                         AadhaarNumber = a.adhar_no,
                                         Medium = a.Medium,
                                         RTE = a.rte



                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;
                var player = (from a in dbContext.fees_discounts
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby a.id
                              select new Stdentfeeclass
                              {
                                  id = a.id.ToString(),

                                  Feegroup = a.name,
                                  amount = a.amount.ToString(),
                                  feecde = a.code,


                              }).ToList();



                //var player = (from p in dbContext.feemasters where p.college_id == college_id select p).ToList();

                ViewBag.feestudetails = player;
            }
            else if (Gender != 0)
            {

                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id
                                     where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.gender == Gender.ToString())

                                     select new StudentInformation
                                     {
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.firstname,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         MobileNumber = a.mobileno,
                                         AadhaarNumber = a.adhar_no,
                                         Medium = a.Medium,
                                         RTE = a.rte



                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;
                var player = (from a in dbContext.fees_discounts
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby a.id
                              select new Stdentfeeclass
                              {
                                  id = a.id.ToString(),

                                  Feegroup = a.name,
                                  amount = a.amount.ToString(),
                                  feecde = a.code,


                              }).ToList();



                //var player = (from p in dbContext.feemasters where p.college_id == college_id select p).ToList();

                ViewBag.feestudetails = player;
            }
            else if (RTE != 0)
            {

                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id
                                     where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.rte == RTE.ToString())

                                     select new StudentInformation
                                     {
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.firstname,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         MobileNumber = a.mobileno,
                                         AadhaarNumber = a.adhar_no,
                                         Medium = a.Medium,
                                         RTE = a.rte



                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;
                var player = (from a in dbContext.fees_discounts
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby a.id
                              select new Stdentfeeclass
                              {
                                  id = a.id.ToString(),

                                  Feegroup = a.name,
                                  amount = a.amount.ToString(),
                                  feecde = a.code,


                              }).ToList();



                //var player = (from p in dbContext.feemasters where p.college_id == college_id select p).ToList();

                ViewBag.feestudetails = player;
            }
            else
            {
                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id
                                     where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.class_id == Class && a.section_id == Section && a.category_id == Category && a.gender == Gender.ToString() && a.rte == RTE.ToString())
                                     select new StudentInformation
                                     {
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.firstname,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         MobileNumber = a.mobileno,
                                         AadhaarNumber = a.adhar_no,
                                         Medium = a.Medium,
                                         RTE = a.rte



                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;
                var player = (from a in dbContext.fees_discounts
                              where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.session_id == listData.academicyear_id && a.is_active == true && a.id == feemaster_grp_type_id)
                              orderby a.id
                              select new Stdentfeeclass
                              {
                                  id = a.id.ToString(),

                                  Feegroup = a.name,
                                  amount = a.amount.ToString(),
                                  feecde = a.code,


                              }).ToList();



                //var player = (from p in dbContext.feemasters where p.college_id == college_id select p).ToList();

                ViewBag.feestudetails = player;
            }
            //   return View(studentlist);


            return View();
        }

        public JsonResult feediscountassignsave(List<student_fees_discounts> allStaff, int feemgpid, string feedescid1)
        {
            int result = 0;

            if (allStaff != null)
            {
                foreach (var staff in allStaff)
                {
                    var player = (from a in dbContext.student_fees_discounts.Where(x => x.college_id == listData.CollegeID && x.year == listData.academic_year && x.student_session_id == staff.student_session_id && x.fees_discount_id == feemgpid && x.is_active == true) select a).FirstOrDefault();
                    if (player == null)
                    {
                        student_fees_discounts obj = new student_fees_discounts
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            student_session_id = staff.student_session_id,
                            fees_discount_id = feemgpid,
                            status = "Paid",
                            description = feedescid1,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user

                        };
                        dbContext.student_fees_discounts.Add(obj);
                        result += dbContext.SaveChanges();
                    }
                    else
                    {
                        student_fees_discounts ft = new student_fees_discounts
                        {

                            student_session_id = staff.student_session_id,
                            fees_discount_id = feemgpid,

                            status = "Paid",
                            description = feedescid1,
                            is_active = true,
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            updated_at = DateTime.Now,
                            modified_by = listData.user

                        };
                        result += dbContext.SaveChanges();
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}