﻿
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartHealth_HDSCL.Controllers
{
    public class FrontCMSController : Controller
    {
        public SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        //public static int CollegeID = 2015, BranchID = 1;
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public FrontCMSController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }


        // GET: FrontCMS
        public ActionResult Page()
        {
            var pageList = dbcontext.front_cms_pages.Select(a => a).ToList();
            //ViewBag.list = pageList;
            return View(pageList);
        }
        
        [HttpGet]
        public ActionResult AddMenu()
        {
            var menuList = dbcontext.front_cms_menus.Select(a => a).ToList();
            ViewBag.list = menuList;
            return View();
        }
        [HttpPost]
        public ActionResult AddMenu(front_cms_menus Model)
        {
            SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
            front_cms_menus menus = new front_cms_menus()
            {
               id=Model.id,
               description=Model.description,
               content_type=Model.content_type,
               slug=Model.slug,
               ext_url=Model.ext_url,
               ext_url_link=Model.ext_url_link,
               is_active=Model.is_active,
               menu=Model.menu,
               open_new_tab=Model.open_new_tab,
               publish=Model.publish             
            };
            dbcontext.front_cms_menus.Add(menus);
            try
            {
                int status = dbcontext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            var menu = (from i in dbcontext.front_cms_menus select i).ToList();
            return View(menu);
        }
       
        [HttpGet]
        public ActionResult MediaManager()
        {
            var media = dbcontext.front_cms_media_gallery.Select(a => a).ToList();
            ViewBag.list = media;
            return View();
        }
        [HttpPost]
        public ActionResult MediaManager(front_cms_media_gallery Model)
        {
            SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
            front_cms_media_gallery mediaManager = new front_cms_media_gallery()
            {
                id=Model.id,
                dir_path=Model.dir_path,
                file_size=Model.file_size,
                file_type=Model.file_type,
                image=Model.image,
                img_name=Model.img_name,
                thumb_name=Model.thumb_name,
                thumb_path=Model.thumb_path,
                vid_title=Model.vid_title,
                vid_url=Model.vid_url
             
            };
            dbcontext.front_cms_media_gallery.Add(mediaManager);
            try
            {
                int status = dbcontext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            var manager = (from i in dbcontext.front_cms_media_gallery select i).ToList();
            return View(manager);
        }
       

        [HttpGet]
        public ActionResult AddBanner()
        {

            try
            {
                var banner =(from fronban in dbcontext.front_cms_media_gallery select fronban.id).ToList();
                if (banner != null)
                {
                   
                    ViewBag.ids = banner;
                }
            }
            catch
            {
                return View();
            }
           
                 
            return View();
        }


        [HttpPost]
        public ActionResult RemoveBanner(int? id)
        {
          try
            {
               front_cms_media_gallery fron = dbcontext.front_cms_media_gallery.Where(i => i.id == id).FirstOrDefault();
                dbcontext.front_cms_media_gallery.Remove(fron);
                int res=dbcontext.SaveChanges();
                             
            }
            catch
            {
                return View("AddBanner");
            }


            return View("AddBanner");
        }

        [HttpPost]
        public ActionResult AddBanner(front_cms_program_photos Model)
        {
            SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
            front_cms_program_photos image = new front_cms_program_photos()
            {
               id=Model.id,
              //Front_cms_programs=Model.Front_cms_programs,
              media_gallery_id=Model.media_gallery_id,
              program_id=Model.program_id
             
            };
            dbcontext.front_cms_program_photos.Add(image);
            try
            {
                int status = dbcontext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            var bannerImage = (from i in dbcontext.front_cms_program_photos select i).ToList();
            return View(bannerImage);
        }
    }
}