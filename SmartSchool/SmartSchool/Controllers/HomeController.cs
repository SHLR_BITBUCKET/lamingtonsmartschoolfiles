﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SmartSchool.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int k = 0;
            string[] allimage = System.IO.Directory.GetFiles(Server.MapPath("~/WebCamImages/"));
            if (allimage.Length > 0)
            {
                string[] fName = new string[allimage.Length];
                foreach (var f in allimage)
                {
                    fName[k] = Path.GetFileName(f);
                    k++;
                }
                ViewBag.Images = fName;
            }

            return View();
        }


        [HttpPost]

        public JsonResult SaveImage(string base64image, string ImgName)
        {
         var res =  SImage(base64image, ImgName);
            if(res == true)
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        public bool SImage(string ImgStr, string ImgName)
        {
            try
            {
                String path = System.Web.HttpContext.Current.Server.MapPath("~/WebCamImages"); //Path
                string imageName = null;
                //Check if directory exist
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }
                if (ImgName == "" || ImgName == null)
                {
                    imageName = DateTime.Now.ToString("ddMMyyyyss");
                }
                else
                {
                    imageName = ImgName + DateTime.Now.ToString("ddMMyyyyss") + ".jpg";
                }

                //set the image path
                string imgPath = Path.Combine(path, imageName);
                string convert = ImgStr.Replace("data:image/jpeg;base64,", String.Empty);
                byte[] imageBytes = Convert.FromBase64String(convert);

                System.IO.File.WriteAllBytes(imgPath, imageBytes);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AngularTest()
        {
            return View();
        }
    }
}