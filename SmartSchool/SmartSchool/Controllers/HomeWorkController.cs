﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using SmartSchool.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity.Migrations;
using System.Web.UI.WebControls;
using System.Net;
using SmartSchool.SchoolMasterClasses;
using System.IO;
using System.Threading.Tasks;
using SmartSchool.Interfaces;
using System.Text.RegularExpressions;
using static SmartSchool.SchoolMasterClasses.StudentInformation;

namespace SmartSchool.Controllers
{
    public class HomeWorkController : Controller
    {
        // GET: HomeWork
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public get_staff_classes gs = new get_staff_classes();
        public custom_master_class cmc = new custom_master_class();
        SmsTextLocalClass SMSClass = new SmsTextLocalClass();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public HomeWorkController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var result = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult student_attendance()
        {
            var getstaffdata = gs.get_staff_wise_classes(0, listData.CollegeID, listData.BranchID, listData.academic_year,  listData.roleid);
            IQueryable<AddClass> iq = dbContext.AddClasses;
            List<SelectListItem> lc = new List<SelectListItem>();
            foreach (var group in getstaffdata)
            {
                int clsid = Convert.ToInt32(group.class_id);
                lc.Add(iq.OrderBy(a => a.class_name).Where(k => k.class_id == clsid).Select(b => new SelectListItem { Value = b.class_id.ToString(), Text = b.class_name }).Distinct().FirstOrDefault());
            }
            ViewBag.cls = lc;
            return View();
        }
        [HttpPost]
        public JsonResult callgetsecsub(int? ddlclass, int? ddlsection)
        {
            int? stafid = gs.getStaffId(listData.CollegeID, listData.BranchID, listData.academic_year);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            var result = dbContext.getSubjects(listData.CollegeID, listData.BranchID, stafid, ddlclass, ddlsection, listData.academic_year, listData.roleid).ToList();

            foreach (var re in result)
            {
                selectLists.Add(new SelectListItem
                {
                    Text = re.name,
                    Value = re.id.ToString()
                });
            }
            SelectListItem st = new SelectListItem()
            {
                Text = "Select",
                Value = "0"
            };
            selectLists.Insert(0, st);
            return Json(selectLists, JsonRequestBehavior.AllowGet);
        }
      
        private void bind_class_rooms()
        {
            var userlist = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.cls = userlist;
        }

        [HttpGet]
        public ActionResult evaluation_report()
        {
            bind_class_rooms();
            ViewBag.GetEvaluationReport = homeworkEvaluationData(null);
            return View();
        }

        private List<evaluation_report> homeworkEvaluationData(int? homeworkid)
        {
            int i = 0;
            List<evaluation_report> modalevaluation = new List<evaluation_report>();
            if (homeworkid > 0)
            {
                modalevaluation = dbContext.Pro_GHWER(listData.CollegeID, listData.BranchID, listData.academic_year, homeworkid).Select(k => new evaluation_report {
                    homework_id = k.homework_id,
                    FirstName = k.firstname,
                    LastName = k.lastname,
                    stringDate = k.stringDate,
                    class_id  = k.class_id,
                    section_id = k.section_id,
                    name = k.name,
                    admission_no = k.admission_no,
                    Description = k.Description,
                    Assigned_By = k.Assigned_by,
                    Evaluated_by = k.Evaluated_by,
                    homework_date = k.homework_date,
                    submit_date = k.submit_date,
                    class_name = k.class_name,
                    Section = k.section,
                    created_by = k.created_by

                }).ToList();
            }
            else
            {
                modalevaluation = (from a in dbContext.homework
                                   join b in dbContext.homework_evaluation on a.id equals b.homework_id into leftjoin
                                   from lj in leftjoin.DefaultIfEmpty()
                                   join c in dbContext.subjects on a.subject_id equals c.id
                                   join d in dbContext.staffs on a.evaluated_by equals d.id into leftjoinstaff
                                   from ljs in leftjoinstaff.DefaultIfEmpty()
                                   where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                   group new { a, lj, c, ljs } by new { lj.homework_id, a.class_id, a.section_id, ljs.name }
                            into gr

                                   select new SchoolMasterClasses.evaluation_report
                                   {
                                       name = gr.Select(k => k.c.name).FirstOrDefault(),
                                       homework_date = gr.Select(k => k.a.homework_date).FirstOrDefault(),
                                       date = gr.Select(k => k.lj.date).FirstOrDefault(),
                                       Evaluated_by = gr.Select(k => k.ljs.name).FirstOrDefault() == null ? "Evaluation Pending Yet." : gr.Select(k => k.ljs.name).FirstOrDefault(),
                                       submit_date = gr.Select(k => k.a.submit_date).FirstOrDefault(),
                                       StudCount = gr.Select(k => k.lj.homework_id).FirstOrDefault() == 0 ? 0 : gr.Count(),
                                       class_id = gr.Select(k => k.a.class_id).FirstOrDefault(),
                                       section_id = gr.Select(k => k.a.section_id).FirstOrDefault(),
                                       subject_id = gr.Select(k=>k.c.id).FirstOrDefault(),
                                       id = gr.Select(k => k.a.id).FirstOrDefault(),

                                   }).ToList();

                foreach (var me in modalevaluation)
                {
                    int? total_students = dbContext.students.Where(k => k.class_id == me.class_id && k.section_id == me.section_id && k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).Select(k => k.id).Count();
                    int? get_pending_students = (total_students - me.StudCount);
                    modalevaluation[i].PendingStudCount = get_pending_students;
                    int? evaluatedStudCount = me.StudCount;
                    double? percentages = ((double)evaluatedStudCount / (double)total_students) * 100;
                    modalevaluation[i].percentage = (int)Math.Ceiling((double)percentages);
                    i++;
                }
            }
            return modalevaluation;
        }

        public ActionResult getHomeworkEvalutionDataById(int id)
        {
            ModelState.Clear();
            List<evaluation_report> ler = homeworkEvaluationData(id).ToList();
            return PartialView(ler);
        }

        [HttpPost]
        public ActionResult evaluation_report(Models.homework HMevaluation, int? class_id, int? section_id, int? subject_id)
        {
            bind_class_rooms();
            ViewBag.GetEvaluationReport = homeworkEvaluationData(null).Where(k => k.class_id == class_id && k.section_id == section_id && k.subject_id == subject_id).ToList();
            ViewBag.sectionid = section_id;
            ViewBag.subjectid = subject_id;
            return View();
        }

        [HttpGet]
        public ActionResult Editevaluation_report(int? Id)
        {
            var data = dbContext.homework_evaluation.Where(a => a.id == Id).Select(pp => pp).FirstOrDefault();
            return View(data);
        }

        [HttpPost]
        public ActionResult Editevaluation_report(homework_evaluation HMevaluation, int? Id)
        {
            var data = dbContext.homework_evaluation.Where(a => a.id == Id).Select(pp => pp).FirstOrDefault();
            if (data.id == HMevaluation.id)
            {
                data.homework_id = HMevaluation.homework_id;
                data.student_id = HMevaluation.student_id;
                data.date = HMevaluation.date;
                data.status = HMevaluation.status;
                data.college_id = HMevaluation.college_id;
                data.branch_id = HMevaluation.branch_id;
                data.created_by = HMevaluation.created_by;
                data.modified_by = HMevaluation.modified_by;
            };
            dbContext.homework_evaluation.AddOrUpdate(data);
            dbContext.SaveChanges();
            return RedirectToAction("evaluation_report");
        }

        [HttpGet]
        public ActionResult Deleteevaluation_report(homework_evaluation HMevaluation, int? Id)
        {
            Delete_HomeWork_Evaluation_When_Update(Id);
            return RedirectToAction("evaluation_report");
        }


        [HttpPost]
        public JsonResult callgetsecto(string ddlclass)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlclass))
            {
                int classid = Convert.ToInt32(ddlclass);
                result = cmc.get_class_sec(classid, listData.CollegeID, listData.BranchID, listData.academic_year);
                dbContext.Configuration.ProxyCreationEnabled = false;
                result = (from s in dbContext.sections
                          join cs in dbContext.class_sections on s.id equals cs.section_id
                          where cs.class_id == classid
                          select new
                          {
                              section = s.section1,
                              section_id = cs.section_id

                          }).ToList();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult callgetsec1(string ddlclass1)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlclass1))
            {
                int classid = Convert.ToInt32(ddlclass1);
                result = cmc.get_class_sec(classid, listData.CollegeID, listData.BranchID, listData.academic_year);
                dbContext.Configuration.ProxyCreationEnabled = false;
                result = (from s in dbContext.sections
                          join cs in dbContext.class_sections on s.id equals cs.section_id
                          where cs.class_id == classid
                          select new
                          {
                              section = s.section1,
                              section_id = cs.section_id

                          }).ToList();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

       

        [HttpGet]
        public ActionResult Add_homework(homework_evaluation model1, int? id, int? class_id1, int? section_id1, int? subject_id1)
        {
            bind_class_rooms();
            var list = (from a in dbContext.homework
                        join b in dbContext.AddClasses on a.class_id equals b.class_id
                        join c in dbContext.sections on a.section_id equals c.id
                        join d in dbContext.subjects on a.subject_id equals d.id
                        join e in dbContext.homework_evaluation on a.id equals e.homework_id into evaldate
                        from f in evaldate.DefaultIfEmpty()
                        where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                        select new SchoolMasterClasses.StuClass
                        {
                            id = a.id,
                            class1 = b.class_name,
                            section1 = c.section1,
                            section_id = c.id,
                            Class_id = b.class_id,
                            name = d.name,
                            homeworkdate = a.homework_date,
                            submissiondate = a.submit_date,
                            evaluationdate = f.date,
                            evaluatedby = a.evaluated_by,
                            Description = a.description,
                            subject_id = a.subject_id,
                            Doc = a.document

                        }
                        ).Distinct().ToList();
            ViewBag.homework = list;

            return View();
        }

        public JsonResult HomeWorkajaxupdate(int values, string disid)
        {
            return Json(new { responsetext = "Updated" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> homework_List(int? id, homework_evaluation model, int cls, int sec, int sub, int? class_id, int? section_id, int? subject_id, int hmid)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.pop11 = "1";

           var pr = (from a in dbContext.homework
                          //join c in dbContext.homework_evaluation on a.id equals c.homework_id
                      join b in dbContext.subjects on a.subject_id equals b.id
                      //join d in dbContext.students on c.student_id equals d.user_id
                      join e in dbContext.AddClasses on a.class_id equals e.class_id
                      join f in dbContext.sections on a.section_id equals f.id
                      //join g in dbContext.roles on a.created_by equals g.roll_name
                      where a.class_id == cls && a.section_id == sec && a.subject_id == sub && a.id == hmid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                      select new SchoolMasterClasses.evaluation_report
                      {
                          name = b.name,
                          homework_date = a.homework_date,
                          created_by = a.created_by,
                          modified_by = a.modified_by,
                          Classes = e.class_name,
                          Section = f.section1,
                          Desription = a.description,
                          submit_date = a.submit_date,
                          createdate = a.create_date,
                          Doc = a.document
                      }).Distinct().ToList();

            ViewBag.list11 = pr.Take(1);
            var list1 = (from a in dbContext.students.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.class_id == cls && x.section_id == sec) select new SchoolMasterClasses.StuClass { firstname = a.firstname, userid = a.user_id }).ToList();
            ViewBag.studhome = list1;

            var pro = (from a in dbContext.homework_evaluation
                       join c in dbContext.homework on a.homework_id equals c.id
                       join g in dbContext.staffs on c.evaluated_by equals g.id
                       where a.homework_id == hmid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                       select new SchoolMasterClasses.evaluation_report
                       {
                           Evaluated_by = g.name,
                           evaluationdate = a.date,
                           createdate = c.create_date,
                           Doc = c.document
                       }).Distinct().ToList();
            ViewBag.newlist = pro.Take(1);

            var getstulist = (from a in dbContext.homework_evaluation
                              join b in dbContext.homework on a.homework_id equals b.id
                              join c in dbContext.students on b.class_id equals c.class_id
                              where (b.section_id == c.section_id && a.student_id == c.user_id && a.homework_id == id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                              select new Studentlist
                              {
                                  student_id = a.student_id,
                                  firstname = c.firstname
                              }).ToList();

            ViewBag.stulist = getstulist;
            bind_class_rooms();
            return View("add_homework");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_homework(Models.homework model, int? class_id1, int? section_id1, int? subject_id1, HttpPostedFileBase FileUpload)
        {
            bind_class_rooms();
            List<StaffInformation> staffInformation = cmc.get_staffid_rolewise(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user);
            if (class_id1 != null)
            {
                var list = (from a in dbContext.homework
                            join b in dbContext.AddClasses on a.class_id equals b.class_id
                            join c in dbContext.sections on a.section_id equals c.id
                            join d in dbContext.subjects on a.subject_id equals d.id
                            join e in dbContext.homework_evaluation on a.id equals e.homework_id into evaldate
                            from f in evaldate.DefaultIfEmpty()
                            where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.section_id == section_id1 && a.class_id == class_id1 && a.subject_id == subject_id1
                            select new SchoolMasterClasses.StuClass
                            {
                                id = a.id,
                                class1 = b.class_name,
                                section1 = c.section1,
                                section_id = c.id,
                                Class_id = b.class_id,
                                name = d.name,
                                homeworkdate = a.homework_date,
                                submissiondate = a.submit_date,
                                evaluationdate = f.date,
                                evaluatedby = a.evaluated_by,
                                Description = a.description,
                                subject_id = a.subject_id,
                                Doc = a.document

                            }
                        ).Distinct();
                ViewBag.homework = list;
                ViewBag.sectionid = section_id1;
            }
            else
            {
                try
                {
                    string filesnames = "";
                    if (FileUpload != null)
                    {
                        string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                        FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Homework/"), savedFileName));    // Save the file
                        filesnames = savedFileName;
                    }
                    Models.homework obj = new Models.homework()
                    {
                        class_id = model.class_id,
                        section_id = model.section_id,
                        subject_id = model.subject_id,
                        homework_date = model.homework_date,
                        submit_date = model.submit_date,
                        document = filesnames,
                        description = model.description,
                        staff_id = staffInformation.Select(k => k.id).SingleOrDefault(),
                        created_by = listData.user,
                        create_date = DateTime.Now,
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        year = listData.academic_year
                    };
                    dbContext.homework.Add(obj);
                    int c = dbContext.SaveChanges();
                    if (c > 0)
                    {
                        List<string> MobNumbers = dbContext.students.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.class_id == model.class_id && k.section_id == model.section_id).Select(k => k.mobileno).ToList();
                        SMSClass.send_message(MobNumbers.ToArray(), "Dear Parent, Home work details: " + model.description + "", listData.CollegeID, listData.BranchID, listData.academic_year);
                        TempData["message"] = "Data Saved Successfully";                       
                        return RedirectToAction("add_homework");
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e);
                    TempData["message"] = "There was an error while saving data, please contact admin";
                    ExceptionLogging.LogException(e);
                }
            }
            return View("add_homework");
        }

        [HttpGet]
        public ActionResult EditHomework(int? Id)
        {
            var edit = dbContext.homework.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            bind_class_rooms();
            return View(edit);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditHomework(Models.homework model, int? Id,  HttpPostedFileBase FileUpload)
        {
            using (var dbTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    List<StaffInformation> staffInformation = cmc.get_staffid_rolewise(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user);
                    string filesnames = "";
                    if (FileUpload != null)
                    {
                        string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                        FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Homework/"), savedFileName)); // Save the file
                        filesnames = savedFileName;
                    }
                    var edit = dbContext.homework.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                    edit.homework_date = model.homework_date;
                    edit.submit_date = model.submit_date;
                    edit.modified_by = listData.user;
                    edit.created_by = model.created_by;
                    edit.college_id = listData.CollegeID;
                    edit.branch_id = listData.BranchID;
                    edit.staff_id = staffInformation.Select(k => k.id).SingleOrDefault();
                    edit.class_id = model.class_id;
                    edit.section_id = model.section_id;
                    edit.subject_id = model.subject_id;
                    edit.description = model.description;
                    edit.document = filesnames;
                    dbContext.homework.AddOrUpdate(edit);
                    int done = dbContext.SaveChanges();
                    if (done > 0)
                    {
                        Delete_HomeWork_Evaluation_When_Update(Id);
                        List<string> MobNumbers = dbContext.students.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.class_id == model.class_id && k.section_id == model.section_id).Select(k => k.mobileno).ToList();
                        SMSClass.send_message(MobNumbers.ToArray(), "Dear Parent, Home work details: " + model.description + "", listData.CollegeID, listData.BranchID, listData.academic_year);
                        TempData["message"] = "Data Edited Successfully, Please Evaluate the Home work Once Students Completed.. Thank You!";
                        dbTransaction.Commit();
                        return RedirectToAction("add_homework");
                    }
                }
                catch (Exception ex)
                {
                    TempData["message"] = "There is an Error While editing the Page..Please Contact Admin?";
                    ExceptionLogging.LogException(ex);
                    dbTransaction.Rollback();
                    return View("add_homework");
                }
            }
            return View("add_homework");
        }

        public void Delete_HomeWork_Evaluation_When_Update(int? Id)
        {
            var data = dbContext.homework_evaluation.Where(a => a.homework_id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            data.ForEach(k => k.modified_by = listData.user);
            data.ForEach(k => k.modified_dt = DateTime.Now);
            dbContext.SaveChanges();
            dbContext.homework_evaluation.RemoveRange(data);
            int s = dbContext.SaveChanges();
            if(s > 0)
            {
                TempData["message"] = "Evaluation Revoked";
            }
        }


        public ActionResult Detailswork(Models.homework model, int Id)
        {
            var data = dbContext.homework.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
            data.homework_date = model.homework_date;
            data.submit_date = model.submit_date;
            data.created_by = model.created_by;
            data.evaluated_by = model.evaluated_by;
            data.class_id = model.class_id;
            data.section_id = model.section_id;
            data.subject_id = model.subject_id;
            data.description = model.description;
            data.staff_id = model.staff_id;
            dbContext.homework.Remove(data);

            return View();



        }


        [HttpGet]
        public ActionResult DeleteHomework(int Id)
        {
            using (var dbTransaction = dbContext.Database.BeginTransaction())
            {
                if (Id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var data = dbContext.homework.Where(a => a.id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                data.modified_by = listData.user;
                data.modified_dt = DateTime.Now;
                dbContext.SaveChanges();
                if (data == null)
                {
                    return HttpNotFound();
                }
                dbContext.homework.Remove(data ?? throw new InvalidOperationException());
                try
                {
                    int c = dbContext.SaveChanges();
                    if (c > 0)
                    {
                        Delete_HomeWork_Evaluation_When_Update(Id);
                        dbTransaction.Commit();
                        TempData["message"] = "Data Deleted Successfully";
                        return RedirectToAction("add_homework");
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e);
                    dbTransaction.Rollback();
                    TempData["message"] = "There was an error while deleting data, please contact admin";
                    ExceptionLogging.LogException(e);
                }
            }
            return RedirectToAction("add_homework");
        }



        [HttpPost]
        public JsonResult Homework_evaluation(List<homework_evaluation> alldata, int? homework_id, string student_id, Models.homework model)
        {
            int? keepHomeworkid = null;
            Delete_HomeWork_Evaluation_When_Update(alldata[0].homework_id);
            List<StaffInformation> staffInformation = cmc.get_staffid_rolewise(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user);
            foreach (var alval in alldata)
            {
                var data = (from a in dbContext.homework_evaluation where a.homework_id == alval.homework_id && a.student_id == alval.student_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
                if (data == null)
                {
                    homework_evaluation obj = new homework_evaluation()
                    {
                        homework_id = alval.homework_id,
                        student_id = alval.student_id,
                        date = alval.date,
                        status = true,
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        created_by = listData.user,
                        year = listData.academic_year
                    };
                    dbContext.homework_evaluation.AddOrUpdate(obj);
                }
                else
                {
                    data.homework_id = alval.homework_id;
                    data.student_id = alval.student_id;
                    data.date = alval.date;
                    data.status = true;
                    data.college_id = listData.CollegeID;
                    data.branch_id = listData.BranchID;
                    data.modified_by = listData.user;
                }
                keepHomeworkid = alval.homework_id;
            }
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                var dataa = (from a in dbContext.homework where a.id == keepHomeworkid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
                dataa.evaluated_by = staffInformation.Select(k => k.id).SingleOrDefault();
                dataa.modified_by = listData.user;
                dbContext.homework.AddOrUpdate(dataa);
                int c = dbContext.SaveChanges();
                if (c != 0)
                {
                    return Json(new { response = "Success" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { response = "Success" }, JsonRequestBehavior.AllowGet);
        }
    }
}