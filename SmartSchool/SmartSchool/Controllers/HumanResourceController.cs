﻿using SmartSchool.Models;
using System.Web.Mvc;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System;
using System.Net;
using System.Data;
using System.Web;
using System.IO;
using System.Data.Entity.Migrations;
using SmartSchool.SchoolMasterClasses;
using SmartSchool.Classes;
//using OfficeOpenXml;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity.Owin;
using SmartSchool;
using System.Threading.Tasks;
using SmartSchool.Controllers;
using SmartSchool.Interfaces;

namespace SmartHealth_HDSCL.Controllers
{
    public class HumanResourceController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public SmsTextLocalClass sms = new SmsTextLocalClass();
        public class leavelist
        {
            public int id { get; set; }
            public string type { get; set; }
        }


        public HumanResourceController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in HumanResourceController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }
        public string Image;
        public int used;
        // GET: HumanResource
        public ActionResult StaffIndex(int? id)
        {
            var staffList = (from a in dbContext.staffs 
                             join b in dbContext.AspNetRoles on a.roll_id equals b.Id into lj
                             from ljq in lj.DefaultIfEmpty()
                             join c in dbContext.departments on a.department equals c.id into ljj
                             from j in ljj.DefaultIfEmpty()
                             join d in dbContext.staff_designation on a.designation equals d.id  into ljd
                             from ljs in ljd.DefaultIfEmpty()
                             where a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                             select new SmartSchool.SchoolMasterClasses.StaffInformation
                             {
                                 id = a.id,
                                 employee_id = a.employee_id,
                                 name = a.name,
                                 rolename = ljq.Name,
                                 department = j.department_name,
                                 designation = ljs.designation,
                                 contact_no = a.contact_no

                             }).ToList();
            Bind_Roles();
            ViewBag.stafflist = staffList;

            return View();
        }

        public class Timetabledetails
        {
            public string Subjectname { get; set; }
            public string Day { get; set; }
            public string Starttime { get; set; }
            public string Endtime { get; set; }
            public int? day_order { get; set; }
        }
        public ActionResult StaffTimetableDisplay()
        {
            var timetabledetails = (from a in dbContext.timetables
                                    join b in dbContext.teacher_subjects on a.teacher_subject_id equals b.id
                                    join c in dbContext.subjects on b.subject_id equals c.id
                                    join d in dbContext.staffs on b.teacher_id equals d.id
                                    where a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && d.email == "akshay@shlrtechnosoft.com"
                                    select new Timetabledetails
                                    {
                                        Subjectname = c.name,
                                        Day = a.day_name,
                                        Starttime = a.start_time,
                                        Endtime = a.end_time,
                                        day_order = a.day_order
                                        
                                    }).ToList().GroupBy(a=>a.day_order);

            Array _array = Array.CreateInstance(typeof(string), timetabledetails.Count(), timetabledetails.Count());
            int i = 0, j = 0, k = 0;
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            string[,,] array2Db = new string[15, 15, 15];
            foreach (var it in timetabledetails)
            {
                //_array.SetValue(it.Day, i, j);
                //_array.SetValue(it.Starttime, i, j + 1);
                //_array.SetValue(it.Subjectname, i, j + 2);
                i++;

            }
            ViewBag.tmeitms = _array;

            //var h = _array.GetLowerBound(0);
            //var kh = _array.GetUpperBound(1);


            //for(int ip=0;ip<_array.Length;ip++)
            //{
            //    for (int kp = 0; k < 4; k++)
            //    {
            //        var t = _array.GetValue(ip, i);
            //    }

            //}


            return View();
        }

        [HttpPost]
        public ActionResult StaffIndex(FormCollection FC)
        {
            Bind_Roles();

            string searchKey = FC["search"].ToString().Trim();

            dynamic staffList;
            if (FC["Role"].ToString() != "")
            {
                string roleId = FC["Role"].ToString();

                staffList = (from a in dbContext.staffs
                             join b in dbContext.AspNetRoles on a.roll_id equals b.Id
                             join c in dbContext.departments on a.department equals c.id
                             join d in dbContext.staff_designation on a.designation equals d.id

                             where a.roll_id == roleId && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year


                             select new SmartSchool.SchoolMasterClasses.StaffInformation
                             {
                                 id = a.id,
                                 employee_id = a.employee_id,
                                 name = a.name,
                                 rolename = b.Name,
                                 department = c.department_name,
                                 designation = d.designation,
                                 contact_no = a.contact_no

                             }).ToList();
            }
            else
            {
                staffList = (from a in dbContext.staffs
                             join b in dbContext.AspNetRoles on a.roll_id equals b.Id
                             join c in dbContext.departments on a.department equals c.id
                             join d in dbContext.staff_designation on a.designation equals d.id
                             where (a.name.ToLower().Contains(searchKey.ToLower()) || a.employee_id.Contains(searchKey)) && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                             select new SmartSchool.SchoolMasterClasses.StaffInformation
                             {
                                 id = a.id,
                                 employee_id = a.employee_id,
                                 name = a.name,
                                 rolename = b.Name,
                                 department = c.department_name,
                                 designation = d.designation,
                                 contact_no = a.contact_no

                             }).ToList();
            }
            ModelState.Clear();
            ViewBag.stafflist = staffList;

            return View();
        }

        private void BindCategory()
        {
            List<category> c = dbContext.categories.Where(x => x.id != 0).ToList();
            category sem = new category
            {
                category1 = "Please select category",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "category1 ", 0);
            ViewBag.viewbagcatlist = selectcat;
        }

        private void Bind_Roles()
        {
            var list = dbContext.AspNetRoles.Where(a => a.Name != "Student" && a.Name != "SuperAdmin" && a.Name != "Parent").OrderBy(a => a.Id).ToList().Select(b => new SelectListItem { Value = b.Id.ToString(), Text = b.Name.ToString() }).ToList();
            ViewBag.roll_id = list;

        }
        private void BindDepartment()
        {
            var list = dbContext.departments.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).OrderBy(a => a.id).ToList().Select(b => new SelectListItem { Value = b.id.ToString(), Text = b.department_name.ToString() }).ToList();
            ViewBag.department = list;

        }

        private void BindDesignation()
        {
            var list = dbContext.staff_designation.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).OrderBy(a => a.id).ToList().Select(b => new SelectListItem { Value = b.id.ToString(), Text = b.designation.ToString() }).ToList();
            ViewBag.designation = list;

        }

        [HttpGet]
        public ActionResult StaffProfile(int? id)
        {
            TempData["id"] = id;
            ViewBag.timeline = dbContext.staff_timeline.OrderByDescending(a => a.timeline_date).Where(a => a.staff_id == id && a.is_active == true).ToList();
            var staffList = (from a in dbContext.staffs
                             join b in dbContext.AspNetRoles on a.roll_id equals b.Id
                             join e in dbContext.AspNetUsers on a.email equals e.Email
                             join c in dbContext.departments on a.department equals c.id
                             join d in dbContext.staff_designation on a.designation equals d.id
                             where a.id == id && a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                             select new SmartSchool.SchoolMasterClasses.StaffInformation
                             {
                                 id = a.id,
                                 employee_id = a.employee_id,
                                 name = a.name,
                                 surname = a.surname,
                                 father_name = a.father_name,
                                 mother_name = a.mother_name,
                                 rolename = b.Name,
                                 contact_no = a.contact_no,
                                 emergency_contact_no = a.emergency_contact_no,
                                 email = a.email,
                                 dob = a.dob,
                                 marital_status = a.marital_status,
                                 gender = a.gender,
                                 date_of_joining = a.date_of_joining,
                                 date_of_leaving = a.date_of_leaving,
                                 image = a.image,
                                 local_address = a.local_address,
                                 permanent_address = a.permanent_address,
                                 qualification = a.qualification,
                                 note = a.note,
                                 work_exp = a.work_exp,
                                 account_title = a.account_title,
                                 bank_account_no = a.bank_account_no,
                                 ifsc_code = a.ifsc_code,
                                 bank_name = a.bank_name,
                                 bank_branch = a.bank_branch,
                                 resume = a.resume,
                                 joining_letter = a.joining_letter,
                                 other_document_file = a.other_document_file,
                                 facebook = b.Name,
                                 user_id = Convert.ToInt32(e.Id),
                                 department = c.department_name,
                                 designation = d.designation,
                                 is_active = true

                             }).FirstOrDefault();

            //---------------- Leaves -----------------------//

            ViewBag.leaveList = (from a in dbContext.staff_leave_details
                                 join b in dbContext.staff_leave_request on a.staff_id equals b.staff_id
                                 join c in dbContext.leave_types on b.leave_type_id equals c.id
                                 where a.staff_id == id && a.leave_type_id == c.id && b.status == "Approve" && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                                 select new SmartSchool.SchoolMasterClasses.staffLeaves
                                 {
                                     id = b.id,
                                     //name = b.name,
                                     leave_type_id = a.leave_type_id,
                                     leave_from = b.leave_from,
                                     leave_to = b.leave_to,
                                     leavetype = c.type,
                                     leave_days = b.leave_days,
                                     date = b.date,
                                     alloted_leave = a.alloted_leave,
                                     Available = a.alloted_leave - b.leave_days,
                                     status = "Approve",
                                     used = a.alloted_leave - (a.alloted_leave - b.leave_days),
                                 }).Distinct().ToList();

            //--------------------------- Attendance ----------------------------/

            ViewBag.present = dbContext.staff_attendance.Where(a => a.staff_id == id && a.staff_attendance_type_id == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Count();
            ViewBag.absent = dbContext.staff_attendance.Where(a => a.staff_id == id && a.staff_attendance_type_id == 3 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Count();
            ViewBag.halfday = dbContext.staff_attendance.Where(a => a.staff_id == id && a.staff_attendance_type_id == 4 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Count();
            ViewBag.late = dbContext.staff_attendance.Where(a => a.staff_id == id && a.staff_attendance_type_id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Count();
            ViewBag.holiday = dbContext.staff_attendance.Where(a => a.staff_id == id && a.staff_attendance_type_id == 5 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Count();

            int[,] att_status = new int[31, 12];
            for (int i = 0; i < 31; i++)
            {
                int mday = i + 1;
                var list = dbContext.staff_attendance.Where(a => a.staff_id == id && a.date.Day == mday && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).OrderBy(a => a.date).ToList();
                int k = 0;

                if (list.Count > 0)
                {
                    foreach (var day in list)
                    {
                        for (int j = k + 0; j < 12; j++)
                        {
                            int month = j + 1;
                            if (month == day.date.Month)
                            {
                                att_status[i, j] = day.staff_attendance_type_id;

                                k = month;
                                break;
                            }
                            else
                            {
                                att_status[i, j] = 0;
                            }

                        }
                    }
                    if (k < 12)
                    {
                        for (int j = k; j < 12; j++)
                        {
                            att_status[i, j] = 0;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < 12; j++)
                    {

                        att_status[i, j] = 0;

                    }
                }

            }
            ViewBag.a = att_status;
            ViewBag.day1 = dbContext.staff_attendance.Where(a => a.staff_id == id && a.date.Day == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).OrderBy(a => a.date).ToList();
            return View(staffList);
        }

        [HttpPost]
        public bool Getalloted(int leavess)
        {
            bool result;
            if (leavess == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public ActionResult AddStaff()
        {
            Bind_Roles();
            BindDepartment();
            BindDesignation();

            var leavelists = dbContext.leave_types.Where(a => a.is_active == true && a.branch_id == listData.BranchID && a.college_id == listData.CollegeID && a.year == listData.academic_year)
             .Select(a => new leavelist
             {
                 id = a.id,
                 type = a.type,

             }).Distinct().ToList();
            ViewBag.leavelists = leavelists;
            var leaveLists1 = (from a in dbContext.staff_leave_details
                               join b in dbContext.leave_types on a.leave_type_id equals b.id
                               join c in dbContext.staffs on a.staff_id equals c.id
                               where b.is_active == true && b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year
                               select new SmartSchool.SchoolMasterClasses.staffLeaveDetails
                               {
                                   id = a.id,
                                   leaveTypeId = b.id,
                                   alloted_leave = a.alloted_leave,
                                   staffId = c.id
                               }).ToList();

            ViewBag.list = leaveLists1;
            return View();
        }


        private void CreateRoles(string email, string mobileno, string rolid)
        {
            var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new Microsoft.AspNet.Identity.RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new Microsoft.AspNet.Identity.UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var rn = roleManager.FindById(rolid);
            if (!(String.IsNullOrEmpty(email)))
            {
                var user = new ApplicationUser();
                user.UserName = email;
                user.Email = email;
                user.PhoneNumber = mobileno;
                string pwds = mobileno;
                var checker = UserManager.Create(user, pwds);
                if (checker.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, rn.Name);
                    string SMSRegards = "Shubada";
                    string message = "Dear  Staff Your profile has been created successfully.To Check Details Visit www.smartschool.shlrtechnosoft.com " + " User Name: " + email + Environment.NewLine + "Password: " + mobileno + Environment.NewLine + "By Regards" + Environment.NewLine + SMSRegards + ".";
                    send_message(mobileno, message);
                }
            }
        }


        public bool send_message(string numbers, string _message)
        {
            string _sendrid = null, _hash = null, removehtml = null, _username = null, _password = null;

            var listdata = sms.get_provider(_college_id: listData.CollegeID, _branch_id: listData.BranchID, academicyear: listData.academic_year);
            if (listdata != null)
            {
                if (listdata.type == "1")
                {
                    _sendrid = listdata.senderid;
                    _username = listdata.username;
                    _password = listdata.password;
                }
                else
                {
                    _sendrid = listdata.senderid;
                    _hash = listdata.authkey;
                    _username = listdata.username;
                }

            }

            removehtml = Regex.Replace(_message, "<.*?>", String.Empty);

            if (numbers != null && numbers.Length == 10)
            {
                string MobNum = numbers.Trim();
                string Message = removehtml.TrimEnd();
                string UserName = _username;
                string Password = _password;
                string SenderID = _sendrid;

                //Currently using smshouse sms provider, if you want to use Textlocal need to enable it.
                string strUrl = "http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=" + UserName + "&password=" + Password + "&sender=" + SenderID + "&mobileno=" + MobNum + "&msg=" + Message;

                WebRequest request = HttpWebRequest.Create(strUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
            }
            else
            {
                return false;
            }

            return true;
        }

        [HttpPost]
        public ActionResult AddStaff(staff Model, HttpPostedFileBase photo, HttpPostedFileBase resume, HttpPostedFileBase joining_letter, HttpPostedFileBase other_document_file, int[] leaves, int[] lids)
        {
            Bind_Roles();
            BindDepartment();
            BindDesignation();
            try
            {
                if (ModelState.IsValid)
                {
                    string Image = "";
                    if (photo != null)
                    {
                        string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        Image = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(photo.FileName);
                        photo.SaveAs(Path.Combine(dirPath, Image)); // Save the file

                    }

                    string Resume = "";
                    if (resume != null)
                    {
                        string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        Resume = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(resume.FileName);
                        resume.SaveAs(Path.Combine(dirPath, Resume)); // Save the file

                    }

                    string J_letter = "";
                    if (joining_letter != null)
                    {
                        string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        J_letter = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(joining_letter.FileName);
                        joining_letter.SaveAs(Path.Combine(dirPath, J_letter)); // Save the file

                    }

                    string Other_doc = "";
                    if (other_document_file != null)
                    {
                        string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        Other_doc = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(other_document_file.FileName);
                        other_document_file.SaveAs(Path.Combine(dirPath, Other_doc)); // Save the file

                    }
                    staff st = new staff()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        employee_id = Model.employee_id,
                        kgid_employee_no = Model.kgid_employee_no,
                        roll_id = Model.roll_id,
                        designation = Model.designation,
                        department = Model.department,
                        name = Model.name,
                        surname = Model.surname,
                        father_name = Model.father_name,
                        mother_name = Model.mother_name,
                        gender = Model.gender,
                        marital_status = Model.marital_status,
                        dob = Model.dob,
                        date_of_joining = Model.date_of_joining,
                        contact_no = Model.contact_no,
                        email = Model.email,
                        emergency_contact_no = Model.emergency_contact_no,
                        local_address = Model.local_address,
                        permanent_address = Model.permanent_address,
                        qualification = Model.qualification,
                        work_exp = Model.work_exp,
                        note = Model.note,
                        created_at = DateTime.Now,
                        image = Image,
                        account_title = Model.account_title,
                        bank_account_no = Model.bank_account_no,
                        bank_name = Model.bank_name,
                        ifsc_code = Model.ifsc_code,
                        aadhar_no = Model.aadhar_no,
                        bank_branch = Model.bank_branch,
                        resume = Resume,
                        joining_letter = J_letter,
                        other_document_file = Other_doc,
                        is_active = true,
                        user_id = Model.email,
                        password = Model.contact_no,
                        year = listData.academic_year,
                        created_by = listData.user
                    };
                    dbContext.staffs.Add(st);
                    int save = dbContext.SaveChanges();

                    int i = 0;
                    foreach (int val in leaves)
                    {
                        staff_leave_details sl = new staff_leave_details()
                        {
                            staff_id = st.id,
                            leave_type_id = lids[i],
                            alloted_leave = val,
                            created_at = DateTime.Now,
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            created_by = listData.user,
                            year = listData.academic_year

                        };
                        dbContext.staff_leave_details.Add(sl);
                        int Save = dbContext.SaveChanges();
                        i++;
                    }

                    if (save > 0)
                    {
                        string roll_id = null, userid = null, email = null, mobileno = null;
                        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(cs))
                        {
                            string query = "SELECT * FROM  [SmartSchoolHD].[dbo].[staff] where id = " + st.id;
                            SqlCommand cmd = new SqlCommand(query, con);
                            con.Open();
                            DataTable dt2 = new DataTable();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt2);
                            if (dt2.Rows.Count > 0)
                            {
                                userid = dt2.Rows[0]["employee_id"].ToString();
                                email = dt2.Rows[0]["email"].ToString();
                                mobileno = dt2.Rows[0]["contact_no"].ToString();
                                roll_id = dt2.Rows[0]["roll_id"].ToString();
                            }
                            con.Close();
                        }
                        user use = new user()
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            user_id = userid,
                            username = email,
                            password = mobileno,
                            role = "Staff",
                            childs = "1",
                            verification_code = "1",
                            is_active = true,
                            created_date = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year
                        };
                        dbContext.users.Add(use);
                        save = dbContext.SaveChanges();

                        CreateRoles(email, mobileno, roll_id);
                        TempData["result"] = "Record inserted successfully";
                        ModelState.Clear();
                    }
                }
                else
                {
                    return View(Model);
                }

            }
            catch (Exception ex)
            {
                //TempData["result"] = "Something went wrong";
            }
            return RedirectToAction("StaffIndex");
        }



        [HttpPost]
        public ActionResult Addtimeline(string tlTitle, string tlDate, HttpPostedFileBase tlFile, string tlDesc, string tlVisible)
        {
            if (tlFile != null)
            {
                string dirPath = Server.MapPath("~/Content/HumanResource/StaffProfile/tlFile/");
                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                Image = "StaffProfile" + DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(tlFile.FileName);
                tlFile.SaveAs(Path.Combine(dirPath, Image)); // Save the file

            }
            var staff_id = TempData["id"];

            DateTime timelineDate = DateTime.Parse(tlDate);
            bool ptStat;

            if (tlVisible == "true")
            {
                ptStat = true;
            }
            else
            {
                ptStat = false;
            }

            staff_timeline pt = new staff_timeline()
            {

                staff_id = Int32.Parse(staff_id.ToString()),
                title = tlTitle,
                timeline_date = timelineDate,
                description = tlDesc,
                document = Image,
                is_active = true,
                date = DateTime.Now,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year,
                created_by = listData.user
            };
            dbContext.staff_timeline.Add(pt);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["message"] = "Record saved Successfully";
            }

            else
            {
                TempData["message"] = "Something Went wrong";
            }

            return RedirectToAction("StaffProfile", new { id = staff_id });
        }

        [HttpGet]
        public ActionResult DeleteTimeline(int? id)
        {
            var staff_id = TempData["id"];
            ViewBag.timeline = dbContext.staff_timeline.Where(a => a.staff_id == id).FirstOrDefault();

            if (id != null)
            {
                staff_timeline agnosi = dbContext.staff_timeline.Where(ss => ss.id == id).FirstOrDefault();
                agnosi.modified_by = listData.user;
                dbContext.SaveChanges();
                if (agnosi != null)
                {
                    var book = dbContext.staff_timeline.Where(ss => ss.is_active == true && ss.id == id).FirstOrDefault();
                    book.is_active = false;
                }
                else
                {
                    //error message donner not founf 
                }

                try
                {
                    int log = dbContext.SaveChanges();
                    TempData["messages"] = "Record delete successfully";

                }
                catch (Exception error)
                {
                    Console.Write(error);
                    TempData["messages"] = "Error while inserting data,Please contact Admin";

                }

            }
            //int? s = null;
            //StaffProfile(s);
            return RedirectToAction("StaffProfile", new { id = staff_id });
        }

        [HttpPost]
        public bool GetemailId(string email)
        {
            var emailid = dbContext.AspNetUsers.Where(a => a.Email == email).FirstOrDefault();
            bool result;
            if (emailid != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        [HttpGet]
        public ActionResult EditStaff(int? id)
        {
            Bind_Roles();
            BindDepartment();
            BindDesignation();
            var leavelists1 = (from a in dbContext.leave_types
                               join b in dbContext.staff_leave_details on a.id equals b.leave_type_id
                               where b.staff_id == id && a.is_active == true && a.branch_id == listData.BranchID && a.college_id == listData.CollegeID && a.year == listData.academic_year
                               select new LeaveTypes
                               {
                                   id = a.id,
                                   allotedleave = b.alloted_leave,
                                   type = a.type
                               }).Distinct().ToList();


            ViewBag.listss = leavelists1;
            ViewBag.lists = dbContext.AspNetRoles.Where(a => a.Id == id.ToString()).FirstOrDefault();
            var sldid = (from a in dbContext.staff_leave_details where a.staff_id == id && a.branch_id == listData.BranchID && a.college_id == listData.CollegeID && a.year == listData.academic_year select a.id).FirstOrDefault();
            TempData["StaffleaveDetailsid"] = sldid;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var staff = dbContext.staffs.Where(a => a.branch_id == listData.BranchID && a.college_id == listData.CollegeID && a.year == listData.academic_year).SingleOrDefault(s => s.id == id);
            ViewBag.aaa = staff;
            if (staff == null)
            {
                return HttpNotFound();
            }
            return View(staff);
        }


        [HttpPost]
        public ActionResult EditStaff(staff Model, HttpPostedFileBase image, HttpPostedFileBase resume, HttpPostedFileBase joining_letter, HttpPostedFileBase other_document_file, int[] leaves, int[] lids)
        {
            Bind_Roles();
            BindDepartment();
            BindDesignation();
            try
            {
                if (ModelState.IsValid)
                {
                    var st = dbContext.staffs.Find(Model.id);
                    if (st.id == Model.id)
                    {
                        string Image = "";
                        if (image != null)
                        {
                            string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                            if (!System.IO.Directory.Exists(dirPath))
                            {
                                System.IO.Directory.CreateDirectory(dirPath);
                            }
                            Image = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(image.FileName);
                            image.SaveAs(Path.Combine(dirPath, Image)); // Save the file

                        }

                        string Resume = "";
                        if (resume != null)
                        {
                            string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                            if (!System.IO.Directory.Exists(dirPath))
                            {
                                System.IO.Directory.CreateDirectory(dirPath);
                            }
                            Resume = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(resume.FileName);
                            resume.SaveAs(Path.Combine(dirPath, Resume)); // Save the file

                        }

                        string J_letter = "";
                        if (joining_letter != null)
                        {
                            string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                            if (!System.IO.Directory.Exists(dirPath))
                            {
                                System.IO.Directory.CreateDirectory(dirPath);
                            }
                            J_letter = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(joining_letter.FileName);
                            joining_letter.SaveAs(Path.Combine(dirPath, J_letter)); // Save the file

                        }

                        string Other_doc = "";
                        if (other_document_file != null)
                        {
                            string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                            if (!System.IO.Directory.Exists(dirPath))
                            {
                                System.IO.Directory.CreateDirectory(dirPath);
                            }
                            Other_doc = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(other_document_file.FileName);
                            other_document_file.SaveAs(Path.Combine(dirPath, Other_doc)); // Save the file

                        }
                        st.employee_id = Model.employee_id;
                        st.roll_id = Model.roll_id;
                        st.designation = Model.designation;
                        st.department = Model.department;
                        st.name = Model.name;
                        st.surname = Model.surname;
                        st.father_name = Model.father_name;
                        st.mother_name = Model.mother_name;
                        st.gender = Model.gender;
                        st.marital_status = Model.marital_status;
                        st.dob = Model.dob;
                        st.date_of_joining = Model.date_of_joining;
                        st.contact_no = Model.contact_no;
                        st.email = Model.email;
                        st.emergency_contact_no = Model.emergency_contact_no;
                        st.local_address = Model.local_address;
                        st.permanent_address = Model.permanent_address;
                        st.qualification = Model.qualification;
                        st.work_exp = Model.work_exp;
                        st.note = Model.note;
                        st.image = Image;
                        st.account_title = Model.account_title;
                        st.bank_account_no = Model.bank_account_no;
                        st.bank_name = Model.bank_name;
                        st.ifsc_code = Model.ifsc_code;
                        st.bank_branch = Model.bank_branch;
                        st.resume = Resume;
                        st.joining_letter = J_letter;
                        st.other_document_file = Other_doc;
                        st.updated_at = DateTime.Now;
                        st.modified_by = listData.user;
                        st.college_id = listData.CollegeID;
                        st.branch_id = listData.BranchID;
                    }
                    dbContext.Entry(st).State = System.Data.Entity.EntityState.Modified;
                    int save = dbContext.SaveChanges();
                    int i = 0;
                    foreach (int val in leaves)
                    {
                        var sid = TempData["StaffleaveDetailsid"];
                        int sd = Convert.ToInt32(sid);
                        var update = (from d in dbContext.staff_leave_details where d.id == sd && d.college_id == listData.CollegeID && d.branch_id == listData.BranchID && d.year == listData.academic_year select d).FirstOrDefault();
                        if (sid == null)
                        {
                            staff_leave_details sl = new staff_leave_details()
                            {
                                staff_id = st.id,
                                leave_type_id = lids[i],
                                alloted_leave = val,
                                college_id = listData.CollegeID,
                                branch_id = listData.BranchID,
                                modified_at = DateTime.Now,
                                modified_by = listData.user
                            };
                            dbContext.staff_leave_details.Add(sl);
                        }

                        else
                        {
                            update.alloted_leave = val;
                            update.college_id = listData.CollegeID;
                            update.branch_id = listData.BranchID;
                            update.modified_at = DateTime.Now;
                            update.modified_by = listData.user;
                        }
                        int Save = dbContext.SaveChanges();
                        i++;
                    }
                    if (save > 0)
                    {
                        TempData["result"] = "Record saved Successfully";
                    }

                    else
                    {
                        TempData["result"] = "Something Went wrong. Please Contact Admin..!";
                    }
                }
                else
                {
                    return View(Model);
                }

            }
            catch
            {
                TempData["result"] = "Something went wrong. Please Contact Admin..!";
            }
            return RedirectToAction("StaffIndex");
        }


        public ActionResult DeleteStaff(int? id)
        {
            if (id != null)
            {
                staff cates = dbContext.staffs.Where(ss => ss.id == id).FirstOrDefault();
                cates.modified_by = listData.user;
                cates.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                if (cates != null)
                {
                    dbContext.staffs.Remove(cates);
                }
                else
                {
                    //error message donner not founf 
                }

                try
                {
                    int log = dbContext.SaveChanges();
                    TempData["messages"] = "Record deleted successfully";

                }
                catch (Exception error)
                {
                    Console.Write(error);
                    TempData["messages"] = "Error while Deleting data,Please contact Admin..!";

                }

            }
            int? s = null;
            StaffIndex(s);
            return RedirectToAction("StaffIndex", "HumanResource", new { id = s });
        }

        public ActionResult DeleteResume(int? id)
        {
            var result = from a in dbContext.staffs where (a.id == id) select a;
            var staff = result.First();
            staff.modified_by = listData.user;
            staff.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            string resume = staff.resume;
            string dirPath = Server.MapPath("~/Content/Profile/Staff/" + staff.employee_id + "/" + resume);
            if (System.IO.File.Exists(dirPath))
            {
                System.IO.File.Delete(dirPath);
            }

            if (result.Count() != 0)
            {

                staff.resume = string.Empty;

                dbContext.SaveChanges();
            }
            return RedirectToAction("StaffProfile", new { id = id });
        }

        public ActionResult DeleteJLetter(int? id)
        {
            var result = from a in dbContext.staffs where (a.id == id) select a;
            var staff = result.First();
            staff.modified_by = listData.user;
            staff.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            string jletter = staff.joining_letter;
            string dirPath = Server.MapPath("~/Content/Profile/Staff/" + staff.employee_id + "/" + jletter);
            if (System.IO.File.Exists(dirPath))
            {
                System.IO.File.Delete(dirPath);
            }

            if (result.Count() != 0)
            {

                staff.joining_letter = string.Empty;

                dbContext.SaveChanges();
            }
            return RedirectToAction("StaffProfile", new { id = id });
        }

        public ActionResult DeleteOtherDoc(int? id)
        {
            var result = from a in dbContext.staffs where (a.id == id) select a;
            var staff = result.First();
            staff.modified_by = listData.user;
            staff.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            string otherDoc = staff.resume;
            string dirPath = Server.MapPath("~/Content/Profile/Staff/" + staff.employee_id + "/" + otherDoc);
            if (System.IO.File.Exists(dirPath))
            {
                System.IO.File.Delete(dirPath);
            }

            if (result.Count() != 0)
            {

                staff.resume = string.Empty;

                dbContext.SaveChanges();
            }
            return RedirectToAction("StaffProfile", new { id = id });
        }

        public ActionResult DissableStaff(int? id)
        {
            var result = from a in dbContext.staffs where (a.id == id) select a;

            if (result.Count() != 0)
            {
                var staff = result.First();
                staff.college_id = listData.CollegeID;
                staff.branch_id = listData.BranchID;
                staff.modified_by = listData.user;
                staff.updated_at = DateTime.Now;
                staff.is_active = false;

                dbContext.SaveChanges();
            }
            return RedirectToAction("StaffProfile", new { id = id });
        }

        public ActionResult EnableStaff(int? id)
        {
            var result = from a in dbContext.staffs where (a.id == id) select a;

            if (result.Count() != 0)
            {
                var staff = result.First();
                staff.college_id = listData.CollegeID;
                staff.branch_id = listData.BranchID;
                staff.modified_by = listData.user;
                staff.updated_at = DateTime.Now;
                staff.is_active = false;

                dbContext.SaveChanges();
            }
            return RedirectToAction("StaffProfile", new { id = id });
        }

        public ActionResult StaffAttendance()
        {

            return View();
        }

        private void BindClass()
        {
            List<AddClass> c = dbContext.AddClasses.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            AddClass sem = new AddClass
            {
                class_name = "----Please Select Class----",
                class_id = 0
            };
            c.Insert(0, sem);
            SelectList selectclass = new SelectList(c, "class_id", "class_name ", 0);
            ViewBag.viewbagclasslist = selectclass;
        }


        private void BindSection()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0).ToList();
            section sem = new section
            {
                section1 = "----Please Select Section----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            ViewBag.viewbagsectionlist = selectsect;
        }


        //----------------------------------LeaveTypes--------------------------------//

        public ActionResult leavetypes()
        {
            var list = (from a in dbContext.leave_types.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).OrderBy(x => x.type) select a).ToList();
            ViewBag.leavelist = list;
            return View();
        }

        [HttpPost]
        public ActionResult leavetypes(leave_types Lt)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    leave_types leavetypes = new leave_types()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        type = Lt.type,
                        is_active = true,
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        year = listData.academic_year

                    };
                    dbContext.leave_types.Add(leavetypes);
                    dbContext.SaveChanges();
                    TempData["result"] = "Saved Successfully";
                }
                else
                {
                    return View(Lt);
                }

            }
            catch
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("leavetypes");
        }

        [HttpGet]
        public ActionResult leaveedit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            leave_types leave = dbContext.leave_types.SingleOrDefault(a => a.id == id);

            if (leave == null)
            {
                return HttpNotFound();
            }

            var list = (from a in dbContext.leave_types.Where(x => x.id != 0).OrderBy(x => x.type) select a).ToList();
            ViewBag.leavelist = list;
            return View(leave);
        }

        [HttpPost]
        public ActionResult leaveedit(leave_types Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    leave_types lt = dbContext.leave_types.Find(Model.id);
                    if (lt.id == Model.id)
                    {
                        lt.type = Model.type;
                        lt.college_id = listData.CollegeID;
                        lt.branch_id = listData.BranchID;
                        lt.modified_by = listData.user;
                        lt.updated_at = DateTime.Now;

                    }
                    dbContext.leave_types.AddOrUpdate(lt);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        TempData["result"] = "Record Updated Successfully";
                    }
                    else
                    {
                        TempData["result"] = "Something Went wrong";
                    }
                }
                else
                {
                    return View(Model);
                }
            }
            catch
            {
                TempData["result"] = "Something went wrong";
            }

            return RedirectToAction("leavetypes");
        }

        public ActionResult leavedelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            leave_types leave = dbContext.leave_types.SingleOrDefault(a => a.id == id);
            leave.modified_by = listData.user;
            leave.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            if (leave == null)
            {
                return HttpNotFound();
            }
            //var st = dbContext.leave_types.SingleOrDefault(x => x.id == id);
            dbContext.leave_types.Remove(leave ?? throw new InvalidOperationException());
            dbContext.SaveChanges();

            return RedirectToAction("leavetypes");
        }


        //-------------------------------Department Start-----------------------------------//

        public ActionResult department()
        {
            var list = (from a in dbContext.departments.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).OrderBy(x => x.department_name) select a).ToList();
            ViewBag.deplist = list;


            return View();
        }

        [HttpPost]
        public ActionResult department(department Dt)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    department depart = new department()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        department_name = Dt.department_name,
                        is_active = true.ToString(),
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        year = listData.academic_year

                    };
                    dbContext.departments.Add(depart);
                    dbContext.SaveChanges();
                    TempData["result"] = "Saved Successfully";
                }
                else
                {
                    return View(Dt);
                }

            }
            catch
            {
                TempData["result"] = "Something went wrong. Please contact Admin.!";
            }

            return RedirectToAction("department");
        }

        [HttpGet]
        public ActionResult departmentedit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            department dept = dbContext.departments.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).SingleOrDefault(a => a.id == id);

            if (dept == null)
            {
                return HttpNotFound();
            }
            var list = (from a in dbContext.departments.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).OrderBy(x => x.department_name) select a).ToList();
            ViewBag.deplist = list;
            return View(dept);
        }

        [HttpPost]
        public ActionResult departmentedit(leave_types Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    department dept = dbContext.departments.Find(Model.id);
                    if (dept.id == Model.id)
                    {
                        dept.department_name = Model.type;
                        dept.modified_by = listData.user;
                        dept.updated_at = DateTime.Now;
                        dept.college_id = listData.CollegeID;
                        dept.branch_id = listData.BranchID;

                    }
                    dbContext.departments.AddOrUpdate(dept);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        TempData["result"] = "Record Updated Successfully";
                    }
                    else
                    {
                        TempData["result"] = "Something Went wrong. Please contact Admin.!";
                    }
                }
                else
                {
                    return View(Model);
                }
            }
            catch
            {
                TempData["result"] = "Something went wrong. Please contact Admin.!";
            }

            return RedirectToAction("department");
        }

        public ActionResult departmentdelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            department dept = dbContext.departments.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).SingleOrDefault(a => a.id == id);
            dept.modified_by = listData.user;
            dept.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            if (dept == null)
            {
                return HttpNotFound();
            }
            dbContext.departments.Remove(dept ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return RedirectToAction("department");
        }

        //----------------------------------Designation Start----------------------------------//

        public ActionResult designation()
        {
            var list = (from a in dbContext.staff_designation.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).OrderBy(x => x.designation) select a).ToList();
            ViewBag.desiglist = list;
            return View();
        }

        [HttpPost]
        public ActionResult designation(staff_designation Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    staff_designation desig = new staff_designation()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        designation = Model.designation,
                        is_active = true,
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        year = listData.academic_year

                    };
                    dbContext.staff_designation.Add(desig);
                    dbContext.SaveChanges();
                    TempData["result"] = "Saved Successfully";
                }
                else
                {
                    return View(Model);
                }

            }
            catch
            {
                TempData["result"] = "Something went wrong. Please contact Admin.!";
            }

            return RedirectToAction("designation");
        }

        [HttpGet]
        public ActionResult designationedit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            staff_designation desig = dbContext.staff_designation.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).SingleOrDefault(a => a.id == id);

            if (desig == null)
            {
                return HttpNotFound();
            }
            var list = (from a in dbContext.staff_designation.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).OrderBy(x => x.designation) select a).ToList();
            ViewBag.desiglist = list;
            return View(desig);
        }

        [HttpPost]
        public ActionResult designationedit(staff_designation Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    staff_designation desig = dbContext.staff_designation.Find(Model.id);

                    if (desig.id == Model.id)
                    {
                        desig.designation = Model.designation;
                        desig.modified_by = listData.user;
                        desig.updated_at = DateTime.Now;
                        desig.college_id = listData.CollegeID;
                        desig.branch_id = listData.BranchID;
                    }
                    dbContext.staff_designation.AddOrUpdate(desig);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        TempData["result"] = "Record Updated Successfully";
                    }
                    else
                    {
                        TempData["result"] = "Something Went wrong. Please contact Admin.!";
                    }
                }
                else
                {
                    return View(Model);
                }
            }
            catch
            {
                TempData["result"] = "Something went wrong. Please contact Admin.!";
            }

            return RedirectToAction("designation");
        }

        public ActionResult designationdelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            staff_designation desig = dbContext.staff_designation.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).SingleOrDefault(a => a.id == id);
            desig.modified_by = listData.user;
            desig.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            if (desig == null)
            {
                return HttpNotFound();
            }
            dbContext.staff_designation.Remove(desig ?? throw new InvalidOperationException());
            dbContext.SaveChanges();

            return RedirectToAction("designation");
        }

        //----------------------------Leave Module----------------------------//

        private void BindLeaveTypes()
        {
            var list = dbContext.leave_types.Where(a => a.is_active == true && a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID).OrderBy(a => a.id).ToList().Select(b => new SelectListItem { Value = b.id.ToString(), Text = b.type.ToString() }).ToList();
            ViewBag.leaveType = list;

        }
        public ActionResult Leaves()
        {
            BindLeaveTypes();
            Bind_Roles();
            
            var is_role= dbContext.AspNetRoles.Where(a => a.Id == listData.roleid).Select(b => b.Name).FirstOrDefault();
            ViewBag.is_role = is_role;
            ViewBag.leaveList = (from a in dbContext.staffs
                                 join b in dbContext.staff_leave_request on a.id equals b.staff_id
                                 join c in dbContext.leave_types on b.leave_type_id equals c.id
                                 where c.is_active == true && a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID
                                 select new SmartSchool.SchoolMasterClasses.staffLeaves
                                 {
                                     id = b.id,
                                     name = a.name,
                                     leave_from = b.leave_from,
                                     leave_to = b.leave_to,
                                     leavetype = c.type,
                                     leave_days = b.leave_days,
                                     date = b.date,
                                     status = b.status
                                 }).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult Leaves(staff_leave_request Model, int? id, HttpPostedFileBase doc, string date1, string date2)
        {
            Bind_Roles();
            BindLeaveTypes();
            try
            {
                string Other_doc = "";
                if (doc != null)
                {
                    string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                    if (!System.IO.Directory.Exists(dirPath))
                    {
                        System.IO.Directory.CreateDirectory(dirPath);
                    }
                    Other_doc = dirPath + DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(doc.FileName);
                    doc.SaveAs(Other_doc); // Save the file

                }
                //if (ModelState.IsValid)
                //{
                DateTime leaveFrom, leaveTo;
                // leaveDate = Request.Form["leaveDate"].Trim().Split('-');

                leaveFrom = DateTime.Parse(date1);
                leaveTo = DateTime.Parse(date2);
                //TimeSpan diff = (leaveFrom - leaveTo);
                //int days = Convert.ToInt32((leaveTo.Date - leaveFrom.Date).TotalDays) + 1;

                if (id == null)
                {
                    staff_leave_request leave = new staff_leave_request()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        staff_id = Model.staff_id,
                        leave_type_id = Model.leave_type_id,
                        leave_from = leaveFrom,
                        leave_to = leaveTo,
                        leave_days = Model.leave_days,
                        status = "pending",
                        employee_remark ="",
                        admin_remark ="",
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        applied_by = listData.user,
                        document_file = Other_doc,
                        date = Model.date,
                        year = listData.academic_year


                    };
                    dbContext.staff_leave_request.Add(leave);
                    int save = dbContext.SaveChanges();
                    if (save > 0)
                    {
                        TempData["result"] = "Record Saved Successfully";
                        ModelState.Clear();
                    }
                }
                else
                {
                    staff_leave_request leave = dbContext.staff_leave_request.Find(id);
                    if (leave.id == id)
                    {
                        leave.leave_type_id = Model.leave_type_id;
                        leave.leave_from = leaveFrom;
                        leave.leave_to = leaveTo;
                        leave.leave_days = Model.leave_days;
                        leave.employee_remark = Model.employee_remark;
                        leave.modified_by = listData.user;
                        leave.updated_at = DateTime.Now;
                        leave.college_id = listData.CollegeID;
                        leave.branch_id = listData.BranchID;
                        leave.date = Model.date;
                        leave.document_file = Other_doc;
                    }
                    dbContext.staff_leave_request.AddOrUpdate(leave);
                    int save = dbContext.SaveChanges();

                    if (save > 0)
                    {
                        TempData["result"] = "Record Updated Successfully";
                    }
                    else
                    {
                        TempData["result"] = "Something Went wrong. Please contact Admin.!";
                    }
                }
                //}
                //else
                //{
                //    return View(Model);
                //}

            }
            catch(Exception ex)
            {
                TempData["result"] = "Something went wrong. Please contact Admin.!";
            }
            return RedirectToAction("Leaves");
        }

        [HttpPost]
        public int? Getleavedays(int leave_days, int staff_id, int leaveType)
        {
            int? alloted = (Int32)dbContext.staff_leave_details.Where(a => a.staff_id == staff_id && a.leave_type_id == leaveType && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(a => a.alloted_leave).FirstOrDefault();

            var re = dbContext.staff_leave_request.Where(a => a.staff_id == staff_id && a.leave_type_id == leaveType && a.status == "approve" && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Sum(x => (int?)x.leave_days) ?? 0;
            int? availab = alloted - re;
            if (leave_days > availab)
            {
                return availab;
            }
            else
            {
                return 0;
            }
        }


        [HttpPost]
        public JsonResult GetStaff(string roleid)
        {
            //int rid = Convert.ToInt32(roleid);
            dbContext.Configuration.ProxyCreationEnabled = false;
            List<staff> stafflist = dbContext.staffs.Where(x => x.roll_id == roleid && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            return Json(stafflist, JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddLeaveRequest()
        {
            BindLeaveTypes();
            Bind_Roles();

            ViewBag.leaveList = (from a in dbContext.staffs
                                 join b in dbContext.staff_leave_request on a.id equals b.staff_id
                                 join c in dbContext.leave_types on b.leave_type_id equals c.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new SmartSchool.SchoolMasterClasses.staffLeaves
                                 {
                                     id = b.id,
                                     name = a.name,
                                     leave_from = b.leave_from,
                                     leave_to = b.leave_to,
                                     leavetype = c.type,
                                     leave_days = b.leave_days,
                                     date = b.date,
                                     status = b.status
                                 }).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult AddLeaveRequest(staff_leave_request Model, int? id, HttpPostedFileBase doc, string date1, string date2)
        {
            Bind_Roles();
            BindLeaveTypes();
            //try
            //{
            string Other_doc = "";
            if (doc != null)
            {
                string dirPath = Server.MapPath("~/Content/Profile/Staff/");
                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                Other_doc = dirPath + DateTime.Now.ToString("MMddHHmmssfff") + Path.GetExtension(doc.FileName);
                doc.SaveAs(Other_doc); // Save the file

            }
            DateTime leaveFrom, leaveTo;
            // leaveDate = Request.Form["leaveDate"].Trim().Split('-');

            leaveFrom = DateTime.Parse(date1);
            leaveTo = DateTime.Parse(date2);
            //TimeSpan diff = (leaveFrom - leaveTo);
            //int days = Convert.ToInt32((leaveTo.Date - leaveFrom.Date).TotalDays) + 1;

            if (id == null)
            {
                staff_leave_request leave = new staff_leave_request()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    staff_id = Model.staff_id,
                    leave_type_id = Model.leave_type_id,
                    leave_from = leaveFrom,
                    leave_to = leaveTo,
                    leave_days = Model.leave_days,
                    status = Model.status,
                    employee_remark = Model.employee_remark,
                    admin_remark = Model.admin_remark,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    applied_by = listData.user,
                    document_file = Other_doc,
                    date = Model.date,
                    year = listData.academic_year
                };
                dbContext.staff_leave_request.Add(leave);
                int sav = dbContext.SaveChanges();
                if (sav > 0)
                {
                    TempData["message"] = "Record saved Successfully";
                    ModelState.Clear();
                }

                else
                {
                    TempData["message"] = "Something Went wrong";
                }

            }
            else
            {
                staff_leave_request leave = dbContext.staff_leave_request.Find(id);
                if (leave.id == id)
                {
                    leave.staff_id = Model.staff_id;
                    leave.leave_type_id = Model.leave_type_id;
                    leave.leave_from = leaveFrom;
                    leave.leave_to = leaveTo;
                    leave.leave_days = Model.leave_days;
                    leave.status = Model.status;
                    leave.employee_remark = Model.employee_remark;
                    leave.admin_remark = Model.admin_remark;
                    leave.modified_by = listData.user;
                    leave.updated_at = DateTime.Now;
                    leave.college_id = listData.CollegeID;
                    leave.branch_id = listData.BranchID;
                    leave.date = Model.date;
                    leave.document_file = Other_doc;
                }
                dbContext.staff_leave_request.AddOrUpdate(leave);
                int save = dbContext.SaveChanges();

                if (save > 0)
                {
                    TempData["result"] = "Record Updated Successfully";
                    ModelState.Clear();
                }
                else
                {
                    TempData["result"] = "Something Went wrong";
                }
            }
            ModelState.Clear();
            return RedirectToAction("Leaves");
        }

        [HttpPost]
        public JsonResult LeaveRecord(int id)
        {
            dbContext.Configuration.ProxyCreationEnabled = false;
            SmartSchool.SchoolMasterClasses.staffLeaves details = (from a in dbContext.staff_leave_request
                                                                   join b in dbContext.staffs on a.staff_id equals b.id
                                                                   join c in dbContext.leave_types on a.leave_type_id equals c.id
                                                                   where a.id == id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                                                   select new SmartSchool.SchoolMasterClasses.staffLeaves
                                                                   {
                                                                       id = a.id,
                                                                       staff_id = a.staff_id,
                                                                       name = b.name,
                                                                       leave_from = a.leave_from,
                                                                       leave_to = a.leave_to,
                                                                       roleid = b.roll_id,
                                                                       admin_remark = a.admin_remark,
                                                                       employee_remark = a.employee_remark,
                                                                       date = a.date,
                                                                       status = a.status,
                                                                       leave_type_id = c.id,
                                                                       leavetype = c.type,
                                                                       leave_days = a.leave_days,
                                                                       applied_by = a.applied_by
                                                                   }).SingleOrDefault();

            return Json(details, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LeaveStatus(string status, string admin_remark, int id)
        {

            dbContext.Configuration.ProxyCreationEnabled = false;
            staff_leave_request leave = dbContext.staff_leave_request.Find(id);

            if (leave.id == id)
            {
                leave.status = status;
                leave.admin_remark = admin_remark;
                leave.modified_by = listData.user;
                leave.updated_at = DateTime.Now;
                leave.college_id = listData.CollegeID;
                leave.branch_id = listData.BranchID;
            }
            dbContext.staff_leave_request.AddOrUpdate(leave);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Updated Successfully";
            }
            else
            {
                TempData["result"] = "Something Went Wrong";
            }
            return Json(save, JsonRequestBehavior.AllowGet);
        }

        //--------------------------- Staff Attendance -------------------------------//

        [HttpGet]
        public ActionResult Staffattendance()
        {
            Bind_Roles();
            return View();
        }

        [HttpPost]
        public ActionResult Staffattendance(staff_attendance model, string roleid)
        {
            Bind_Roles();
            // Attendance type is common for all schools, so need to mention collegeid and branchid
            ViewBag.attd_list = dbContext.attendence_type.Where(a => a.is_active == "true").OrderBy(a => a.id).ToList();

            if (roleid != null)
            {
                var result = (from x in dbContext.staffs.Where(a => a.roll_id == roleid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                              join a in dbContext.AspNetRoles on x.roll_id equals a.Id
                              from y in dbContext.staff_attendance
                                  .Where(y => y.staff_id == x.id && y.date == model.date && y.college_id == listData.CollegeID && y.branch_id == listData.BranchID && y.year == listData.academic_year).DefaultIfEmpty()

                              select new SmartSchool.SchoolMasterClasses.staffattendance
                              {
                                  id = x.id,
                                  employee_id = x.employee_id,
                                  rolename = a.Name,
                                  name = x.name,
                                  surname = x.surname,
                                  atype_id = y.staff_attendance_type_id,
                                  remark = y.remark,
                                  aid = y.id
                              }).ToList();

                ViewBag.staff_a = result;
                TempData["result"] = result.Count;
            }

            return View();
        }
        [HttpPost]
        public JsonResult SaveAttendance(List<staff_attendance> allStaff, string holiday)
        {
            int result = 0;
            int attendance_type;
            if (allStaff != null)
            {
                foreach (var staff in allStaff)
                {
                    if (!string.IsNullOrEmpty(holiday))
                    {
                        attendance_type = Convert.ToInt32(holiday);
                    }
                    else
                    {
                        attendance_type = staff.staff_attendance_type_id;
                    }
                    if (staff.id.Equals(0))
                    {
                        staff_attendance obj = new staff_attendance
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            staff_id = staff.staff_id,
                            date = staff.date,
                            staff_attendance_type_id = attendance_type,
                            remark = staff.remark,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year

                        };
                        dbContext.staff_attendance.Add(obj);
                        result += dbContext.SaveChanges();
                    }
                    else
                    {
                        staff_attendance sa = dbContext.staff_attendance.Find(staff.id);
                        if (sa.id == staff.id)
                        {
                            sa.staff_attendance_type_id = attendance_type;
                            sa.remark = staff.remark;
                            sa.updated_at = DateTime.Now;
                            sa.modified_by = listData.user;
                            sa.college_id = listData.CollegeID;
                            sa.branch_id = listData.BranchID;

                        }
                        dbContext.staff_attendance.AddOrUpdate(sa);
                        result += dbContext.SaveChanges();
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        ////////////////////////////////////staff_attendance_Report/////////////////////////////////////////

        private void bind_month()
        {
            var yr = dbContext.staff_attendance.Select(y => y.date.Month).ToList();
            ViewBag.year = yr;
        }

        private void bind_year()
        {
            var yr = (from a in dbContext.staff_attendance where a.is_active != null & a.date != null select a.date.Year).Distinct().ToList();
            ViewBag.year = yr;
        }

        [HttpGet]
        public ActionResult StaffAttendanceReport()
        {
            Bind_Roles();
            return View();
        }
        [HttpPost]
        public ActionResult StaffAttendanceReport(staff_attendance model, int? Month, int? Year, string roleid)
        {
            var Mn = Month.Value;
            var Yr = Year.Value;
            Bind_Roles();
            bind_month();
            bind_year();

            // Attendance type is common for all schools, so need to mention collegeid and branchid
            var attendancetype = dbContext.attendence_type.Where(a => a.is_active == "true").OrderBy(a => a.id).ToList();
            ViewBag.attd_list = attendancetype;

            var stu_rep = (from a in dbContext.staff_attendance
                           join b in dbContext.staffs on new { staff_id = a.staff_id } equals new { staff_id = b.id }
                           where
                             b.roll_id == roleid &&
                             a.date.Month == Mn
                             && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                           group new { a, b } by new
                           {
                               a.staff_id,
                               b.name,

                           } into g
                           select new SmartSchool.SchoolMasterClasses.Staff_attendance_cls_sec

                           {
                               id = g.Key.staff_id,
                               Name = g.Key.name,

                               present = g.Count(p => (
                               p.a.staff_attendance_type_id == 1 ? (System.Int64?)1 : null) != null),
                               late = g.Count(p => (
                                p.a.staff_attendance_type_id == 2 ? (System.Int64?)1 : null) != null),
                               absent = g.Count(p => (
                               p.a.staff_attendance_type_id == 3 ? (System.Int64?)1 : null) != null),
                               halfday = g.Count(p => (
                               p.a.staff_attendance_type_id == 4 ? (System.Int64?)1 : null) != null),
                               holiday = g.Count(p => (
                               p.a.staff_attendance_type_id == 5 ? (System.Int64?)1 : null) != null)
                           }).ToList();

            ViewBag.count = stu_rep;
            ViewBag.month = Mn;
            ViewBag.selyear = Yr;

            return View();
        }

        [HttpPost]
        public JsonResult Attlist(int? id, int? Month, int? Year)

        {
            var Mn = Month.Value;
            var Yr = Year.Value;
            Bind_Roles();
            bind_month();
            bind_year();
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.pop11 = "1";
            var pr = (from a in dbContext.staff_attendance
                      join b in dbContext.staffs on a.staff_id equals b.id
                      join c in dbContext.attendence_type on a.staff_attendance_type_id equals c.id
                      where a.staff_id == id && a.date.Month == Mn && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                      select new SmartSchool.SchoolMasterClasses.Staff_attendance_cls_sec
                      {
                          id = a.id,
                          Name = b.name,
                          type = c.key_value,
                          date = a.date

                      }).Distinct().ToList();

            ViewBag.list11 = pr.Distinct();

            return Json(pr, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        public JsonResult GetCode(string userId)
        {
            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            Task<string> token = UserManager.GeneratePasswordResetTokenAsync(userId);
            //AccountController acc = new AccountController();

            return Json(token, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public bool Getconfirmpassword(string ConfirmPassword, string Password)
        {
            bool result;
            if (ConfirmPassword != Password)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public class staffdata
        {
            public int? SlNo { get; set; }
            public string StaffName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string RoleName { get; set; }
        }
        public ActionResult StaffLoginCredentials()
        {
            var staffDetails = (from sf in dbContext.staffs
                                join rol in dbContext.AspNetRoles on sf.roll_id equals rol.Id
                                where sf.college_id == listData.CollegeID && sf.branch_id == listData.BranchID && sf.year == listData.academic_year
                                select new staffdata
                                {
                                    SlNo = sf.id,
                                    StaffName = sf.name,
                                    Email = sf.email,
                                    Password = sf.password,
                                    RoleName = rol.Name
                                }).ToList();

            ViewBag.stafflistc = staffDetails;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> UpdateStudentCredentials(string Email, string Password, string rolename)
        {
            if (Email != "" && Password != "" && rolename != "")
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                var user = await userManager.FindByEmailAsync(Email);
                if (user != null)
                {
                    var validPass = await userManager.PasswordValidator.ValidateAsync(Password);
                    if (validPass.Succeeded)
                    {
                        var UpdatePassword = userManager.FindByName(Email);
                        UpdatePassword.PasswordHash = userManager.PasswordHasher.HashPassword(Password);
                        var res = userManager.Update(UpdatePassword);
                        if (res.Succeeded)
                        {
                            string SMSRegards = "Lamington School Hubli";
                            string message = "Dear Staff your profile has been created successfully.To Check Details Visit http://lamingtonschool.com " + " UserName: " + Email + Environment.NewLine + "Password: " + Password + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                            send_message(Password, message);
                            return Json("Success", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    var userpar = new ApplicationUser();
                    userpar.UserName = Email;
                    userpar.Email = Email;
                    userpar.PhoneNumber = Password;
                    var checker1 = UserManager.Create(userpar, Password);
                    if (checker1.Succeeded)
                    {
                        if (rolename == "SuperAdmin")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "SuperAdmin");
                        }
                        else if (rolename == "Admin")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "Admin");
                        }
                        else if (rolename == "Teacher")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "Teacher");
                        }
                        else if (rolename == "Accountant")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "Accountant");
                        }
                        else if (rolename == "Librarian")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "Librarian");
                        }
                        else if (rolename == "Receptionist")
                        {
                            var result1 = UserManager.AddToRole(userpar.Id, "Receptionist");
                        }
                        string SMSRegards = "Lamington School Hubli";
                        string message = "Dear Staff your profile has been created successfully.To Check Details Visit http://lamingtonschool.com " + " UserName: " + Email + Environment.NewLine + "Password: " + Password + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                        send_message(Password, message);
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json("Please update EMAIL,PASSWORD and ROLE of this Staff..Thank you!", JsonRequestBehavior.AllowGet);
            }
            return Json("Something Wrong..Please Contact Admin!", JsonRequestBehavior.AllowGet);
        }
    }
}