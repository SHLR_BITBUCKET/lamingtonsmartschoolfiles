﻿using SmartSchool.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Data.Entity;
using System.IO;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using SmartSchool.Classes;
using SmartSchool.SchoolMasterClasses;
using SmartSchool.Interfaces;

namespace SmartSchool.Controllers
{
    public class IncomeController : Controller
    {
        //public int CollegeID = 205, BranchID = 1;
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        // GET: Income
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public IncomeController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }


        public ActionResult Index()
        {
            return View();
        }

        //Semester Name Binding
        private void DdlIncomehead()
        {
            List<income_head> c = dbContext.income_head.Where(x => x.ihid !=0).ToList();
            income_head inc = new income_head
            {
                income_category = " select ",
                ihid = 0
            };
            c.Insert(0, inc);
            SelectList selectincomehead = new SelectList(c, "ihid", "income_category ", 0);
            ViewBag.selectedincomehead = selectincomehead;
        }

        [HttpPost]
        public ActionResult Addincome(income model, HttpPostedFileBase FileUpload)
        {
            DdlIncomehead();
            string filesnames="";
            string pathcheck = Server.MapPath("~/Content/Addincome/");
            if (FileUpload != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(FileUpload.FileName);
                string dirPath = Server.MapPath("~/Content/Addincome/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }

                FileUpload.SaveAs(Path.Combine(Server.MapPath("~/Content/Addincome/"), savedFileName)); // Save the file
                 filesnames = savedFileName;
            }
            income inc = new income()
            {
                college_id=listData.CollegeID,
                branch_id=listData.BranchID,
                inc_head_id = model.inc_head_id,
                name = model.name,
                invoice_no = model.invoice_no,
                date = model.date,
                amount = model.amount,
                documents= filesnames,
                note = model.note,
                is_active=true,
                created_at = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year
            };
            dbContext.incomes.Add(inc);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Inserted Successfully";
                ModelState.Clear();
            }
            else
            {
                TempData["result"] = "Something went wrong";
            }
           return RedirectToAction("Addincome");
        }

        [HttpGet]
        public ActionResult Addincome()
        {
            DdlIncomehead();
            var income = (from a in dbContext.incomes
                          join b in dbContext.income_head on a.inc_head_id equals b.ihid.ToString()
                          where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                          select new Incomedata
                          {
                              Iid=a.iid,
                              Name = a.name,
                              Invoice_no = a.invoice_no,
                              Date = a.date,
                              Inc_head_id = b.income_category,
                              Amount = a.amount,
                              Documents=a.documents
                            }).ToList();
            ViewBag.Addincomelist = income;
            return View();
        }

        [HttpPost]
        public ActionResult Editincome(income model)
        {
            DdlIncomehead();
            income Editincome = dbContext.incomes.Find(model.iid);
            if (Editincome.iid == model.iid)
            {
                Editincome.name = model.name;
                Editincome.invoice_no = model.invoice_no;
                Editincome.amount = model.amount;
                Editincome.date = model.date;
                Editincome.modified_by = listData.user;
                Editincome.updated_at = DateTime.Now;
                Editincome.college_id = listData.CollegeID;
                Editincome.branch_id = listData.BranchID;
            }
            dbContext.incomes.AddOrUpdate(Editincome);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Addincome");
        }



        [HttpGet]
        public async Task<ActionResult> Editincome(int? id)
        {
            DdlIncomehead();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            income Editincome = await dbContext.incomes.FindAsync(id);
            if (Editincome == null)
            {
                return HttpNotFound();
            }
            return View(Editincome);

        }

        [HttpGet]
        public ActionResult Deleteincome(int? id)
        {
            if (id != 0)
            {
                income incomedelete = dbContext.incomes.Where(x => x.iid == id).FirstOrDefault();
                incomedelete.modified_by = listData.user;
                incomedelete.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.incomes.Remove(incomedelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Addincome", "Income");
        }
        
        [HttpPost]
        public ActionResult Incomehead(income_head model)
        {
            try
            {
                income_head income = new income_head()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    income_category = model.income_category,
                    description = model.description,
                    is_active = true,
                    created_by = listData.user,
                    created_at = DateTime.Now

                };
                dbContext.income_head.Add(income);
                int save = dbContext.SaveChanges();
                if (save > 0)
                {
                    TempData["message"] = "Record Deleted Successfully";
                    ModelState.Clear();
                }
                else
                {
                    TempData["message"] = "Something went wrong";
                }
            }
            catch(Exception error)
            {
                Console.Write(error);
                TempData["message"] = "Something went wrong";
            }
            var incomelis = (from a in dbContext.income_head.Where(x => x.ihid != 0) select a).ToList();
            ViewBag.Incomelist = incomelis;
            return View();
        }

        [HttpGet]
        public ActionResult Incomehead()
        {
            var incomelis = (from a in dbContext.income_head.Where(x => x.ihid != 0) select a).ToList();
            ViewBag.Incomelist = incomelis;
            return View();
        }


        [HttpPost]
        public ActionResult Editincomehead(income_head model)
        {
            income_head Editincome = dbContext.income_head.Find(model.ihid);
            if (Editincome.ihid == model.ihid)
            {
                Editincome.ihid = model.ihid;
                Editincome.income_category = model.income_category;
                Editincome.description = model.description;
                Editincome.is_active = model.is_active;
                Editincome.college_id = listData.CollegeID;
                Editincome.branch_id = listData.BranchID;
                Editincome.modified_by = listData.user;
                Editincome.updated_at = DateTime.Now;

            }
            dbContext.income_head.AddOrUpdate(Editincome);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["result"] = "Record Edited Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Incomehead");
        }



        [HttpGet]
        public async Task<ActionResult> Editincomehead(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            income_head Editincome = await dbContext.income_head.FindAsync(id);
            if (Editincome == null)
            {
                return HttpNotFound();
            }
            return View(Editincome);

        }

        [HttpGet]
        public ActionResult Deleteincomehead(int? id)
        {
            if (id != 0)
            {
                income_head incomedelete = dbContext.income_head.Where(x => x.ihid == id).FirstOrDefault();
                incomedelete.modified_by = listData.user;
                incomedelete.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.income_head.Remove(incomedelete);
            }
            else
            {

            }
            int save = dbContext.SaveChanges();
            if (save != 0)
            {
                TempData["result"] = "Record Deleted Successfully";
            }
            else
            {
                TempData["result"] = "Something Went wrong";
            }
            return RedirectToAction("Incomehead", "Income");
        }
        

        [HttpPost]
        public ActionResult Expensesearch(income model, DateTime? fromdate, DateTime? todate)
        {
            var Incomelis = (from a in dbContext.incomes
                             join b in dbContext.income_head on a.inc_head_id equals b.ihid.ToString()
                             where (a.date >= fromdate && a.date <= todate) && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                             select new Expensedetails
                             {
                                 Name = a.name,
                                 Invoice_no = a.invoice_no,
                                 Inc_head_id = b.income_category,
                                 Date = a.date,
                                 Amount = a.amount
                            }).ToList();
                            ViewBag.Incomesearch = Incomelis;
            return View();
        }

        [HttpGet]
        public ActionResult Expensesearch(DateTime? fromdate, DateTime? todate)
        {
            var Incomelis = (from a in dbContext.incomes
                             join b in dbContext.income_head on a.inc_head_id equals b.ihid.ToString()
                             where (a.date >= fromdate && a.date <= todate) && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                             select new Expensedetails
                             {
                                 Name = a.name,
                                 Invoice_no = a.invoice_no,
                                 Inc_head_id = b.income_category,
                                 Date = a.date,
                                 Amount = a.amount
                             }).ToList();
           
            ViewBag.Incomesearch = Incomelis;
            return View();
        }

    }
}