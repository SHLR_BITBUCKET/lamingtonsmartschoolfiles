﻿using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SmartSchool.Controllers
{ 
    public class LibraryController : Controller
    {
        public SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        public get_staff_classes gs = new get_staff_classes();
        //public int CollegeID = 2015, BranchID = 1;
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;


        public LibraryController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }


        public ActionResult Index()
        {
            return View();
        }
        private void bind_class_rooms()
        {
            var userlist = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.cls = userlist;
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var result = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult bknumber(string subject)
        {
            var getbookno = (from d in dbcontext.books where d.book_title == subject select d.book_no).FirstOrDefault();
            string res = "";
            if (getbookno != null)
            {
                res = getbookno;
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
       

        public JsonResult pingenerate(string prefix)
        {
            var res = dbcontext.libarary_members.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.member_type.Contains(prefix)).Max(a => a.member_type_id) + 1;
            var re = res + 1;
            string s = re.ToString();
            string cardNo = null;
           // var maxuid = res.Max();
           if(prefix == "smartstu")
            { 
           if (res==null)
            {
                ViewBag.pins = "STUL1";
                cardNo = "STUL1";
            }
            else
            { 
           
            string Userid = string.Concat("STUL", re);
            ViewBag.pins = Userid;
                cardNo = Userid;
            }
            }
           else
            {
                if (re == null)
                {
                   
                    cardNo = "STAL1";
                }
                else
                {
                    string Userid = string.Concat("STAL", re);
                    
                    cardNo = Userid;
                }
            }

            return Json(cardNo, JsonRequestBehavior.AllowGet);

        }
       

        public void pingenerate1()
        {           
            var max = (from l in dbcontext.libarary_members where l.member_type_id == 1 select l.library_card_no).Max();
            int mx = Convert.ToInt32(max);
            var inc = mx + 1;
           
            string Libcardno = "STAL" + inc;
            ViewBag.pins = Libcardno;

        }

        [HttpGet]
        public ActionResult Book()
        {
            string numbers = "0123456789";
            string characters = numbers;
            characters += numbers;
            int length = 4;
            string book_no = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (book_no.IndexOf(character) != -1);
                ViewBag.book_no +=  character;

            }
            return View();
        }

        [HttpPost]
        public ActionResult Book(book model)
        {
            var bk = (from a in dbcontext.books where a.subject == model.subject && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a).FirstOrDefault();
            if (bk == null)
            {
                book book_s = new book()
                {
                    book_title = model.book_title,
                    book_no = model.book_no,
                    isbn_no = model.isbn_no,
                    publish = model.publish,
                    author = model.author,
                    subject = model.subject,
                    rack_no = model.rack_no,
                    qty = model.qty,
                    perunitcost = model.perunitcost,
                    postdate = model.postdate,
                    description = model.description,
                    created_at = DateTime.Now,
                    available = model.available,
                    created_by = listData.user,
                    is_active = true,
                    branch_id = listData.BranchID,
                    college_id = listData.CollegeID,
                    year = listData.academic_year
                };
                dbcontext.books.Add(book_s);
            }
            else
            {
                bk.book_title = model.book_title;
                bk.book_no = model.book_no;
                bk.isbn_no = model.isbn_no;
                bk.publish = model.publish;
                bk.author = model.author;
                bk.subject = model.subject;
                bk.rack_no = model.rack_no;
                bk.qty = model.qty;
                bk.perunitcost = model.perunitcost;
                bk.postdate = model.postdate;
                bk.description = model.description;
                bk.updated_at = DateTime.Now;
                bk.available = model.available;
                bk.modified_by = listData.user;
                bk.branch_id = listData.BranchID;
                bk.college_id = listData.CollegeID;
            }
            try
            {
                int c = dbcontext.SaveChanges();
                TempData["message"] = "Data Saved Successfully";
            }
            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Book");
        }


        [HttpGet]
        public ActionResult Booklist()
        {
            var Booklist = (from a in dbcontext.books.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
            ViewBag.Bookdetails = Booklist;

            return View();
        }

        [HttpPost]
        public ActionResult Booklist(book model)
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditBooklist(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = dbcontext.books.Where(a => a.bid == Id).Select(pp => pp).FirstOrDefault();
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        [HttpPost]
        public ActionResult EditBooklist(book model, int? Id)
        {
            var data = dbcontext.books.Where(a => a.bid == Id).Select(pp => pp).FirstOrDefault();
            var oldqty = (from a in dbcontext.books where a.bid == Id select a.qty).FirstOrDefault();
            if (data.bid == model.bid)
            {
                data.book_title = model.book_title;
                data.book_no = model.book_no;
                data.author = model.author;
                data.available = model.available;
                data.description = model.description;
                data.isbn_no = model.isbn_no;
                data.is_active = model.is_active;
                data.perunitcost = model.perunitcost;
                data.publish = model.publish;
                data.rack_no = model.rack_no;
                data.subject = model.subject;
                data.qty = model.qty;
                data.postdate = model.postdate;
                data.modified_by = listData.user;
                data.updated_at = DateTime.Now;
                data.college_id = listData.CollegeID;
                data.branch_id = listData.BranchID;
            };
            dbcontext.books.AddOrUpdate(data);
            int c = dbcontext.SaveChanges();

            if (c == 1)
            {
                var newqty = model.qty - oldqty;
                book_purchase_history bph = new book_purchase_history()
                {
                    book_title = model.book_title,
                    book_no = model.book_no,
                    author = model.author,
                    available = model.available,
                    description = model.description,
                    isbn_no = model.isbn_no,
                    is_active = model.is_active,
                    perunitcost = model.perunitcost,
                    publish = model.publish,
                    // publish = model.publish,
                    rack_no = model.rack_no,
                    subject = model.subject,
                    qty = newqty,
                    postdate = model.postdate,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    modified_by = listData.user,
                    updated_at = DateTime.Now
                    

                };

                dbcontext.book_purchase_history.AddOrUpdate(bph);
                dbcontext.SaveChanges();
            }
            return RedirectToAction("Booklist");
        }

        [HttpGet]
        public ActionResult DeleteBooklist(int? Id)
        {

            if (Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = dbcontext.books.Where(a => a.bid == Id).FirstOrDefault();
            data.modified_by = listData.user;
            data.updated_at = DateTime.Now;
            dbcontext.SaveChanges();

            if (data == null)
            {
                return HttpNotFound();
            }

            dbcontext.books.Remove(data ?? throw new InvalidOperationException());
            try
            {
                int c = dbcontext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully";
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while Deleting data, please contact admin";
            }
            return RedirectToAction("Booklist");
        }

       

        public ActionResult delMember(string del_id)
        {
            if (del_id != null)
            {
                var data = dbcontext.libarary_members.Where(a => a.member_type == del_id).Select(pp => pp).FirstOrDefault();
                data.modified_by = listData.user;
                data.updated_at = DateTime.Now;
                dbcontext.SaveChanges();

                dbcontext.libarary_members.Remove(data);
                dbcontext.SaveChanges();
            }
            return RedirectToAction("Teacher");
        }

        public ActionResult deleteMember(string delete_id)
        {
            if (delete_id != null)
            {
                var data = dbcontext.libarary_members.Where(a => a.member_type == delete_id).Select(pp => pp).FirstOrDefault();
                data.modified_by = listData.user;
                data.updated_at = DateTime.Now;
                dbcontext.SaveChanges();

                dbcontext.libarary_members.Remove(data);
                dbcontext.SaveChanges();
            }
            return RedirectToAction("Student");
        }


        public void StudMember(string card_no, int memberid, int? class_id, string user_id, string AdmissionNumber)
        {
       
            string val = "smartstu";
           
            var  max = dbcontext.libarary_members.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID  && a.member_type.Contains(val)).Max(a => a.member_type_id) + 1;
            int?  mem_type_id;
            if(max == null)
            {
                mem_type_id = 1;
            }
            else
            {
                mem_type_id = max;
            }
            libarary_members libmembr = new libarary_members()
            {
                library_card_no = card_no,
                member_type = user_id,
                member_id = Convert.ToInt32(AdmissionNumber),
                created_at = DateTime.Now,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                is_active = true,
                modified_by = listData.user,
                created_by = listData.user,
                member_type_id= mem_type_id

            };
            dbcontext.libarary_members.Add(libmembr);
            dbcontext.SaveChanges();
            ViewBag.studentdeatils = cmc.get_student_class(class_id, listData.CollegeID, listData.BranchID, listData.academic_year);
            
        }

        public void TeacherMember(string card_no, int memberid, int? class_id, string emp_id, string id)
        {
            string val = "Staff";

            var max = dbcontext.libarary_members.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID  && a.member_type.Contains(val)).Max(a => a.member_type_id) + 1;
            int? mem_type_id;
            if (max == null)
            {
                mem_type_id = 1;
            }
            else
            {
                mem_type_id = max;
            }

            libarary_members libmembr = new libarary_members()
            {
                library_card_no = card_no,
                member_type = emp_id,
                member_id = Convert.ToInt32(id),
                created_at = DateTime.Now,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                is_active = true,
                created_by = listData.user,
                member_type_id = mem_type_id
            };

            dbcontext.libarary_members.Add(libmembr);
            dbcontext.SaveChanges();          
        }

        [HttpPost]
        public ActionResult Student(student model, int? class_id, int? section_id, string card_no, int? delete_id, string user_id, string AdmissionNumber)
        {
            bind_class_rooms();
           
            int memberid = 0;
            int clickedid = 0;

            if (delete_id != 0 && delete_id != null)
            {
                
            }
            else
            {
                ViewBag.studentdeatils = cmc.get_student_class(class_id, listData.CollegeID, listData.BranchID, listData.academic_year);
               
                if (!String.IsNullOrEmpty(Request.Form["memberid"]))
                {
                    clickedid = Convert.ToInt32(Request.Form["memberid"]);
                }
                memberid = clickedid;
                if (card_no != null && !String.IsNullOrEmpty(card_no))
                {
                    StudMember(card_no, memberid, class_id, user_id, AdmissionNumber);

                }
            }
            
            return View();
        }

        [HttpGet]
        public ActionResult Student()
        {
            bind_class_rooms();
            return View();
        }

        [HttpGet]
        public ActionResult Teacher(staff model)
        {
            
            var teach = (from a in dbcontext.staffs
                         join c in dbcontext.libarary_members on a.employee_id equals c.member_type into lm
                         from lms in lm.DefaultIfEmpty()
                         where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.roll_id!= "dd184641-0108-450d-8f21-68a5d36922f4"
                         select new SchoolMasterClasses.StaffInformation
                         {
                             id = a.id,
                             email = a.email,
                             name = a.name,
                             surname = a.surname,
                             contact_no = a.contact_no,
                             dob = a.dob ,
                             library_card_no = lms.library_card_no,
                             member_id = lms.member_id,
                             emp_id = a.employee_id,
                             member_type = lms.member_type

                         }).ToList();
            ViewBag.TeacherDetails = teach;


            return View();
        }
      
        [HttpPost]
        public ActionResult Teacher(staff model, int? class_id, int? section_id, string card_no, int? del_id, string emp_id, string id)
        {
            int memberid = 0;
            int clickedid = 0;

            if (del_id != 0 && del_id != null)
            {

            }
            else
            {
               
                if (!String.IsNullOrEmpty(Request.Form["memberid"]))
                {
                    clickedid = Convert.ToInt32(Request.Form["memberid"]);
                }
                memberid = clickedid;
                if (card_no != null && !String.IsNullOrEmpty(card_no))
                {
                    TeacherMember(card_no, memberid, class_id, emp_id, id);
                    card_no = null;
                }
            }
            return RedirectToAction("Teacher");
        }

        [HttpGet]
        public ActionResult Library_Member()
        {
           
            var model = dbcontext.Database.SqlQuery<members>("select a.library_card_no, a.member_type, b.employee_id as empstu_id, b.name, b.surname, null as admission_no, b.contact_no,a.member_id  from libarary_members a inner join staff b on  a.college_id = b.college_id and b.employee_id = a.member_type union all select a.library_card_no, a.member_type, c.user_id, c.firstname, c.lastname, c.admission_no, c.mobileno, a.member_id  from libarary_members a inner join students c on a.member_type = c.user_id and a.college_id = c.college_id and a.college_id="+listData.CollegeID+" and c.college_id=" + listData.CollegeID + "").ToList();
            ViewBag.LibraryMember = model;

            return View();
        }

        [HttpPost]
        public ActionResult Library_Member(int? id)
        {

            return View();
        }


        [HttpGet]
        public ActionResult Book_Issue(string Id)
        {
            var listbook = dbcontext.books.Where(a=>a.college_id==listData.CollegeID && a.branch_id==listData.BranchID && a.is_active==true).OrderBy(a => a.bid).ToList().Select(b => new SelectListItem { Text = b.book_title.ToString(), Value = b.bid.ToString() }).ToList();
            ViewBag.getSubject = listbook;

            var userid = (from a in dbcontext.students where a.user_id == Id && a.college_id==listData.CollegeID && a.branch_id==listData.BranchID && a.is_active==true select a.user_id).FirstOrDefault();

            if (Id == userid)
            {
                var bk_isuue = (from a in dbcontext.students
                                join c in dbcontext.libarary_members on a.user_id equals c.member_type
                                where a.user_id == Id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                select new SchoolMasterClasses.StudentInformation
                                {
                                    AdmissionNumber = a.admission_no,
                                    FirstName = a.firstname,
                                    MobileNumber = a.mobileno,
                                    LastName = a.lastname,
                                    member_id = c.member_id,
                                    member_type = c.member_type,
                                    library_card_no = c.library_card_no,
                                    Gender = a.gender,
                                }).ToList();
                ViewBag.BookIsuue = bk_isuue;
            }
            else
            {
                var bmem = (from a in dbcontext.staffs
                            join c in dbcontext.libarary_members on a.employee_id equals c.member_type
                            where a.employee_id == Id && a.year == listData.academic_year
                            select new SchoolMasterClasses.StudentInformation
                            {
                                FirstName = a.name,
                                MobileNumber = a.contact_no,
                                LastName = a.surname,
                                member_id = c.member_id,
                                member_type = c.member_type,
                                library_card_no = c.library_card_no,
                                Gender = a.gender,
                            }).ToList();
                ViewBag.BookIsuue = bmem;
            }

            var books = (from a in dbcontext.books
                         join b in dbcontext.book_issues on a.bid equals b.bid
                         where b.member_type == Id && a.year == listData.academic_year & b.is_active == true
                         select new SchoolMasterClasses.Book
                         {
                             subject = a.subject,
                             book_no = a.book_no,
                             issue_date = b.issue_date,
                             return_date = b.return_date
                         }).ToList();
            ViewBag.GetBookIssue = books;
            return View();
        }

        [HttpPost]
        public ActionResult Book_Issue(book_issues b_issue, int? id, int? bid, int? member_id, string member_type)
        {
            var listbook = dbcontext.books.OrderBy(a => a.bid).ToList().Select(b => new SelectListItem { Text = b.book_title.ToString(), Value = b.bid.ToString() }).ToList();
            ViewBag.getSubject = listbook;

            var l_id = (from z in dbcontext.libarary_members where z.member_type == member_type select z.id).FirstOrDefault();

            var updateisuuebook = (from k in dbcontext.book_issues where k.member_type == member_type & k.bid == bid select k).FirstOrDefault();
            var ss = TempData["count"].ToString();
            int sc = Convert.ToInt32(ss);
            if (sc != 0)
            {

                if (updateisuuebook == null)
                {
                    book_issues bk_issue = new book_issues()
                    {
                        bid = bid,
                        book_id = bid,
                        issue_date = DateTime.Now,
                        return_date = b_issue.return_date,
                        is_returned = 1,
                        member_id = l_id,
                        member_type = member_type,
                        is_active = true,
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        created_at = DateTime.Now,
                        created_by = listData.user,
                       
                    };
                    dbcontext.book_issues.Add(bk_issue);
                }
                else
                {
                    updateisuuebook.bid = bid;
                    updateisuuebook.book_id = bid;
                    updateisuuebook.issue_date = DateTime.Now;
                    updateisuuebook.return_date = b_issue.return_date;
                    updateisuuebook.is_returned = 1;
                    updateisuuebook.member_id = l_id;
                    updateisuuebook.member_type = member_type;
                    updateisuuebook.is_active = true;
                    updateisuuebook.college_id = listData.CollegeID;
                    updateisuuebook.branch_id = listData.BranchID;
                    updateisuuebook.modified_by = listData.user;
                }
                dbcontext.SaveChanges();
            }
            else
            {
                TempData["message"] = "No More Books";
            }
            return RedirectToAction("Book_Issue");
        }

        [HttpGet]
        public ActionResult returnbookhide()
        {

            return View();
        }

        [HttpPost]
        public ActionResult returnbookhide(book_issues b_issue, string mtype, string bsubject, int? bid)
        {
            var listbook = dbcontext.books.OrderBy(a => a.bid).ToList().Select(b => new SelectListItem { Text = b.book_title.ToString(), Value = b.bid.ToString() }).ToList();
            ViewBag.getSubject = listbook;

            var bookid = (from b in dbcontext.books where b.subject == bsubject select b.bid).FirstOrDefault();
            var b_id = (from a in dbcontext.book_issues where a.member_type == mtype & a.bid == bookid select a).FirstOrDefault();

            var l_id = (from z in dbcontext.libarary_members where z.member_type == mtype select z.id).FirstOrDefault();
            if (b_id == null)
            {
                book_issues bk_issue = new book_issues()
                {
                    bid = bid,
                    book_id = null,
                    issue_date = DateTime.Now,
                    return_date = b_issue.return_date,
                    is_returned = 1,
                    member_id = l_id,
                    member_type = mtype,
                    is_active = false,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    created_at = DateTime.Now,
                    created_by = listData.user

                };
                dbcontext.book_issues.AddOrUpdate(bk_issue);
            }
            else
            {

                b_id.bid = b_id.bid;
                b_id.book_id = null;
                b_id.return_date = DateTime.Now;
                b_id.is_returned = 1;
                b_id.member_id = l_id;
                b_id.member_type = mtype;
                b_id.is_active = false;
                b_id.college_id = listData.CollegeID;
                b_id.branch_id = listData.BranchID;
                b_id.modified_by = listData.user;
                dbcontext.SaveChanges();

            }
            return RedirectToAction("Book_Issue", new { @Id = mtype });
        }


        public JsonResult BookAvailable(int? bid)
        {
            var gettotalbook = (from a in dbcontext.books where a.bid == bid && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year select a.available).FirstOrDefault();
            var countbooks = (from b in dbcontext.book_issues where b.book_id == bid && b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year select b).Count();
            int gettotbook = Convert.ToInt32(gettotalbook);
            int ct = gettotbook - countbooks;
            TempData["count"] = ct;
            int res = 0;
            if (ct != 0)
            {
                res = ct;
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}
