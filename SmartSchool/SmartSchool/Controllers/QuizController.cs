﻿using SmartSchool.Classes;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartSchool.Controllers
{
    public class QuizController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData; public QuizController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in QuizController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
        // GET: Quiz
        private void BindClass()
        {
            var list = dbContext.AddClasses.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).OrderBy(a => a.class_id).ToList().Select(b => new SelectListItem { Value = b.class_id.ToString(), Text = b.class_name }).ToList();
            ViewBag.classlist = list;

        }
        public void BindQuiz()
        {
            List<Quiz> quiz = dbContext.Quizs.Where(a => a.QuizID != 0 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            Quiz q = new Quiz
            {
                QuizID = 0,
                QuizName = "Please Select Quiz"
            };
            quiz.Insert(0, q);
            SelectList quizdata = new SelectList(quiz, "QuizID", "QuizName", 0);
            ViewBag.QuizList = quizdata;
        }
        [HttpPost]
        public JsonResult getSubjects(int? ddlclass)
        {
            var getstaffdata = (from a in dbContext.subjects
                                join b in dbContext.teacher_subjects on a.id equals b.subject_id
                                join c in dbContext.class_sections on b.class_section_id equals c.id
                                where c.class_id == ddlclass
                                select new StudentInformation
                                {
                                    class_id = c.class_id,
                                    SubjectName = a.name,
                                    Subjectid = a.id

                                }).ToList();
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult QuizCreate()
        {
            BindClass();
            var list = (from a in dbContext.Quizs
                        join b in dbContext.AddClasses on a.ClassID equals b.class_id
                        join c in dbContext.subjects on a.SubjectID equals c.id
                        where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                        select new StudentInformation
                        {
                            id = a.QuizID,
                            Class = b.class_name,
                            SubjectName = c.name,
                            Desription = a.QuizName,
                            Status1 = a.Status


                        }).ToList();
            ViewBag.QuizList = list;
            return View();
        }
        [HttpPost]
        public ActionResult QuizCreate(Quiz _quiz)
        {
            BindClass();

            var CategoryCheck = dbContext.Quizs.
                Where(q => q.ClassID == _quiz.ClassID && q.SubjectID == _quiz.SubjectID && q.college_id == listData.CollegeID && q.branch_id == listData.BranchID && q.year == listData.academic_year).Any();
            if (CategoryCheck == true)
            {
                TempData["msg"] = "Quiz for this Class and Subject Already Exist!  Please Add Questions";
            }
            else
            {
                Quiz insert = new Quiz()
                {
                    ClassID = _quiz.ClassID,
                    SubjectID = _quiz.SubjectID,
                    Status = true,
                    QuizName = _quiz.QuizName,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year = listData.academic_year,
                    CreatedDate = DateTime.Now
                };
                try
                {
                    try
                    {
                        dbContext.Quizs.Add(insert);
                        int c = dbContext.SaveChanges();

                        TempData["result"] = "Quiz Added Successfully";
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                        TempData["result"] = "Something Went Wrong";
                    }
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            return RedirectToAction("QuizCreate", "Quiz");
        }
        [HttpGet]
        public ActionResult QuizDetails(int _QuizID)
        {
            var Quiz = dbContext.Quizs.Single(p => p.QuizID == _QuizID && p.college_id == listData.CollegeID && p.branch_id == listData.BranchID && p.year == listData.academic_year);
            ViewBag.QuizName = Quiz.QuizName;
            ViewBag.QuizID = Quiz.QuizID;

            var Questions = dbContext.Questions.Where(qu => qu.QuizID == _QuizID && qu.college_id == listData.CollegeID && qu.branch_id == listData.BranchID && qu.year == listData.academic_year).OrderByDescending(d => d.CreatedDate).ToList();
            var _questionIDs = (from que in dbContext.Questions where que.QuizID == _QuizID && que.college_id == listData.CollegeID && que.branch_id == listData.BranchID && que.year == listData.academic_year && que.Status == true select que.QuestionID).ToList();

            var answers = dbContext.Answers.Where(i => _questionIDs.Contains((int)i.QuestionID) && i.college_id == listData.CollegeID && i.branch_id == listData.BranchID && i.year == listData.academic_year).ToList();
            ViewBag.Answers = answers;

            return View(Questions);
        }
        protected bool checkQuizID(int? _QuizId)
        {
            var qid = (from quiz in dbContext.Quizs
                       join test in dbContext.Test_History on quiz.QuizID equals test.QuizID
                       where test.QuizID == _QuizId && quiz.college_id == listData.CollegeID && quiz.branch_id == listData.BranchID && quiz.year == listData.academic_year
                       select new Classes.MyAnswerModel
                       {
                           QuizID = test.QuizID
                       }).Count();

            if (Convert.ToInt32(qid) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpGet]
        public ActionResult QuizDelete(int? _QuizId)
        {
            bool retVal = checkQuizID(_QuizId);
            if (retVal == true)
            {
                TempData["result"] = "Quiz Taken..Cannot Be Deleted..!";
                return RedirectToAction("QuizCreate");
            }
            else
            {
                var quiz = dbContext.Quizs.Where(a => a.QuizID == _QuizId && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                if (quiz != null)
                {
                    dbContext.Quizs.Remove(quiz);
                    dbContext.SaveChanges();
                    try
                    {
                        TempData["result"] = "Record deleted Successfully..!";
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Something Went Wrong While Deleting Data");
                    }
                }
            }
            return RedirectToAction("QuizCreate");

        }

        [HttpPost]
        public JsonResult quizstatus(string id)
        {
            var updte = (from updatequiz in dbContext.Quizs where updatequiz.QuizID.ToString() == id select updatequiz).FirstOrDefault();
            //getval = idss > 0 ? true : false;
            if (updte != null && (updte.Status == false || updte.Status == null))
            {
                updte.Status = true;
                updte.ModifiedDate = DateTime.Now;
                dbContext.Quizs.AddOrUpdate(updte);
            }
            else if (updte != null && updte.Status == true)
            {
                updte.Status = false;
                updte.ModifiedDate = DateTime.Now;
                dbContext.Quizs.AddOrUpdate(updte);
            };
            try
            {

                int save = dbContext.SaveChanges();

                if (save == 1)
                {
                    //TempData["result"] = "Record Edited Successfully!";
                }//ModelState.Clear();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                TempData["result"] = "Something Went Wrong!";
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult QuestionInsert(int _quizID)
        {
            int _QuizID = Convert.ToInt32(Session["_CategoryID"]) != 0 ? Convert.ToInt32(Session["_CategoryID"]) : Convert.ToInt32(Session["_QuizID"]);

            var Quiz = dbContext.Questions.Where(a => a.QuizID == _quizID && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            var Quizs = dbContext.Quizs.Where(a => a.QuizID == _quizID && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();//dbContext.Quizs.Single(p => p.QuizID == _QuizID);
            ViewBag.QuizNames = Quiz;
            ViewBag.QuizID = Quizs.QuizID;
            ViewBag.QuizName = Quizs.QuizName;

            var Questions = dbContext.Questions.Where(qu => qu.QuizID == _quizID && qu.college_id == listData.CollegeID && qu.branch_id == listData.BranchID && qu.year == listData.academic_year).OrderByDescending(d => d.ModifiedDate).ThenBy(k => k.CreatedDate).ToList();
            var _questionIDs = (from que in dbContext.Questions where que.QuizID == _quizID && que.college_id == listData.CollegeID && que.branch_id == listData.BranchID && que.year == listData.academic_year select que.QuestionID).ToList();

            var answers = dbContext.Answers.Where(i => _questionIDs.Contains((int)i.QuestionID)).ToList();
            ViewBag.Answers = answers;
            Session["_QuizID"] = _quizID;
            ViewBag._QuizID = _quizID;
            BindQuiz();
            return View();
        }
        [HttpPost]
        public ActionResult QuestionInsert(String QuestionText, EditQueClasses queClasses)
        {
            int _quizID = Convert.ToInt32(Session["_QuizID"]);
            ViewBag._QuizID = _quizID;

            Question _insertQuestion = new Question()
            {
                QuizID = _quizID,
                QuestionText = QuestionText,
                CreatedDate = DateTime.Now,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year,

                Status = true
            };
            dbContext.Questions.Add(_insertQuestion);
            dbContext.SaveChanges();
            for (int i = 0; i < 4; i++)
            {
                Answer _insertAnswer = new Answer()
                {
                    QuestionID = _insertQuestion.QuestionID,
                    AnswerText = queClasses.AnswerText[i],
                    IsAnswer = queClasses.IsAnswer[i],
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year = listData.academic_year,
                    CreatedDate = DateTime.Now
                };
                dbContext.Answers.Add(_insertAnswer);
            }

            try
            {
                int c = dbContext.SaveChanges();

                ModelState.Clear();
                TempData["result"] = "Question Added Successfully";
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                TempData["result"] = "Something Went Wrong";
            }

            return RedirectToAction("QuestionInsert", new { _quizID = _quizID });
        }

        [HttpGet]
        public ActionResult EditQuestions(int? queID, int? _QuizID)
        {

            ViewBag._QuizID = queID;
            Session["_QuizID"] = _QuizID;
            Session["_QueID"] = queID;
            //   BindQuiz();
            var Qdata = (from qu in dbContext.Questions
                         join ass in dbContext.Answers on qu.QuestionID equals ass.QuestionID
                         where ass.QuestionID == queID && qu.Status == true && qu.college_id == listData.CollegeID && qu.branch_id == listData.BranchID && qu.year == listData.academic_year
                         select new MyAnswerModel
                         {
                             QuestionName = qu.QuestionText,
                             AnswerName = ass.AnswerText,
                             isAnswer = (bool)ass.IsAnswer,
                             AnswerID = ass.AnswerID
                         }).ToList();
            ViewBag.data = Qdata;
            if (ViewBag.data.Count == 0)
            {
                TempData["msg"] = "Question is in InActive State Please Active and Edit";
                return RedirectToAction("QuestionInsert", new { _quizID = _QuizID });
            }

            EditQueClasses myAnswerModel = new EditQueClasses
            {
                question = Qdata.Select(k => k.QuestionName).FirstOrDefault(),
                options = Qdata.Select(k => k.AnswerName).ToList(),
                IsAnswer = Qdata.Select(k => k.isAnswer).ToList(),
                answerID = Qdata.Select(k => k.AnswerID).ToList(),
                quizId = _QuizID
            };
            return View(myAnswerModel);
        }

        [HttpPost]
        public ActionResult EditQuestions(String question, EditQueClasses queClasses)
        {
            int _quizID = Convert.ToInt32(Session["_QuizID"]);
            int _queID = Convert.ToInt32(Session["_QueID"]);
            ViewBag._QuizID = _quizID;

            Question _insertQuestion = new Question()
            {
                QuizID = _quizID,
                QuestionID = _queID,
                QuestionText = question,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                Status = true

            };
            dbContext.Questions.AddOrUpdate(k => new { k.QuizID, k.QuestionID }, _insertQuestion);
            dbContext.SaveChanges();

            for (int i = 0; i < 4; i++)
            {
                int ansid = queClasses.answerID[i];
                var updateData = dbContext.Answers.Where(k => k.AnswerID == ansid && k.QuestionID == _insertQuestion.QuestionID && k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).FirstOrDefault();

                updateData.AnswerText = queClasses.options[i];
                updateData.IsAnswer = queClasses.IsAnswer[i];
                updateData.college_id = listData.CollegeID;
                updateData.branch_id = listData.BranchID;
                updateData.year = listData.academic_year;
                updateData.CreatedDate = DateTime.Now;
                updateData.ModifiedDate = DateTime.Now;
                dbContext.Answers.AddOrUpdate(updateData);
            }
            try
            {
                int c = dbContext.SaveChanges();
                ModelState.Clear();
                TempData["msg"] = "Question Edited Successfully";
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                TempData["msg"] = "Something Went Wrong";
            }
            Session["_QuizID"] = null;
            Session["_QueID"] = null;
            return RedirectToAction("QuestionInsert", new { _quizID = _quizID });
        }


        protected bool checkQuidandQuesId(int? quizid, int? quesid)
        {
            var Qdata = (from th in dbContext.Test_History
                         join qu in dbContext.Quizs on th.QuizID equals qu.QuizID
                         join que in dbContext.Questions on th.QuestionID equals que.QuestionID
                         where th.QuizID == quizid && th.QuestionID == quesid && qu.QuizID == que.QuizID
                         select new Classes.MyAnswerModel
                         {
                             QuizID = th.QuizID,
                             QuestionID = th.QuestionID
                         }).Count();
            if (Qdata > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public ActionResult DeleteQuestions(int? _QuizID, int? queID)
        {
            //int _quizID = Convert.ToInt32(Session["_QuizID"]);
            //int _queID = Convert.ToInt32(Session["_QueID"]);

            bool retVal = checkQuidandQuesId(_QuizID, queID);
            if (retVal == true)
            {
                TempData["msg"] = "Question Attended..Cannot be Deleted!";
            }
            else
            {
                var que = dbContext.Questions.Where(k => k.QuestionID == queID && k.QuizID == _QuizID && k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).FirstOrDefault();
                dbContext.Questions.Remove(que);
                int sav = dbContext.SaveChanges();
                if (sav == 0)
                {
                    TempData["msg"] = "Cannot be Delete because It Has Foregin key Reference";
                }

                else if (sav > 0)
                {
                    TempData["msg"] = "Question Deleted Successfully!";
                }
                else
                {
                    TempData["msg"] = "Something went wrong, Contact Admin..!";
                }

            }
            return RedirectToAction("QuestionInsert", new { _quizID = _QuizID });
        }
        public JsonResult stdstatus(string id)
        {
            var updte = (from updatestudent in dbContext.Questions where updatestudent.QuestionID.ToString() == id select updatestudent).FirstOrDefault();
            //getval = idss > 0 ? true : false;
            if (updte != null && (updte.Status == false || updte.Status == null))
            {
                updte.Status = true;
                updte.ModifiedDate = DateTime.Now;
                dbContext.Questions.AddOrUpdate(updte);
            }
            else if (updte != null && updte.Status == true)
            {
                updte.Status = false;
                updte.ModifiedDate = DateTime.Now;
                dbContext.Questions.AddOrUpdate(updte);
            };
            try
            {

                ////dbcontext.ContributorProfiles.AddOrUpdate(updte);
                int save = dbContext.SaveChanges();

                if (save == 1)
                {
                    //TempData["result"] = "Record Edited Successfully!";
                }//ModelState.Clear();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                TempData["result"] = "Something Went Wrong!";
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}