﻿using OfficeOpenXml;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace SmartSchool.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        //public int CollegeID = 2015, BranchID = 1;
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;


        public ReportController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StudentInformation()
        {
            return View();
        }
        public ActionResult StudentImport()
        {
            return View();

        }

       

        public ActionResult StudentAdmission(SchoolMasterClasses.StudentInformation model, HttpPostedFileBase ImageFile)
        {
            BindCategory();
            BindClass();
            BindSection();
            BindHosteRooms();
            
            string stufilesnames = "";
            if (ImageFile != null)
            {
                string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + Path.GetFileName(ImageFile.FileName);
                string dirPath = Server.MapPath("~/Content/Profile/");

                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }


                ImageFile.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), savedFileName)); // Save the file
                stufilesnames = savedFileName;
            }
            student sa = new student()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                user_id = "1",
                admission_no = model.AdmissionNumber,
                roll_no = model.RollNumber,
                firstname = model.FirstName,
                lastname = model.LastName,
                gender = model.Gender,
                dob = model.DateOfBirth,
                // category_id = model.Category,
                religion = Convert.ToInt32(model.Religion),
                cast = Convert.ToInt32(model.Caste),
                mobileno = model.MobileNumber,
                email = model.Email,
                admission_date = model.AdmissionDate,
                blood_group = model.BloodGroup,
                //  state = model.StudentHouse.ToString(),
                height = model.Height,
                weight = model.Weight,
                measurement_date = DateTime.Today,
                father_name = model.FatherName,
                mother_name = model.MotherName,
                father_phone = model.FatherPhone,
                mother_phone = model.MotherPhone,
                father_occupation = model.FatherOccupation,
                mother_occupation = model.MotherOccupation,
                father_pic = stufilesnames,
                mother_pic = model.MotherPhoto,
                guardian_name = model.GuardianName,
                guardian_relation = model.GuardianRelation,
                guardian_email = model.GuardianEmail,
                guardian_occupation = model.GuardianOccupation,
                guardian_phone = model.GuardianPhone,
                guardian_address = model.GuardianAddress,
                guardian_pic = model.GuardianPhoto,
                current_address = model.CurrentAddress,
                permanent_address = model.PermanentAddress,
                //   hostel_room_id = model.RoomNumber(),
                bank_account_no = model.BankAccountNumber,
                bank_name = model.BankName,
                ifsc_code = model.IFSCCode,
                rte = model.RTE,
                previous_school = model.PreviousSchoolDetails,
                note = model.Note,
                created_by = listData.user,
                created_date = DateTime.Now,
            };
            dbContext.students.Add(sa);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();

        }

        private void BindCategory()
        {
            List<category> c = dbContext.categories.Where(x => x.id != 0).ToList();
            category sem = new category
            {
                category1 = "----Please select category----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "category1 ", 0);
            ViewBag.viewbagcatlist = selectcat;
        }

        private void BindClass()
        {
            List<AddClass> c = dbContext.AddClasses.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year).ToList();
            AddClass cla = new AddClass

            {
                class_name = "select class name",
                class_id = 0
            };
            c.Insert(0, cla);
            SelectList selectclass = new SelectList(c, "class_id", "class_name", 0);
            ViewBag.viewbagclasslist = selectclass;
        }

        private void BindSection()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0).ToList();
            section sem = new section
            {
                section1 = "----Please Select Section----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            ViewBag.viewbagsectionlist = selectsect;
        }


        private void BindHosteRooms()
        {
            List<hostel_rooms> c = dbContext.hostel_rooms.Where(x => x.id != 0).ToList();
            hostel_rooms sem = new hostel_rooms
            {
                room_no = "----Please Select Rooms----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectRooms = new SelectList(c, "id", "room_no ", 0);
            ViewBag.viewbagRoomslist = selectRooms;
        }
        private void BindStudentHouse()
        {
            List<school_houses> c = dbContext.school_houses.Where(x => x.id != 0).ToList();
            school_houses sem = new school_houses
            {
                house_name = "----Please Select House----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectHouse = new SelectList(c, "id", "house_name ", 0);
            ViewBag.viewbagHouselist = selectHouse;
        }

        [HttpGet]
        public ActionResult StudentAdmissionlist()
        {
            BindSection();
            BindClass();
            var studetails = (from a in dbContext.students.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;

            // return View(studetails);
            return View();
        }

        [HttpGet]
        public ActionResult guardiareport()
        {
            BindSection();
            BindClass();
            var studetails = (from a in dbContext.students.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;

            // return View(studetails);
            return View();
        }

        [HttpGet]
        public ActionResult disablestudentslist()
        {

            BindSection();
            BindClass();
            var studetails = (from a in dbContext.students.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;

            // return View(studetails);
            return View();
        }

        [HttpGet]
        public ActionResult logindetailreport()
        {

            BindSection();
            BindClass();
            var studetails = (from a in dbContext.students.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;

            return View();
        }
        [HttpPost]
        public ActionResult StudentSchoolHouse(school_houses model)
        {
            school_houses schhouse = new school_houses
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                house_name = model.house_name,
                description = model.description,
                created_date = DateTime.Now,
                created_by = listData.user

            };
            dbContext.school_houses.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();
        }

        public ActionResult StudentHouse(school_houses model)
        {

            school_houses schhouse = new school_houses
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                house_name = model.house_name,
                description = model.description,
                is_active = model.is_active,
                created_date = DateTime.Now,
                created_by = listData.user

            };
            dbContext.school_houses.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();
        }

        [HttpGet]
        public ActionResult StudentHouse()
        {
            var studetails = (from a in dbContext.school_houses.Where(x => x.college_id != 0) select a).ToList();
            ViewBag.studentlist = studetails;
            return View();
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        [HttpPost]
        public ActionResult Edit(school_houses house)
        {
            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == house.id);


            if (employee != null)
            {
                employee.house_name = house.house_name;
                employee.description = house.description;
                employee.is_active = house.is_active;
                employee.modified_by = listData.user;
                employee.modified_date = DateTime.Now;
                employee.college_id = listData.CollegeID;
                employee.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            return RedirectToAction("StudentHouse");

        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == id);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var employee = dbContext.school_houses.SingleOrDefault(x => x.id == id);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.school_houses.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return RedirectToAction("StudentHouse");
        }
        public ActionResult StudentCategory(category cat)
        {


            category schhouse = new category
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                category1 = cat.category1,
                is_active = cat.is_active,
                created_by = listData.user,
                created_date = DateTime.Now,

            };
            dbContext.categories.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();
        }

        [HttpGet]
        public ActionResult StudentCategory()
        {
            var Catdetails = (from a in dbContext.categories.Where(x => x.college_id != 0) select a).ToList();
            if (Catdetails != null)
            {
                ViewBag.catlist = Catdetails;
            }
           
            return View();
        }

        public ActionResult Editcat(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.categories.SingleOrDefault(e => e.id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Editcat(category house)
        {
            var cat = dbContext.categories.SingleOrDefault(e => e.id == house.id);


            if (cat != null)
            {

                cat.category1 = house.category1;
                cat.is_active = house.is_active;
                cat.modified_by = listData.user;
                cat.updated_date = DateTime.Now;
                cat.college_id = listData.CollegeID;
                cat.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            return RedirectToAction("StudentHouse");

        }

        public ActionResult Deletecat(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.categories.SingleOrDefault(e => e.id == id);
            

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Deletecat(int id)
        {
            var employee = dbContext.categories.SingleOrDefault(x => x.id == id);
            employee.modified_by = listData.user;
            employee.updated_date = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.categories.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return RedirectToAction("StudentHouse");
        }

        [HttpGet]
        public ActionResult studentfee()
        {
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id
                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     // Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
           
            return View();
        }

        [HttpGet]
        public ActionResult searchpayment()
        {
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id
                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     // Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }

        [HttpGet]
        public ActionResult feesstatement()
        {
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id

                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     //   Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte



                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
           
            return View();
        }

        public ActionResult baancefeereort()
        {
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id

                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     //   Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte



                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }

        public ActionResult feesmaster()
        {
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id

                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     //   Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte



                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }



        public ActionResult student_report()
        {
            
            BindClass();
            BindSection();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id

                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     //  Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte

                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
          
            return View();
        }


     

        public ActionResult Transaction_report()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Transaction_report(string fromDate, string toDate)
        {
            if (fromDate != null && toDate != null)
            {
                DateTime frmDt = DateTime.Parse(fromDate);
                DateTime toDt = DateTime.Parse(toDate);
                ViewBag.datedisplay = "-Records From " + frmDt.ToShortDateString() + " To " + toDt.ToShortDateString() + "";

                var feepaiddetails = (from l in dbContext.fee_deposit_amountdetails
                                      join x in dbContext.students on l.student_id equals x.user_id
                                      join z in dbContext.AddClasses on x.class_id equals z.class_id

                                      join y in dbContext.student_fees_deposite on l.dpid equals y.id
                                      join s in dbContext.fee_groups on y.fee_groups_feetype_id equals s.id
                                      where l.date >= frmDt && l.date <= toDt

                                      select new Stdentfeeclass1
                                      {
                                          amount = l.amount,
                                          Discount = l.amount_discount,
                                          fine = l.amount_fine,
                                          mode = l.payment_mode,

                                          Date = l.date,
                                          // Date = String.Format(l.date.ToString(), dateformat),

                                          id = l.dpid,
                                          //Invoice_Id = l.inv_no.ToString(),
                                          UserId = l.student_id,
                                          Name = x.firstname,
                                          Class = z.class_name,
                                          Feetype = s.name,
                                          //total = (l.amount - double.Parse(l.amount_discount)) + double.Parse(l.amount_fine)
                                      }).ToList();
                ViewBag.feedetails = feepaiddetails;
                var total = feepaiddetails.Select(a => a.amount).Sum();
                ViewBag.total = total;
                var fine = feepaiddetails.Select(z => Convert.ToDouble(z.fine)).Sum();
                ViewBag.finetotal = fine;
                var Dictotal = feepaiddetails.Select(y => Convert.ToDouble(y.Discount)).Sum();
                ViewBag.Dictotal = Dictotal;
                ViewBag.alltotal = (total - Dictotal) + fine;

                var incm = (from a in dbContext.incomes
                            join b in dbContext.income_head on a.inc_head_id equals b.ihid.ToString()
                            where a.date >= frmDt && a.date <= toDt

                            select new expenses_Report
                            {
                                id = a.iid,
                                Date = a.date,
                                Expense_Head = b.income_category,
                                Name = a.name,
                                InvoiceNo = a.invoice_no,
                                Amount = a.amount

                            }).ToList();
                ViewBag.incmm = incm;
                ViewBag.tot = incm.Sum(x => x.Amount);



                var exp = (from a in dbContext.expenses
                           join b in dbContext.expense_head on a.exp_head_id equals b.id
                           where a.date >= frmDt && a.year == listData.academic_year && a.date <= toDt

                           select new expenses_Report
                           {
                               id = a.eid,
                               Date = a.date,
                               Expense_Head = b.exp_category,
                               Name = a.name,
                               InvoiceNo = a.invoice_no,
                               Amount = a.amount

                           }).ToList();
                ViewBag.exprep = exp;
                ViewBag.tot1 = exp.Sum(x => x.Amount);
                return View();
            }
            else
            {
                var feepaiddetails = (from l in dbContext.fee_deposit_amountdetails
                                      join x in dbContext.students on l.student_id equals x.user_id
                                      join z in dbContext.AddClasses on x.class_id equals z.class_id

                                      join y in dbContext.student_fees_deposite on l.dpid equals y.id
                                      join s in dbContext.fee_groups on y.fee_groups_feetype_id equals s.id
                                      select new Stdentfeeclass1
                                      {
                                          amount = l.amount,
                                          Discount = l.amount_discount,
                                          fine = l.amount_fine,
                                          mode = l.payment_mode,

                                          Date = l.date,
                                          id = l.dpid,

                                          UserId = l.student_id,
                                          Name = x.firstname,
                                          Class = z.class_name,
                                          Feetype = s.name,



                                      }).ToList();
                ViewBag.feedetails = feepaiddetails;

                var total = feepaiddetails.Select(a => a.amount).Sum();
                ViewBag.total = total;
                var fine = feepaiddetails.Select(z => Convert.ToDouble(z.fine)).Sum();
                ViewBag.finetotal = fine;
                var Dictotal = feepaiddetails.Select(y => Convert.ToDouble(y.Discount)).Sum();
                ViewBag.Dictotal = Dictotal;
                ViewBag.alltotal = (total - Dictotal) + fine;

                var incm = (from a in dbContext.incomes
                            join b in dbContext.income_head on a.inc_head_id equals b.ihid.ToString()

                            select new expenses_Report
                            {
                                id = a.iid,
                                Date = a.date,
                                Expense_Head = b.income_category,
                                Name = a.name,
                                InvoiceNo = a.invoice_no,
                                Amount = a.amount

                            }).ToList();
                ViewBag.incmm = incm;
                ViewBag.tot = incm.Sum(x => x.Amount);
            }
            var exp1 = (from a in dbContext.expenses
                        join b in dbContext.expense_head on a.exp_head_id equals b.id


                        select new expenses_Report
                        {
                            id = a.eid,
                            Date = a.date,
                            Expense_Head = b.exp_category,
                            Name = a.name,
                            InvoiceNo = a.invoice_no,
                            Amount = a.amount

                        }).ToList();
            ViewBag.exprep = exp1;
            ViewBag.tot1 = exp1.Sum(x => x.Amount);
            return View();
        }
        //Transaction Report End//
    }
}