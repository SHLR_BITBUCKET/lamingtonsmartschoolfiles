﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartSchool.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartSchool.SchoolMasterClasses;
using Newtonsoft.Json;
using SmartSchool.Interfaces;
using static SmartSchool.SchoolMasterClasses.StudentInformation;

namespace SmartSchool.Controllers
{
    public class RolesController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();

        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public RolesController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in RolesController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        // GET: Roles
        public ActionResult Index()
        {
            ViewBag.list = dbcontext.AspNetRoles.Where(k=>k.Id != "dd184641-0108-450d-8f21-68a5d36922f4").ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.list = dbcontext.AspNetRoles.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                ViewBag.list = context.Roles.ToList();

                context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
                {
                    Name = collection["RoleName"]
                });
                int save = context.SaveChanges();
                if (save > 0)
                {
                    TempData["message"] = "Role Created Successfully";
                }

                else
                {
                    TempData["message"] = "Oops..... Somethong Went Wrong.....!!!!";
                }
                return RedirectToAction("Index");
            }
            catch
            {

                return View();
            }
        }
        public ActionResult Edit(string roleName)
        {
            ViewBag.list = dbcontext.AspNetRoles.ToList();
            var thisRole = dbcontext.AspNetRoles.Where(r => r.Name.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            ViewBag.Role = thisRole;
            ViewBag.pop3 = "1";
            return View("Index");
        }


        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AspNetRole role, string RoleId, string RoleName)
        {
            ViewBag.list = dbcontext.AspNetRoles.ToList();
            try
            {
                var ID = RoleId;
                var idz = dbcontext.AspNetRoles.Where(x => x.Id == ID).Select(a => a.Id).FirstOrDefault();
                AspNetRole asp = dbcontext.AspNetRoles.Find(idz);
                if (asp.Id == idz)
                {
                    asp.Id = asp.Id;
                    asp.Name = RoleName;
                                     
                }

                dbcontext.AspNetRoles.AddOrUpdate(asp);
                int save = dbcontext.SaveChanges();
                if (save > 0)
                {
                    TempData["message"] = "Record edited Successfully!";
                    ModelState.Clear();
                }
                else
                {
                    TempData["message"] = "Please Try Again!";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(string RoleName)
        {
            var thisRole = context.Roles.Where(r => r.Name.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            context.Roles.Remove(thisRole);
            int save = context.SaveChanges();
            if (save > 0)
            {
                TempData["message"] = "Record Deleted Successfully!";
                ModelState.Clear();
            }
            else
            {
                TempData["message"] = "Please Try Again!";
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult AssignRole(string roleid)
        {            
            var user = (from asp in dbcontext.AspNetRoles where asp.Id == roleid select asp.Name).FirstOrDefault();
            if (user == "SuperAdmin")
            {
                nodatainroles();
            }
            else
            {
                ViewBag.getall = (from pg in dbcontext.permission_group
                                  join pc in dbcontext.permission_category on pg.id equals pc.perm_group_id
                                  join pr in dbcontext.roles_permissions on pc.id equals pr.perm_cat_id into nep
                                  from x in nep.DefaultIfEmpty()
                                  where x.college_id == listData.CollegeID && x.year == listData.academic_year && x.branch_id == listData.BranchID && x.role_id == roleid
                                  select new AssignRole
                                  {
                                      id = pg.id,//cat id
                                      name = pg.name,//cat name
                                      Mname = pc.name,//role name
                                      cat_id = pc.id,
                                      can_view = x.can_view == null ? 0 : (int)x.can_view,
                                      can_add = x.can_add == null ? 0 : (int)x.can_add,
                                      can_edit = x.can_edit == null ? 0 : (int)x.can_edit,
                                      can_delete = x.can_delete == null ? 0 : (int)x.can_delete
                                  }).ToList();

                if (ViewBag.getall.Count == 0)
                {
                    nodatainroles();
                }

            }

            return View();
        }


        protected void nodatainroles()
        {
            ViewBag.getall = (from pg in dbcontext.permission_group
                              join pc in dbcontext.permission_category on pg.id equals pc.perm_group_id
                              join pr in dbcontext.roles_permissions on pc.id equals pr.perm_cat_id into nep
                              from x in nep.DefaultIfEmpty()
                              where x.college_id == 0 && x.branch_id == 0 && x.year == listData.academic_year
                              select new AssignRole
                              {
                                  id = pg.id,//cat id
                                  name = pg.name,//cat name
                                  Mname = pc.name,//role name
                                  cat_id = pc.id,
                                  can_view = x.can_view == null ? 0 : (int)x.can_view,
                                  can_add = x.can_add == null ? 0 : (int)x.can_add,
                                  can_edit = x.can_edit == null ? 0 : (int)x.can_edit,
                                  can_delete = x.can_delete == null ? 0 : (int)x.can_delete
                              }).ToList();
        }


        [HttpPost]
        public JsonResult roles_save(List<saveroles> allval, string getpath)
        {
            bool res = false;
            foreach (var items in allval)
            {
                var catid = items.catid;
                var fea = items.get_fea;
                var name = items.modelname;
                var cv = items.get_fun[0];
                var ca = items.get_fun[1];
                var ce = items.get_fun[2];
                var cd = items.get_fun[3];

               res = saveroles(Convert.ToInt32(catid), Convert.ToInt32(cv), Convert.ToInt32(ca), Convert.ToInt32(ce), Convert.ToInt32(cd), getpath);
            }
            if (res == true)
            {
                return Json("Roles Saved Successfully..!", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Session expired... Please Login Again!", JsonRequestBehavior.AllowGet);
            }
        }
        protected bool saveroles(int catid, int? cv, int? ca, int? ce, int? cd, string aspnetroleid)
        {
            if (listData != null)
            {
                dynamic sesval = Session["sessiondata"];
                roles_permissions pr = new roles_permissions
                {
                    role_id = aspnetroleid,
                    perm_cat_id = catid,
                    can_view = cv,
                    can_add = ca,
                    can_edit = ce,
                    can_delete = cd,
                    created_at = DateTime.Now,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    created_by = listData.user

                };
                dbcontext.roles_permissions.AddOrUpdate(k => new { k.role_id, k.perm_cat_id, k.college_id, k.branch_id }, pr);
                dbcontext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        public ActionResult AssignRole()
        {
            return View();
        }

    }
}