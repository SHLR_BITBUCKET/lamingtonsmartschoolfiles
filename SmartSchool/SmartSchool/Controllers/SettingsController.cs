﻿using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SmartSchool.Interfaces;

namespace SmartSchool.Controllers
{
    public class SettingsController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public SettingsController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in SettingsController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Notificationsetting()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Emailconfig()

        {
            var emailsett = dbContext.email_config.Where(s => s.college_id == listData.CollegeID && s.branch_id == listData.BranchID && s.year == listData.academic_year).ToList();
            ViewBag.emailList = emailsett;
            return View();
        }

        [HttpPost]
        public ActionResult Emailconfig(email_config model)
        {
            email_config email_Config = new email_config()
            {
                email_type = model.email_type,
                smtp_server = model.smtp_server,
                smtp_port = model.smtp_port,
                smtp_password = model.smtp_password,
                smtp_username = model.smtp_username,
                ssl_tls = model.ssl_tls,
                is_active = model.is_active,
                created_at = DateTime.Now,
                created_by = listData.user,
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                year = listData.academic_year
            };
            dbContext.email_config.AddOrUpdate(email_Config);
            dbContext.SaveChanges();
            TempData["message"] = "Data Saved Successfully..!";
            Emailconfig();
            return View();
        }

        [HttpGet]
        public ActionResult EditEmailconfig(int? Id)
        {
            var editemail = (from b in dbContext.email_config where b.ecid == Id select b).FirstOrDefault();
            return View(editemail);
        }

        [HttpPost]
        public ActionResult EditEmailconfig(email_config model, int? id)
        {
            email_config el = dbContext.email_config.Where(a => a.ecid == id).FirstOrDefault();
            if (el != null)
            {
                el.email_type = model.email_type;
                el.smtp_username = model.smtp_username;
                el.smtp_password = model.smtp_password;
                el.smtp_server = model.smtp_server;
                el.smtp_port = model.smtp_port;
                el.ssl_tls = model.ssl_tls;
                el.is_active = model.is_active;
                el.modified_by = listData.user;
                el.modified_at = DateTime.Now;
                el.college_id = listData.CollegeID;
                el.branch_id = listData.BranchID;
            };
            dbContext.email_config.AddOrUpdate(el);
            int checksave = dbContext.SaveChanges();
            if (checksave > 0)
            {
                TempData["message"] = "Data Updated Successfully..!";
            }
            return RedirectToAction("Emailconfig");
        }

        public ActionResult DeleteEmailConfig(int? id)
        {
            try
            {
                var sesData = dbContext.email_config.Where(k => k.college_id == listData.CollegeID && k.year == listData.academic_year && k.branch_id == listData.BranchID && k.ecid == id).FirstOrDefault();
                sesData.modified_by = listData.user;
                sesData.modified_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.email_config.Remove(sesData);
                dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully..!";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Emailconfig");
        }

        [HttpGet]
        public ActionResult Smsconfig()
        {
            var SmsConfigList = dbContext.sms_config.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
            ViewBag.smsList = SmsConfigList;
            return View();
        }

        [HttpPost]
        public ActionResult Smsconfig(sms_config model)
        {
            try
            {
                sms_config smc = new sms_config()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    name = model.name,
                    username = model.username,
                    type = model.type,
                    authkey = model.authkey,
                    senderid = model.senderid,
                    password = model.password,
                    is_active = model.is_active,
                    created_at = DateTime.Now,
                    year = listData.academic_year,
                    created_by = listData.user
                };
                dbContext.sms_config.AddOrUpdate(smc);
                dbContext.SaveChanges();
                TempData["message"] = "Data Saved Successfully..!";
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong Please Contact Admin...!";
                ExceptionLogging.LogException(ex);
            }
            return View("Smsconfig");
        }

        [HttpGet]
        public ActionResult EditSmsConfig(int? Id)
        {
            var editsession = (from b in dbContext.sms_config where b.scid == Id select b).FirstOrDefault();
            return View(editsession);
        }

        [HttpPost]
        public ActionResult EditSmsConfig(sms_config model, int? Id)
        {
            var editsession = (from b in dbContext.sms_config where b.scid == Id select b).FirstOrDefault();
            if (editsession != null)
            {
                editsession.name = model.name;
                editsession.username = model.username;
                editsession.authkey = model.authkey;
                editsession.senderid = model.senderid;
                editsession.password = model.password;
                editsession.is_active = model.is_active;
                editsession.updated_at = DateTime.Now;
                editsession.modified_by = listData.user;
                editsession.college_id = listData.CollegeID;
                editsession.branch_id = listData.BranchID;
                editsession.type = model.type;
            }
            dbContext.sms_config.AddOrUpdate(editsession);
            try
            {
                int c = dbContext.SaveChanges();
                TempData["message"] = "Data Edited Successfully";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error while editing data, please contact admin";
            }
            return RedirectToAction("Smsconfig");
        }

        public ActionResult DeleteSmsConfig(int? id)
        {
            try
            {
                var sesData = dbContext.sms_config.Where(k => k.college_id == listData.CollegeID && k.year == listData.academic_year && k.branch_id == listData.BranchID && k.scid == id).FirstOrDefault();
                sesData.modified_by = listData.user;
                sesData.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.sms_config.Remove(sesData);
                dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully..!";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error deleting data, please contact admin";
            }
            return RedirectToAction("Smsconfig");
        }



        public ActionResult Frontcms()
        {
            return View();
        }
        public ActionResult Roles()
        {
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id
                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     //  Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }

        public ActionResult Backup()
        {
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 //join d in dbContext.categories on a.category_id equals d.id
                                 select new SchoolMasterClasses.StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.firstname,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     // Category = a.category_id,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }

        [HttpGet]
        public ActionResult AddLangauage()
        {
            ViewBag.listlangus = dbContext.languages.Where(a => a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult AddLangauage(language model)
        {
            try
            {
                language lg = new language()
                {
                    language1 = model.language1,
                    code = model.code,
                    is_active = model.is_active,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    year = listData.academic_year,
                    created_by = listData.user,
                    created_at = DateTime.Now
                };
                dbContext.languages.AddOrUpdate(lg);
                dbContext.SaveChanges();
                var sLanguages = dbContext.languages.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
                sLanguages.ForEach(k => k.is_active = k.id.Equals(lg.id) == true ? true : false);
                dbContext.SaveChanges();
                TempData["message"] = "Data Saved Successfully...!";
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong Please Contact Admin...!";
                ExceptionLogging.LogException(ex);
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public ActionResult Language(int? id)
        {
            try
            {
                var updte = (from langchange in dbContext.languages where langchange.college_id == listData.CollegeID && langchange.year == listData.academic_year && langchange.branch_id == listData.BranchID select langchange).ToList();
                foreach (var items in updte)
                {
                    items.is_active = items.id == id ? items.is_active = true : items.is_active = false;
                    items.updated_at = DateTime.Now;
                    items.modified_by = listData.user;
                    items.college_id = listData.CollegeID;
                    items.branch_id = listData.BranchID;
                    items.year = listData.academic_year;
                    dbContext.languages.AddOrUpdate(items);
                    dbContext.SaveChanges();
                    TempData["message"] = "Data Updated Successfully...!";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong Please Contact Admin...!";
                ExceptionLogging.LogException(ex);
            }
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult DeleteLanguage(int? id)
        {
            try
            {
                var delItem = dbContext.languages.Where(k => k.id == id).FirstOrDefault();
                delItem.modified_by = listData.user;
                delItem.updated_at = DateTime.Now;
                if (delItem != null)
                {
                    dbContext.languages.Remove(delItem);
                    dbContext.SaveChanges();
                    TempData["message"] = "Data Deleted Successfully...!";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong Please Contact Admin...!";
                ExceptionLogging.LogException(ex);
            }
            return RedirectToAction("AddLangauage", "Settings");
        }

        [HttpPost]
        public ActionResult Schsettings(sch_settings model)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Schsettings()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Sessions()
        {
            var session = (from a in dbContext.sessions.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID) select a).OrderBy(k => k.session1).ToList();
            ViewBag.sessionlist = session;
            return View();
        }

        [HttpPost]
        public ActionResult Sessions(session model, int? Id, string session1)
        {

            var data = (from s in dbContext.sessions where s.session1 == session1 && s.college_id == listData.CollegeID && s.branch_id == listData.BranchID select s).FirstOrDefault();
            if (data == null)
            {
                session ss = new session()
                {
                    session1 = model.session1,
                    created_at = DateTime.Now,
                    created_by = listData.user,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    is_active = false
                };
                dbContext.sessions.Add(ss);

            }
            else
            {
                data.session1 = model.session1;
                data.updated_at = DateTime.Now;
                data.modified_by = listData.user;
                data.college_id = listData.CollegeID;
                data.branch_id = listData.BranchID;
                data.is_active = true;

            }
            try
            {
                int c = dbContext.SaveChanges();
                var sd = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID).ToList();
                sd.ForEach(k => k.is_active = k.session1.Equals(session1) == true ? true : false);
                dbContext.SaveChanges();
                TempData["message"] = "Data Saved Successfully";
                ModelState.Clear();
            }

            catch (Exception e)
            {
                Console.Write(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            Sessions();
            return View("Sessions");

        }

        [HttpGet]
        public ActionResult Editsession(int? Id)
        {
            var editsession = (from b in dbContext.sessions where b.sid == Id select b).ToList().FirstOrDefault();
            return View(editsession);
        }

        [HttpPost]
        public ActionResult Editsession(session model, int? Id)
        {
            var editsession = (from b in dbContext.sessions where b.sid == Id select b).ToList().FirstOrDefault();
            if (editsession != null)
            {
                editsession.session1 = model.session1;
                editsession.updated_at = DateTime.Now;
                editsession.modified_by = listData.user;
                editsession.college_id = listData.CollegeID;
                editsession.branch_id = listData.BranchID;
                editsession.is_active = model.is_active;
            }
            dbContext.sessions.AddOrUpdate(editsession);
            try
            {
                int c = dbContext.SaveChanges();
                var sd = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID).ToList();
                sd.ForEach(k => k.is_active = k.sid.Equals(Id) == true ? true : false);
                dbContext.SaveChanges();
                TempData["message"] = "Data Edited Successfully";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Sessions");
        }

        public ActionResult DeleteSession(int? id)
        {
            try
            {
                var sesData = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.year == listData.academic_year && k.branch_id == listData.BranchID && k.sid == id).FirstOrDefault();
                sesData.modified_by = listData.user;
                sesData.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                dbContext.sessions.Remove(sesData);
                dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully..!";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Sessions");
        }


        [HttpGet]
        public ActionResult users(string ddl_id, int? classid)
        {
            ViewBag.viewbagclasslist = cmc.BindClass(listData.CollegeID, listData.BranchID, listData.academic_year);
            int? ddlid = Convert.ToInt32(ddl_id);
            ViewBag.studentlist = cmc.get_student_class(ddlid, listData.CollegeID, listData.BranchID, listData.academic_year);
            get_parentdata(classid);
            ViewBag.staffdata = cmc.get_staffdata(listData.CollegeID, listData.BranchID, "2019");
            return View();
        }

        public void get_parentdata(int? classid)
        {
            var p_data = (from u in dbContext.users
                          join s in dbContext.students
                          on
                          new { u.college_id, u.branch_id, u.user_id }
                          equals
                          new { s.college_id, s.branch_id, user_id = s.parent_id }
                          where u.college_id == listData.CollegeID && u.year == listData.academic_year && u.branch_id == listData.BranchID && u.role == "Parent" && s.class_id == classid
                          select new StudentInformation
                          {
                              IsActive = u.is_active,
                              GuardianName = s.guardian_name,
                              GuardianPhone = s.guardian_phone,
                              ParentUserName = u.username,
                              id = s.id
                          }
                          ).ToList();

            ViewBag.parentdata = p_data;
        }

        public void get_staffdata()
        {
            var p_data = (from s in dbContext.staffs
                              //join r in dbContext.roles on s.roll_id equals r.id
                          where s.college_id == listData.CollegeID && s.branch_id == listData.BranchID
                          select new StaffInformation
                          {
                              id = s.id,
                              employee_id = s.employee_id,
                              name = s.name,
                              email = s.email,
                              //rolename = r.roll_name,
                              designation = s.designation.ToString(),
                              department = s.department.ToString(),
                              contact_no = s.contact_no,
                              is_active = s.is_active
                          }
                          ).ToList();

            ViewBag.staffdata = p_data;
        }


        public JsonResult update_data(string[] get_val, string[] get_dval, string[] get_pval, string[] get_pdval, string[] get_sval, string[] get_sdval)
        {
            dynamic updte = null;
            string exp = null;
            int exp1 = 0;
            if (get_val != null)
            {
                foreach (var values in get_val)
                {
                    bool getval = false;
                    int idss = Convert.ToInt32(values);
                    updte = (from updatestudent in dbContext.students where updatestudent.id == idss select updatestudent).FirstOrDefault();
                    
                    getval = idss > 0 ? true : false;
                    if (getval == true)
                    {
                        exp = Boolean.TrueString;
                        updte.is_active = exp;
                        
                    }
                    dbContext.SaveChanges();
                }
            }
            if (get_dval != null)
            {
                foreach (var values_d in get_dval)
                {
                    bool getval_d = false;
                    int idss_d = Convert.ToInt32(values_d);
                    updte = (from updatestudent in dbContext.students where updatestudent.id == idss_d select updatestudent).FirstOrDefault();
                    getval_d = idss_d > 0 ? false : true;
                    if (getval_d == false)
                    {
                        exp = Boolean.FalseString;
                        updte.is_active = exp;
                    }

                    dbContext.SaveChanges();
                }
            }

            if (get_pval != null)
            {
                foreach (var values in get_pval)
                {
                    bool getval = false;
                    int idss = Convert.ToInt32(values);
                    updte = (from u in dbContext.users
                             join s in dbContext.students
                             on
                             new { u.college_id, u.branch_id, u.user_id }
                             equals
                             new { s.college_id, s.branch_id, user_id = s.parent_id }
                             where u.college_id == listData.CollegeID && u.year == listData.academic_year && u.branch_id == listData.BranchID && u.role == "Parent" && s.id == idss
                             select u
                          ).FirstOrDefault();
                    
                    getval = idss > 0 ? true : false;
                    if (getval == true)
                    {
                        exp = Boolean.TrueString;
                        updte.is_active = exp;
                    }
                    dbContext.SaveChanges();
                }
            }


            if (get_pdval != null)
            {
                foreach (var values_d in get_pdval)
                {
                    bool getval_d = false;
                    int idss_d = Convert.ToInt32(values_d);
                    updte = (from u in dbContext.users
                             join s in dbContext.students
                             on
                             new { u.college_id, u.branch_id, u.user_id }
                             equals
                             new { s.college_id, s.branch_id, user_id = s.parent_id }
                             where u.college_id == listData.CollegeID && u.year == listData.academic_year && u.branch_id == listData.BranchID && u.role == "Parent" && s.id == idss_d
                             select u

                         ).FirstOrDefault();

                    ViewBag.de = updte;
                    getval_d = idss_d > 0 ? false : true;
                    if (getval_d == false)
                    {
                        exp = Boolean.FalseString;
                        updte.is_active = exp;
                    }

                    dbContext.SaveChanges();
                }
            }



            if (get_sval != null)
            {
                foreach (var values in get_sval)
                {
                    bool getval = false;
                    int idss = Convert.ToInt32(values);
                    updte = (from s in dbContext.staffs
                             where s.college_id == listData.CollegeID && s.year == listData.academic_year && s.branch_id == listData.BranchID && s.id == idss
                             select s
                          ).FirstOrDefault();
                    getval = idss > 0 ? true : false;
                    if (getval == true)
                    {

                        exp1 = 1;
                        updte.is_active = exp1;
                    }
                    dbContext.SaveChanges();
                }
            }


            if (get_sdval != null)
            {
                foreach (var values_d in get_sdval)
                {
                    bool getval_d = false;
                    int idss_d = Convert.ToInt32(values_d);
                    updte = (from s in dbContext.staffs
                             where s.college_id == listData.CollegeID && s.year == listData.academic_year && s.branch_id == listData.BranchID && s.id == idss_d
                             select s
                           ).FirstOrDefault();

                    ViewBag.de = updte;
                    getval_d = idss_d > 0 ? false : true;
                    if (getval_d == false)
                    {
                        exp1 = 0;
                        updte.is_active = exp1;
                    }
                    dbContext.SaveChanges();
                }
            }



            return Json(new { response = "Success" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult json_return(string ddl_id)
        {
            return Json(new { ddl_id = ddl_id }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult json_returnParent(string ddlp_id)
        {
            return Json(new { ddlp_id = ddlp_id }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult module()
        {
            ViewBag.viewbagsectionlist = cmc.BindSection(listData.CollegeID, listData.BranchID, listData.academic_year);
            return View();
        }

        public JsonResult module(string[] favorite)
        {
            dynamic updte = null;
            bool getval;
            //   string[] modNames = new string[] { "F", "SI", "FC", "IC", "EP", "AT", "EX", "AC", "HR", "CM", "DC", "HW", "LB", "CR", "FS", "RP", "SS" };

            int[] modNames = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
            int i = 0;
            int j = 0;
            foreach (var items in modNames)
            {
                for (i = j; i < favorite.Length; i++)
                {
                    int idss = Convert.ToInt32(favorite[i]);
                    updte = (from modulePermission in dbContext.permission_group where modulePermission.id == items select modulePermission).FirstOrDefault();
                    getval = idss > 0 ? true : false;
                    updte.is_active = getval;
                    dbContext.SaveChanges();
                    break;
                }
                j = i + 1;
            }
            return Json(new { responstext = "Success" }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult get_module_data(string[] favorite)
        {
            return Json(new { favorite = favorite }, JsonRequestBehavior.AllowGet);
        }


        //////////////////////////////////////////////logo/////////////////////////////////////////////////
        [HttpPost]
        public JsonResult AppLogoupload()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        int id = 1;

                        string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + fname;
                        string dirPath = Server.MapPath("~/Content/Logo/");

                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }


                        file.SaveAs(Path.Combine(Server.MapPath("~/Content/Logo/"), savedFileName)); // Save the file

                        sch_settings sch = dbContext.sch_settings.Find(id);
                        {
                            sch.app_logo = savedFileName;
                            sch.created_by = listData.user;
                            sch.created_at = DateTime.Now;
                            sch.college_id = listData.CollegeID;
                            sch.branch__id = listData.BranchID;
                        };
                        dbContext.sch_settings.AddOrUpdate(sch);
                        dbContext.SaveChanges();
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }

        }

        [HttpPost]
        public ActionResult Logoupload()
        {

            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        int id = 1;

                        string savedFileName = DateTime.Now.ToString("MMddHHmmssfff") + fname;
                        string dirPath = Server.MapPath("~/Content/Logo/");

                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        file.SaveAs(Path.Combine(Server.MapPath("~/Content/Logo/"), savedFileName)); // Save the file

                        sch_settings sch = dbContext.sch_settings.Find(id);
                        {
                            sch.image = savedFileName;
                            sch.modified_by = listData.user;
                            sch.updated_at = DateTime.Now;
                            sch.college_id = listData.CollegeID;
                            sch.branch__id = listData.BranchID;
                        };
                        dbContext.sch_settings.AddOrUpdate(sch);
                        dbContext.SaveChanges();
                    }

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }

        }

        public void Ddldession()
        {
            ViewBag.session = dbContext.sessions.Select(a => new SelectListItem
            {
                Value = a.sid.ToString(),
                Text = a.session1,

            }).ToList();
        }

        public ActionResult frontcmssettings()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Gen_Settings()
        {
            List<SelectListItem> ls = dbContext.languages.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).Select(a => new SelectListItem
            {
                Value = a.code,
                Text = a.language1
            }).ToList();
            SelectListItem st = new SelectListItem()
            {
                Text = "Select",
                Value = "0"
            };
            ls.Insert(0, st);
            ViewBag.language = ls;

            List<SelectListItem> lss = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.session1==listData.academic_year && k.is_active==true).Select(a => new SelectListItem
            {
                Value = a.sid.ToString(),
                Text = a.session1,
                Selected = a.is_active.Equals(true)
            }).ToList();

            SelectListItem sts = new SelectListItem()
            {
                Text = "Select",
                Value = "0"
            };
            lss.Insert(0, sts);
            ViewBag.session = lss;

            ViewBag.gslist = dbContext.sch_settings.Where(a=>a.is_active==true && a.college_id==listData.CollegeID && a.branch__id==listData.BranchID).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult Gen_Settings(sch_settings model, HttpPostedFileBase app_logo, HttpPostedFileBase image)
        {
            using (var dbTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    string path = null, pic = null, cimage = null;
                    if (app_logo != null)
                    {
                        pic = System.IO.Path.GetFileName(app_logo.FileName);
                        path = System.IO.Path.Combine(Server.MapPath("~/Content/Logo"), pic);
                        app_logo.SaveAs(path);
                    }
                    if(image != null)
                    {
                        cimage = System.IO.Path.GetFileName(image.FileName);
                        path = System.IO.Path.Combine(Server.MapPath("~/Content/Logo"), cimage);
                        image.SaveAs(path);
                    }
                    sch_settings schs = new sch_settings()
                    {
                        name = model.name,
                        email = model.email,
                        phone = model.phone,
                        address = model.address,
                        lang_id = model.lang_id,
                        date_format = model.date_format,
                        currency = model.currency,
                        currency_symbol = model.currency_symbol,
                        is_rtl = model.is_rtl,
                        timezone = model.timezone,
                        session_id = model.session_id,
                        start_month = model.start_month,
                        image = cimage,
                        theme = model.theme,
                        is_active = model.is_active,
                        fee_due_days = model.fee_due_days,
                        class_teacher = model.class_teacher,
                        mobile_api_url = model.mobile_api_url,
                        app_logo = pic,
                        college_id = listData.CollegeID,
                        branch__id = listData.BranchID,
                        created_at = DateTime.Now,
                        created_by = listData.user,
                        year = dbContext.sessions.Where(k => k.sid == model.session_id).Select(k => k.session1).FirstOrDefault()
                    };
                    dbContext.sch_settings.AddOrUpdate(schs);

                    //Setting Session(Academic year)
                    var sessionTabledata = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
                    sessionTabledata.ForEach(j => j.is_active = j.sid.Equals(model.session_id) == true ? true : false);

                    //Setting Language
                    var setLanguage = dbContext.languages.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
                    setLanguage.ForEach(j => j.is_active = j.code.Equals(model.lang_id) == true ? true : false);

                    int ok = dbContext.SaveChanges();
                    int insertedId = schs.schid;
                    if (insertedId > 0)
                    {
                        //In General settings enable true for this id and make other false
                        var setGS = dbContext.sch_settings.Where(k => k.college_id == listData.CollegeID && k.branch__id == listData.BranchID && k.year == listData.academic_year).ToList();
                        setGS.ForEach(j => j.is_active = j.schid.Equals(insertedId) == true ? true : false);
                    }

                    dbContext.SaveChanges();
                    if (ok > 0)
                    {
                        TempData["message"] = "Data Saved Successfully and Please login again to take effect of Settings..!";
                        dbTransaction.Commit();
                        return RedirectToAction("Login", "Account", new { v = TempData["message"] });
                    }
                }
                catch (Exception ex)
                {
                    TempData["message"] = "There was an error while saving data, please contact admin";
                    dbTransaction.Rollback();
                    ExceptionLogging.LogException(ex);
                }
            }
            return RedirectToAction("Gen_Settings");
        }

        [HttpGet]
        public ActionResult EditGSconfig(int? id)
        {
            var generalSettingList = dbContext.sch_settings.Where(k => k.schid == id && k.college_id == listData.CollegeID && k.branch__id == listData.BranchID && k.year == listData.academic_year).FirstOrDefault();

            SelectListItem st = new SelectListItem()
            {
                Text = "Select",
                Value = "0"
            };

            List<SelectListItem> ls = dbContext.languages.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).Select(a => new SelectListItem
            {
                Value = a.code,
                Text = a.language1,
                Selected = true

            }).ToList();

            ls.Insert(0, st);
            ViewBag.language = ls;

            List<SelectListItem> lss = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).Select(a => new SelectListItem
            {
                Value = a.sid.ToString(),
                Text = a.session1,
                Selected = a.is_active
            }).ToList();

            SelectListItem sts = new SelectListItem()
            {
                Text = "Select",
                Value = "0"
            };
            lss.Insert(0, sts);
            ViewBag.session = lss;

            return View(generalSettingList);
        }

        [HttpPost]
        public ActionResult EditGSconfig(sch_settings model, int? id, HttpPostedFileBase app_logo, HttpPostedFileBase image)
        {
            using (var dbTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    string path = null, pic = null, cimage = null;
                    if (app_logo != null)
                    {
                        pic = System.IO.Path.GetFileName(app_logo.FileName);
                        path = System.IO.Path.Combine(Server.MapPath("~/Content/Logo"), pic);
                        app_logo.SaveAs(path);
                    }
                    if (image != null)
                    {
                        cimage = System.IO.Path.GetFileName(image.FileName);
                        path = System.IO.Path.Combine(Server.MapPath("~/Content/Logo"), cimage);
                        image.SaveAs(path);
                    }
                    var editgenSettings = (from b in dbContext.sch_settings where b.schid == id select b).ToList().FirstOrDefault();
                    if (editgenSettings != null)
                    {
                        editgenSettings.name = model.name;
                        editgenSettings.phone = model.phone;
                        editgenSettings.address = model.address;
                        if (pic != null)
                        {
                            editgenSettings.app_logo = pic;
                        }
                        editgenSettings.class_teacher = model.class_teacher;
                        editgenSettings.currency = model.currency;
                        editgenSettings.currency = model.currency_symbol;
                        editgenSettings.date_format = model.date_format;
                        editgenSettings.email = model.email;
                        editgenSettings.fee_due_days = model.fee_due_days;
                        if (cimage != null)
                        {
                            editgenSettings.image = cimage;
                        }
                        editgenSettings.is_active = model.is_active;
                        editgenSettings.is_rtl = model.is_rtl;
                        editgenSettings.lang_id = model.lang_id;
                        editgenSettings.mobile_api_url = model.mobile_api_url;
                        editgenSettings.session_id = model.session_id;
                        editgenSettings.start_month = model.start_month;
                        editgenSettings.theme = model.theme;
                        editgenSettings.timezone = model.timezone;
                        editgenSettings.updated_at = DateTime.Now;
                        editgenSettings.modified_by = listData.user;
                        editgenSettings.college_id = listData.CollegeID;
                        editgenSettings.branch__id = listData.BranchID;

                        editgenSettings.year = listData.academic_year;
                    }
                    dbContext.sch_settings.AddOrUpdate(editgenSettings);

                    //Setting Session(Academic year)
                    var sessionTabledata = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
                    sessionTabledata.ForEach(j => j.is_active = j.sid.Equals(model.session_id) == true ? true : false);

                    //Setting Language
                    var setLanguage = dbContext.languages.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
                    setLanguage.ForEach(j => j.is_active = j.code.Equals(model.lang_id) == true ? true : false);

                    int c = dbContext.SaveChanges();
                    int insertedId = editgenSettings.schid;
                    if (insertedId > 0)
                    {
                        //In General settings enable true for this id and make other false
                        var setGS = dbContext.sch_settings.Where(k => k.college_id == listData.CollegeID && k.branch__id == listData.BranchID && k.year == listData.academic_year).ToList();
                        setGS.ForEach(j => j.is_active = j.schid.Equals(insertedId) == true ? true : false);
                    }
                    dbContext.SaveChanges();

                    if (c > 0)
                    {
                        TempData["message"] = "Data Updated Successfully and Please login again to take effect of Settings..!";
                        dbTransaction.Commit();
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception e)
                {
                    ExceptionLogging.LogException(e);
                    TempData["message"] = "There was an error while saving data, please contact admin";
                }
            }
            return RedirectToAction("Gen_Settings");
        }

        public ActionResult DeleteGSConfig(int? id)
        {
            try
            {
                var sesData = dbContext.sch_settings.Where(k => k.college_id == listData.CollegeID && k.year == listData.academic_year && k.branch__id == listData.BranchID && k.schid == id).FirstOrDefault();
                sesData.modified_by = listData.user;
                sesData.updated_at = DateTime.Now;
                dbContext.SaveChanges();

                string fullPath = Request.MapPath("~/Content/Logo/" + sesData.app_logo);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                dbContext.sch_settings.Remove(sesData);
                dbContext.SaveChanges();
                TempData["message"] = "Data Deleted Successfully..!";
            }
            catch (Exception e)
            {
                ExceptionLogging.LogException(e);
                TempData["message"] = "There was an error while saving data, please contact admin";
            }
            return RedirectToAction("Gen_Settings");
        }
        private void Bindsession()
        {
            var u_list = dbContext.sessions.OrderBy(a => a.sid).ToList().Select(b => new SelectListItem { Value = b.sid.ToString(), Text = b.session1.ToString() }).ToList();
            ViewBag.clas1 = u_list;
        }


        [HttpGet]
        public ActionResult PaymentMethod()
        {

            var el = dbContext.payment_settings.Where(a => a.id == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            ViewBag.el = el;

            var el1 = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            ViewBag.el1 = el1;

            return View();
        }

        [HttpPost]
        public ActionResult PaymentMethod(payment_settings mod, string pass3, string pass4, string checkbox_sms)
        {
            payment_settings el = dbContext.payment_settings.Where(a => a.id == 1).FirstOrDefault();
            if (el.id == 1)
            {

                el.api_email = mod.api_email;
                el.api_password = mod.api_password;
                el.created_by = listData.user;
                el.created_at = DateTime.Now;
                el.college_id = listData.CollegeID;
                el.branch_id = listData.BranchID;
                dbContext.payment_settings.AddOrUpdate(el);
                dbContext.SaveChanges();

            }

            return RedirectToAction("PaymentMethod");
        }

        public ActionResult PaymentMethodInsta(payment_settings mod, string pass1, string pass2)
        {

            payment_settings ins = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();

            if (ins.id == 2)
            {

                ins.api_email = mod.api_email;
                ins.api_password = mod.api_password;
                ins.created_by = listData.user;
                ins.created_at = DateTime.Now;
                ins.college_id = listData.CollegeID;
                ins.branch_id = listData.BranchID;
                dbContext.payment_settings.AddOrUpdate(ins);
                dbContext.SaveChanges();

            }
            return RedirectToAction("PaymentMethod");
        }

        [HttpPost]
        public ActionResult PaymentMethodgateway(payment_settings mod, string checkbox_sms, string checkbox_sms1)
        {
            if (checkbox_sms == "rozarpay")
            {
                payment_settings ins = dbContext.payment_settings.Where(a => a.id == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                if (ins.id == 1)
                {
                    ins.payment_type = checkbox_sms;
                    ins.created_by = listData.user;
                    ins.created_at = DateTime.Now;
                    ins.college_id = listData.CollegeID;
                    ins.branch_id = listData.BranchID;
                    ins.is_active = true;

                    dbContext.payment_settings.AddOrUpdate(ins);
                    dbContext.SaveChanges();
                    payment_settings insa1 = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                    if (insa1.id == 2)
                    {
                        insa1.is_active = false;
                    

                        dbContext.payment_settings.AddOrUpdate(insa1);
                        dbContext.SaveChanges();
                    }
                }
            }
            else if (checkbox_sms == "Instamojo")
            {
                payment_settings insa = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                if (insa.id == 2)
                {
                    insa.payment_type = checkbox_sms;
                    insa.is_active = true;

                    dbContext.payment_settings.AddOrUpdate(insa);
                    dbContext.SaveChanges();
                    payment_settings ins = dbContext.payment_settings.Where(a => a.id == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                    if (ins.id == 1)
                    {
                        ins.is_active = false;

                        dbContext.payment_settings.AddOrUpdate(ins);
                        dbContext.SaveChanges();
                    }
                }
            }
            else if (checkbox_sms == "None")
            {
                payment_settings ins = dbContext.payment_settings.Where(a => a.id == 1 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                payment_settings insa = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                if (ins.id == 1)
                {

                    ins.payment_type = checkbox_sms;
                    ins.is_active = false;

                    dbContext.payment_settings.AddOrUpdate(ins);
                    dbContext.SaveChanges();
                    payment_settings insa1 = dbContext.payment_settings.Where(a => a.id == 2 && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).FirstOrDefault();
                    if (insa1.id == 2)
                    {
                        insa1.is_active = false;

                        dbContext.payment_settings.AddOrUpdate(insa1);
                        dbContext.SaveChanges();
                    }
                }
            }

            return RedirectToAction("PaymentMethod");
        }

    }
}


