﻿using SmartSchool.Models;
using System.Web.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System;
using System.Net;
using System.Data;
using System.Web;
using System.IO;
using System.Data.OleDb;
//using OfficeOpenXml;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartSchool.SchoolMasterClasses;
using System.Globalization;
using System.Data.Entity.Migrations;
using OfficeOpenXml;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using SmartSchool.Interfaces;
using SmartSchool.Classes;

namespace SmartSchool.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public get_staff_classes gs = new get_staff_classes();
        public custom_master_class cmc = new custom_master_class();
        public date_format df = new date_format();
        public SmsTextLocalClass sms = new SmsTextLocalClass();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        private SaveCapturedPhoto CapturedPhoto = new SaveCapturedPhoto();
        public StudentController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        public string datefm()
        {
            string datefm = df.get_date(listData.CollegeID, listData.BranchID, listData.academic_year);
            return datefm;
        }
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public JsonResult callgetstudent(string ddlclass, string ddlsec)
        {
            dynamic result = null;
            if (!string.IsNullOrEmpty(ddlsec))
            {
                int classid = Convert.ToInt32(ddlclass);
                int sectionid = Convert.ToInt32(ddlsec);

                result = (from s in dbContext.students
                          where s.class_id == classid && s.section_id == sectionid && s.college_id == listData.CollegeID && s.branch_id == listData.BranchID && s.year == listData.academic_year
                          select new
                          {
                              Value = s.user_id,
                              Text = s.firstname
                          }).ToList();

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            BindClass();
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Upload(FormCollection formCollection, HttpPostedFileBase uploadfile)
        {
            try
            {
                string SLNO = "0";
                int s = 0;
                BindClass();
                //BindSection();
                ExcelPackage exl = new ExcelPackage();
                exl.Dispose();
                var usersList = new List<student>();
                if (Request != null)
                {
                    HttpPostedFileBase file = Request.Files["uploadfile"];
                    if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                    {
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.Rows;
                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var user = new student();
                                user.college_id = listData.CollegeID;
                                user.branch_id = listData.BranchID;
                                user.class_id = Convert.ToInt32(formCollection["class_id"]);
                                user.section_id = Convert.ToInt32(formCollection["section_id"]);
                                user.Medium = formCollection["Medium"].ToString();

                                if (workSheet.Cells[rowIterator, 1].Value == null)
                                {
                                    user.roll_no = "1";
                                }
                                else
                                {
                                    user.roll_no = workSheet.Cells[rowIterator, 1].Value.ToString();
                                }
                                if (workSheet.Cells[rowIterator, 2].Value == null)
                                {
                                    user.firstname = null;
                                }
                                else
                                {
                                    user.firstname = workSheet.Cells[rowIterator, 2].Value.ToString();
                                }
                                if (workSheet.Cells[rowIterator, 3].Value == null)
                                {
                                    user.lastname = null;
                                }
                                else
                                {
                                    user.lastname = workSheet.Cells[rowIterator, 3].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 4].Value == null)
                                {
                                    user.gender = null;
                                }
                                else
                                {
                                    user.gender = workSheet.Cells[rowIterator, 4].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 5].Value == null)
                                {
                                    user.dob = DateTime.Today.ToString();
                                }
                                else
                                {
                                    user.dob = workSheet.Cells[rowIterator, 5].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 6].Value == null)
                                {
                                    user.category_id = 1;
                                }
                                else
                                {
                                    //user.category_id = 1;
                                    user.category_id = Convert.ToInt32(workSheet.Cells[rowIterator, 6].Value.ToString());
                                }

                                if (workSheet.Cells[rowIterator, 7].Value == null)
                                {
                                    user.religion = null;
                                }
                                else
                                {
                                    user.religion = Convert.ToInt32(workSheet.Cells[rowIterator, 7].Value.ToString());
                                }

                                if (workSheet.Cells[rowIterator, 8].Value == null)
                                {
                                    user.cast = null;
                                }
                                else
                                {
                                    user.cast = Convert.ToInt32(workSheet.Cells[rowIterator, 8].Value.ToString());
                                }

                                if (workSheet.Cells[rowIterator, 9].Value == null)
                                {
                                    user.mobileno = null;
                                }
                                else
                                {
                                    user.mobileno = workSheet.Cells[rowIterator, 9].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 10].Value == null)
                                {
                                    user.email = null;
                                }
                                else
                                {
                                    user.email = workSheet.Cells[rowIterator, 10].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 11].Value == null)
                                {
                                    user.admission_date = DateTime.Today.ToShortDateString();
                                }
                                else
                                {
                                    user.admission_date = workSheet.Cells[rowIterator, 11].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 12].Value == null)
                                {
                                    user.blood_group = null;
                                }
                                else
                                {
                                    user.blood_group = workSheet.Cells[rowIterator, 12].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 13].Value == null)
                                {
                                    user.school_house_id = null;
                                }
                                else
                                {
                                    user.school_house_id = Convert.ToInt32(workSheet.Cells[rowIterator, 13].Value.ToString());

                                }

                                if (workSheet.Cells[rowIterator, 14].Value == null)
                                {
                                    user.height = null;
                                }
                                else
                                {
                                    user.height = workSheet.Cells[rowIterator, 14].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 15].Value == null)
                                {
                                    user.weight = null;
                                }
                                else
                                {
                                    user.weight = workSheet.Cells[rowIterator, 15].Value.ToString();
                                }


                                if (workSheet.Cells[rowIterator, 16].Value == null)
                                {

                                    user.measurement_date = DateTime.Today;
                                }
                                else
                                {
                                    DateTime dateTime = DateTime.ParseExact(workSheet.Cells[rowIterator, 16].Value.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    user.measurement_date = dateTime;
                                }


                                if (workSheet.Cells[rowIterator, 17].Value == null)
                                {
                                    user.father_name = null;
                                }
                                else
                                {
                                    user.father_name = workSheet.Cells[rowIterator, 17].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 18].Value == null)
                                {
                                    user.father_phone = null;
                                }
                                else
                                {
                                    user.father_phone = workSheet.Cells[rowIterator, 18].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 19].Value == null)
                                {
                                    user.father_occupation = null;
                                }
                                else
                                {
                                    user.father_occupation = workSheet.Cells[rowIterator, 19].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 20].Value == null)
                                {
                                    user.mother_name = null;
                                }
                                else
                                {
                                    user.mother_name = workSheet.Cells[rowIterator, 20].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 21].Value == null)
                                {
                                    user.mother_phone = null;
                                }
                                else
                                {
                                    user.mother_phone = workSheet.Cells[rowIterator, 21].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 22].Value == null)
                                {
                                    user.mother_occupation = null;
                                }
                                else
                                {
                                    user.mother_occupation = workSheet.Cells[rowIterator, 22].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 23].Value == null)
                                {
                                    user.guardian_is = null;
                                }
                                else
                                {
                                    user.guardian_is = workSheet.Cells[rowIterator, 23].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 24].Value == null)
                                {
                                    user.guardian_name = null;
                                }
                                else
                                {
                                    user.guardian_name = workSheet.Cells[rowIterator, 24].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 25].Value == null)
                                {
                                    user.guardian_relation = null;
                                }
                                else
                                {
                                    user.guardian_relation = workSheet.Cells[rowIterator, 25].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 26].Value == null)
                                {
                                    user.guardian_email = null;
                                }
                                else
                                {
                                    user.guardian_email = workSheet.Cells[rowIterator, 26].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 27].Value == null)
                                {
                                    user.guardian_phone = null;
                                }
                                else
                                {
                                    user.guardian_phone = workSheet.Cells[rowIterator, 27].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 28].Value == null)
                                {
                                    user.guardian_occupation = null;
                                }
                                else
                                {
                                    user.guardian_occupation = workSheet.Cells[rowIterator, 28].Value.ToString();
                                }
                                if (workSheet.Cells[rowIterator, 29].Value == null)
                                {
                                    user.guardian_address = null;
                                }
                                else
                                {
                                    user.guardian_address = workSheet.Cells[rowIterator, 29].Value.ToString();
                                }
                                if (workSheet.Cells[rowIterator, 30].Value == null)
                                {
                                    user.current_address = null;
                                }
                                else
                                {
                                    user.current_address = workSheet.Cells[rowIterator, 30].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 31].Value == null)
                                {
                                    user.permanent_address = null;
                                }
                                else
                                {
                                    user.permanent_address = workSheet.Cells[rowIterator, 31].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 32].Value == null)
                                {
                                    user.bank_account_no = null;
                                }
                                else
                                {
                                    user.bank_account_no = workSheet.Cells[rowIterator, 32].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 33].Value == null)
                                {
                                    user.bank_name = null;
                                }
                                else
                                {
                                    user.bank_name = workSheet.Cells[rowIterator, 33].Value.ToString();
                                }


                                if (workSheet.Cells[rowIterator, 34].Value == null)
                                {
                                    user.ifsc_code = null;
                                }
                                else
                                {
                                    user.ifsc_code = workSheet.Cells[rowIterator, 34].Value.ToString();
                                }


                                if (workSheet.Cells[rowIterator, 35].Value == null)
                                {
                                    user.adhar_no = null;
                                }
                                else
                                {
                                    user.adhar_no = workSheet.Cells[rowIterator, 35].Value.ToString();
                                }


                                if (workSheet.Cells[rowIterator, 36].Value == null)
                                {
                                    user.caste_certificate_no = null;
                                }
                                else
                                {
                                    user.caste_certificate_no = workSheet.Cells[rowIterator, 36].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 37].Value == null)
                                {
                                    user.rte = null;
                                }
                                else
                                {
                                    user.rte = workSheet.Cells[rowIterator, 37].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 38].Value == null)
                                {
                                    user.previous_school = null;
                                }
                                else
                                {
                                    user.previous_school = workSheet.Cells[rowIterator, 38].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 39].Value == null)
                                {
                                    user.note = null;
                                }
                                else
                                {
                                    user.note = workSheet.Cells[rowIterator, 39].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 40].Value == null)
                                {
                                    user.sts_number = null;
                                }
                                else
                                {
                                    user.sts_number = workSheet.Cells[rowIterator, 40].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 41].Value == null)
                                {
                                    user.gr_number = null;
                                }
                                else
                                {
                                    user.gr_number = workSheet.Cells[rowIterator, 41].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 42].Value == null)
                                {
                                    user.father_aadhaar_no = null;
                                }
                                else
                                {
                                    user.father_aadhaar_no = workSheet.Cells[rowIterator, 42].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 43].Value == null)
                                {
                                    user.mother_aadhaar_no = null;
                                }
                                else
                                {
                                    user.mother_aadhaar_no = workSheet.Cells[rowIterator, 43].Value.ToString();
                                }

                                if (workSheet.Cells[rowIterator, 44].Value == null)
                                {
                                    user.income_certificate_no = null;
                                }
                                else
                                {
                                    user.income_certificate_no = workSheet.Cells[rowIterator, 44].Value.ToString();
                                }
                                user.is_active = true;

                                var isEmailAlreadyExists = dbContext.AspNetUsers.Any(x => x.Email == user.email);
                                if (isEmailAlreadyExists)
                                {
                                    TempData["emailexistaerror"] = user.email + " Student with this email already exists..!";
                                    return View();
                                }
                                var isGuardianEmailAlreadyExists = dbContext.AspNetUsers.Any(x => x.Email == user.guardian_email);
                                if (isGuardianEmailAlreadyExists)
                                {
                                    TempData["emailexistaerror"] = user.guardian_email + " Guardian with this email already exists..!";
                                    return View();
                                }
                                user.year = listData.academic_year;
                                user.created_by = listData.user;

                                dbContext.students.Add(user);
                                s = await dbContext.SaveChangesAsync();
                                if (s > 0)
                                {
                                    string parent_userid = null, userid = null, email = null, guardianemail = null, mobileno = null, guardianmobileno = null;
                                    string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                                    using (SqlConnection con = new SqlConnection(cs))
                                    {
                                        string query = "SELECT * FROM  [SmartSchoolHD].[dbo].[students] where id = " + user.id;
                                        SqlCommand cmd = new SqlCommand(query, con);
                                        con.Open();
                                        DataTable dt2 = new DataTable();
                                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                                        da.Fill(dt2);
                                        if (dt2.Rows.Count > 0)
                                        {
                                            userid = dt2.Rows[0]["user_id"].ToString();
                                            parent_userid = dt2.Rows[0]["parent_id"].ToString();
                                            email = dt2.Rows[0]["email"].ToString();
                                            guardianemail = dt2.Rows[0]["guardian_email"].ToString();
                                            mobileno = dt2.Rows[0]["mobileno"].ToString();
                                            guardianmobileno = dt2.Rows[0]["guardian_phone"].ToString();
                                        }
                                        con.Close();
                                    }

                                    user use = new user()
                                    {
                                        college_id = listData.CollegeID,
                                        branch_id = listData.BranchID,
                                        user_id = userid,
                                        username = email,
                                        password = mobileno,
                                        role = "Student",
                                        childs = "1",
                                        verification_code = "1",
                                        is_active = true,
                                        created_date = DateTime.Now,
                                        created_by = listData.user,
                                        year = listData.academic_year,
                                    };
                                    dbContext.users.Add(use);
                                    s += dbContext.SaveChanges();

                                    CreateRoles(userid, parent_userid, email, guardianemail, mobileno, email, mobileno, guardianmobileno);

                                    CreateParent(parent_userid, guardianemail, guardianmobileno, guardianemail, guardianmobileno);
                                }
                            }
                        }
                    }
                    if (s > 0)
                    {
                        TempData["success"] = s + " Record Inserted Successfully!";
                        ExcelPackage egf = new ExcelPackage();
                        egf.Dispose();
                        ModelState.Clear();
                        return View();
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["emailexistaerror"] = "Something Wrong Please contact admin..!";
                ExceptionLogging.LogException(ex);
                return View();
            }
            return View();
        }


        public bool send_message(string numbers, string _message)
        {
            string _sendrid = null, _hash = null, removehtml = null, _username = null, _password = null;

            var listdata = sms.get_provider(_college_id: listData.CollegeID, _branch_id: listData.BranchID, academicyear: listData.academic_year);
            if (listdata != null)
            {
                if (listdata.type == "1")
                {
                    _sendrid = listdata.senderid;
                    _username = listdata.username;
                    _password = listdata.password;
                }
                else
                {
                    _sendrid = listdata.senderid;
                    _hash = listdata.authkey;
                    _username = listdata.username;
                }

            }
            removehtml = Regex.Replace(_message, "<.*?>", String.Empty);

            if (numbers != null && numbers.Length == 10)
            {
                string MobNum = numbers.Trim();
                string Message = removehtml.TrimEnd();
                string UserName = _username;
                string Password = "Shlrdemo@123";
                string SenderID = _sendrid;
                string Trans = "Trans";
                int DCS = 0;
                int Flashsms = 0;
                int route = 7;
                int pid = 1;

                //Currently using smshouse sms provider, if you want to use Textlocal need to enable it.
                //string strUrl = "http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=" + UserName + "&password=" + Password + "&sender=" + SenderID + "&mobileno=" + MobNum + "&msg=" + Message;
                string strUrl = "http://45.114.143.23/api/mt/SendSMS?user=" + UserName + "&password=" + Password + "&senderid=" + SenderID + "&channel=" + Trans + "&DCS=" + DCS + "&flashsms=" + Flashsms + "&number=" + MobNum + "&text=" + Message + "& route=" + route + "&peid=" + pid;
                WebRequest request = HttpWebRequest.Create(strUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
            }
            else
            {
                return false;
            }
            return true;
        }

        [HttpGet]
        public ActionResult GetClassSection(int? classid)
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.sections = gs.get_staff_wise_classes(classid, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return View();
        }

        [HttpGet]
        public ActionResult studentdetails(StudentInformation model)
        {
            GetClassSection(null);

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }
        [HttpPost]
        public ActionResult studentdetails(int? class_id, int? section_id)
        {
            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.class_id == class_id && a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }
        [HttpPost]
        public JsonResult StudentReceipt(int? id)
        {


            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id into ad
                                 from d in ad.DefaultIfEmpty()
                                 join e in dbContext.religionmasters on a.religion equals e.id into ae
                                 from e in ae.DefaultIfEmpty()
                                 join f in dbContext.castemasters on a.cast equals f.id into af
                                 from f in af.DefaultIfEmpty()
                                 join g in dbContext.categoriesmasters on a.category_id equals g.id into ag
                                 from g in ag.DefaultIfEmpty()
                                 join h in dbContext.bloodgroupmasters on a.blood_group equals h.id.ToString() into ah
                                 from h in ah.DefaultIfEmpty()
                                 join i in dbContext.sch_settings on a.college_id equals i.college_id

                                 where a.id == id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year

                                 select new StudentInformation
                                 {
                                     college_id = a.college_id,
                                     branch_id = a.branch_id,
                                     collgename = i.name,
                                     branchname = i.address,
                                     AdmissionNumber = a.admission_no,
                                     RollNumber = a.roll_no,
                                     FirstName = a.firstname,
                                     LastName = a.lastname,
                                     Email = a.email,
                                     State = a.state,
                                     City = a.city,
                                     Pincode = a.pincode,
                                     //religion=e.religionname,
                                     //cast=f.castename,
                                     //catrgory=g.category_name,
                                     BloodGroup = h.bloodgroupname,
                                     AadhaarNumber = a.adhar_no,
                                     Religion = e.religionname,
                                     Caste = f.castename,
                                     
                                     sts_number = a.sts_number,
                                     BankAccountNumber = a.bank_account_no,
                                     BankName = a.bank_name,
                                     IFSCCode = a.ifsc_code,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     FatherPhone = a.father_phone,
                                     MotherPhone = a.mother_phone,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     GuardianOccupation = a.guardian_occupation,
                                     GuardianAddress = a.guardian_address,
                                     GuardianEmail = a.guardian_email,
                                     Class = b.class_name,
                                     Section = c.section1,
                                     year = a.year,
                                     Medium = a.Medium,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     AdmissionDate = a.admission_date,
                                     //admission_date=a.admission_date,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno


                                 }).FirstOrDefault();
            return Json(StudyMaterial, JsonRequestBehavior.AllowGet);
        }
        private void CreateRoles(string userid, string pareid, string email, string guardianmail, string password, string username, string mobileno, string gaurdianphone)
        {
            var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (userid.Contains("smartstu") && !(String.IsNullOrEmpty(userid)))
            {
                var user = new ApplicationUser();
                user.UserName = email;
                user.Email = email;
                user.PhoneNumber = mobileno;
                string pwds = password;
                var checker = UserManager.Create(user, pwds);
                if (checker.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Student");
                    string SMSRegards = "Lamington School Hubli";
                    string message = "Dear Student your profile has been created successfully.To Check Details Visit www.lamingtonschool.com " + " User Name: " + email + Environment.NewLine + "Password: " + mobileno + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                    //send_message(mobileno, message, listData.academic_year);
                    send_message(mobileno, message);
                }
            }
            if (pareid.Contains("smartpar") && !(String.IsNullOrEmpty(pareid)))
            {
                var userpar = new ApplicationUser();
                userpar.UserName = guardianmail;
                userpar.Email = guardianmail;
                userpar.PhoneNumber = gaurdianphone;
                string pwds1 = password;
                var checker1 = UserManager.Create(userpar, pwds1);
                if (checker1.Succeeded)
                {
                    var result1 = UserManager.AddToRole(userpar.Id, "Parent");
                    string SMSRegards = "Lamington School Hubli";
                    string message = "Dear Parent your child profile has been created successfully.To Check Details Visit www.smartschool.com " + " User Name: " + email + Environment.NewLine + "Password: " + mobileno + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                    //send_message(mobileno, message, listData.academic_year);
                    send_message(mobileno, message);
                }
            }
        }


        private void CreateParent(string pareid, string email, string password, string username, string mobileno)
        {
            user usep = new user()
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                user_id = pareid,
                username = email,
                password = password,
                role = "Parent",
                childs = "1",
                verification_code = "1",
                is_active = true,
                created_date = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year
                //    
            };
            dbContext.users.Add(usep);
            dbContext.SaveChanges();
        }

        [HttpGet]
        public ActionResult StudentAdmission()
        {
            BindClass();
            BindCategory();
            BindHosteRooms();
            BindStudentHouse();
            Bindbloodgroup();
            Bindcaste();
            Bindreligion();
            return View();
        }

        [HttpPost]
        public JsonResult SaveImage(string base64image, string ImgName)
        {
            var res = CapturedPhoto.SImage(base64image, ImgName);
            if (res != null)
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> UpdateStudentCredentials(string Email, string Password)
        {
            if (Email != "" && Password != "")
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                var user = await userManager.FindByEmailAsync(Email);
                if (user != null)
                {
                    var validPass = await userManager.PasswordValidator.ValidateAsync(Password);
                    if (validPass.Succeeded)
                    {
                        var UpdatePassword = userManager.FindByName(Email);
                        UpdatePassword.PasswordHash = userManager.PasswordHasher.HashPassword(Password);
                        var res = userManager.Update(UpdatePassword);
                        if (res.Succeeded)
                        {
                            string SMSRegards = "Lamington School Hubli";
                            string message = "Dear Parent your child profile has been created successfully.To Check Details Visit http://lamingtonschool.com " + " User Name: " + Email + Environment.NewLine + "Password: " + Password + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                            send_message(Password, message);
                            return Json("Success", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    var userpar = new ApplicationUser();
                    userpar.UserName = Email;
                    userpar.Email = Email;
                    userpar.PhoneNumber = Password;
                    var checker1 = UserManager.Create(userpar, Password);
                    if (checker1.Succeeded)
                    {
                        var result1 = UserManager.AddToRole(userpar.Id, "Student");
                        string SMSRegards = "Lamington School Hubli";
                        string message = "Dear Parent your child profile has been created successfully.To Check Details Visit http://lamingtonschool.com " + " User Name: " + Email + Environment.NewLine + "Password: " + Password + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                        send_message(Password, message);
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json("Please update EMAIL and PASSWORD of this Student..Thank you!", JsonRequestBehavior.AllowGet);
            }
            return Json("Something Wrong..Please Contact Admin!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult StudentAdmission(StudentAddmissionDetails model, string LPhoto, HttpPostedFileBase Studentphoto, HttpPostedFileBase FatherPhoto, HttpPostedFileBase matpostedFile, HttpPostedFileBase GuardianPhoto, HttpPostedFileBase firstdocument, HttpPostedFileBase Second_doc, HttpPostedFileBase Third_doc, HttpPostedFileBase Four_doc, string lblstuidnew)
        {
            //using (var dbTransaction = dbContext.Database.BeginTransaction())
            //{
            try
            {
                BindClass();
                BindCategory();

                BindHosteRooms();
                Bindbloodgroup();
                Bindcaste();
                Bindreligion();
                BindStudentHouse();

                string stuphoto = "";
                string fatphoto = "";
                string matphoto = "";
                string gurphoto = "";
                string firstdoc = "";
                string secdoc = "";
                string thirddoc = "";
                string fourdoc = "";
                int stid = 0;

                Random rno = new Random();
                stid = rno.Next(0, 2);
                if (Studentphoto != null)
                {
                    string filename = stid + Path.GetFileName(Studentphoto.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    Studentphoto.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file

                    stuphoto = filename;
                }
                if (FatherPhoto != null)
                {
                    string filename = stid + "Father" + Path.GetFileName(FatherPhoto.FileName);
                    FatherPhoto.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file

                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    fatphoto = filename;

                }
                if (matpostedFile != null)
                {
                    string filename = stid + "Mother" + Path.GetFileName(matpostedFile.FileName);
                    matpostedFile.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file

                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    matphoto = filename;
                }
                if (GuardianPhoto != null)
                {
                    string filename = stid + "Guardian" + Path.GetFileName(GuardianPhoto.FileName);
                    GuardianPhoto.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file

                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    gurphoto = filename;
                }
                if (stuphoto == null || stuphoto == "" && LPhoto != null)
                {
                    stuphoto = LPhoto;
                }
                student sa = new student()
                {
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    roll_no = model.roll_no,
                    sts_number = model.sts_number,
                    firstname = model.firstname,
                    lastname = model.lastname,
                    gender = model.gender,
                    dob = model.dob,
                    category_id = Convert.ToInt32(model.category_id),
                    religion = Convert.ToInt32(model.religion),
                    cast = Convert.ToInt32(model.cast),
                    mobileno = model.mobileno,
                    email = model.email,
                    admission_date = model.admission_date,
                    blood_group = model.blood_group,
                    state = "Karnataka",
                    image = stuphoto,
                    height = model.height,
                    weight = model.weight,
                    measurement_date = DateTime.Today,
                    father_name = model.father_name,
                    mother_name = model.mother_name,
                    father_phone = model.father_phone,
                    father_aadhaar_no = model.father_aadhaar_no,
                    mother_phone = model.mother_phone,
                    father_occupation = model.father_occupation,
                    mother_occupation = model.mother_occupation,
                    mother_aadhaar_no = model.mother_aadhaar_no,
                    father_pic = fatphoto,
                    mother_pic = matphoto,
                    guardian_name = model.guardian_name,
                    guardian_relation = model.guardian_relation,
                    guardian_email = model.guardian_email,
                    guardian_occupation = model.guardian_occupation,
                    guardian_phone = model.guardian_phone,
                    guardian_address = model.guardian_address,
                    guardian_pic = gurphoto,
                    current_address = model.current_address,
                    permanent_address = model.permanent_address,
                    bank_account_no = model.bank_account_no,
                    bank_name = model.bank_name,
                    branch_name = model.branch_name,
                    ifsc_code = model.ifsc_code,
                    rte = model.rte,
                    previous_school = model.previous_school,
                    note = model.note,
                    class_id = Convert.ToInt32(model.class_id),
                    section_id = Convert.ToInt32(model.section_id),
                    Medium = model.Medium,
                    adhar_no = model.adhar_no,
                    caste_certificate_no = model.caste_certificate_no,
                    income_certificate_no = model.income_certificate_no,
                    gr_number = model.gr_number,
                    micr_number = model.micr_number,
                    created_date = DateTime.Now,
                    is_active = true,
                    year = listData.academic_year,
                    created_by = listData.user,
                    Password = model.mobileno
                };
                var isEmailAlreadyExists = dbContext.AspNetUsers.Any(x => x.Email == model.email);
                if (isEmailAlreadyExists)
                {
                    TempData["emailexistaerror"] = model.email + " Student with this email already exists..!";
                    return View();
                }
                var isGuardianEmailAlreadyExists = dbContext.AspNetUsers.Any(x => x.Email == model.guardian_email);
                if (isGuardianEmailAlreadyExists)
                {
                    TempData["emailexistaerror"] = model.guardian_email + " Guardian with this email already exists..!";
                    return View();
                }

                dbContext.students.Add(sa);
                int c = dbContext.SaveChanges();
                if (c > 0)
                {
                    string parent_userid = null, userid = null, email = null, guardianemail = null, mobileno = null, guardianmobileno = null;
                    string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        string query = "SELECT * FROM  [SmartSchoolHD].[dbo].[students] where id = " + sa.id;
                        SqlCommand cmd = new SqlCommand(query, con);
                        con.Open();
                        DataTable dt2 = new DataTable();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt2);
                        if (dt2.Rows.Count > 0)
                        {
                            userid = dt2.Rows[0]["user_id"].ToString();
                            parent_userid = dt2.Rows[0]["parent_id"].ToString();
                            email = dt2.Rows[0]["email"].ToString();
                            guardianemail = dt2.Rows[0]["guardian_email"].ToString();
                            mobileno = dt2.Rows[0]["mobileno"].ToString();
                        }
                        con.Close();
                    }

                    user use = new user()
                    {
                        college_id = listData.CollegeID,
                        branch_id = listData.BranchID,
                        user_id = userid,
                        username = email,
                        password = mobileno,
                        role = "Student",
                        childs = "1",
                        verification_code = "1",
                        is_active = true,
                        created_date = DateTime.Now,
                        created_by = listData.user,
                        year = listData.academic_year,
                    };
                    dbContext.users.Add(use);
                    if (lblstuidnew != null)
                    {
                        student_sibling sb = new student_sibling()
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            student_id = userid,
                            sibling_student_id = lblstuidnew,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year
                        };
                        dbContext.student_sibling.Add(sb);
                    }
                    if (firstdocument != null)
                    {
                        string filename = userid + "Father" + Path.GetFileName(firstdocument.FileName);
                        firstdocument.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename)); // Save the file

                        var path = Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename);
                        firstdoc = filename;
                        studentsdocs(userid, model.Title1, firstdoc);
                    }
                    if (Second_doc != null)
                    {
                        string filename = userid + "Father" + Path.GetFileName(Second_doc.FileName);
                        Second_doc.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename)); // Save the file

                        var path = Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename);
                        secdoc = filename;
                        studentsdocs(userid, model.Title2, secdoc);
                    }
                    if (Third_doc != null)
                    {
                        string filename = userid + "Mother" + Path.GetFileName(Third_doc.FileName);
                        Third_doc.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename)); // Save the file

                        var path = Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename);
                        thirddoc = filename;
                        studentsdocs(userid, model.Title3, thirddoc);
                    }
                    if (Four_doc != null)
                    {
                        string filename = userid + "Guardian" + Path.GetFileName(Four_doc.FileName);
                        Four_doc.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename)); // Save the file

                        var path = Path.Combine(Server.MapPath("~/Content/Profile/Documents"), filename);
                        fourdoc = filename;
                        studentsdocs(userid, model.Title4, fourdoc);
                    }
                    dbContext.SaveChanges();
                    CreateRoles(userid, parent_userid, email, guardianemail, mobileno, email, mobileno, model.guardian_phone);
                    CreateParent(parent_userid, guardianemail, guardianmobileno, guardianemail, guardianmobileno);

                    ViewBag.result = "Record Inserted Successfully!";
                    //         dbTransaction.Commit();
                    ModelState.Clear();
                }
                else
                {
                    ViewBag.result = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["emailexistaerror"] = ex.Message + "There is an Error While Saving the Details..Please Contact Admin?";
                ExceptionLogging.LogException(ex);
                //dbTransaction.Rollback();
                return View("StudentAdmission");
            }
            //}
            return View();
        }

        public ActionResult studentsdocs(string studentid, string title, string document)
        {
            student_doc dc = new student_doc
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                student_id = studentid,
                title = title,
                doc = document,
                created_dt = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year
            };
            dbContext.student_doc.Add(dc);
            dbContext.SaveChanges();
            return View();

        }
        private void Bindbloodgroup()
        {
            List<bloodgroupmaster> c = dbContext.bloodgroupmasters.ToList();
            bloodgroupmaster sem = new bloodgroupmaster
            {
                bloodgroupname = "Select Blood Group",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "bloodgroupname ", 0);
            ViewBag.viewbagbloodgrouplist = selectcat;
        }


        private void Bindreligion()
        {
            List<religionmaster> c = dbContext.religionmasters.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
            religionmaster sem = new religionmaster
            {
                religionname = "Select Religion",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "religionname ", 0);
            ViewBag.viewbagreligionlist = selectcat;
        }
        private void Bindcaste()
        {
            List<castemaster> c = dbContext.castemasters.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
            castemaster sem = new castemaster
            {
                castename = "Select Caste",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "castename ", 0);
            ViewBag.viewbagcastelist = selectcat;
        }
        private void BindCategory()
        {
            List<category> c = dbContext.categories.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year).ToList();
            category sem = new category
            {
                category1 = "Select Category",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectcat = new SelectList(c, "id", "category1 ", 0);
            ViewBag.viewbagcatlist = selectcat;
        }
        private void BindClass()
        {
            var u_lists = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            ViewBag.viewbagclasslist = u_lists;
        }

        public void getStudentSection()
        {
            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
        }
        [HttpPost]
        public ActionResult AddGooglemeet(Google_meet_Class model)
        {
            var logeduserid = dbContext.staffs.Where(a => a.roll_id == listData.roleid && a.email == listData.user && a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(a => a.id).FirstOrDefault();
            Google_meet_Class gmc = new Google_meet_Class()
            {

                class_id = model.class_id,
                section_id = model.section_id,
                subject_id = model.subject_id,
                meeting_date = model.meeting_date,
                meeting_Link = model.meeting_Link,
                meeting_time = model.meeting_time,
                User_id = logeduserid,
                Status = true,
                year = listData.academic_year,
                Created_date = DateTime.Now,
                Created_by = listData.current_user_name,
                College_id = listData.CollegeID,
                Branch_id = listData.BranchID,


            };
            dbContext.Google_meet_Class.Add(gmc);
            int save = dbContext.SaveChanges();
            if (save > 0)
            {
                TempData["message"] = "Record inserted Successfully";
                ModelState.Clear();
                return RedirectToAction("AddGooglemeet");
            }
            else
            {
                TempData["message"] = "Something went wrong";
            }
            return RedirectToAction("Source");

        }

        [HttpPost]
        public JsonResult getSecandSub(int? ddlclass)
        {
            if (listData.roleid == "dd184641-0108-450d-8f21-68a5d36922f4")
            {
                var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
                //var subList = (from a in dbContext.class_subjects
                //               join b in dbContext.subjects on a.subjectId equals b.id
                //               where a.class_id == ddlclass && a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year
                //               select new SelectListItem
                //               {
                //                   Text = b.name,
                //                   Value = b.id.ToString()
                //               }).ToList();

                var subList = (from ts in dbContext.subjects
                               where ts.college_id == listData.CollegeID && ts.branch_id == listData.BranchID && ts.year == listData.academic_year && ts.is_active == true
                               select new SelectListItem
                               {
                                   Text = ts.name,
                                   Value = ts.id.ToString()

                               }).Distinct().ToList();
                return Json(new { getstaffdata, subList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<SelectListItem> subList = new List<SelectListItem>();
                var staffid = dbContext.staffs.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.year == listData.academic_year && k.user_id == listData.user).Select(k => k.id).FirstOrDefault();
                var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
                subList = gs.getteacherSubjects(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid, staffid);
                if (subList.Count == 0)
                {
                    subList = (from a in dbContext.class_subjects
                               join b in dbContext.subjects on a.subjectId equals b.id
                               where a.class_id == ddlclass && a.is_active == true && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && b.college_id == listData.CollegeID && b.branch_id == listData.BranchID && b.year == listData.academic_year
                               select new SelectListItem
                               {
                                   Text = b.name,
                                   Value = b.id.ToString()
                               }).ToList();
                }
                return Json(new { getstaffdata, subList }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult AddGooglemeet()
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);

            var googlemeet = (from a in dbContext.Google_meet_Class
                              join b in dbContext.AddClasses on a.class_id equals b.class_id
                              join c in dbContext.sections on a.section_id equals c.id
                            join d in dbContext.subjects on a.subject_id equals d.id
                              where a.College_id == listData.CollegeID && a.Branch_id == listData.BranchID && a.year == listData.academic_year &&  a.Status==true
                              select new StudentInformation
                              {
                                  Class = b.class_name,
                                  Section = c.section1,
                                 
                                  id = a.Id,
                                 Meetingdate=a.meeting_date,
                                MeetingTime=a.meeting_time,
                                SubjectName=d.name
                              }).ToList();

            ViewBag.GooglemeetList = googlemeet;

            dbContext.Google_meet_Class.Where(g => g.College_id == listData.CollegeID && g.Branch_id == listData.BranchID && g.year == listData.academic_year && g.Status == true).ToList();

            return View();
        }


        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getTeacher(int? ddlsection, int? ddlclass)
        {
            var teacherdata = gs.getteacher(ddlclass, ddlsection, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(teacherdata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getTeacherSubjects(int? ddlsection, int? ddlclass, int? ddlstafid)
        {
            var teacherdata = gs.getteacherSubjects(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid, ddlstafid);
            return Json(teacherdata, JsonRequestBehavior.AllowGet);
        }

        private void BindHosteRooms()
        {
            List<hostel_rooms> c = dbContext.hostel_rooms.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            hostel_rooms sem = new hostel_rooms
            {
                room_no = "Select Rooms",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectRooms = new SelectList(c, "id", "room_no ", 0);
            ViewBag.viewbagRoomslist = selectRooms;
        }
        private void BindStudentHouse()
        {
            List<school_houses> c = dbContext.school_houses.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year).ToList();
            school_houses sem = new school_houses
            {
                house_name = "Select House",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectHouse = new SelectList(c, "id", "house_name ", 0);
            ViewBag.viewbagHouselist = selectHouse;
        }


        public ActionResult StudentAdmissionlist()
        {
            GetClassSection(null);
            BindStudentHouse();
            Bindbloodgroup();
            Bindcaste();
            BindCategory();
            Bindreligion();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     AdmissionDate = a.admission_date,
                                     Class = b.class_name,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        [HttpPost]
        public ActionResult StudentAdmissionlist(int class_id, int section_id)
        {
            Bindbloodgroup();
            Bindcaste();
            BindCategory();
            Bindreligion();
            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     AdmissionDate = a.admission_date,
                                     Class = b.class_name,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }

        [HttpGet]
        public ActionResult GuardianReport()
        {
            GetClassSection(null);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }


        [HttpPost]
        public ActionResult GuardianReport(int class_id, int section_id)
        {
            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        [HttpGet]
        public ActionResult disablestudentslist()
        {
            GetClassSection(null);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.is_active != true && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;


            // return View(studetails);
            return View();
        }

        [HttpPost]
        public ActionResult disablestudentslist(int class_id, int section_id)
        {
            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     MobileNumber = a.mobileno,
                                     GuardianName = a.guardian_name,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     FatherName = a.firstname,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        [HttpGet]
        public ActionResult logindetailreport()
        {
            GetClassSection(null);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     Email = a.email,
                                     Password = a.Password == null ? a.mobileno : a.Password
                                 }).Distinct().ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        [HttpPost]
        public ActionResult logindetailreport(int class_id, int section_id)
        {

            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join e in dbContext.users on a.user_id equals e.user_id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.class_id==class_id && a.section_id == section_id && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     Email = a.email,
                                     Password = a.Password == null ? a.mobileno : a.Password
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }
        [HttpPost]
        public ActionResult StudentSchoolHouse(school_houses model)
        {

            school_houses schhouse = new school_houses
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                house_name = model.house_name,
                description = model.description,
                created_by = listData.user,
                created_date = DateTime.Now,

            };
            dbContext.school_houses.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();
        }


        [HttpPost]
        public ActionResult StudentHouse(school_houses model)
        {
            school_houses schhouse = new school_houses
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                house_name = model.house_name,
                description = model.description,
                is_active = true,
                created_date = DateTime.Now,
                created_by = listData.user,
                year = listData.academic_year

            };
            dbContext.school_houses.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }

            return View();
        }

        [HttpGet]
        public ActionResult StudentHouse()
        {
            var studetails = (from a in dbContext.school_houses.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).ToList();
            ViewBag.studentlist = studetails;

            return View();
        }

        public ActionResult Editstudent(int? id)
        {
            BindCategory();
            GetClassSection(null);
            getStudentSection();
            BindHosteRooms();
            BindStudentHouse();
            Bindbloodgroup();
            Bindcaste();
            Bindreligion();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee1 = (from v in dbContext.students
                             where v.admission_no == id && v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year
                             select v).FirstOrDefault();

            var stusibling = (from v in dbContext.students
                              join s in dbContext.student_sibling on v.user_id equals s.sibling_student_id
                              join c in dbContext.AddClasses on v.class_id equals c.class_id
                              join se in dbContext.sections on v.section_id equals se.id
                              where s.student_id == employee1.user_id && s.is_active != false && s.college_id == listData.CollegeID && s.branch_id == listData.BranchID && s.year == listData.academic_year
                              select new StudentInformation
                              {
                                  Class = c.class_name,
                                  Section = se.section1,
                                  AdmissionNumber = v.admission_no,
                                  FirstName = v.firstname,
                                  StudentPhoto = v.image,
                                  UserName = v.user_id
                              }).ToList();


            ViewBag.studentsiblinglist = stusibling;

            if (employee1 == null)
            {
                return HttpNotFound();
            }
            return View(employee1);
        }

        public ActionResult Studentdashboard(int? id)
        {
            var Studentid = (from a in dbContext.students.Where(x => x.college_id == listData.CollegeID && x.admission_no == id) select a.user_id).FirstOrDefault();

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.college_id == listData.CollegeID && a.user_id == Studentid && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     sts_number = a.sts_number,
                                     Email = a.email,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     FatherOccupation = a.father_occupation,
                                     FatherPhone = a.father_phone,
                                     FatherAadharNumber = a.father_aadhaar_no,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone,
                                     MotherOccupation = a.mother_occupation,
                                     MotherAadharNumber = a.mother_aadhaar_no,
                                     GuardianName = a.guardian_name,
                                     GuardianEmail = a.guardian_email,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     GuardianOccupation = a.guardian_occupation,
                                     GuardianAddress = a.guardian_address,
                                     GuardianPhoto = a.guardian_pic,
                                     FatherPhoto = a.father_pic,
                                     MotherPhoto = a.mother_pic,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                     CurrentAddress = a.current_address,
                                     PermanentAddress = a.permanent_address,
                                     BankAccountNumber = a.bank_account_no,
                                     BankName = a.bank_name,
                                     BranchName = a.branch_name,
                                     IncomeCertificateNumber = a.income_certificate_no,
                                     GRNumber = a.gr_number,
                                     BloodGroup = a.blood_group,
                                     StudentHouse = a.school_house_id.ToString(),
                                     Height = a.height,
                                     Weight = a.weight,
                                     AsonDate = a.measurement_date.ToString(),
                                     PreviousSchoolDetails = a.previous_school,
                                     IFSCCode = a.ifsc_code,
                                     MICRNumber = a.micr_number,
                                     CasteCertificateNumber = a.caste_certificate_no

                                 }).FirstOrDefault();
            ViewBag.studentdetails = StudyMaterial;


            var max = (from l in dbContext.fee_deposit_amountdetails where l.student_id == Studentid && l.college_id == listData.CollegeID && l.branch_id == listData.BranchID && l.year == listData.academic_year select l).ToList();
            int sum = max.Sum(c => Convert.ToInt32(c.amount));
            int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));
            int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));

            List<Stdentfeeclass> sf = new List<Stdentfeeclass>();
            var player = (from a in dbContext.student_fees_master
                          join b in dbContext.fee_groups_feetype on a.fee_groups_id equals b.fee_session_group_id
                          join c in dbContext.fee_groups on b.fee_groups_id equals c.id
                          join p in dbContext.feetypes on b.feetype_id equals p.id
                          where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && a.student_session_id == Studentid && a.year == listData.academic_year)

                          select new Stdentfeeclass
                          {
                              // id = id.ToString(),
                              Feemasterid = b.fee_session_group_id.ToString(),
                              Feetype = b.feetype_id.ToString(),
                              fegroupid = c.name,
                              feecde = p.code,
                              duedate = b.due_date.ToString(),
                              stats = a.is_active.ToString(),
                              amount = b.amount.ToString(),
                              paymentid = "",
                              mode = "Cash",
                              Date = b.due_date.ToString(),
                              Discount = sumdis.ToString(),
                              fine = sumfin.ToString(),
                              UserId = Studentid,
                          }).ToList();

            ViewBag.studentlist = player;

            if (player == null)
            {
                return HttpNotFound();
            }
            var studentdocs = (from c in dbContext.student_doc
                               where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && c.year == listData.academic_year && c.student_id == Studentid)
                               select new StudentInformation
                               {
                                   UserName = c.student_id,
                                   Title1 = c.title,
                                   Documents1 = c.doc
                               }).ToList();

            ViewBag.studentlistdocs = studentdocs;

            var studenttimeline = (
                 from c in dbContext.student_timeline
                 where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && c.year == listData.academic_year && c.student_id == Studentid)
                 select new StudentInformation
                 {
                     UserName = c.student_id,
                     Title1 = c.title,
                     Documents1 = c.document,
                     Desription = c.description,
                     Date = c.timeline_date.ToString()
                 }).ToList();
            ViewBag.studentlisttimeline = studenttimeline;
            ViewBag.studentlist = player;

            var students = dbContext.Database.SqlQuery<SchoolMasterClasses.Subjectdetailsstudent>("SELECT  distinct name as SubjectName ,full_marks as Fullmarks,passing_marks as Minmarks, get_marks as Get_marks  FROM  students as a inner join class_sections as b on b.class_id = a.class_id and b.section_id = a.section_id inner join teacher_subjects as c on b.id = c.class_section_id inner join subjects as d on d.id = c.subject_id inner join exam_schedules as e  on e.class_id = a.class_id and e.section_id = a.section_id and b.id = e.teacher_subject_id inner join exam_results as f  on e.esid = f.exam_schedule_id and user_id = student_id  where user_id = 'smartstu6'").ToList();
            ViewBag.marklist = students;
            return View(StudyMaterial);
        }

        [HttpPost]
        public async Task<ActionResult> Editstudent(student stuinfo, HttpPostedFileBase StupostedFile, HttpPostedFileBase FatherPhoto, HttpPostedFileBase matpostedFile, HttpPostedFileBase GuardianPhoto, string lblstuidnew)
        {
            try
            {
                string stuphoto = "";
                string fatphoto = "";
                string matphoto = "";
                string gurphoto = "";
                BindCategory();
                GetClassSection(stuinfo.class_id);
                BindHosteRooms();
                BindStudentHouse();
                Bindbloodgroup();
                Bindcaste();
                Bindreligion();

                var Studentsdetails = dbContext.students.Where(v => v.college_id == listData.CollegeID && v.branch_id == listData.BranchID && v.year == listData.academic_year).SingleOrDefault(e => e.admission_no == stuinfo.admission_no);
                var stid = "";
                stid = "smartstu" + stuinfo.admission_no;
                var pareid = "smartpar" + stuinfo.admission_no;
                //int i = 0;

                if (StupostedFile != null)
                {
                    string filename = stid + Path.GetFileName(StupostedFile.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    StupostedFile.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file
                    stuphoto = filename;
                }
                else
                {
                    string filename = stuinfo.image;
                    stuphoto = filename;
                }

                if (FatherPhoto != null)
                {
                    string filename = stid + "Father" + Path.GetFileName(FatherPhoto.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    FatherPhoto.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file
                    fatphoto = filename;
                }
                else
                {
                    string filename = stuinfo.image;
                    fatphoto = filename;
                }

                if (matpostedFile != null)
                {
                    string filename = stid + "Mother" + Path.GetFileName(matpostedFile.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    matpostedFile.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file
                    matphoto = filename;

                }
                else
                {
                    string filename = stuinfo.mother_pic;
                    matphoto = filename;
                }

                if (GuardianPhoto != null)
                {
                    string filename = stid + "Guardian" + Path.GetFileName(GuardianPhoto.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile"), filename);
                    GuardianPhoto.SaveAs(Path.Combine(Server.MapPath("~/Content/Profile/"), filename)); // Save the file
                    gurphoto = filename;
                }
                else
                {
                    string filename = stuinfo.guardian_pic;
                    gurphoto = filename;
                }

                if (Studentsdetails != null)
                {
                    Studentsdetails.roll_no = stuinfo.roll_no;
                    Studentsdetails.class_id = stuinfo.class_id;
                    Studentsdetails.section_id = stuinfo.section_id;
                    Studentsdetails.firstname = stuinfo.firstname;
                    Studentsdetails.lastname = stuinfo.lastname;
                    Studentsdetails.gender = stuinfo.gender;
                    Studentsdetails.dob = stuinfo.dob;
                    Studentsdetails.category_id = stuinfo.category_id;
                    Studentsdetails.cast = stuinfo.cast;
                    Studentsdetails.religion = stuinfo.religion;
                    Studentsdetails.mobileno = stuinfo.mobileno;
                    Studentsdetails.email = stuinfo.email;
                    Studentsdetails.admission_date = stuinfo.admission_date;
                    Studentsdetails.image = stuphoto;
                    Studentsdetails.blood_group = stuinfo.blood_group;
                    Studentsdetails.height = stuinfo.height;
                    Studentsdetails.weight = stuinfo.weight;
                    Studentsdetails.measurement_date = stuinfo.measurement_date;
                    Studentsdetails.father_name = stuinfo.father_name;
                    Studentsdetails.mother_name = stuinfo.mother_name;
                    Studentsdetails.father_phone = stuinfo.father_phone;
                    Studentsdetails.mother_phone = stuinfo.mother_phone;
                    Studentsdetails.father_occupation = stuinfo.father_occupation;
                    Studentsdetails.mother_occupation = stuinfo.mother_occupation;
                    Studentsdetails.father_pic = fatphoto;
                    Studentsdetails.mother_pic = matphoto;
                    Studentsdetails.guardian_is = stuinfo.guardian_is;
                    Studentsdetails.guardian_name = stuinfo.guardian_name;
                    Studentsdetails.guardian_phone = stuinfo.guardian_phone;
                    Studentsdetails.guardian_occupation = stuinfo.guardian_occupation;
                    Studentsdetails.guardian_relation = stuinfo.guardian_relation;
                    Studentsdetails.guardian_email = stuinfo.guardian_email;
                    Studentsdetails.guardian_address = stuinfo.guardian_address;
                    Studentsdetails.guardian_pic = gurphoto;
                    Studentsdetails.bank_account_no = stuinfo.bank_account_no;
                    Studentsdetails.bank_name = stuinfo.bank_name;
                    Studentsdetails.ifsc_code = stuinfo.ifsc_code;
                    Studentsdetails.adhar_no = stuinfo.adhar_no;
                    Studentsdetails.Medium = stuinfo.Medium;
                    Studentsdetails.rte = stuinfo.rte;
                    Studentsdetails.previous_school = stuinfo.previous_school;
                    Studentsdetails.note = stuinfo.note;
                    Studentsdetails.is_active = Studentsdetails.is_active;
                    Studentsdetails.modified_by = listData.user;
                    Studentsdetails.updated_date = DateTime.Now;
                    Studentsdetails.college_id = listData.CollegeID;
                    Studentsdetails.branch_id = listData.BranchID;

                    if (lblstuidnew != null)
                    {
                        student_sibling sb = new student_sibling()
                        {
                            college_id = listData.CollegeID,
                            branch_id = listData.BranchID,
                            student_id = stid,
                            sibling_student_id = lblstuidnew,
                            is_active = true,
                            created_at = DateTime.Now,
                            created_by = listData.user,
                            year = listData.academic_year
                        };
                        dbContext.student_sibling.AddOrUpdate(sb);
                    }
                    dbContext.students.AddOrUpdate(Studentsdetails);
                    int s = dbContext.SaveChanges();
                    if (s > 0)
                    {
                        TempData["message"] = "Record Updated Successfully..!";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Something Wrong Please contact admin..!";
                await ExceptionLogging.LogException(ex);
                return RedirectToAction("studentdetails");
            }
            return RedirectToAction("studentdetails");

        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        [HttpPost]
        public ActionResult Edit(school_houses house)
        {
            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == house.id);


            if (employee != null)
            {
                employee.house_name = house.house_name;
                employee.description = house.description;
                employee.is_active = house.is_active;
                dbContext.SaveChanges();
            }
            return RedirectToAction("StudentHouse");

        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var employee = dbContext.school_houses.SingleOrDefault(x => x.id == id);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.school_houses.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return RedirectToAction("StudentHouse");
        }
        [HttpPost]
        public ActionResult StudentCategory(category cat)
        {
            category schhouse = new category
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                category1 = cat.category1,
                is_active = cat.is_active,
                created_date = DateTime.Now,
                year = listData.academic_year,
                created_by = listData.user
            };

            dbContext.categories.Add(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            StudentCategory();
            return View();
        }


        public ActionResult StudentCategory()
        {
            var Catdetails = (from a in dbContext.categories.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).ToList();
            if (Catdetails != null)
            {
                ViewBag.catlist = Catdetails;
            }
            return View();
        }


        public ActionResult Editcate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.categories.SingleOrDefault(e => e.id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            //return View(employee);
            return PartialView("Editcate", employee);

        }

        [HttpPost]
        public JsonResult Editcatdisplay(int values, int status)
        {
            bool val = status == 1 ? true : false;
            var updte = (from device in dbContext.categories where device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year && device.id == values && device.is_active == val select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.category1, updte.id });
        }

        public JsonResult Editcategory(int? id, string CAT, string status)
        {
            bool val = status == "true" ? true : false;
            var player = (from a in dbContext.categories where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.id == id select a).FirstOrDefault();
            if (player != null)
            {
                player.category1 = CAT;
                player.is_active = val;
                player.modified_by = listData.user;
                player.updated_date = DateTime.Now;
                player.college_id = listData.CollegeID;
                player.branch_id = listData.BranchID;
                player.year = listData.academic_year;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Deletecat(int? id)
        {
            var employee = dbContext.categories.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID);
            employee.is_active = false;
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult StudentReport()
        {

            BindCategory();
            GetClassSection(null);

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();

        }
        [HttpPost]
        public ActionResult StudentReport(int? class_id, int? section_id, int? gender, int? rte, int category_id)
        {

            BindCategory();
            GetClassSection(class_id);
            var classid = (from a in dbContext.students.Where(x => x.college_id == listData.CollegeID && x.class_id == class_id && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).ToList();
            if (class_id != null)
            {
                if (category_id != 0)
                {
                    var StudyMaterial = (from a in dbContext.students
                                         join b in dbContext.AddClasses on a.class_id equals b.class_id
                                         join c in dbContext.sections on a.section_id equals c.id
                                         join d in dbContext.categories on a.category_id equals d.id
                                         where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.category_id == category_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                         select new StudentInformation
                                         {
                                             Section = c.section1,
                                             AdmissionNumber = a.admission_no,
                                             FirstName = a.firstname,
                                             FatherName = a.firstname,
                                             DateOfBirth = a.dob,
                                             Gender = a.gender,
                                             Category = d.category1,
                                             MobileNumber = a.mobileno,
                                             AadhaarNumber = a.adhar_no,
                                             Medium = a.Medium,
                                             RTE = a.rte
                                         }).ToList();
                    ViewBag.studentlist = StudyMaterial;
                }
                else if (category_id != 0 && gender != null)
                {

                    var StudyMaterial = (from a in dbContext.students
                                         join b in dbContext.AddClasses on a.class_id equals b.class_id
                                         join c in dbContext.sections on a.section_id equals c.id
                                         join d in dbContext.categories on a.category_id equals d.id
                                         where (a.college_id == listData.CollegeID && a.gender == gender.ToString() && a.class_id == class_id && a.section_id == section_id && a.category_id == category_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                         select new StudentInformation
                                         {
                                             Section = c.section1,
                                             AdmissionNumber = a.admission_no,
                                             FirstName = a.firstname,
                                             FatherName = a.firstname,
                                             DateOfBirth = a.dob,
                                             Gender = a.gender,
                                             Category = d.category1,
                                             MobileNumber = a.mobileno,
                                             AadhaarNumber = a.adhar_no,
                                             Medium = a.Medium,
                                             RTE = a.rte
                                         }).ToList();
                    ViewBag.studentlist = StudyMaterial;
                }
                else if (category_id != 0 && gender != null && rte != null)
                {

                    var StudyMaterial = (from a in dbContext.students
                                         join b in dbContext.AddClasses on a.class_id equals b.class_id
                                         join c in dbContext.sections on a.section_id equals c.id
                                         join d in dbContext.categories on a.category_id equals d.id
                                         where (a.college_id == listData.CollegeID && a.rte == rte.ToString() && a.gender == gender.ToString() && a.class_id == class_id && a.section_id == section_id && a.category_id == category_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)

                                         select new StudentInformation
                                         {
                                             Section = c.section1,
                                             AdmissionNumber = a.admission_no,
                                             FirstName = a.firstname,
                                             FatherName = a.firstname,
                                             DateOfBirth = a.dob,
                                             Gender = a.gender,
                                             Category = d.category1,
                                             MobileNumber = a.mobileno,
                                             AadhaarNumber = a.adhar_no,
                                             Medium = a.Medium,
                                             RTE = a.rte
                                         }).ToList();
                    ViewBag.studentlist = StudyMaterial;
                }

                else
                {

                    var StudyMaterial = (from a in dbContext.students
                                         join b in dbContext.AddClasses on a.class_id equals b.class_id
                                         join c in dbContext.sections on a.section_id equals c.id
                                         join d in dbContext.categories on a.category_id equals d.id
                                         where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                         select new StudentInformation
                                         {
                                             Section = c.section1,
                                             AdmissionNumber = a.admission_no,
                                             FirstName = a.firstname,
                                             FatherName = a.firstname,
                                             DateOfBirth = a.dob,
                                             Gender = a.gender,
                                             Category = d.category1,
                                             MobileNumber = a.mobileno,
                                             AadhaarNumber = a.adhar_no,
                                             Medium = a.Medium,
                                             RTE = a.rte
                                         }).ToList();
                    ViewBag.studentlist = StudyMaterial;
                }
            }
            else
            {
                var StudyMaterial = (from a in dbContext.students
                                     join b in dbContext.AddClasses on a.class_id equals b.class_id
                                     join c in dbContext.sections on a.section_id equals c.id
                                     join d in dbContext.categories on a.category_id equals d.id
                                     where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.category_id == category_id && a.gender == gender.ToString() && a.rte == rte.ToString() && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                     select new StudentInformation
                                     {
                                         Section = c.section1,
                                         AdmissionNumber = a.admission_no,
                                         FirstName = a.firstname,
                                         FatherName = a.firstname,
                                         DateOfBirth = a.dob,
                                         Gender = a.gender,
                                         Category = d.category1,
                                         MobileNumber = a.mobileno,
                                         AadhaarNumber = a.adhar_no,
                                         Medium = a.Medium,
                                         RTE = a.rte
                                     }).ToList();
                ViewBag.studentlist = StudyMaterial;
            }
            //   return View(studentlist);
            return View();

        }

        public ActionResult Edithouse(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = dbContext.categories.SingleOrDefault(e => e.id == id && e.college_id == listData.CollegeID && e.branch_id == listData.BranchID && e.year == listData.academic_year);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return PartialView("Editcate", employee);
        }


        [HttpPost]
        public JsonResult EditHousedisplay(int values)
        {
            var updte = (from device in dbContext.school_houses where device.college_id == listData.CollegeID && device.id == values && device.is_active == true && device.branch_id == listData.BranchID && device.year == listData.academic_year select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.house_name, updte.description });
        }

        [HttpPost]
        public JsonResult Edithouse(int? id, string CAT, string desc)
        {

            var player = (from a in dbContext.school_houses.Where(x => x.college_id == listData.CollegeID && x.id == id && x.is_active == true && x.branch_id == listData.BranchID && x.year == listData.academic_year) select a).FirstOrDefault();
            if (player != null)
            {
                player.house_name = CAT;
                player.description = desc;
                player.modified_date = DateTime.Now;
                player.is_active = true;
                player.modified_by = listData.user;
                player.college_id = listData.CollegeID;
                player.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Deletehouse(int? id)
        {
            var employee = dbContext.school_houses.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID && x.is_active == true && x.branch_id == listData.BranchID && x.year == listData.academic_year);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            employee.is_active = false;
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult DeleteSbling(string studentidsibnew)
        {
            var employee = dbContext.student_sibling.SingleOrDefault(x => x.sibling_student_id == studentidsibnew && x.college_id == listData.CollegeID && x.is_active == true && x.branch_id == listData.BranchID && x.year == listData.academic_year);
            employee.modified_by = listData.user;
            employee.updated_at = DateTime.Now;
            dbContext.SaveChanges();

            employee.is_active = false;
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult Deletedoc(string studentidsibnew)
        {
            var employee = dbContext.student_doc.SingleOrDefault(x => x.student_id == studentidsibnew && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year);
            employee.modified_by = listData.user;
            employee.updated_dt = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.student_doc.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult addtimeline(string id, string CAT, string desc, DateTime date1, string File1)
        {

            var player = (from a in dbContext.student_timeline.Where(a => a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year) select a).FirstOrDefault();
            string filepath = null;
            if (Request.Files.Count > 0)
            {
                if (File1 != null)
                {
                    string filename = id + Path.GetFileName(File1);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile/Timeline/"), filename);
                    filepath = "~/Content/Profile/Timeline/" + filename;
                }
            }
            student_timeline ft = new student_timeline
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                student_id = id,
                description = desc,
                title = CAT,
                timeline_date = date1,
                document = filepath,
                is_active = true,
                date = date1,
                created_by = listData.user,
                created_dt = DateTime.Now,
                year = listData.academic_year

            };
            dbContext.student_timeline.Add(ft);
            dbContext.SaveChanges();
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Editreligiondisplay(int values, int status)
        {
            bool val = status == 1 ? true : false;
            var updte = (from device in dbContext.religionmasters where device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year && device.id == values && device.is_active == val select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.religionname, updte.id });
        }


        public JsonResult Editreligion(int? id, string CAT, string status)
        {
            bool val = status == "true" ? true : false;
            var player = (from a in dbContext.religionmasters where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.id == id select a).FirstOrDefault();
            if (player != null)
            {
                player.religionname = CAT;
                player.is_active = val;
                player.modifieddate = DateTime.Now;
                player.modified_by = listData.user;
                player.college_id = listData.CollegeID;
                player.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Deletereligion(int? id)
        {
            var employee = dbContext.religionmasters.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year);
            employee.modified_by = listData.user;
            employee.modifieddate = DateTime.Now;
            dbContext.SaveChanges();

            employee.is_active = false;
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Studentreligion()
        {
            var Catdetails = (from a in dbContext.religionmasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).ToList();
            if (Catdetails != null)
            {
                ViewBag.catlist = Catdetails;
            }
            return View();
        }


        [HttpPost]
        public ActionResult Studentreligion(religionmaster cat)
        {
            religionmaster schhouse = new religionmaster
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                religionname = cat.religionname,
                is_active = cat.is_active,
                createddate = DateTime.Now,
                year = listData.academic_year,
                created_by = listData.user
            };
            dbContext.religionmasters.AddOrUpdate(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            StudentCategory();
            return RedirectToAction("Studentreligion", "Student");
        }


        [HttpPost]
        public JsonResult Editcastedisplay(int values, int status)
        {
            bool val = status == 1 ? true : false;
            var updte = (from device in dbContext.castemasters where device.college_id == listData.CollegeID && device.branch_id == listData.BranchID && device.year == listData.academic_year && device.id == values && device.is_active == val select device).FirstOrDefault();
            return Json(new { responstext = updte.id, updte.castename, updte.id });
        }


        public JsonResult Editcaste(int? id, string CAT, string status)
        {
            bool val = status == "true" ? true : false;
            var player = (from a in dbContext.castemasters where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.id == id select a).FirstOrDefault();
            if (player != null)
            {
                player.castename = CAT;
                player.is_active = val;
                player.modifieddate = DateTime.Now;
                player.modified_by = listData.user;
                player.college_id = listData.CollegeID;
                player.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Deletecaste(int? id)
        {
            var employee = dbContext.castemasters.SingleOrDefault(x => x.id == id && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year);
            employee.modified_by = listData.user;
            employee.modifieddate = DateTime.Now;
            dbContext.SaveChanges();

            employee.is_active = false;
            dbContext.SaveChanges();
            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Studentcaste()
        {
            var Catdetails = (from a in dbContext.castemasters.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.year == listData.academic_year && x.is_active == true) select a).ToList();
            if (Catdetails != null)
            {
                ViewBag.catlist = Catdetails;
            }
            return View();
        }


        [HttpPost]
        public ActionResult Studentcaste(castemaster cat)
        {
            castemaster schhouse = new castemaster
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                castename = cat.castename,
                is_active = cat.is_active,
                createddate = DateTime.Now,
                year = listData.academic_year,
                created_by = listData.user
            };
            dbContext.castemasters.AddOrUpdate(schhouse);
            int c = dbContext.SaveChanges();
            if (c > 0)
            {
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            StudentCategory();
            return RedirectToAction("Studentcaste", "Student");
        }

        [HttpGet]
        public ActionResult Smslogindetail()
        {
            BindClass();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     Email = a.email,
                                     Password = a.Password == null ? a.mobileno : a.Password
                                 }).Distinct().ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        [HttpPost]
        public ActionResult Smslogindetail(int class_id, int section_id)
        {

            BindClass();
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join e in dbContext.users on a.user_id equals e.user_id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && b.class_id == class_id && c.id == section_id && a.year == listData.academic_year)
                                 select new StudentInformation
                                 {
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     Email = a.email,
                                     Password = a.Password == null ? a.mobileno : a.Password
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            //   return View(studentlist);
            return View();
        }

        public class get_stucredentials
        {
            public string Email { get; set; }
            public string Password { get; set; }

        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SmsStudentCredentials(string Email, string Password, List<get_stucredentials> allStaff)
        {
            foreach (var item in allStaff)
            {
                Email = item.Email.Trim();
                Password = item.Password.Trim();


                if (Email != "" && Password != "")
                {
                    ApplicationDbContext context = new ApplicationDbContext();
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                    var user = await userManager.FindByEmailAsync(Email);
                    if (user != null)
                    {
                        var validPass = await userManager.PasswordValidator.ValidateAsync(Password);
                        if (validPass.Succeeded)
                        {
                            var UpdatePassword = userManager.FindByName(Email);
                            UpdatePassword.PasswordHash = userManager.PasswordHasher.HashPassword(Password);
                            var res = userManager.Update(UpdatePassword);
                            if (res.Succeeded)
                            {
                                string SMSRegards = "Lamington School Hubli";
                                string message = "Dear Parent your child profile has been created successfully.To Check Details Visit http://lamingtonschool.com " + " User Name: " + Email + Environment.NewLine + "Password: " + Password + Environment.NewLine + "Regards," + Environment.NewLine + SMSRegards + ".";
                                send_message(Password, message);

                            }
                        }
                    }
                }

                else
                {
                    return Json("Something Wrong..Please Contact Admin!", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Success", JsonRequestBehavior.AllowGet);

        }

        //////////////////////////////////////Geeta Code for moving student from section a to b//////////////////////////////////
        private void BindSection()
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0 && x.year == listData.academic_year).ToList();
            section sem = new section
            {
                section1 = "Select Section",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            ViewBag.viewbagsectionlist = selectsect;
        }

        private void BindStudent()
        {
            List<student> c = dbContext.students.Where(x => x.id != 0 && x.year == listData.academic_year).ToList();
            SelectList selectcat = new SelectList(c, "user_id", "firstname");
            ViewBag.viewbagStudent = selectcat;
        }



        [HttpGet]
        public ActionResult Shifttob()
        {
            BindClass();
            //BindSection();
            BindStudent();
            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
            //var StudyMaterial = (from a in dbContext.students
            //                     join b in dbContext.AddClasses on a.class_id equals b.class_id
            //                     join c in dbContext.sections on a.section_id equals c.id
            //                     join d in dbContext.categories on a.category_id equals d.id
            //                     join e in dbContext.student_fees_master on a.user_id equals e.student_session_id
            //                     where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.is_active == true)

            //                     select new StudentInformation
            //                     {
            //                         Class = b.class_name,
            //                         Section = c.section1,
            //                         AdmissionNumber = a.admission_no,
            //                         FirstName = a.firstname,
            //                         FatherName = a.father_name,
            //                         DateOfBirth = a.dob,
            //                         Gender = a.gender,
            //                         Category = d.category1,
            //                         MobileNumber = a.mobileno,
            //                         AadhaarNumber = a.adhar_no,
            //                         Medium = a.Medium,
            //                         RTE = a.rte,
            //                         UserName = a.user_id
            //                     }).ToList();
            //ViewBag.studentlist = StudyMaterial;
            return View();


        }

        [HttpPost]
        public ActionResult Shifttob(int class_id, int section_id)
        {
            BindClass();
            //BindSection();
            BindStudent();


            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 join e in dbContext.student_fees_master on a.user_id equals e.student_session_id
                                 where (a.college_id == listData.CollegeID && a.class_id == class_id && a.section_id == section_id && a.year == listData.academic_year && a.is_active == true)

                                 select new StudentInformation
                                 {
                                     id = a.id,
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     UserName = a.user_id,
                                     year = listData.academic_year

                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;

            TempData["section"] = section_id;
            // ViewBag.sectionid = section_id;


            return View();


        }

        [HttpPost]
        public JsonResult ToSection(int[] values, int ToSec)
        {


            int i = 0;
            foreach (int sec in values)
            {
                if (sec != 0)
                {
                    for (int ar = i; ar < values.Length; ar++)
                    {
                        int index = values[ar];
                        if (index != 0)
                        {
                            var data = dbContext.students.Where(a => a.id == index).Select(a => a).FirstOrDefault();/*(from a in dbContext.students where a=>a.id==index select a).SingleOrDefault();*/
                            data.section_id = ToSec;

                            data.modified_by = listData.user;
                            data.updated_date = DateTime.Now;
                            dbContext.students.AddOrUpdate(data);
                            dbContext.SaveChanges();
                            //i = i + 1;



                        }
                    }



                }
            }
            return Json(new { lcount = 1, responstext = "Student moved Successfully" }, JsonRequestBehavior.AllowGet);
        }
        public class UserModel
        {
            public bool Checkbox { get; set; }

        }
        //[HttpGet]
        //public ActionResult AcademicYearstudent()
        //{
        //    ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);

        //    return View();
        //}
        [HttpGet]
        public ActionResult AcademicYearstudent(StudentInformation model)
        {
            GetClassSection(null);
            var sesval = dbContext.sessions.Where(k => k.college_id == listData.CollegeID && k.branch_id == listData.BranchID && k.is_active == true).Select(k => new SelectListItem
            {
                Text = k.session1,
                Value = k.session1
            }).ToList();
            //   sesval.Insert(0, new SelectListItem { Text = "Select", Value = "0" });         
            ViewBag.SessionYear = sesval;
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     id = a.id,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }
        [HttpPost]
        public ActionResult AcademicYearstudent(int? class_id, int? section_id, string year)
        {
            GetClassSection(class_id);
            string years = year;
            string Year = years.Substring(5, years.Length - 5);
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.class_id == class_id && a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.admission_date.Contains(Year))

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     year = year,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }
        [HttpGet]
        public ActionResult AluminiStudents(StudentInformation model)
        {
            GetClassSection(null);

            var StudyMaterial = (from a in dbContext.studentsAluminis
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number,
                                     AdmissionDate = a.admission_date
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            //   return View(studentlist);
            return View();
        }
        [HttpPost]
        public ActionResult AluminiStudents(int? class_id, int? section_id)
        {
            GetClassSection(class_id);
            var StudyMaterial = (from a in dbContext.studentsAluminis
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.class_id == class_id && a.section_id == section_id && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     MotherName = a.mother_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     id = a.id,
                                     Category = d.category1,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     sts_number = a.sts_number,
                                     AdmissionDate=a.admission_date
                                 }).ToList();
            ViewBag.studentlist = StudyMaterial;
            return View();
        }

    }
}