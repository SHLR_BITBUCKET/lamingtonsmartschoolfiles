﻿using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartSchool.Classes;


namespace SmartSchool.Controllers
{
    public class SuperAdminController : Controller
    {
        // GET: SuperAdmin
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public usercreation uc = new usercreation();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public SuperAdminController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in AdminController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }




        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InstitutionDetailsView(InstitutionDetail model)
        {
            InstitutionDetail review = new InstitutionDetail()
            {
                college_id = model.college_id,
                college_name = model.college_name,
                branch_description = model.branch_description,
                branch_id = model.branch_id,
                branch_address = model.branch_address,
                place = model.place,
                city = model.city,
                country = model.country,
                state = model.state,
                super_admin_name = model.super_admin_name,
                pin_code = model.pin_code,
                phone_no = model.phone_no,
                email = model.email,
                website = model.website,
                fax = model.fax,
                created_at = DateTime.Now,
                created_by = listData.user
                
            };
            dbContext.InstitutionDetails.Add(review);
            int c = dbContext.SaveChanges();

            if (c > 0)
            {
                InstitutionDetailsView();
                //college_id = review.college_id;
                //branch_id = review.branch_id;
                string password = "SuperAdmin@123";
                dynamic role_id = uc.CreateRoles(model.email, model.email, password, model.phone_no);
             
                staff sf = new staff
                {
                    roll_id = role_id[0].RoleId,
                    college_id = listData.CollegeID,
                    branch_id = listData.BranchID,
                    user_id = model.email,
                    password = password,
                    name = model.super_admin_name,
                    created_by=listData.user,
                    created_at=DateTime.Now,
                    is_active=true
                    
                };
                dbContext.staffs.Add(sf);
                dbContext.SaveChanges();
                ViewBag.result = "Record Inserted Successfully!";
                ModelState.Clear();
            }
            else
            {
                ViewBag.result = "Something went wrong";
            }
            return View();
        }

        [HttpGet]
        public ActionResult InstitutionDetailsView()
        {
            List<InstitutionDetail> ll = (from a in dbContext.InstitutionDetails.Where(x => x.college_id != 0) select a).ToList();
            if (ll != null)
            {
                ViewBag.institutedetailsist = ll;
            }
            return View();            
        }

        [HttpPost]
        public JsonResult editajax(string clgid, string brhid)
        {
            int colgid = Convert.ToInt32(clgid);
            int brchid = Convert.ToInt32(brhid);
            dbContext.Configuration.ProxyCreationEnabled = false;
            List<InstitutionDetail> lid = (from li in dbContext.InstitutionDetails where li.college_id == colgid  select li).OrderByDescending(li=> li.branch_id).ToList();
            return Json(new { lid }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int? id, int? branh_id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.InstitutionDetails.SingleOrDefault(e => e.college_id == id && e.branch_id == branh_id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        [HttpPost]
        public ActionResult Edit(InstitutionDetail house)
        {
            var employee = dbContext.InstitutionDetails.SingleOrDefault(e => e.college_id == house.college_id && e.branch_id == house.branch_id);


            if (employee != null)
            {
                employee.college_name = house.college_name;
                employee.branch_description = house.branch_description;
                employee.branch_address = house.branch_address;
                employee.place = house.place;
                employee.city = house.city;
                employee.state = house.state;
                employee.country = house.country;
                employee.pin_code = house.pin_code;
                employee.email = house.email;
                employee.phone_no = house.phone_no;
                employee.website = house.website;
                employee.fax = house.fax;
                employee.modified_by = listData.user;
                employee.updated_at = DateTime.Now;
                employee.college_id = listData.CollegeID;
                employee.branch_id = listData.BranchID;
                dbContext.SaveChanges();
            }
            uc.CreateRoles(house.email, house.email, "SuperAdmin@123", house.phone_no);
            return RedirectToAction("InstitutionDetailsView");

        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var employee = dbContext.school_houses.SingleOrDefault(e => e.id == id);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var employee = dbContext.school_houses.SingleOrDefault(x => x.id == id);
            employee.modified_by = listData.user;
            employee.modified_date = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.school_houses.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();
            return RedirectToAction("InstitutionDetailsView");
        }


    }
}