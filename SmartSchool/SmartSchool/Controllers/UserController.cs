﻿using SmartSchool.Models;
using System.Web.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System;
using System.Net;
using System.Data;
using System.Web;
using System.IO;
using System.Data.OleDb;
using OfficeOpenXml;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartSchool.SchoolMasterClasses;
using System.Globalization;
using System.Data.Entity.Migrations;
using SmartSchool.Classes;
using SmartSchool.Interfaces;
using static SmartSchool.SchoolMasterClasses.StudentInformation;

namespace SmartSchool.Controllers
{

    public class UserController : Controller
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public custom_master_class cmc = new custom_master_class();

        public get_staff_classes gs = new get_staff_classes();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;

        public UserController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in UserController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }

        }

        public date_format df = new date_format();
        static string dateformat = null;

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public void getStudentClass()
        {
            ViewBag.cls = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
        }

        public void getStudentSection()
        {
            ViewBag.sections = gs.getAllSections(listData.CollegeID, listData.BranchID, listData.academic_year);
        }

        [HttpPost]
        public JsonResult callgetsec(int? ddlclass)
        {
            var getstaffdata = gs.get_staff_wise_classes(ddlclass, listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult attendence()
        {
            getStudentClass();
            getStudentSection();
            bind_month();
            bind_year();
            return View();
        }

        [HttpPost]
        public ActionResult attendence(int? class_id, int? section_id, int? Month, int? year)
        {
            getStudentClass();
            getStudentSection();
            var Mn = Month.Value;
            bind_month();
            bind_year();
            var stu_rep = (from a in dbContext.student_attendences
                           join b in dbContext.students on new { Student_session_id = a.student_session_id } equals new { Student_session_id = b.user_id }
                           where
                             b.class_id == class_id &&
                             b.section_id == section_id &&
                             a.date.Month == Mn && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && b.email == HttpContext.User.Identity.Name
                           group new { a, b } by new
                           {
                               a.student_session_id,
                               b.firstname,
                               b.lastname
                           } into g
                           select new SchoolMasterClasses.Stud_attendance_cls_sec
                           {
                               user_id = g.Key.student_session_id,
                               FirstName = g.Key.firstname,
                               LastName = g.Key.lastname,
                               present = g.Count(p => (
                               p.a.attendence_type_id == 1 ? (System.Int64?)1 : null) != null),
                               late = g.Count(p => (
                                p.a.attendence_type_id == 2 ? (System.Int64?)1 : null) != null),
                               absent = g.Count(p => (
                               p.a.attendence_type_id == 3 ? (System.Int64?)1 : null) != null),
                               halfday = g.Count(p => (
                               p.a.attendence_type_id == 4 ? (System.Int64?)1 : null) != null),
                               holiday = g.Count(p => (
                               p.a.attendence_type_id == 5 ? (System.Int64?)1 : null) != null)
                           }).ToList();

            ViewBag.count = stu_rep;
            ViewBag.month = Mn;
            ViewBag.selyear = year;
            ViewBag.sectionid = section_id;
            return View();
        }


        public ActionResult Studentdashboard()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 join e in dbContext.AspNetUsers on a.email equals e.Email
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.user_id == listData.user && a.year == listData.academic_year && a.is_active == true && a.class_id == Cls_Sec_Student.class_id && a.section_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     Email = a.email,
                                     user_id=e.Id,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     FatherOccupation = a.father_occupation,
                                     FatherPhone = a.father_phone,
                                     MotherName = a.mother_name,
                                     MotherPhone = a.mother_phone,
                                     MotherOccupation = a.mother_occupation,
                                     GuardianName = a.guardian_name,
                                     GuardianEmail = a.guardian_email,
                                     GuardianRelation = a.guardian_relation,
                                     GuardianPhone = a.guardian_phone,
                                     GuardianOccupation = a.guardian_occupation,
                                     GuardianAddress = a.guardian_address,
                                     GuardianPhoto = a.guardian_pic,
                                     FatherPhoto = a.father_pic,
                                     MotherPhoto = a.mother_pic,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     roll_id=listData.roleid,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                     CurrentAddress = a.current_address,
                                     PermanentAddress = a.permanent_address,
                                     BankAccountNumber = a.bank_account_no,
                                     BankName = a.bank_name,
                                     BloodGroup = a.blood_group,
                                     StudentHouse = a.school_house_id.ToString(),
                                     Height = a.height,
                                     Weight = a.weight,
                                     AsonDate = a.measurement_date.ToString(),
                                     PreviousSchoolDetails = a.previous_school,
                                     IFSCCode = a.ifsc_code

                                 }).FirstOrDefault();
            ViewBag.studentdetails = StudyMaterial;

            var students = (from a in dbContext.students
                            join b in dbContext.class_sections on a.class_id equals b.class_id
                            join o in dbContext.teacher_subjects on b.id equals o.class_section_id
                            join d in dbContext.subjects on o.subject_id equals d.id
                            join e in dbContext.exam_schedules on a.class_id equals e.class_id
                            join f in dbContext.exam_results on e.esid equals f.exam_schedule_id
                            where a.college_id == listData.CollegeID && f.student_id == listData.user && b.section_id == a.section_id && e.section_id == a.section_id && o.subject_id == e.teacher_subject_id
                            select new SchoolMasterClasses.Subjectdetailsstudent
                            {
                                SubjectName = d.name,
                                Fullmarks = e.full_marks,
                                Minmarks = e.passing_marks,
                                Get_marks = f.get_marks

                            }).Distinct().ToList();

            ViewBag.marklist = students;
            var max = (from l in dbContext.fee_deposit_amountdetails where l.student_id == listData.user && l.college_id == listData.CollegeID  && l.branch_id == listData.BranchID select l).ToList();
            if (max.Count != 0)
            {
                int sum = max.Sum(c => Convert.ToInt32(c.amount));
                dateformat = df.get_date(listData.CollegeID, listData.BranchID, listData.academic_year);

                int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));
                int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));
                List<Stdentfeeclass> sf = new List<Stdentfeeclass>();
                var StuFeeDetails = dbContext.Pro_StudentFeePaidDetails(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).ToList();
                ViewBag.StuFeePaidDetails = StuFeeDetails;

                var AmountAssigned = ViewBag.StuFeePaidDetails[0].AmtAssigned;
                var AmountDetails = ViewBag.StuFeePaidDetails[0].amount_detail;
                Decimal amount = Convert.ToDecimal(AmountDetails);


                //var player = (from a in dbContext.student_fees_master
                //              join b in dbContext.fee_groups_feetype on a.fee_session_group_id equals b.fee_session_group_id
                //              join c in dbContext.fee_groups on b.fee_groups_id equals c.id
                //              join p in dbContext.feetypes on b.feetype_id equals p.id
                //              where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && a.student_session_id == listData.user)

                //              select new Stdentfeeclass
                //              {

                //                  Feemasterid = b.fee_session_group_id.ToString(),
                //                  Feetype = b.feetype_id.ToString(),
                //                  fegroupid = c.name,
                //                  feecde = p.code,
                //                  duedate = b.due_date.ToString(),
                //                  stats = a.is_active.ToString(),
                //                  amount = b.amount.ToString(),
                //                  paymentid = "",
                //                  mode = "Cash",
                //                  Date = b.due_date.ToString(),
                //                  Discount = sumdis.ToString(),
                //                  fine = sumfin.ToString(),
                //                  UserId = listData.user,
                //                  //paid = k.amount_detail,
                //                  //balance = b.amount.ToString()
                //              }).ToList();

                //ViewBag.studentlist = player;

               

            }

          
            var studentdocs = (
                  from c in dbContext.student_doc

                  where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID  && c.student_id == listData.user)
                  select new StudentInformation
                  {
                      UserName = c.student_id,
                      Title1 = c.title,
                      Documents1 = c.doc
                  }).ToList();




            ViewBag.studentlistdocs = studentdocs;

            var studenttimeline = (
                 from c in dbContext.student_timeline

                 where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && c.year == listData.academic_year && c.student_id == listData.user)
                 select new StudentInformation
                 {
                     UserName = c.student_id,
                     Title1 = c.title,
                     Documents1 = c.document,
                     Desription = c.description,
                     Date = c.timeline_date.ToString()

                 }).ToList();


            ViewBag.studentlisttimeline = studenttimeline;


           
            //var students = dbContext.Database.SqlQuery<SchoolMasterClasses.Subjectdetailsstudent>("SELECT  distinct name as SubjectName ,full_marks as Fullmarks,passing_marks as Minmarks, get_marks as Get_marks  FROM  students as a inner join class_sections as b on b.class_id = a.class_id and b.section_id = a.section_id inner join teacher_subjects as c on b.id = c.class_section_id inner join subjects as d on d.id = c.subject_id inner join exam_schedules as e  on e.class_id = a.class_id and e.section_id = a.section_id and b.id = e.teacher_subject_id inner join exam_results as f  on e.esid = f.exam_schedule_id and user_id = student_id  where user_id = '" + listData.user + "'").ToList();
            //ViewBag.marklist = students;

            return View(StudyMaterial);

        }

        public ActionResult getfees(string stuid)
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.categories on a.category_id equals d.id
                                 where (a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.user_id == listData.user && a.class_id == Cls_Sec_Student.class_id && a.section_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {
                                     Class = b.class_name,
                                     Section = c.section1,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     FatherName = a.father_name,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     Category = d.category1,
                                     RollNumber = a.roll_no,
                                     MobileNumber = a.mobileno,
                                     AadhaarNumber = a.adhar_no,
                                     Medium = a.Medium,
                                     RTE = a.rte,
                                     StudentPhoto = a.image,
                                     UserName = a.user_id,
                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;


            var max = (from l in dbContext.fee_deposit_amountdetails where l.student_id == listData.user && l.college_id == listData.CollegeID && l.branch_id == listData.BranchID select l).ToList();
            if (max.Count != 0)
            {
                int sum = max.Sum(c => Convert.ToInt32(c.amount));
                dateformat = df.get_date(listData.CollegeID, listData.BranchID, listData.academic_year);

                int sumdis = max.Sum(c => Convert.ToInt32(c.amount_discount));
                int sumfin = max.Sum(c => Convert.ToInt32(c.amount_fine));
                List<Stdentfeeclass> sf = new List<Stdentfeeclass>();
                var StuFeeDetails = dbContext.Pro_StudentFeePaidDetails(listData.CollegeID, listData.BranchID, listData.academic_year, listData.user).ToList();
                ViewBag.StuFeePaidDetails = StuFeeDetails;

                var AmountAssigned = ViewBag.StuFeePaidDetails[0].AmtAssigned;
                var AmountDetails = ViewBag.StuFeePaidDetails[0].amount_detail;
                Decimal amount = Convert.ToDecimal(AmountDetails);


                //var player = (from a in dbContext.student_fees_master
                //              join b in dbContext.fee_groups_feetype on a.fee_session_group_id equals b.fee_session_group_id
                //              join c in dbContext.fee_groups on b.fee_groups_id equals c.id
                //              join p in dbContext.feetypes on b.feetype_id equals p.id
                //              where (c.college_id == listData.CollegeID && c.branch_id == listData.BranchID && a.student_session_id == listData.user)

                //              select new Stdentfeeclass
                //              {

                //                  Feemasterid = b.fee_session_group_id.ToString(),
                //                  Feetype = b.feetype_id.ToString(),
                //                  fegroupid = c.name,
                //                  feecde = p.code,
                //                  duedate = b.due_date.ToString(),
                //                  stats = a.is_active.ToString(),
                //                  amount = b.amount.ToString(),
                //                  paymentid = "",
                //                  mode = "Cash",
                //                  Date = b.due_date.ToString(),
                //                  Discount = sumdis.ToString(),
                //                  fine = sumfin.ToString(),
                //                  UserId = listData.user,
                //                  //paid = k.amount_detail,
                //                  //balance = b.amount.ToString()
                //              }).ToList();

                //ViewBag.studentlist = player;

                return View();

            }
            return View();

        }



        public ActionResult homework()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);
            var StudyMaterial = (from a in dbContext.homework
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.subjects on a.subject_id equals d.id
                                 join k in dbContext.staffs on a.evaluated_by equals k.id into stf
                                 from st in stf.DefaultIfEmpty()
                                 join e in dbContext.homework_evaluation on a.id equals e.homework_id into evaldate
                                 from f in evaldate.DefaultIfEmpty()
                                 where a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.class_id == Cls_Sec_Student.class_id && a.section_id == Cls_Sec_Student.section_id
                                 select new StudentInformation
                                 {
                                     id = a.id,
                                     Class = b.class_name,
                                     Section = c.section1,
                                     SubName = d.name,
                                     homework_date = a.homework_date.ToString(),
                                     HomeworkSubmissiondate = a.submit_date.ToString(),
                                     Documents1 = a.document,
                                     HomeworkEvaluationdate = f.date.ToString(),
                                     Evaluated_by = st.name,
                                     Status = f.status.ToString()
                                 }).Distinct().ToList();
            ViewBag.studentdetails = StudyMaterial;
            return View();
        }

        public ActionResult getHomeworkEvalutionDataById(int id)
        {
            ModelState.Clear();
            List<evaluation_report> ler = getHomeWorkEvaluationReport(id).ToList();
            return PartialView(ler);
        }

        private List<evaluation_report> getHomeWorkEvaluationReport(int? id)
        {
            List<evaluation_report> modalevaluation = new List<evaluation_report>();
            modalevaluation = dbContext.Pro_GHWER(listData.CollegeID, listData.BranchID, listData.academic_year, id).Where(k => k.user_id == listData.user).Select(k => new evaluation_report
            {
                homework_id = k.homework_id,
                FirstName = k.firstname,
                LastName = k.lastname,
                stringDate = k.stringDate,
                class_id = k.class_id,
                section_id = k.section_id,
                name = k.name,
                admission_no = k.admission_no,
                Description = k.Description,
                Assigned_By = k.Assigned_by,
                Evaluated_by = k.Evaluated_by,
                homework_date = k.homework_date,
                submit_date = k.submit_date,
                class_name = k.class_name,
                Section = k.section,
                created_by = k.created_by,
            }).ToList();
            return modalevaluation;
        }

        public ActionResult Editevaluation_report(int? id)
        {

            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);

            var StudyMaterial = (from a in dbContext.homework_evaluation
                                 join c in dbContext.homework on a.homework_id equals c.id
                                 join b in dbContext.subjects on c.subject_id equals b.id
                                 join d in dbContext.students on a.student_id equals d.user_id
                                 join e in dbContext.AddClasses on c.class_id equals e.class_id
                                 join f in dbContext.sections on c.section_id equals f.id
                                 where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && d.user_id == listData.user && d.admission_no == id && d.class_id == Cls_Sec_Student.class_id && d.section_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {
                                     AdmissionNumber = d.admission_no,
                                     FirstName = d.firstname,
                                     LastName = d.lastname,
                                     SubName = b.name,
                                     homework_date = c.homework_date.ToString(),
                                     HomeworkSubmissiondate = a.date.ToString(),
                                     Class = e.class_name,
                                     Section = f.section1,
                                     Desription = c.description,
                                     HomeworkEvaluationdate = a.date.ToString(),
                                     Evaluated_by = c.evaluated_by.ToString(),

                                 }).ToList();
            ViewBag.studentdetailsmodal = StudyMaterial;



            return View();

        }

        public ActionResult assignment()
        {

            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);



            var StudyMaterial = (from a in dbContext.contents
                                 join c in dbContext.ContentTypes on a.type equals c.id
                                 // join e in dbContext.class_sections on a.cls_sec_id equals e.id
                                 where (a.college_id == listData.CollegeID  && a.branch_id == listData.BranchID && a.type==1 && a.class_id == Cls_Sec_Student.class_id && a.cls_sec_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {

                                     SubName = a.title,
                                     Title1 = c.contentname,
                                     Date = a.date.ToString(),
                                     File = a.file


                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }

        public ActionResult Studymaterial()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);




            var StudyMaterial = (from a in dbContext.contents
                                 join c in dbContext.ContentTypes on a.type equals c.id
                                 // join e in dbContext.class_sections on a.cls_sec_id equals e.id
                                 where (a.college_id == listData.CollegeID  && a.branch_id == listData.BranchID && a.type==2 && a.class_id == Cls_Sec_Student.class_id && a.cls_sec_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {

                                     SubName = a.title,
                                     Title1 = c.contentname,
                                     Date = a.date.ToString(),
                                     File = a.file


                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }

        public ActionResult Syllabus()
        {

            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);




            var StudyMaterial = (from a in dbContext.contents
                                 join c in dbContext.ContentTypes on a.type equals c.id
                                 //join e in dbContext.class_sections on a.cls_sec_id equals e.id
                                 where (a.college_id == listData.CollegeID  && a.branch_id == listData.BranchID && a.type==3 && a.class_id == Cls_Sec_Student.class_id && a.cls_sec_id == Cls_Sec_Student.section_id)
                                 select new StudentInformation
                                 {

                                     SubName = a.title,
                                     Title1 = c.contentname,
                                     Date = a.date.ToString(),
                                     File = a.file


                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }

        public ActionResult Otherdownloads()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);




            var StudyMaterial = (from a in dbContext.contents
                                 join c in dbContext.ContentTypes on a.type equals c.id
                                 //join e in dbContext.class_sections on a.cls_sec_id equals e.id
                                 where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && c.contentname == "Other Download" && a.for_all_classes == "true" || a.class_id == Cls_Sec_Student.class_id && a.cls_sec_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {

                                     SubName = a.title,
                                     Title1 = c.contentname,
                                     Date = a.date.ToString(),
                                     File = a.file

                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }

        public ActionResult Subject()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);



            var StudyMaterial = (from a in dbContext.subjects
                                 join c in dbContext.teacher_subjects on a.id equals c.subject_id
                                 join d in dbContext.staffs on c.teacher_id equals d.id
                                 join e in dbContext.class_sections on c.class_section_id equals e.id

                                 where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && e.class_id == Cls_Sec_Student.class_id && e.section_id == Cls_Sec_Student.section_id)

                                 select new StudentInformation
                                 {

                                     SubName = a.name,
                                     SubCode = a.code,
                                     Teacher = d.name,
                                     SubjectType = a.type

                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }

        public ActionResult Teacher()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);
            var StudyMaterial = (from a in dbContext.staffs
                                 join b in dbContext.teacher_subjects on a.id equals b.teacher_id
                                 join c in dbContext.subjects on b.subject_id equals c.id
                                 join d in dbContext.class_sections on b.class_section_id equals d.id
                                 join e in dbContext.students on d.class_id equals e.class_id
                                 where (a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && e.user_id == listData.user)

                                 select new StudentInformation
                                 {

                                     FirstName = a.name,
                                     SubjectName = c.name,
                                     Email = a.email,
                                     MobileNumber = a.contact_no,

                                 }).ToList();
            ViewBag.studentdetails = StudyMaterial;



            return View();

        }
        [HttpGet]
        public ActionResult Examschedule()
        {


            //var Classid = (from a in dbContext.students.Where(x => x.user_id == listData.user && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true) select new { a.class_id }).FirstOrDefault();
            //var Sectionid = (from a in dbContext.students.Where(x => x.user_id == listData.user && x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true) select new { a.section_id }).FirstOrDefault();

            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);






            var examlsit = (from a in dbContext.exam_schedules
                            join b in dbContext.AddClasses on a.class_id equals b.class_id
                            join c in dbContext.sections on a.section_id equals c.id
                            join d in dbContext.exams on a.category_id equals d.eid
                            join e in dbContext.subjects on a.teacher_subject_id equals e.id
                            where a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && a.class_id == Cls_Sec_Student.class_id && a.section_id == Cls_Sec_Student.section_id 
                            select new Exam_schedulesdata
                            {
                                ExamName = d.name,
                                ClassName = b.class_name,
                                SectionName = c.section1,
                                SubjectName = e.name,
                                Date_of_exam = a.date_of_exam,
                                Start_to = a.start_to,
                                End_from = a.end_from,
                                Room_no = a.room_no,
                                Full_marks = a.full_marks,
                                Passing_marks = a.passing_marks
                            }).ToList();

            ViewBag.examschedulelist = examlsit;
            return View();

        }

        public ActionResult Book(book model)
        {
            var Booklist = (from a in dbContext.books.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true) select a).ToList();
            ViewBag.Bookdetails = Booklist;

            return View();
        }

        public ActionResult Bookissue()
        {
            var listbook = dbContext.books.Where(x => x.college_id == listData.CollegeID && x.branch_id == listData.BranchID && x.is_active == true).OrderBy(a => a.bid).ToList().Select(b => new SelectListItem { Text = b.book_title.ToString(), Value = b.bid.ToString() }).ToList();
            ViewBag.getSubject = listbook;
            var books = (from a in dbContext.books
                         join b in dbContext.book_issues on a.bid equals b.bid
                         join c in dbContext.libarary_members on b.member_id equals c.id
                         where c.member_type == listData.user && a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && a.is_active == true
                         select new SchoolMasterClasses.StudentInformation
                         {
                             book_title = a.book_title,
                             SubName = a.subject,
                             book_no = a.book_no,
                             issue_date = b.issue_date,
                             return_date = b.return_date
                         }).ToList();
            ViewBag.Bookdetails = books;
            return View();
        }

        public ActionResult ClassTimetable()
        {
            var getvaltt1 = (from a in dbContext.timetables
                             join b in dbContext.teacher_subjects on a.teacher_subject_id equals b.id
                             join c in dbContext.subjects on b.subject_id equals c.id
                             join d in dbContext.class_sections on b.class_section_id equals d.id
                             join e in dbContext.AddClasses on d.class_id equals e.class_id
                             join f in dbContext.students on d.class_id equals f.class_id
                             where d.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && b.is_active == true && d.section_id == f.section_id && f.user_id == listData.user
                             select new SchoolMasterClasses.timetables
                             {
                                 classes = e.class_name,
                                 name = c.name,
                                 day_name = a.day_name,
                                 start_time = a.start_time,
                                 end_time = a.end_time,
                                 room_no = a.room_no
                             }).ToList();
            ViewBag.Gettimetable = getvaltt1;
            ViewBag.getname = getvaltt1.Select(a => a.name).Distinct().ToList();
            ViewBag.cls_id = getvaltt1.Select(a => a.classes).Distinct().ToList();
            var s = ViewBag.cls_id;
            return View();

        }

        public ActionResult marklist()
        {

            string stuid = listData.user;
            //var students = dbContext.Database.SqlQuery<SchoolMasterClasses.Subjectdetailsstudent>(" SELECT distinct d.name as SubjectName ,e.full_marks as Fullmarks,e.passing_marks as Minmarks, f.get_marks as Get_marks  from students as a inner join class_sections as b on b.class_id = a.class_id and b.section_id = a.section_id inner join teacher_subjects as c on b.id = c.class_section_id inner join subjects as d on d.id = c.subject_id inner join exam_schedules as e  on e.class_id = a.class_id and e.section_id = a.section_id and c.subject_id = e.teacher_subject_id inner join exam_results as f  on e.esid = f.exam_schedule_id where a.college_id = "+listData.CollegeID+ " and a.user_id ="+stuid+"").ToList();

            var students=(from a in dbContext.students  
                           join b in dbContext.class_sections on a.class_id equals b.class_id 
                           join o in dbContext.teacher_subjects on b.id equals o.class_section_id
                           join d in dbContext.subjects  on o.subject_id equals  d.id
                            join e in dbContext.exam_schedules on a.class_id equals  e.class_id
                            join f in dbContext.exam_results on e.esid equals f.exam_schedule_id
                           where a.college_id ==listData.CollegeID && f.student_id ==listData.user && b.section_id == a.section_id && e.section_id == a.section_id && o.subject_id == e.teacher_subject_id
                            select new SchoolMasterClasses.Subjectdetailsstudent
                            {
                                SubjectName = d.name,
                                Fullmarks = e.full_marks,
                                Minmarks = e.passing_marks,
                                Get_marks = f.get_marks
                               
                            }).Distinct().ToList();

            ViewBag.marklist = students;
            return View();

        }

        public JsonResult addtimeline(string id, string CAT, string desc, DateTime date1, string File1)
        {

            var player = (from a in dbContext.student_timeline.Where(x => x.student_id == listData.user && x.college_id == listData.CollegeID && x.year == listData.academic_year && x.branch_id == listData.BranchID && x.is_active == true) select a).FirstOrDefault();
            string filepath = null;
            if (Request.Files.Count > 0)
            {
                if (File1 != null)
                {
                    string filename = listData.user + Path.GetFileName(File1);
                    var path = Path.Combine(Server.MapPath("~/Content/Profile/Timeline/"), filename);
                    filepath = "~/ Content / Profile/Timeline/" + filename;
                }
            }
            student_timeline ft = new student_timeline
            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                student_id = listData.user,
                description = desc,
                title = CAT,
                timeline_date = date1,
                document = filepath,
                is_active = true,
                date = date1,
                created_by = "Admin",
                created_dt = DateTime.Now
            };
            dbContext.student_timeline.Add(ft);
            dbContext.SaveChanges();


            // }


            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult addDoc(string id, string CAT, string File1)
        {

            var player = (from a in dbContext.student_doc.Where(x => x.student_id == listData.user && x.college_id == listData.CollegeID && x.year == listData.academic_year && x.branch_id == listData.BranchID) select a).FirstOrDefault();


            string filepath = null;



            string Image = "";
            if (File1 != null)
                {

                    string filename = Path.GetFileName(File1);
                   

                    filepath =filename;
                }
            

            student_doc ft = new student_doc

            {
                college_id = listData.CollegeID,
                branch_id = listData.BranchID,
                student_id = listData.user,
                title = CAT,
                doc = filepath,
                updated_dt = DateTime.Now,
                modified_by = "Admin",

            };


            dbContext.student_doc.Add(ft);
            dbContext.SaveChanges();


            return Json(new { responstext = "Updated " }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Deletedoc(string studentidsibnew)
        {
            var employee = dbContext.student_doc.SingleOrDefault(x => x.student_id == studentidsibnew && x.college_id == listData.CollegeID && x.year == listData.academic_year && x.branch_id == listData.BranchID);
            employee.modified_by = listData.user;
            employee.updated_dt = DateTime.Now;
            dbContext.SaveChanges();

            dbContext.student_doc.Remove(employee ?? throw new InvalidOperationException());
            dbContext.SaveChanges();

            return Json(new { responstext = "Records Deleted " }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult notification()
        {

            //var Booklist = (from a in dbContext.messages.Where(x => x.mid != 0 && x.title != null) select a).ToList();


            //ViewBag.marklist = Booklist;


            //return View();

            IQueryable<send_notification> isn = dbContext.send_notification;
            ViewBag.isn = (from isnval in isn where isnval.college_id == listData.CollegeID && isnval.year == listData.academic_year && isnval.branch_id == listData.BranchID && isnval.visible_student == true && isnval.is_active != false select isnval).ToList();
            return View();

        }

          private void bind_month()
        {
            var yr = dbContext.student_attendences.Where(a => a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && a.year == listData.academic_year).Select(y => y.date.Month).ToList();
            ViewBag.year = yr;
        }

        private void bind_year()
        {
            var yr = (from a in dbContext.students where a.is_active != null && a.college_id == listData.CollegeID && a.year == listData.academic_year && a.branch_id == listData.BranchID && a.year == listData.academic_year && a.created_date != null select a.created_date.Value.Year).Distinct().ToList();
            ViewBag.year = yr;
        }

        public ActionResult attendance_report(student_attendences model, student mdl)
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);

            getStudentClass();
            //var Mn = Month.Value;
            bind_month();
            bind_year();
            //TempData["Message"] = Mn;
            var stu_rep = (from a in dbContext.student_attendences
                           join b in dbContext.students on new { Student_session_id = a.student_session_id } equals new { Student_session_id = b.user_id }
                           where
                             b.class_id == Cls_Sec_Student.class_id &&
                             b.section_id == Cls_Sec_Student.section_id &&
                             a.date.Month == DateTime.Now.Month && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year && b.user_id == listData.user
                           group new { a, b } by new
                           {
                               a.student_session_id,
                               b.firstname,
                               b.lastname
                           } into g
                           select new SchoolMasterClasses.Stud_attendance_cls_sec
                           {
                               user_id = g.Key.student_session_id,
                               FirstName = g.Key.firstname,
                               LastName = g.Key.lastname,
                               present = g.Count(p => (
                               p.a.attendence_type_id == 16 ? (System.Int64?)1 : null) != null),
                               late = g.Count(p => (
                                p.a.attendence_type_id == 17 ? (System.Int64?)1 : null) != null),
                               absent = g.Count(p => (
                               p.a.attendence_type_id == 18 ? (System.Int64?)1 : null) != null),
                               halfday = g.Count(p => (
                               p.a.attendence_type_id == 19 ? (System.Int64?)1 : null) != null),
                               holiday = g.Count(p => (
                               p.a.attendence_type_id == 20 ? (System.Int64?)1 : null) != null)
                           }).ToList();

            ViewBag.count = stu_rep;
            ViewBag.month = DateTime.Now.Month;
            ViewBag.selyear = DateTime.Now.Year;
            ViewBag.sectionid = Cls_Sec_Student.section_id;
            return View();
        }

        public JsonResult GetEvents()
        {

            var Booklist = (from a in dbContext.student_attendences
                            join b in dbContext.attendence_type on a.attendence_type_id equals b.id
                            where a.student_session_id == listData.user && b.college_id == listData.CollegeID && b.year == listData.academic_year && b.branch_id == listData.BranchID && a.is_active == true
                            select new SchoolMasterClasses.Stud_attendance
                            {
                                date = a.date,
                                attendance = b.key_value,
                                color = b.color
                            }).ToList();



            return new JsonResult { Data = Booklist, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        [HttpPost]
        public JsonResult Attlist(string id)
        {
            //var Mn = month.Value;
            getStudentClass();
            bind_month();
            bind_year();
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.pop11 = "1";
            var pr = (from a in dbContext.student_attendences
                      join b in dbContext.students on a.student_session_id equals b.user_id
                      join c in dbContext.attendence_type on a.attendence_type_id equals c.id
                      where a.student_session_id == id && a.date.Month == DateTime.Now.Month && a.college_id == listData.CollegeID && a.branch_id == listData.BranchID && a.year == listData.academic_year
                      select new SchoolMasterClasses.Stud_attendance_cls_sec
                      {
                          id = a.id,
                          FirstName = b.firstname,
                          LastName = b.lastname,
                          type = c.type,
                          RollNumber = b.roll_no,
                          AdmissionNumber = b.admission_no,
                          Year = b.year,
                          date = a.date

                      }).Distinct().ToList();

            ViewBag.list11 = pr;
            return Json(pr, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StartQuiz()
        {
            Session["ColgId"] = listData.CollegeID;
            Session["BrcId"] = listData.BranchID;
            Session["year"] = listData.academic_year;
            return Redirect("/ngquiz/index.html");

        }

        [HttpGet]
        public ActionResult StudentQuizResult()
        {

            var StudnetID = (from a in dbContext.students where a.email == User.Identity.Name select a.id).FirstOrDefault();
            ViewBag.StudentQuizResult = (from a in dbContext.StudentResults
                                         join b in dbContext.AddClasses on a.ClassId equals b.class_id
                                         join c in dbContext.subjects on a.SubjectId equals c.id
                                         where a.UserId == StudnetID
                                         select new SchoolMasterClasses.timetables
                                         {
                                             classes = b.class_name,
                                             name = c.name,
                                             ScoredMarks = a.ScoredMarks,
                                             TotalMarks = a.TotalMarks,
                                             CreatedDate = a.CreatedDate

                                         }).ToList();
            return View();
        }


        public ActionResult playVideos()
        {
            ViewBag.classlist = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return View();
        }


        [HttpPost]
        public JsonResult getSubjects(int? ddlclass)
        {
            var getstaffdata = gs.getteacherSubjects(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid, null);
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getVideos(int? ddlsubid, int? ddlclass)
        {
            //var getVideos = dbContext.videos(, listData.BranchID, ddlclass, ddlsubid, listData.academic_year).ToList();
            var getVideos = dbContext.videos.Where(a => a.class_id == 7 && a.subject_id == ddlsubid).ToList();
            return Json(getVideos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult JoinMeeting()
        {
            var Cls_Sec_Student = cmc.get_class_sec_stu(user: listData.user, collegeid: listData.CollegeID, branchid: listData.BranchID, sessionyear: listData.academic_year);
            DateTime dateAndTime = DateTime.Now;
            DateTime? date = dateAndTime.Date;
            var meeting = (from a in dbContext.Google_meet_Class
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.sections on a.section_id equals c.id
                                 join d in dbContext.subjects on a.subject_id equals d.id
                                 join k in dbContext.staffs on a.User_id equals k.id into stf
                                 from st in stf.DefaultIfEmpty()
                                
                                 where a.College_id == listData.CollegeID && a.year == listData.academic_year &&  a.Status==true&& a.Branch_id == listData.BranchID && a.year == listData.academic_year && a.class_id == Cls_Sec_Student.class_id && a.section_id == Cls_Sec_Student.section_id && a.meeting_date>=date
                                 select new StudentInformation
                                 {
                                     id = a.Id,
                                     Class = b.class_name,
                                     Section = c.section1,
                                     SubName = d.name,
                                     Meetingdate=a.meeting_date,
                                     MeetingTime=a.meeting_time,
                                     MeetingLink= "https://"+a.meeting_Link,
                                     Evaluated_by = st.name
                                     
                                 }).Distinct().ToList();

           
            ViewBag.meetingdetails = meeting;
            return View();
        }
        public JsonResult GetCode(string userId)
        {

            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            Task<string> token = UserManager.GeneratePasswordResetTokenAsync(userId);
            // AccountController acc = new AccountController();

            return Json(token, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> ResetPassword(ResetPassword model)
        {
            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            bool status = false;
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return Json(false);
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                student stu = dbContext.students.Find(model.Id);

                if (stu.Password != model.Password)
                {
                    stu.Password = model.Password;
                    stu.modified_by = listData.user;
                    stu.updated_date = DateTime.Now;
                    stu.is_active = true;
                    stu.college_id = listData.CollegeID;
                    stu.branch_id = listData.BranchID;


                }
                dbContext.students.AddOrUpdate(stu);
                int save = dbContext.SaveChanges();
                if (save > 0)
                {
                    TempData["result"] = "Record Updated Successfully";
                }
                else
                {
                    TempData["result"] = "Something Went wrong";
                }

                status = true;
            }

            return Json(status);
        }
        [HttpPost]
        public bool Getconfirmpassword(string ConfirmPassword, string Password)
        {
            //var emailid = dbContext.AspNetUsers.Where(a => a.Email == email).FirstOrDefault();
            bool result;
            if (ConfirmPassword != Password)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

    }
   

}