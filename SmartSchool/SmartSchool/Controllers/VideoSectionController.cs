﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using SmartSchool.Interfaces;
using SmartSchool.Models;
using SmartSchool.SchoolMasterClasses;

namespace SmartSchool.Controllers
{
    public class VideoSectionController : Controller
    {
        public SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        public get_staff_classes gs = new get_staff_classes();
        private readonly ISessionStore _UsesessionStore;
        public get_session_data1 listData;
        public VideoSectionController(ISessionStore _GetsessionStore)
        {
            try
            {
                _UsesessionStore = _GetsessionStore;
                listData = _UsesessionStore.Get<get_session_data1>("currentSession") as get_session_data1;
                if (listData == null)
                {
                    throw new Exception("Session not been set in VideoController");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
            }
        }
        // GET: VideoSection
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult playVideos()
        {
            ViewBag.classlist = gs.getClass(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid);
            return View();
        }


        [HttpPost]
        public JsonResult getSubjects(int? ddlclass)
        {
            var getstaffdata = gs.getteacherSubjects(listData.CollegeID, listData.BranchID, listData.academic_year, listData.roleid, null);
            return Json(getstaffdata, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getVideos(int? ddlsubid, int? ddlclass)
        {
            //var getVideos = dbcontext.videos(listData.CollegeID, listData.BranchID, ddlclass, ddlsubid, listData.academic_year).ToList();
            var getVideos = dbcontext.videos.Where(a => a.class_id == ddlclass && a.subject_id == ddlsubid).ToList();
            return Json(getVideos, JsonRequestBehavior.AllowGet);
        }
    }
}