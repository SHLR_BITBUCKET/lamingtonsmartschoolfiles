﻿using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartSchool.Interfaces
{
    public interface ISessionStore
    {
        T Get<T>(string key);
        void Set<T>(string key, get_session_data1 entry);
    }
}
