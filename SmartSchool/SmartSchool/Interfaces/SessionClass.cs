﻿using SmartSchool.SchoolMasterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.Interfaces
{
    public class SessionClass : ISessionStore
    {
        public T Get<T>(string key)
        {
            HttpContext context = GetHttpContext();
            return (T)context.Session[key];
        }

        public void Set<T>(string key, get_session_data1 entry)
        {
            HttpContext context = GetHttpContext();
            context.Session[key] = entry;
            if(context.Session["currentSession"] == null)
            {
                throw new Exception("Session not been set yet...?");
            }
        }

        private HttpContext GetHttpContext()
        {
            HttpContext context = HttpContext.Current;
            if(context == null)
            {
                throw new Exception("Session not been set yet...?");
            }
            return context;
        }

    }
}