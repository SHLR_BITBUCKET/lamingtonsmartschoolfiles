//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class book_issues
    {
        public int id { get; set; }
        public Nullable<int> bid { get; set; }
        public Nullable<int> book_id { get; set; }
        public Nullable<System.DateTime> return_date { get; set; }
        public Nullable<System.DateTime> issue_date { get; set; }
        public Nullable<int> is_returned { get; set; }
        public Nullable<int> member_id { get; set; }
        public bool is_active { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string member_type { get; set; }
        public string year { get; set; }
    }
}
