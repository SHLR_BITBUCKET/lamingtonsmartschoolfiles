//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class expense_head
    {
        public int id { get; set; }
        public string exp_category { get; set; }
        public string description { get; set; }
        public Nullable<bool> is_active { get; set; }
        public string is_deleted { get; set; }
        public System.DateTime created_at { get; set; }
        public System.DateTime updated_at { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string year { get; set; }
    }
}
