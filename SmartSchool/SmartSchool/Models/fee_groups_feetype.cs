//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class fee_groups_feetype
    {
        public int id { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
        public Nullable<int> fee_session_group_id { get; set; }
        public Nullable<int> fee_groups_id { get; set; }
        public Nullable<int> feetype_id { get; set; }
        public Nullable<int> session_id { get; set; }
        public Nullable<System.DateTime> due_date { get; set; }
        public Nullable<decimal> amount { get; set; }
        public bool is_active { get; set; }
        public System.DateTime created_at { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string year { get; set; }
    
        public virtual fee_groups fee_groups { get; set; }
        public virtual fee_session_groups fee_session_groups { get; set; }
    }
}
