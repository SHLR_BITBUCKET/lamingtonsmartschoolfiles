//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class front_cms_pages
    {
        public int id { get; set; }
        public string page_type { get; set; }
        public Nullable<int> is_homepage { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string slug { get; set; }
        public string meta_title { get; set; }
        public string meta_description { get; set; }
        public string meta_keyword { get; set; }
        public string feature_image { get; set; }
        public string description { get; set; }
        public System.DateTime publish_date { get; set; }
        public Nullable<int> publish { get; set; }
        public Nullable<int> sidebar { get; set; }
        public Nullable<bool> is_active { get; set; }
        public System.DateTime created_at { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string year { get; set; }
    }
}
