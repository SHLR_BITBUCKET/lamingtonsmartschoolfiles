//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class item_issue
    {
        public int id { get; set; }
        public string user_type { get; set; }
        public string issue_to { get; set; }
        public int issue_by { get; set; }
        public System.DateTime issue_date { get; set; }
        public Nullable<System.DateTime> return_date { get; set; }
        public int item_category_id { get; set; }
        public int item_id { get; set; }
        public int quantity { get; set; }
        public string note { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<bool> is_active { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string year { get; set; }
        public Nullable<bool> Status { get; set; }
    }
}
