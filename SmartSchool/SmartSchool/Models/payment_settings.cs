//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class payment_settings
    {
        public int id { get; set; }
        public string year { get; set; }
        public string payment_type { get; set; }
        public string api_username { get; set; }
        public string api_secret_key { get; set; }
        public string salt { get; set; }
        public string api_publishable_key { get; set; }
        public string api_password { get; set; }
        public string api_signature { get; set; }
        public string api_email { get; set; }
        public string paypal_demo { get; set; }
        public string account_no { get; set; }
        public Nullable<bool> is_active { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public Nullable<int> college_id { get; set; }
        public Nullable<int> branch_id { get; set; }
    }
}
