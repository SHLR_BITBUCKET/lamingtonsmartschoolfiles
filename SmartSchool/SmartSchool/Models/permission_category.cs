//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class permission_category
    {
        public int id { get; set; }
        public Nullable<int> perm_group_id { get; set; }
        public string name { get; set; }
        public string short_code { get; set; }
        public Nullable<int> enable_view { get; set; }
        public Nullable<int> enable_add { get; set; }
        public Nullable<int> enable_edit { get; set; }
        public Nullable<int> enable_delete { get; set; }
        public System.DateTime created_at { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string form_name { get; set; }
        public string year { get; set; }
    }
}
