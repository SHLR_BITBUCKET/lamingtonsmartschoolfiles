//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class staff_leave_details
    {
        public int id { get; set; }
        public int staff_id { get; set; }
        public int leave_type_id { get; set; }
        public Nullable<int> alloted_leave { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public int college_id { get; set; }
        public int branch_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> modified_at { get; set; }
        public string year { get; set; }
    
        public virtual leave_types leave_types { get; set; }
    }
}
