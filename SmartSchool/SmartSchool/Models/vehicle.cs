//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartSchool.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vehicle
    {
        public int id { get; set; }
        public string vehicle_no { get; set; }
        public string vehicle_model { get; set; }
        public string manufacture_year { get; set; }
        public string driver_name { get; set; }
        public string driver_licence { get; set; }
        public string driver_contact { get; set; }
        public string note { get; set; }
        public System.DateTime created_at { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string year { get; set; }
        public Nullable<bool> is_active { get; set; }
    }
}
