﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class Contentdata
    {
        public int Cid { get; set; }
        public string Title { get; set; }
        public string Contentname { get; set; }
        public string Available { get; set; }
        public string File { get; set; }
        public string Allsuperadmin { get; set; }
        public string Forallclasses { get; set; }
        public string Class { get; set; }


        public int? type { get; set; }

        public string All_Super_Admin { get; set; }
        public string Student { get; set; }
        public string For_All_Classes { get; set; }
        public Nullable<int> class_id { get; set; }
        public Nullable<int> cls_sec_id { get; set; }
        public string file { get; set; }
        public string note { get; set; }
        public string is_active { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string FilePath { get; internal set; }

        public int id { get; set; }
        public string ContentType1 { get; set; }

    }
}