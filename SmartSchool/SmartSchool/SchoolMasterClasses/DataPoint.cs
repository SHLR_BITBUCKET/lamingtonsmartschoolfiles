﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SmartSchool.Models
{
    [DataContract]
    public class DataPoint
    {
        public DataPoint(string lable, double y)
        {
            this.Label = lable;
            this.Y = y;
        }
        public DataPoint(string lable, double? y)
        {
            this.Label = lable;
            this.Y = y;
        }
        public DataPoint(string Subject, double IA1, double IA2, double IA3)
        {
            this.Subject = Subject;
            this.IA1 = IA1;
            this.IA2 = IA2;
            this.IA3 = IA3;
        }
        public DataPoint(int? a, string standard1)
        {
            this.standard1 = standard1;
            this.A = a;
        }
        public DataPoint(string standard1, int Present, int Late, int Absent, int Half, int Holiday)
        {
            this.standard1 = standard1;
            this.Present = Present;
            this.Late = Late;
            this.Absent = Absent;
            this.Half = Half;
            this.Holiday = Holiday;
        }
        public DataPoint(string standard1, int MaleCount, int FemaleCount)
        {
            this.standard1 = standard1;
            this.MaleCount = MaleCount;
            this.FemaleCount = FemaleCount;
        }
        public DataPoint(string Subject, int X, double Y)
        {
            this.Subject = Subject;
            this.X = X;
            this.Y = Y;
        }


        //(string lable, double y)
        //(string lable, double? y)
        //(int x, double IA1, double IA2, double IA3)
        //(string Subject, double IA1, double IA2, double IA3)
        //(int x, int? a)
        //(int standard, int Present, int Late, int Absent, int Half, int Holiday)
        //(int standard, int MaleCount, int FemaleCount)
        //(string Subject, int X, double Y)
        
            
        //Explicitly setting the name to be used while serializing to JSON.
        //[DataMember(Name = "x")]
        //public int X = 0;

        //Explicitly setting the name to be used while serializing to JSON.


        [DataMember(Name = "y")]
        public Nullable<double> Y = null;

        [DataMember(Name = "label")]
        public string Label = null;

        [DataMember(Name = "x")]
        public int X = 0;

        [DataMember(Name = "a")]
        public Nullable<double> A = null;

        [DataMember(Name = "IA1")]
        public double IA1 = 0;

        [DataMember(Name = "IA2")]
        public double IA2 = 0;

        [DataMember(Name = "IA3")]
        public double IA3 = 0;

        [DataMember(Name = "standard")]
        public int standard = 0;

        [DataMember(Name = "standard1")]
        public string standard1 = null;

        [DataMember(Name = "Present")]
        public int Present = 0;

        [DataMember(Name = "Late")]
        public int Late = 0;

        [DataMember(Name = "Absent")]
        public int Absent = 0;

        [DataMember(Name = "Half")]
        public int Half = 0;

        [DataMember(Name = "Holiday")]
        public int Holiday = 0;

        [DataMember(Name = "Subject")]
        public string Subject = null;

        [DataMember(Name = "MaleCount")]
        public int MaleCount = 0;

        [DataMember(Name = "FemaleCount")]
        public int FemaleCount = 0;

    }
}