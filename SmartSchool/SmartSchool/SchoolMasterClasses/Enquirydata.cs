﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class Enquirydata
    {
        public int Aeid { get; set; }
        public int College_id { get; set; }
        public int Branch_id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string Reference { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Follow_up_date { get; set; }
        public string Note { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }
        public string Assigned { get; set; }
        public Nullable<int> @Class { get; set; }
        public string No_of_child { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Created_by { get; set; }
        public string Modified_by { get; set; }

        public int Sid { get; set; }
        public string Source1 { get; set; }

    }
}