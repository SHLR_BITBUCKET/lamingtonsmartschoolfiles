﻿using SmartSchool.SchoolMasterClasses;
using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using context = System.Web.HttpContext;

namespace SmartSchool.Models
{
    public class ExceptionLogging
    {

        public static async Task LogException(Exception exdb)
        {
            SmartSchoolHDEntities db = new SmartSchoolHDEntities();

            string exepurl = context.Current.Request.Url.ToString();
            ExceptionLog exModel = new ExceptionLog()
            {
                exceptionMsg = exdb.Message.ToString(),
                exceptionType = exdb.GetType().Name.ToString(),
                exceptionSource = exdb.StackTrace.ToString(),
                exceptionURL = exepurl,
                createdAt = DateTime.Now
            };

            db.ExceptionLogs.Add(exModel);
            int s = db.SaveChanges();
            if (s > 0)
            {
                string errorMessage = "ExceptionMessage: " + exdb.Message.ToString() + Environment.NewLine + "InnerException :" + exdb.InnerException.ToString() + Environment.NewLine + "ExceptionType: " + exdb.GetType().Name.ToString() + Environment.NewLine + "ExceptionSource :" + exdb.StackTrace.ToString() + Environment.NewLine + "ExceptionURL :" + exepurl + Environment.NewLine + "CreatedAt :" + DateTime.Now;
                await  SendMail("technical@shlrtechnosoft.in", "SmartSchool Exceptions", errorMessage);
            }
            //Response.Redirect("~/Account/Login");

        }

        public static async Task SendMail(string to, string subject, string body)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(to));
            message.From = (new MailAddress("shlrenquiry@gmail.com"));
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "shlrenquiry@gmail.com",
                    Password = "shlr2019"
                };

                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
        }
    }
}