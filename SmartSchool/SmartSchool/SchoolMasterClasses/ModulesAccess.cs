﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class ModulesAccess
    {
        SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        public List<Custom_Merge_permissions> RoleModulesAccess(string role_id, int? group_id, int? colgid, int? branchid)
        {           
            var user = (from asp in dbcontext.AspNetRoles where asp.Id == role_id select asp.Name).FirstOrDefault();
            if (user == "SuperAdmin")
            {
                var get_role_modules = (from i in dbcontext.permission_group
                                        join j in dbcontext.permission_category on i.id equals j.perm_group_id
                                        join k in dbcontext.roles_permissions on j.id equals k.perm_cat_id
                                        where k.role_id == role_id && j.perm_group_id == group_id && k.college_id == colgid && k.branch_id == branchid && i.is_active==true
                                        select new Custom_Merge_permissions
                                        {
                                            id = i.id,
                                            name = i.name,
                                            short_code = i.short_code,
                                            cat_id = j.id,
                                            cat_name = j.name,
                                            cat_short_code = j.short_code,
                                            form_nam = j.form_name,
                                            role_id = k.role_id,
                                            perm_cat_id = k.perm_cat_id,
                                            can_view = k.can_view,
                                            can_add = k.can_add,
                                            can_edit = k.can_edit,
                                            can_delete = k.can_delete,
                                            year_as_order = j.year
                                            
                                        }).ToList().OrderBy(k => int.Parse(k.year_as_order ?? 0.ToString()));

                var count_can_view = get_role_modules.Sum(k => k.can_view);
                if (count_can_view != 0)
                {
                    return get_role_modules.ToList();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var get_role_modules = (from i in dbcontext.permission_group
                                        join j in dbcontext.permission_category on i.id equals j.perm_group_id
                                        join k in dbcontext.roles_permissions on j.id equals k.perm_cat_id
                                        where k.role_id == role_id && j.perm_group_id == group_id && k.college_id == colgid && k.branch_id == branchid
                                        select new Custom_Merge_permissions
                                        {
                                            id = i.id,
                                            name = i.name,
                                            short_code = i.short_code,
                                            cat_id = j.id,
                                            cat_name = j.name,
                                            cat_short_code = j.short_code,
                                            form_nam = j.form_name,
                                            role_id = k.role_id,
                                            perm_cat_id = k.perm_cat_id,
                                            can_view = k.can_view,
                                            can_add = k.can_add,
                                            can_edit = k.can_edit,
                                            can_delete = k.can_delete

                                        }).ToList();

                var count_can_view = get_role_modules.Sum(k => k.can_view);
                if (count_can_view != 0)
                {
                    return get_role_modules;
                }
                else
                {
                    return null;
                }
            }         
        }
     

        public List<Custom_Merge_permissions> RoleAccess(string role_id, int? group_id, int? cat_id, int? college_id, int? branch_id)
        {
            var getaccess = (from i in dbcontext.permission_group
                             join j in dbcontext.permission_category on i.id equals j.perm_group_id
                             join k in dbcontext.roles_permissions on j.id equals k.perm_cat_id
                             where k.role_id == role_id && j.perm_group_id == group_id && k.perm_cat_id == cat_id && k.college_id == college_id && k.branch_id == branch_id
                             select new Custom_Merge_permissions
                             {
                                 can_add = k.can_add,
                                 can_edit = k.can_edit,
                                 can_delete = k.can_delete
                             }).ToList();
            return getaccess;
        }

        public class Custom_Merge_permissions
        {
            public int id { get; set; }
            public string name { get; set; }
            public string short_code { get; set; }
            public int cat_id { get; set; }
            public string cat_name { get; set; }
            public string cat_short_code { get; set; }
            public string role_id { get; set; }
            public Nullable<int> perm_cat_id { get; set; }
            public Nullable<int> can_view { get; set; }
            public Nullable<int> can_add { get; set; }
            public Nullable<int> can_edit { get; set; }
            public Nullable<int> can_delete { get; set; }
            public string form_nam { get; set; }
            public string year_as_order { get; set; }
        }

    }
}