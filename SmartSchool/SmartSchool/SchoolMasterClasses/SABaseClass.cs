﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SmartSchool.Resources;

namespace SmartSchool.SchoolMasterClasses
{
    public abstract class SABaseClass
    {
        public int id { get; set; }
        public int college_id { get; set; }
        public int branch_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime modified_at { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
    }
    public class search_expenses : SABaseClass
    {
        public DateTime from_date { get; set; }
        public DateTime to_date { get; set; }
        public string search { get; set; }
        public string Name { get; set; }

        public string InvoiceNo { get; set; }
        public string Expense_Head { get; set; }
        public DateTime? Date { get; set; }
        public double? Amount { get; set; }
        public string expense_category { get; set; }
    }
    public class expenses_Report : SABaseClass
    {
        public DateTime from_date { get; set; }
        public DateTime to_date { get; set; }
        public string search { get; set; }
        public string Name { get; set; }

        public string InvoiceNo { get; set; }
        public string Expense_Head { get; set; }
        public DateTime? Date { get; set; }
        public double? Amount { get; set; }
        public int? eid { get; set; }
        public string Document { get; set; }
    }
    public class ClassTimetable : SABaseClass
    {
        public int Id { get; set; }
        public string classes { get; set; }
        public string section { get; set; }
    }

    public class AssignClassteacher : ClassTimetable
    {
        public string name { get; set; }
    }

    public class AddClassTimetable : ClassTimetable
    {
        public string subjects { get; set; }
        public string class_id { get; set; }
        public string section_id { get; set; }
        public string day_name { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
        public int room_no { get; set; }
    }

    public class AssignSubject : AddClassTimetable
    {
        public string name { get; set; }
    }
    public class addclass : ClassTimetable
    {
    }

    public class Promote : ClassTimetable
    {
        public DateTime Year { get; set; }
        public int? AdmissionNumber { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string DateOfBirth { get; set; }
    }
    public class homework : Promote

    {
        public string @class { get; set; }
        public string Subject { get; set; }
        public string document { get; set; }
        public string staff_id { get; set; }
        public DateTime submit_date { get; set; }
        public DateTime homework_date { get; set; }
        public DateTime evaluation_date { get; set; }
        public string Description { get; set; }
        public string Doc { get; set; }

    }
    public class StuClass : SABaseClass
    {
        public String firstname { get; set; }
        public string AdmissionNumber { get; set; }
        public string class1 { get; set; }
        public int section_id { get; set; }
        public int? subject_id { get; set; }
        public int Class_id { get; set; }
        public string section1 { get; set; }
        public string name { get; set; }
        public DateTime? homeworkdate { get; set; }
        public DateTime? submissiondate { get; set; }
        public Nullable<DateTime> evaluationdate { get; set; }
        public int? evaluatedby { get; set; }
        public string Description { get; set; }
        public string userid { get; set; }
        public string Doc { get; set; }
    }
    public class Stdentfeeclass
    {
        [Key]
        public string id { get; set; }
        public string fegroupid { get; set; }
        public string feecde { get; set; }
        public string duedate { get; set; }
        public string stats { get; set; }
        public string amount { get; set; }
        public string paymentid { get; set; }
        public string description { get; set; }

        public string mode { get; set; }
        public string Date { get; set; }
        public string Discount { get; set; }
        public string fine { get; set; }
        public string paid { get; set; }
        public string balance { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Feetype { get; set; }
        public string Feegroup { get; set; }
        public string Action { get; set; }
        public string Section { get; set; }
        public string Admissionno { get; set; }
        public string Rollno { get; set; }
        public string DOB { get; set; }
        public string Deposit { get; set; }
        public string FatherName { get; set; }
        public string Admissiondate { get; set; }
        public string Feemasterid { get; set; }
        public string Feetypeid { get; set; }
        public string UserId { get; set; }
        public string Invoice_Id { get; set; }

        public double? partial_amount { get; set; }
        public DateTime? partial_Date { get; set; }
        public int? partial_dpid { get; set; }


    }

    public class Subjectdetailsstudent
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public int? Esid { get; set; }
        public Nullable<int> Admission_no { get; set; }
        public string FatherName { get; set; }
        public string Rollno { get; set; }
        public string student_id { get; set; }
        public string Studentname { get; set; }
        public int? Get_marks { get; set; }
        public string attendence { get; set; }
        public string StudentID { get; set; }
        public int? Fullmarks { get; set; }
        public int? Minmarks { get; set; }
        public int? Maxmarks { get; set; }
    }
    public class timetables : SABaseClass
    {
        public string name { get; set; }
        public string day_name { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public string room_no { get; set; }
        public string classes { get; set; }
        public int? ScoredMarks { get; set; }
        public int? TotalMarks { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
    public class StudentInformation
    {
        public int? AdmissionNumber { get; set; }
        public string RollNumber { get; set; }
        public string Category { get; set; }
        public string roll_id { get; set; }
        public bool? Status1 { get; set; }
        public string Type { get; set; }
        public string sts_number { get; set; }
        public int? class_id { get; set; }
        public int? section_id { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public int? category_id { get; set; }
        public string collgename { get; set; }
        public string branchname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string State { get; set; }
      
        public string City { get; set; }

        public DateTime? Meetingdate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingLink { get; set; }
        public string Pincode { get; set; }
        public string DateOfBirth { get; set; }
        //public string Category { get; set; }
        public string Religion { get; set; }
        public string Caste { get; set; }
        public string user_id { get; set; }
        [Required(ErrorMessage = "You must provide a phone number")]

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]

        [Display(Name = nameof(Resource1.Emergency_Number), ResourceType = typeof(Resource1))]
        public string MobileNumber { get; set; }


        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public string AdmissionDate { get; set; }
        public string StudentPhoto { get; set; }
        public string BloodGroup { get; set; }
        public string StudentHouse { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string AsonDate { get; set; }
        public string FatherName { get; set; }
        public string FatherPhone { get; set; }
        public string FatherOccupation { get; set; }
        public string FatherPhoto { get; set; }
        public string FatherAadharNumber { get; set; }
        public string MotherName { get; set; }
        public string MotherPhone { get; set; }
        public string MotherOccupation { get; set; }
        public string MotherPhoto { get; set; }
        public string MotherAadharNumber { get; set; }
        [Required(ErrorMessage = "Please Enter Guardian Name")]
        public string GuardianName { get; set; }
        public string GuardianRelation { get; set; }
        public string GuardianEmail { get; set; }
        public string GuardianPhoto { get; set; }
        [Required(ErrorMessage = "You must provide a Guardian phone number")]
        public string GuardianPhone { get; set; }
        public string GuardianOccupation { get; set; }
        public string GuardianAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string RouteList { get; set; }
        public string Hostel { get; set; }
        public string RoomNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }

        [Required(ErrorMessage = "You must Select RTE")]
        public string RTE { get; set; }
        public string PreviousSchoolDetails { get; set; }
        public string Note { get; set; }
        public string Title1 { get; set; }
        public string Documents1 { get; set; }
        public string Title2 { get; set; }
        public string Documents2 { get; set; }
        public string Title3 { get; set; }
        public string Documents3 { get; set; }
        public string Title4 { get; set; }
        public string Documents4 { get; set; }
        public string Sibalingname { get; set; }
        public string Sibalingid { get; set; }
        public bool? IsActive { get; set; }
        public string ParentUserName { get; set; }
        public int? id { get; set; }
        public int? member_id { get; set; }
        public string library_card_no { get; set; }
        public bool? is_activelms { get; set; }
        public string member_type { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string ParentPassword { get; set; }
        public string Certificate { get; set; }
        public string Idcard { get; set; }
        public string homework_date { get; set; }
        public string Desription { get; set; }
        public string Evaluated_by { get; set; }
        public string Homeworksubmit_date { get; set; }
        public string Status { get; set; }
        public string SubName { get; set; }
        public string SubCode { get; set; }
        public string HomeworkSubmissiondate { get; set; }
        public string HomeworkEvaluationdate { get; set; }
        public string Date { get; set; }
        public string File { get; set; }
        public string Teacher { get; set; }
        public string SubjectType { get; set; }
        public string Total { get; set; }
        public string Discount { get; set; }
        public string Fine { get; set; }
        public string Paid { get; set; }
        public string Balance { get; set; }
        public string member_types { get; set; }

        public string Iagroupname { get; set; }
        public string Ianame { get; set; }
        public int Iagroupid { get; set; }
        public int Iaid { get; set; }

        public int Subjectid { get; set; }
        public string SubjectName { get; set; }
        public string Activity1 { get; set; }
        public string Activity2 { get; set; }

        public string AchivementsTest { get; set; }
        public string Maxmarks { get; set; }
        public string Passingmarks { get; set; }
        public string Scoredmarks { get; set; }
        public string book_title { get; set; }

        public string book_no { get; set; }

        public string author { get; set; }

        public string Medium { get; set; }
        public string AadhaarNumber { get; set; }
        public string CasteCertificateNumber { get; set; }
        public string GRNumber { get; set; }
        public string MICRNumber { get; set; }

        public string BranchName { get; set; }
        public string IncomeCertificateNumber { get; set; }
        public Nullable<System.DateTime> issue_date { get; set; }
        public Nullable<System.DateTime> return_date { get; set; }


        public int? exam_schedule_id { get; set; }
        public string student_id { get; set; }
        public int? get_marks { get; set; }
        public DateTime? date_of_exam { get; set; }
        public bool? is_active { get; set; }
        public int? college_id { get; set; }
        public int? branch_id { get; set; }
        public string year { get; set; }
        public int? esid { get; set; }
        public int? exam_id { get; set; }
        public int? teacher_subject_id { get; set; }
        public string room_no { get; set; }
        public int? full_marks { get; set; }
        public int? passing_marks { get; set; }

        public string exam_name { get; set; }
    }

        

        public class StudentIAmarksInformation : SABaseClass
        {
            [Key]
            public int? AdmissionNumber { get; set; }
            public string RollNumber { get; set; }
            public int? class_id { get; set; }
            public int? section_id { get; set; }
            public string Class { get; set; }
            public string Section { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Gender { get; set; }
            public string DateOfBirth { get; set; }
            public string Category { get; set; }
            public string Religion { get; set; }
            public string Caste { get; set; }
            public string user_id { get; set; }

            public string Email { get; set; }

            public string Iagroupname { get; set; }
            public string Ianame { get; set; }
            public int Iagroupid { get; set; }
            public int Iaid { get; set; }

            public int Subjectid { get; set; }
            public string SubjectName { get; set; }
            public string Activity1 { get; set; }
            public string Activity2 { get; set; }

            public string AchivementsTest { get; set; }
            public string Maxmarks { get; set; }
            public string Passingmarks { get; set; }
            public string Scoredmarks { get; set; }
            public string get_marks { get; set; }


            public int? FA1_A1 { get; set; }
            public int? FA1_A2 { get; set; }
            public int? FA1_W { get; set; }

            public int? FA2_A1 { get; set; }
            public int? FA2_A2 { get; set; }
            public int? FA2_W { get; set; }

            public int? FA3_A1 { get; set; }
            public int? FA3_A2 { get; set; }
            public int? FA3_W { get; set; }
            public int? FA4_A1 { get; set; }
            public int? FA4_A2 { get; set; }
            public int? FA4_W { get; set; }

        }
        public class staffLeaveDetails : SABaseClass
        {
            //public int id { get; set; }
            public int staffId { get; set; }
            public int leaveTypeId { get; set; }
            public int? alloted_leave { get; set; }
        }
        public class staffLeaves : SABaseClass
        {
            public string roleid { get; set; }

            public string name { get; set; }
            public string leavetype { get; set; }
            public int staff_id { get; set; }
            public int leave_type_id { get; set; }
            public System.DateTime leave_from { get; set; }
            public System.DateTime leave_to { get; set; }
            public int leave_days { get; set; }
            public string employee_remark { get; set; }
            public string admin_remark { get; set; }
            public string status { get; set; }
            public string applied_by { get; set; }
            public string document_file { get; set; }
            public System.DateTime date { get; set; }
            public int? alloted_leave { get; set; }
            public int? Available { get; set; }
            public int? used { get; set; }


        }
        public class staffattendance : StaffInformation
        {
            public System.DateTime date { get; set; }
            public int staff_id { get; set; }
            public int staff_attendance_type_id { get; set; }
            public string remark { get; set; }
            public int? aid { get; set; }
            public int? atype_id { get; set; }
        }
        public class library_member : SABaseClass
        {
            public int? AdmissionNumber { get; set; }
            public string FirstName { get; set; }
            public string MobileNumber { get; set; }
            public string LastName { get; set; }
            public string library_card_no { get; set; }
            public int member_id { get; set; }
            public string member_type { get; set; }
        }
        public class Book
        {
            public int bid { get; set; }
            public string book_title { get; set; }
            public string book_no { get; set; }
            public string isbn_no { get; set; }
            public string subject { get; set; }
            public string rack_no { get; set; }
            public string publish { get; set; }
            public string author { get; set; }
            public Nullable<int> qty { get; set; }
            public Nullable<double> perunitcost { get; set; }
            public Nullable<System.DateTime> postdate { get; set; }
            public string description { get; set; }
            public string available { get; set; }
            public string is_active { get; set; }
            public Nullable<System.DateTime> created_at { get; set; }
            public Nullable<System.DateTime> updated_at { get; set; }
            public Nullable<int> college_id { get; set; }
            public Nullable<int> branch_id { get; set; }
            public string created_by { get; set; }
            public string modified_by { get; set; }
            public Nullable<System.DateTime> return_date { get; set; }
            public Nullable<System.DateTime> issue_date { get; set; }

        }
        public class teacher_subj : SABaseClass
        {
            public string name { get; set; }
            public string Teacher_name { get; set; }
            public string Classes { get; set; }
            public string Section { get; set; }

        }
       
        public class StaffInformation : SABaseClass
        {
            public string employee_id { get; set; }
            public int? MobileNumber { get; set; }
            public int? mobile_number { get; set; }
            public string department { get; set; }
            public string designation { get; set; }
            public string qualification { get; set; }
            public string work_exp { get; set; }
            public string library_card_no { get; set; }
            public int? member_id { get; set; }
            public string emp_id { get; set; }
            public string member_type { get; set; }
            public string name { get; set; }
            public string surname { get; set; }
            public string father_name { get; set; }
            public string mother_name { get; set; }
            public string contact_no { get; set; }
            public string emergency_contact_no { get; set; }
            public string email { get; set; }
            public Nullable<System.DateTime> dob { get; set; }
            public string marital_status { get; set; }
            public Nullable<System.DateTime> date_of_joining { get; set; }
            public Nullable<System.DateTime> date_of_leaving { get; set; }
            public string local_address { get; set; }
            public string permanent_address { get; set; }
            public string note { get; set; }
            public string image { get; set; }
            public string password { get; set; }
            public string member_types { get; set; }
            public string gender { get; set; }
            public string account_title { get; set; }
            public string bank_account_no { get; set; }
            public string bank_name { get; set; }
            public string ifsc_code { get; set; }
            public string bank_branch { get; set; }
            public string payscale { get; set; }
            public string basic_salary { get; set; }
            public string epf_no { get; set; }
            public string contract_type { get; set; }
            public string shift { get; set; }
            public string location { get; set; }
            public string facebook { get; set; }
            public string twitter { get; set; }
            public string linkedin { get; set; }
            public string instagram { get; set; }
            public string resume { get; set; }
            public string joining_letter { get; set; }
            public string resignation_letter { get; set; }
            public string other_document_name { get; set; }
            public string other_document_file { get; set; }
            public Nullable<int> user_id { get; set; }
            public Nullable<bool> is_active { get; set; }
            public string verification_code { get; set; }
            public string rolename { get; set; }
        }
        public class email_sms_for_all : SABaseClass
        {
            public string MobileNumber { get; set; }
            public string Email { get; set; }
            public string FatherPhone { get; set; }
            public string MotherPhone { get; set; }
            public string GuardianEmail { get; set; }
            public string IsActive { get; set; }
            public string contact_no { get; set; }
            public string emergency_contact_no { get; set; }
            public string email { get; set; }
            public DateTime? Date { get; set; }
            public String SMS { get; set; }
            public String Group { get; set; }
            public String Individual { get; set; }
            public String Class { get; set; }
            public String Title { get; set; }
        }
        public class Studentlist : SABaseClass
        {
            public string student_id { get; set; }
            public string firstname { get; set; }
        }
        public class Stud_attendance : SABaseClass
        {
            public string classes { get; set; }
            public string user_id { get; set; }
            public string section { get; set; }
            public string color { get; set; }
            public System.DateTime? date { get; set; }
            public int? AdmissionNumber { get; set; }
            public string RollNumber { get; set; }
            public string remark { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int? student_session_id { get; set; }
            public string attendance { get; set; }
            public int? aid { get; set; }
            public int? atype_id { get; set; }
            public int? class_id { get; set; }
            public int? section_id { get; set; }

        }
        public class Stud_attendance_cls_sec : SABaseClass
        {
            public string user_id { get; set; }
            public string classes { get; set; }
            public string section { get; set; }
            public System.DateTime? date { get; set; }
            public int? AdmissionNumber { get; set; }
            public string RollNumber { get; set; }
            public string remark { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Year { get; set; }
            public string type { get; set; }
            public string stud_id { get; set; }
            public int? ids { get; set; }
            public int? present { get; set; }
            public int? late { get; set; }
            public int? absent { get; set; }
            public int? halfday { get; set; }
            public int? holiday { get; set; }
            internal int? attendance_type_id;


        }
        public class attendance_report : Stud_attendance_cls_sec
        {
            public string Month { get; set; }
        }
        public class notificationsetting
        {
            public string type { get; set; }
            public string[] ismailorsms { get; set; }
            public string display_notification { get; set; }

        }
        public class members : SABaseClass
        {
            public string library_card_no { get; set; }
            public string member_type { get; set; }
            public string employee_id { get; set; }
            public string name { get; set; }
            public string surname { get; set; }
            public int? admission_no { get; set; }
            public string contact_no { get; set; }
            public int? member_id { get; set; }
        }
        public class StudentMarksDetails
        {
            [Key]
            public int? AdmissionNumber { get; set; }
            public string RollNumber { get; set; }
            public int? ClassID { get; set; }
            public int? SectionID { get; set; }
            public string Name { get; set; }

            public string user_id { get; set; }
            public string FatherName { get; set; }
            public string MotherName { get; set; }
            public string Result { get; set; }
            public int? Grandtotal { get; set; }
            public double? Percentage { get; set; }
            public double? Marks { get; set; }
            public int Exam_schedule_id { get; set; }
        }
        public class saveroles
        {
            public string catid { get; set; }
            public string get_fea { get; set; }
            public string[] get_fun { get; set; }
            public string modelname { get; set; }
        }
        public class AssignRole
        {
            public int id { get; set; }
            public string name { get; set; }
            public string Mname { get; set; }
            public int cat_id { get; set; }
            public string short_name { get; set; }
            public int can_view { get; set; }
            public int can_add { get; set; }
            public int can_edit { get; set; }
            public int can_delete { get; set; }

        }
        public class StudentDetailsformarks
        {
            public int? Admission_no { get; set; }
            public string Roll_no { get; set; }
            public string Firstname { get; set; }
            public string User_id { get; set; }
            public string FatherName { get; set; }
        }
        public class Expensedetails
        {
            public string Name { get; set; }
            public string Invoice_no { get; set; }
            public string Inc_head_id { get; set; }
            public DateTime? Date { get; set; }
            public int? Amount { get; set; }
        }

        public class Staff_attendance_cls_sec : SABaseClass
        {
            public System.DateTime? date { get; set; }
            public string remark { get; set; }
            public string Name { get; set; }
            public int? Year { get; set; }
            public string type { get; set; }
            public string staff_id { get; set; }
            public int? ids { get; set; }
            public int? present { get; set; }
            public int? late { get; set; }
            public int? absent { get; set; }
            public int? halfday { get; set; }
            public int? holiday { get; set; }
        }

        public class Staff_attendance_report : Stud_attendance_cls_sec
        {
            public string Month { get; set; }

        }

        public class newstudent
        {
            public string userID { get; set; }
        }

        public class listpromotestudents
        {
            public int id { get; set; }
            public int admission { get; set; }
            public string passORfail { get; set; }
            public string continueORleave { get; set; }
        }
     

        public class evaluation_report
        {
            public int? id { get; set; }
            public int? homework_id { get; set; }
            public string Subject { get; set; }
            public int? AdmissionNumber { get; set; }
            public string FirstName { get; set; }
            [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? homework_date { get; set; }
            public DateTime? submit_date { get; set; }
            public string created_by { get; set; }
            public string modified_by { get; set; }
            public string Description { get; set; }
            public string Doc { get; set; }
            public int? percentage { get; set; }
            public int? StudCount { get; set; }
            public int? PendingStudCount { get; set; }
            public int? class_id { get; set; }
            public int? section_id { get; set; }
            public int? subject_id { get; set; }
            public bool Status { get; set; }
            public string name { get; set; }
            public System.DateTime date { get; set; }

            public string stringDate { get; set; }
            public string class_name { get; set; }
            public string Classes { get; set; }
            public string Section { get; set; }
            public int? admission_no { get; set; }
            public string Desription { get; set; }
            public string Evaluated_by { get; set; }
            public string LastName { get; set; }
            public string Staffname { get; set; }
            public string stafflastname { get; set; }
            public DateTime? createdate { get; set; }
            public DateTime? evaluationdate { get; set; }
            public string Assigned_By { get; set; }
        }
    
}