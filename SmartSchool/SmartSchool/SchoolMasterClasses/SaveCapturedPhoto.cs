﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class SaveCapturedPhoto
    {   
        public string SImage(string ImgStr, string ImgName)
        {
            try
            {
                String path = System.Web.HttpContext.Current.Server.MapPath("~/Content/Profile"); //Path
                string imageName = null;
                //Check if directory exist
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }
                if (ImgName == "" || ImgName == null)
                {
                    imageName = DateTime.Now.ToString("ddMMyyyyss");
                }
                else
                {
                    imageName = ImgName + DateTime.Now.ToString("ddMMyyyyss") + ".jpg";
                }

                //set the image path
                string imgPath = Path.Combine(path, imageName);
                string convert = ImgStr.Replace("data:image/jpeg;base64,", String.Empty);
                byte[] imageBytes = Convert.FromBase64String(convert);

                System.IO.File.WriteAllBytes(imgPath, imageBytes);
                return imageName;
            }
            catch (Exception ex)
            {
                return  null;
            }
        }
    }
}