﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace SmartSchool.SchoolMasterClasses
{
    public class SmsTextLocalClass
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public sms_config get_provider(int? _college_id, int? _branch_id, string academicyear)
        {
            var getdata = (from a in dbContext.sms_config where a.is_active == true && a.college_id == _college_id && a.branch_id == _branch_id && a.year == academicyear select a).FirstOrDefault();
            return getdata;
        }

        public bool send_message(string[] numbers, string _message, int? collegeid, int? branchid, string academicyear)
        {
            string _sendrid = null, _hash = null, removehtml = null, _username = null, _password = null;

            var listdata = get_provider(_college_id: collegeid, _branch_id: branchid, academicyear: academicyear);
            if (listdata != null)
            {
                if (listdata.type == "1")
                {
                    _sendrid = listdata.senderid;
                    _username = listdata.username;
                    _password = listdata.password;
                }
                else
                {
                    _sendrid = listdata.senderid;
                    _hash = listdata.authkey;
                    _username = listdata.username;
                }

            }
            removehtml = Regex.Replace(_message, "<.*?>", String.Empty);
            foreach (string Num in numbers)
            {
                if (Num != null && Num.Length == 10)
                {
                    
                    string MobNum = Num.Trim();
                    string Message = removehtml.TrimEnd();
                    string UserName = _username;
                    string Password = "Shlrdemo@123";
                    string SenderID = _sendrid;
                    string Trans = "Trans";
                    int DCS = 0;
                    int Flashsms = 0;
                    int route = 7;
                    int pid = 1;
                    string strUrl = "http://smslocal.smshouse.in/api2/send/?username=admin@shlrtechnosoft.in&hash=68f2d3b7daed6223fe9e802da15ec201440a1e42f9105e0a61b2d5630889b522&numbers=91" + MobNum + "&sender=SHLRTE&message=" + Message + "";
                    //Currently using smshouse sms provider, if you want to use Textlocal need to enable it.
                    //string strUrl = "http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=" + UserName + "&password=" + Password + "&sender=" + SenderID + "&mobileno=" + MobNum + "&msg=" + Message;
                    //string strUrl = "http://45.114.143.23/api/mt/SendSMS?user=" + UserName + "&password=" + Password + "&senderid=" + SenderID + "&channel=" + Trans + "&DCS=" + DCS + "&flashsms=" + Flashsms + "&number=" + MobNum + "&text=" + Message + "& route=" + route + "&peid=" + pid;
                    WebRequest request = HttpWebRequest.Create(strUrl);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    string dataString = readStream.ReadToEnd();

                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}