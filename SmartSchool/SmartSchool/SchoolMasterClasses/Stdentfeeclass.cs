﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartSchool.Models
{
    public class Stdentfeeclass1
    {
        [Key]
        public int? id { get; set; }
        public string fegroupid { get; set; }
        public string feecde { get; set; }
        public string duedate { get; set; }
        public string stats { get; set; }
        public double? amount { get; set; }
        public string paymentid { get; set; }
        public string description { get; set; }
        public string mode { get; set; }
        public DateTime? Date { get; set; }
        public string Discount { get; set; }
        public string fine { get; set; }
        public string paid { get; set; }
        public string balance { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Feetype { get; set; }
        public string Feegroup { get; set; }
        public string Action { get; set; }
        public string Section { get; set; }
        public string Admissionno { get; set; }
        public string Rollno { get; set; }
        public string DOB { get; set; }
        public string Deposit { get; set; }
        public string FatherName { get; set; }
        public string Admissiondate { get; set; }
        public string Feemasterid { get; set; }
        public string UserId { get; set; }
        public string Invoice_Id { get; set; }
        public string fee_session_group_id { get; set; }
        public double? total { get; set; }
    }
}