﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartSchool.SchoolMasterClasses
{
    public class StudentInformation
    {
        [Key]
        public int? AdmissionNumber { get; set; }
        public string RollNumber { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Category { get; set; }
        public string Religion { get; set; }
        public string Caste { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string AdmissionDate { get; set; }
        public string StudentPhoto { get; set; }
        public string BloodGroup { get; set; }
        public string StudentHouse { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string AsonDate { get; set; }
        public string FatherName { get; set; }
        public string FatherPhone { get; set; }
        public string FatherOccupation { get; set; }
        public string FatherPhoto { get; set; }
        public string MotherName { get; set; }
        public string MotherPhone { get; set; }
        public string MotherOccupation { get; set; }
        public string MotherPhoto { get; set; }
        public string GuardianName { get; set; }
        public string GuardianRelation { get; set; }
        public string GuardianEmail { get; set; }
        public string GuardianPhoto { get; set; }
        public string GuardianPhone { get; set; }
        public string GuardianOccupation { get; set; }
        public string GuardianAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string RouteList { get; set; }
        public string Hostel { get; set; }
        public string RoomNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public string BanName { get; set; }
        public string IFSCCode { get; set; }
        public string NationalIdentificationNumber { get; set; }
        public string LocalIdentificationNumber { get; set; }
        public string RTE { get; set; }
        public string PreviousSchoolDetails { get; set; }
        public string Note { get; set; }
        public string Title { get; set; }
        public string Documents { get; set; }
        public string IsActive { get; set; }
        public string ParentUserName { get; set; }
        public int id { get; set; }
        public int? member_id { get; set; }
        public string library_card_no { get; set; }
        public bool? is_activelms { get; set; }
        public string member_type { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string ParentPassword { get; set; }
        public string Certificate { get; set; }
        public string Idcard { get; set; }
        public string homework_date { get; set; }
        public string Desription { get; set; }
        public string Evaluated_by { get; set; }
        public string Homeworksubmit_date { get; set; }
        public string Status { get; set; }
        public string SubName { get; set; }
        public string SubCode { get; set; }
        public string HomeworkSubmissiondate { get; set; }
        public string HomeworkEvaluationdate { get; set; }
        public string Date { get; set; }
        public string File { get; set; }
        public string Teacher { get; set; }
        public string SubjectType { get; set; }
        public string Total { get; set; }
        public string Discount { get; set; }
        public string Fine { get; set; }
        public string Paid { get; set; }
        public string Balance { get; set; }
        public string user_id { get; set; }
        public string member_types { get; set; }
    }
}