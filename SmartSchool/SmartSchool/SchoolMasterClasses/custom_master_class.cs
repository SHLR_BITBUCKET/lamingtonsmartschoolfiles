﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static SmartSchool.SchoolMasterClasses.StudentInformation;

namespace SmartSchool.SchoolMasterClasses
{
    public class custom_master_class
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();

        public SelectList BindClass(int? collegeid, int? branchid, string academicyear)
        {
            List<AddClass> c = dbContext.AddClasses.Where(x =>  x.college_id == collegeid && x.branch_id == branchid && x.year == academicyear).ToList();
            SelectList selectclass = new SelectList(c, "class_id", "class_name");
            return selectclass;

        }

        public SelectList BindSection(int? collegeid, int? branchid, string academicyear)
        {
            List<section> c = dbContext.sections.Where(x => x.id != 0 && x.college_id == collegeid && x.branch_id == branchid && x.year == academicyear).ToList();
            section sem = new section
            {
                section1 = "----Please Select Section----",
                id = 0
            };
            c.Insert(0, sem);
            SelectList selectsect = new SelectList(c, "id", "section1 ", 0);
            return selectsect;
        }
        public List<StudentInformation> get_student_class(int? classid, int? collegeid, int? branchid, string academicyear)
        {

            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 join c in dbContext.libarary_members on a.admission_no equals c.member_id into lm
                                 from lms in lm.DefaultIfEmpty()
                                 where a.class_id == classid && lms.member_type != "Teacher" && a.college_id == collegeid && a.branch_id == branchid && a.year == academicyear
                                 select new StudentInformation
                                 {
                                     id = a.id,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     LastName = a.lastname,
                                     Class = b.class_name,
                                     FatherName = a.father_name,
                                     MobileNumber = a.mobileno,
                                     IsActive = a.is_active,
                                     GuardianName = a.guardian_name,
                                     GuardianPhone = a.guardian_phone,
                                     ParentUserName = "User",
                                     member_id = lms.member_id,
                                     library_card_no = lms.library_card_no,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     is_activelms = lms.is_active,
                                     member_type = a.user_id,
                                     member_types = lms.member_type
                                 }).ToList();
            return StudyMaterial;
        }

        public List<StudentInformation> get_student_classs(int? college_id, int? branch_id, int year, int? classid)
        {
            var StudyMaterial = (from a in dbContext.students
                                 join b in dbContext.AddClasses on a.class_id equals b.class_id
                                 where a.college_id == b.college_id && a.branch_id == b.branch_id
                                 join c in dbContext.libarary_members on a.user_id equals c.member_type into lm
                                 from lms in lm.DefaultIfEmpty()
                                 where a.college_id == college_id && a.branch_id == branch_id && a.year == year.ToString()
                                 select new StudentInformation
                                 {
                                     id = a.id,
                                     AdmissionNumber = a.admission_no,
                                     FirstName = a.firstname,
                                     LastName = a.lastname,
                                     Class = b.class_name,
                                     FatherName = a.father_name,
                                     MobileNumber = a.mobileno,
                                     IsActive = a.is_active,
                                     GuardianName = a.guardian_name,
                                     GuardianPhone = a.guardian_phone,
                                     ParentUserName = "User",
                                     library_card_no = lms.library_card_no,
                                     member_id = lms.member_id,
                                     DateOfBirth = a.dob,
                                     Gender = a.gender,
                                     is_activelms = lms.is_active,
                                     member_type = lms.member_type,
                                     user_id = a.user_id
                                 }).ToList();
            return StudyMaterial;
        }

        public List<dynamic> get_all_student(int? college_id, int? branch_id, string year, string[] individualStudents, string mobilenumber)
        {
            List<dynamic> ls = new List<dynamic>();
            if (individualStudents != null)
            {
                foreach (var loops in individualStudents)
                {
                    int getid = Convert.ToInt32(loops);
                    var all_student = (from a in dbContext.students
                                       where a.college_id == college_id && a.branch_id == branch_id && a.is_active == true && a.id == getid && a.year.ToString() == year /*|| a.mobileno == mobilenum*/
                                       select new StudentInformation
                                       {
                                           FirstName = a.firstname,
                                           id = a.id,
                                           MobileNumber = a.mobileno,
                                           Email = a.email,
                                           IsActive = a.is_active,
                                           user_id = a.user_id,
                                           AdmissionNumber = a.admission_no
                                       }).ToList();

                    ls.Add(all_student);
                }
                return ls;
            }
            else
            {
                var all_student = (from a in dbContext.students
                                   where a.college_id == college_id && a.branch_id == branch_id && a.year.ToString() == year && a.is_active == true
                                   select new StudentInformation
                                   {
                                       FirstName = a.firstname,
                                       id = a.id,
                                       MobileNumber = a.mobileno,
                                       Email = a.email,
                                       IsActive = a.is_active,
                                       AdmissionNumber = a.admission_no

                                   }).ToList();
                ls.Add(all_student);
                return ls;
            }
        }


        public List<StaffInformation> get_staffdata(int? college_id, int? branch_id, string year)
        {
            var p_data = (from s in dbContext.staffs
                          join r in dbContext.roles on s.roll_id equals r.id.ToString()
                          where s.college_id == college_id && s.branch_id == branch_id && s.year.ToString() == year
                          select new StaffInformation
                          {
                              id = s.id,
                              employee_id = s.employee_id,
                              name = s.name,
                              email = s.email,
                              rolename = r.roll_name,
                              designation = s.designation.ToString(),
                              department = s.department.ToString(),
                              contact_no = s.contact_no,
                              is_active = s.is_active
                          }
                          ).ToList();

            return p_data;
        }

        public List<dynamic> get_all_staffs(int? college_id, int? branch_id, string year, string rolename, string[] individualstaff)
        {
            List<dynamic> ls = new List<dynamic>();

            if (individualstaff != null && rolename != null)
            {
                foreach (var loops in individualstaff)
                {
                    int perid = Convert.ToInt32(loops);
                    var s_data = (from s in dbContext.staffs
                                  join r in dbContext.AspNetRoles on s.roll_id equals r.Id.ToString()
                                  where s.college_id == college_id && s.branch_id == branch_id && s.year == year.ToString() && s.id == perid && s.is_active == true
                                  select new StaffInformation
                                  {
                                      id = s.id,
                                      employee_id = s.employee_id,
                                      name = s.name,
                                      email = s.email,
                                      rolename = r.Name,
                                      designation = s.designation.ToString(),
                                      department = s.department.ToString(),
                                      contact_no = s.contact_no,
                                      is_active = s.is_active
                                  }
                              ).ToList();
                    ls.Add(s_data);
                }
                return ls;
            }

            else
            {
                var s_data = (from s in dbContext.staffs
                              join r in dbContext.AspNetRoles on s.roll_id equals r.Id
                              where s.college_id == college_id && s.branch_id == branch_id && s.year == year.ToString() && s.is_active == true && r.Name == rolename
                              select new StaffInformation
                              {
                                  id = s.id,
                                  employee_id = s.employee_id,
                                  name = s.name,
                                  email = s.email,
                                  rolename = r.Name,
                                  designation = s.designation.ToString(),
                                  department = s.department.ToString(),
                                  contact_no = s.contact_no,
                                  is_active = s.is_active
                              }
                             ).ToList();
                ls.Add(s_data);
                return ls;
            }

        }

        public List<dynamic> get_all_parent(int? college_id, int? branch_id, string year, string[] individualStudents, string mobilenumber)
        {
            List<dynamic> ls = new List<dynamic>();
            if (individualStudents != null)
            {
                foreach (var loops in individualStudents)
                {
                    int perid = Convert.ToInt32(loops);
                    var ps_data = (from a in dbContext.students
                                   where a.college_id == college_id && a.branch_id == branch_id && a.year.ToString() == year && a.is_active == true && a.id == perid
                                   select new StudentInformation
                                   {
                                       IsActive = a.is_active,
                                       GuardianName = a.guardian_name,
                                       GuardianPhone = a.guardian_phone,
                                       GuardianEmail = a.guardian_email,
                                       id = a.id,
                                       AdmissionNumber = a.admission_no
                                   }
                              ).ToList();
                    ls.Add(ps_data);
                }
                return ls;
            }
            else
            {
                var ps_data = (from u in dbContext.users
                               join s in dbContext.students
                               on
                               new { u.college_id, u.branch_id, u.user_id }
                               equals
                               new { s.college_id, s.branch_id, user_id = s.parent_id }
                               where u.college_id == college_id && u.branch_id == branch_id && s.year.ToString() == year && u.role == "Parent" && u.is_active == true
                               select new StudentInformation
                               {
                                   IsActive = u.is_active,
                                   GuardianName = s.guardian_name,
                                   GuardianPhone = s.guardian_phone,
                                   ParentUserName = u.username,
                                   GuardianEmail = s.guardian_email,
                                   id = s.id,
                                   AdmissionNumber = s.admission_no
                               }
                              ).ToList();
                ls.Add(ps_data);
                return ls;
            }
        }

        public dynamic get_students_classwise(int? college_id, int? branch_id, string year, int class_id)
        {
            IQueryable<student> ls = dbContext.students;
            dynamic res = (from stu in ls where stu.college_id == college_id && stu.branch_id == branch_id && stu.class_id == class_id && stu.year == year.ToString() select new { stu.email, stu.admission_no }).ToList();
            return res;
        }

        public IQueryable<StudentInformation> get_students_classwise1(int? college_id, int? branch_id, string year, int class_id)
        {
            IQueryable<StudentInformation> ls = (from stu in dbContext.students
                           where stu.college_id == college_id && stu.branch_id == branch_id && stu.class_id == class_id && stu.year == year.ToString()
                           select new StudentInformation
                           {
                               FirstName = stu.firstname,
                               id = stu.id,
                               MobileNumber = stu.mobileno,
                               Email = stu.email,
                               IsActive = stu.is_active,
                               user_id = stu.user_id,
                               AdmissionNumber = stu.admission_no
                           }).AsQueryable();
            return ls;
        }


        public dynamic get_class_sec(int _classid, int? collegeid, int? branchid, string sessionyear)
        {
            dynamic result = null;
            if (_classid != 0)
            {

                result = (from s in dbContext.sections
                          join cs in dbContext.class_sections on s.id equals cs.section_id
                          where cs.class_id == _classid && cs.college_id == collegeid && cs.branch_id == branchid && cs.year == sessionyear
                          select new
                          {
                              Text = s.section1,
                              Value = cs.section_id.ToString()

                          }).ToList();

            }
            return result;
        }

        public dynamic get_class_secsub(int _classid, int _sectionid, int? collegeid, int? branchid, string academicyear)
        {
            dynamic result = null;
            if (_classid != 0 && _sectionid != 0)
            {

                result = (from cs in dbContext.class_sections
                          join sb in dbContext.teacher_subjects on cs.id equals sb.class_section_id
                          join s in dbContext.subjects on sb.subject_id equals s.id
                          where cs.section_id == _sectionid && cs.class_id == _classid && cs.college_id == collegeid && cs.branch_id == branchid && cs.year == academicyear
                          select new
                          {
                              name = s.name,
                              subject_id = s.id

                          }).Distinct().ToList();
            }
            return result;
        }

        public bool send_mail(string email, string subjs, string message)
        {
            if (!String.IsNullOrEmpty(email) && !String.IsNullOrEmpty(subjs) && !String.IsNullOrEmpty(message))
            {
                string addss = email.Trim();
                string subj = subjs;
                string body = message;
                string froms = "shlrenquiry@gmail.com";

                MailMessage mail = new MailMessage();
                mail.To.Add(new MailAddress(addss));
                mail.From = new MailAddress(froms);
                mail.Subject = subj;
                string Body = body;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("shlrenquiry@gmail.com", "shlr2019"); // Enter seders User name and password  
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(mail);
            }
            else
            {
                return false;
            }
            return true;
        }
        public StudentInformation get_class_sec_stu(string user, int? collegeid, int? branchid, string sessionyear)
        {
            int? classid = dbContext.students.Where(k => k.user_id == user && k.college_id == collegeid && k.branch_id == branchid && k.year == sessionyear).Select(k => k.class_id).SingleOrDefault();
            int? sectionid = dbContext.students.Where(k => k.user_id == user && k.college_id == collegeid && k.branch_id == branchid && k.year == sessionyear).Select(k => k.section_id).SingleOrDefault();
            StudentInformation si = new StudentInformation()
            {
                class_id = classid,
                section_id = sectionid
            };
            return si;

        }

        public List<StaffInformation> get_staffid_rolewise(int? college_id, int? branch_id, string year, string userid)
        {
            var p_data = (from s in dbContext.staffs
                          join r in dbContext.AspNetRoles on s.roll_id equals r.Id
                          where s.college_id == college_id && s.branch_id == branch_id && s.year == year && s.user_id == userid
                          select new StaffInformation
                          {
                              id = s.id,
                              employee_id = s.employee_id,
                              name = s.name,
                              email = s.email,
                              rolename = r.Name,
                              designation = s.designation.ToString(),
                              department = s.department.ToString(),
                              contact_no = s.contact_no,
                              is_active = s.is_active
                          }
                          ).ToList();
            return p_data;
        }
    }
}