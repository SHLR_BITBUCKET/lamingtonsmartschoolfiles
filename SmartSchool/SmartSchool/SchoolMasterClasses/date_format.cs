﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartSchool.Models;

namespace SmartSchool.Models
{
    public class date_format
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
     
        public string get_date(int? collegeid, int? branchid, string academicyear)
        {
            var date_fromat = (from dd in dbContext.sch_settings where dd.college_id == collegeid && dd.branch__id == branchid && dd.year == academicyear select dd.date_format).FirstOrDefault();
            if (date_fromat != null)
            {
                return date_fromat.ToString();
            }
            else
            {
                return date_fromat = "dd-mm-yyyy";
            }

        }
    }
}