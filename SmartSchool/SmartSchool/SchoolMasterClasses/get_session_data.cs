﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class get_session_data1
    {
        public string roleid { get; set; }
        public int CollegeID { get; set; }
        public int BranchID { get; set; }
        public string user { get; set; }
        public int timeout { get; set; }
        public string datetime { get; set; }
        public string academic_year { get; set; }
        public int? academicyear_id { get; set; }
        public string ApplicationLogo { get; set; }

        public string current_user_name { get; set; }
        public string School_name { get; set; }
        public string person_logo { get; set; }
    }

    public class session_variables
    {
        public int? ses_id { get; set; }
        public string ses_year { get; set; }
    }

}