﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SmartSchool.SchoolMasterClasses
{
    public class get_staff_classes
    {
        SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        private custom_master_class cmc = new custom_master_class();
        List<SelectListItem> result = new List<SelectListItem>();


        public int? getStaffId(int? colgid, int? brnchid, string academicyear)
        {
            var user = HttpContext.Current.User.Identity.Name;
            var staffid = dbcontext.staffs.Where(k => k.college_id == colgid && k.branch_id == brnchid && k.year == academicyear && k.user_id == user).Select(k => k.id).FirstOrDefault();
            return staffid;
        }

        public IEnumerable<dynamic> getClass(int? colgid, int? brnchid, string academicyear, string rolid)
        {
            List<SelectListItem> ls = null;
            if (rolid == "dd184641-0108-450d-8f21-68a5d36922f4")
            {
                ls = dbcontext.AddClasses.Where(x => x.college_id == colgid && x.branch_id == brnchid && x.year == academicyear).Select(a => new SelectListItem
                {
                    Value = a.class_id.ToString(),
                    Text = a.class_name
                }).OrderBy(n => n.Text).ToList();

                return ls;
            }
            else if (rolid == "96e52a8d-ebc1-4108-af31-b1334e651046")
            {

                var stuClass = (from C in dbcontext.AddClasses
                                join S in dbcontext.students on C.class_id equals S.class_id
                                where C.college_id == colgid && C.branch_id == brnchid && C.year == academicyear && S.email == HttpContext.Current.User.Identity.Name
                                select new SelectListItem
                                {
                                    Value = C.class_id.ToString(),
                                    Text = C.class_name
                                }).ToList();
                return stuClass;
            }
            else
            {
                int? stfid = getStaffId(colgid, brnchid, academicyear);
                ls = (from ct in dbcontext.class_teacher
                      join cs in dbcontext.class_sections on ct.class_id equals cs.class_id
                      join c in dbcontext.AddClasses on cs.class_id equals c.class_id
                      where ct.staff_id == stfid && cs.college_id == colgid && cs.branch_id == brnchid && cs.year == academicyear && cs.class_id == ct.class_id
                      select new SelectListItem
                      {
                          Text = c.class_name,
                          Value = c.class_id.ToString()
                      }).Distinct().ToList();
                SelectListItem st = new SelectListItem()
                {
                    Text = "Select",
                    Value = "0"
                };
                ls.Insert(0, st);
                return ls;
            }
        }

        public IEnumerable<dynamic> get_staff_wise_classes(int? clasid, int? colgid, int? brnchid, string academicyear, string rolid)
        {
            IQueryable<class_teacher> iqc = dbcontext.class_teacher;
            IQueryable<staff> iqs = dbcontext.staffs;
            List<SelectListItem> ls = new List<SelectListItem>();

            if (rolid == "dd184641-0108-450d-8f21-68a5d36922f4")
            {
                result = (from s in dbcontext.sections
                          join cs in dbcontext.class_sections on s.id equals cs.section_id
                          where cs.class_id == clasid && cs.college_id == colgid && cs.branch_id == brnchid && cs.year == academicyear
                          select new SelectListItem
                          {
                              Text = s.section1,
                              Value = cs.section_id.ToString()
                          }).ToList();

            }
            else if(rolid == "96e52a8d-ebc1-4108-af31-b1334e651046")
            {
                var stuSec = (from C in dbcontext.sections
                               join S in dbcontext.students on C.id equals S.section_id
                               where C.college_id == colgid && C.branch_id == brnchid && C.year == academicyear && S.email == HttpContext.Current.User.Identity.Name
                               select new SelectListItem
                               {
                                   Value = C.id.ToString(),
                                   Text = C.section1
                               }).ToList();
                return stuSec;
            }
            else
            {
                int? stfid = getStaffId(colgid, brnchid, academicyear);

                result = (from s in dbcontext.sections
                          join cs in dbcontext.class_sections on s.id equals cs.section_id
                          join ct in dbcontext.class_teacher on cs.section_id equals ct.section_id
                          where ct.class_id == clasid && ct.staff_id == stfid && ct.college_id == colgid && ct.branch_id == brnchid && ct.year == academicyear && cs.class_id == ct.class_id
                          select new SelectListItem
                          {
                              Text = s.section1,
                              Value = cs.section_id.ToString()

                          }).ToList();
            }
            return result;
        }

        public List<SelectListItem> getteacher(int? clasid, int? sectionid, int? colgid, int? brnchid, string academicyear, string rolid)
        {
            if (rolid == "dd184641-0108-450d-8f21-68a5d36922f4")
            {
                result = (from s in dbcontext.class_teacher
                          join cs in dbcontext.class_sections on s.section_id equals cs.section_id
                          join ss in dbcontext.sections on s.section_id equals ss.id
                          join st in dbcontext.staffs on s.staff_id equals st.id
                          where s.class_id == cs.class_id && cs.college_id == ss.college_id && s.class_id == clasid && s.section_id == sectionid && cs.branch_id == ss.branch_id && s.college_id == colgid && s.branch_id == brnchid && s.year == academicyear
                          select new SelectListItem
                          {
                              Text = st.name,
                              Value = s.staff_id.ToString()

                          }).Distinct().ToList();
                return result;
            }
            else
            {
                var result = (from s in dbcontext.class_teacher
                              join cs in dbcontext.class_sections on s.section_id equals cs.section_id
                              join ss in dbcontext.sections on s.section_id equals ss.id
                              join st in dbcontext.staffs on s.staff_id equals st.id
                              where s.class_id == cs.class_id && cs.college_id == ss.college_id && cs.branch_id == ss.branch_id && s.class_id == clasid && s.section_id == sectionid && s.college_id == colgid && s.branch_id == brnchid && s.year == academicyear
                              select new SelectListItem
                              {
                                  Text = st.name,
                                  Value = s.staff_id.ToString()

                              }).ToList();
                return result;
            }
        }

        public List<SelectListItem> getteacherSubjects(int? colgid, int? brnchid, string academicyear, string rolid, int? staffid)
        {
            if (rolid == "dd184641-0108-450d-8f21-68a5d36922f4" || rolid == "96e52a8d-ebc1-4108-af31-b1334e651046")
            {
                result = (from ts in dbcontext.subjects
                          where ts.college_id == colgid && ts.branch_id == brnchid && ts.year == academicyear
                          select new SelectListItem
                          {
                              Text = ts.name,
                              Value = ts.id.ToString()

                          }).Distinct().ToList();
                return result;
            }
            else
            {
                var result = (from ts in dbcontext.teacher_subjects
                              join ct in dbcontext.class_teacher on ts.teacher_id equals ct.staff_id
                              join cs in dbcontext.class_sections on ts.class_section_id equals cs.id
                              join st in dbcontext.staffs on ts.teacher_id equals st.id
                              join s in dbcontext.subjects on ts.subject_id equals s.id
                              where ts.college_id == ct.college_id && ts.teacher_id == staffid && ts.branch_id == ct.branch_id && ts.college_id == colgid && ts.branch_id == brnchid && ts.year == academicyear
                              select new SelectListItem
                              {
                                  Text = s.name,
                                  Value = ts.subject_id.ToString()

                              }).Distinct().ToList();
                return result;
            }
        }


        public List<SelectListItem> getclass_staffwise(int? colgid, int? branchid, string acadenmic_year, string userid, string roleid)
        {
            var getstaffdata = get_staff_wise_classes(0, colgid, branchid, acadenmic_year, roleid);
            IQueryable<AddClass> iq = dbcontext.AddClasses;
            List<SelectListItem> lc = new List<SelectListItem>();
            foreach (var group in getstaffdata)
            {
                int clsid = Convert.ToInt32(group.class_id);
                lc.Add(iq.OrderBy(a => a.class_name).Where(k => k.class_id == clsid && k.college_id == colgid && k.branch_id == branchid && k.year == acadenmic_year).Select(b => new SelectListItem { Value = b.class_id.ToString(), Text = b.class_name }).Distinct().FirstOrDefault());
            }
            return lc;
        }


        public string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }

        public IEnumerable<SelectListItem> getAllSections(int? collegeid, int? branchid, string academicyear)
        {
            var Slist = dbcontext.sections.Where(k => k.college_id == collegeid && k.branch_id == branchid && k.year == academicyear).Select(
              k => new SelectListItem
              {
                  Text = k.section1,
                  Value = k.id.ToString()
              }) as IEnumerable<SelectListItem>;

            return Slist;
        }

    }
}