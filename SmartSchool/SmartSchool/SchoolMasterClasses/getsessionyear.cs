﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public static class getsessionyear
    {
        public static SmartSchoolHDEntities dbcontext = new SmartSchoolHDEntities();
        public static string load_session_data(int? college_id, int? branch_id)
        {
            var sesdata = (from sd in dbcontext.sessions where sd.college_id == college_id && sd.branch_id == branch_id && sd.is_active == true select sd.session1).FirstOrDefault();
            string sdd = sesdata.Substring(0, 4);          
            return sdd.ToString();
        }
    }
}