﻿using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Script.Services;
using System.IO;
using System.Net;
using System.Text;

namespace SmartSchool.SchoolMasterClasses
{
    /// <summary>
    /// Summary description for module_show_hide
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class module_show_hide : System.Web.Services.WebService
    {
        [WebMethod]
        public void get_modules()//This Function/webmethod Get the activated and deactivated modules list and passing the list values to _AdminLayoutPage.cshtml page
        {
            List<permission_group> lp = new List<permission_group>();
            permission_group pg = new permission_group();
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "SELECT * FROM  [SmartSchoolHD].[dbo].[permission_group]";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    lp.Add(new permission_group()
                    {
                        id = rdr.GetInt32(rdr.GetOrdinal("id")),
                        is_active = rdr.GetBoolean(rdr.GetOrdinal("is_active")),
                        short_code = rdr.GetString(rdr.GetOrdinal("short_code"))
                    });

                    //pg.id = Convert.ToInt32(rdr["Id"]);                  
                    //lp.Add(rdr.GetOrdinal("id"));
                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(lp));

            // return JsonConvert.SerializeObject(lp);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void get_studentdata(string ddl_id)
        {
            List<student> ls = new List<student>();
            student pg = new student();
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            if (ddl_id != null)
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    string query = "SELECT * FROM  [SmartSchoolHD].[dbo].[students] where class_id = " + ddl_id + "";
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                      ls.Add(new student()
                        {

                            id = rdr.GetInt32(rdr.GetOrdinal("id")),
                          //is_active = rdr.GetString(rdr.GetOrdinal("is_active"))
                          is_active = Convert.ToBoolean(rdr.GetOrdinal("is_active"))

                      });
                    }
                }
            }
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            Context.Response.Write(js1.Serialize(ls));                
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void get_templates()
        {
            String result;
            string apiKey = "ZV8dVW4zhec-eg0uH4206fetokQt4cQ6WxGgxsZKuj";

            String url = "https://api.textlocal.in/get_templates/?apikey=" + apiKey;
            //refer to parameters to complete correct url string

            StreamWriter myWriter = null;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);

            objRequest.Method = "POST";
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(url);
            objRequest.ContentType = "application/x-www-form-urlencoded";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(url);
            }
            catch (Exception e)
            {
              //  return e.Message;
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                // Close and clean up the StreamReader
                sr.Close();
            }
            Console.WriteLine(result);
            //Console.ReadLine();
           // return result;
            JavaScriptSerializer js2 = new JavaScriptSerializer();
            Context.Response.Write(js2.Serialize(result));

        }
    }
}
