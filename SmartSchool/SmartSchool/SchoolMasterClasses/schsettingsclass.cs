﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class schsettingsclass
    {
        public int schid { get; set; }
        public int college_id { get; set; }
        public int branch__id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string lang_id { get; set; }
        public string dise_code { get; set; }
        public string date_format { get; set; }
        public string currency { get; set; }
        public string currency_symbol { get; set; }
        public string is_rtl { get; set; }
        public string timezone { get; set; }
        public Nullable<int> session_id { get; set; }
        public string start_month { get; set; }
        public string image { get; set; }
        public string theme { get; set; }
        public string is_active { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public string cron_secret_key { get; set; }
        public Nullable<int> fee_due_days { get; set; }
        public string class_teacher { get; set; }
        public string mobile_api_url { get; set; }
        public string app_primary_color_code { get; set; }
        public string app_secondary_color_code { get; set; }
        public string app_logo { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string session { get; set; }
       



    }
}