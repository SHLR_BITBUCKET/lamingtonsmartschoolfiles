﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartSchool.SchoolMasterClasses
{
    public class usercreation
    {
        public SmartSchoolHDEntities dbContext = new SmartSchoolHDEntities();
        public List<AspNetUserRole> CreateRoles(string userid, string email, string password, string mobileno)
        {
           dynamic role_id = null;
            var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
            ApplicationDbContext context = new ApplicationDbContext();
            dynamic res = null;
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("SuperAdmin"))
            {
                role.Name = "SuperAdmin";
                roleManager.Create(role);
            }           
            else
            {
                var user = new ApplicationUser();
                user.UserName = userid;
                user.Email = email;
                user.PhoneNumber = mobileno;
                string pwds = password;
                var checker = UserManager.Create(user, pwds);
                if (checker.Succeeded)
                {
                    res = UserManager.AddToRole(user.Id, "SuperAdmin");
                    role_id = (from ss in dbContext.AspNetUserRoles where ss.UserId == user.Id select ss).ToList();

                }                
            }
            return role_id;
             
        }
    }


    

}