﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using SmartSchool.Models;

[assembly: OwinStartupAttribute(typeof(SmartSchool.Startup))]
namespace SmartSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
           // CreateRoles();
        }
        private void CreateRoles()
        {
            var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("SuperAdmin"))
            {
                role.Name = "SuperAdmin";
                roleManager.Create(role);
            }
            else
            {
                var user = new ApplicationUser();
                user.UserName = "SuperAdmin";
                user.Email = "puneeth@shlrtechnosoft.com";
                string pwds = "SuperAdmin@123";
                var checker = UserManager.Create(user, pwds);
                if (checker.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "SuperAdmin");
                }
            }

            //else if (!roleManager.RoleExists("Admin"))
            //{
            //    role.Name = "Admin";
            //    roleManager.Create(role);
            //}
            //else if (!roleManager.RoleExists("Staff"))
            //{
            //    role.Name = "Staff";
            //    roleManager.Create(role);
            //}
            //else if (!roleManager.RoleExists("Student"))
            //{
            //    role.Name = "Student";
            //    roleManager.Create(role);
            //}
        }
    }
}
