﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebCam.aspx.cs" Inherits="SmartSchool.WebCam" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src='<%=ResolveUrl("~/jsfiles/jquery.webcam.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        var pageUrl = '<%=ResolveUrl("~/WebCam.aspx") %>';
        $(function () {
            jQuery("#Camera").webcam({
                width: 320,
                height: 240,
                mode: "save",
                swffile: '<%=ResolveUrl("~/jsfiles/jscam.swf") %>',
                debug: function (type, status) {
                    //$('#Status').append(type + ": " + status + '<br /><br />');
                },
                onSave: function (data) {
                    $.ajax({
                        type: "POST",
                        url: pageUrl + "/GetCapturedImage",
                        data: '',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            $("[id*=imgCapture]").css("visibility", "visible");
                            $("[id*=imgCapture]").attr("src", r.d);
                            alert("Live Image Captured Successfully");
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                },
                onCapture: function () {
                    webcam.save(pageUrl);
                }
            });
        });
        function Capture() {
            var ImgNam = $('#txtImgName').val();
            if (ImgNam == null || ImgNam == "") {
                alert('Please Enter Name of the Image..!');
                return false;
            }

            webcam.capture();
            return false;
        }


        function PassTxtVal() {

        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="XX-Large" Text="APP FOR CAPTURE LIVE PHOTO"></asp:Label>
        <br />
        <br />

        <br />
        <br />
        <br />

        <div class="container-fluid">
            <div class="row centered">
                <div class="col-sm-8 col-md-6" style="align-items: center; padding-left: 314px;">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Large" Text="ENTER NAME FOR THE IMAGE:"></asp:Label>
                    <br />
                    <br />
                    <table border="1">
                        <tr>
                            <td>Live Image From WebCam<br />
                                <div id="Camera"></div>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="col-sm-8 col-md-6" style="align-items: center; padding-left: 90px;">
                    <asp:TextBox ID="txtImgName" runat="server" OnTextChanged="txtImgName_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <br />
                    <br />
                    <table border="1">
                        <tr>
                            <td>Captured Image<br />
                                <asp:Image ID="imgCapture" runat="server" Style="visibility: hidden; width: 320px; height: 240px" />
                            </td>
                        </tr>
                    </table>

                    <br />
                    <asp:Button ID="btnCapture" Text="Capture Image and Save" runat="server" OnClientClick="return Capture();" BackColor="#FF9933" />
                </div>

            </div>

        </div>
    </form>
</body>
</html>
